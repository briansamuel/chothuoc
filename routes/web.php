<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


if (!defined('FM_USE_ACCESS_KEYS')) {
    define('FM_USE_ACCESS_KEYS', true); // TRUE or FALSE
}

if (!defined('FM_DEBUG_ERROR_MESSAGE')) {
    define('FM_DEBUG_ERROR_MESSAGE', false); // TRUE or FALSE
}

Route::domain(Config::get('domain.web.domain'))->group(function () {
    Route::namespace('FrontSite')->group(function () {
        Route::get('/', ['uses' => 'HomeController@index'])->name('home');
        Route::get('/tin-tuc/', ['uses' => 'NewsController@index'])->name('news');

        
        Route::get('/san-pham/', ['uses' => 'ProductsController@index'])->name('product.index');
        Route::post('/san-pham/', ['uses' => 'ProductsController@index'])->name('product.index');
        Route::get('/san-pham/{slug}/', ['uses' => 'ProductsController@detail'])->name('product.detail');
        Route::get('/danh-muc/{slug}/', ['uses' => 'ProductsController@category'])->name('product.category');
        Route::get('/application/files/locNhanh_ajax.php', ['uses' => 'ProductsController@filterProduct'])->name('product.locNhanh_ajax');
        Route::post('/application/files/locNhanh_ajax.php', ['uses' => 'ProductsController@resetFilterProduct'])->name('product.resetFilterProduct');
        Route::get('/ajax/', ['uses' => 'HomeController@ajaxAction'])->name('home.ajaxAction');
        Route::get('ajax/common/search/', ['uses' => 'HomeController@ajaxSearch'])->name('home.ajaxSearch');
        Route::post('ajax/common/rating/show', ['uses' => 'HomeController@ajaxRatingShow'])->name('home.ajaxRatingShow');
        Route::post('ajax/common/rating/execute', ['uses' => 'HomeController@ajaxRatingExecute'])->name('home.ajaxRatingExecute');

        // Ingredient Route
        Route::get('/hoat-chat/', ['uses' => 'IngredientController@index'])->name('ingredient');
        Route::get('/{post_slug}.hc', ['uses' => 'IngredientController@detail'])->name('ingredient.detail');
        Route::get('/hoat-chat/char={character}', ['uses' => 'IngredientController@character'])->name('ingredient.character');
        
       
        // Cart
        Route::get('/cart.htm', ['uses' => 'CartController@index'])->name('cart');
        Route::post('/checkout.htm', ['uses' => 'CartController@checkout'])->name('checkout.index');
        Route::get('/checkout-success.htm', ['uses' => 'CartController@checkoutSuccess'])->name('checkoutSuccess');
        Route::get('/modules/giohang/giohang_ajax.php', ['uses' => 'CartController@addToCart'])->name('addToCart');
        Route::get('/modules/giohang/giohang_tinhthanh.php', ['uses' => 'CartController@ajaxAdress'])->name('ajaxAdress');

        Route::middleware(['user_auth'])->group(function () {
            Route::get('/thong-tin-tai-khoan.htm', ['uses' => 'AccountController@index'])->name('infoAccount.index');
            Route::post('/thong-tin-tai-khoan.htm', ['uses' => 'AccountController@updateAccount'])->name('infoAccount.updateAccount');
            Route::get('/quan-ly-don-hang.htm', ['uses' => 'AccountController@order'])->name('infoAccount.order');
            Route::get('/san-pham-da-xem.htm', ['uses' => 'AccountController@viewed'])->name('infoAccount.viewed');
            Route::get('/san-pham-yeu-thich.htm', ['uses' => 'AccountController@liked'])->name('infoAccount.liked');
            Route::get('/san-pham-mua-sau.htm', ['uses' => 'AccountController@willbuy'])->name('infoAccount.willbuy');
            Route::get('/logout.htm', ['uses' => 'AccountController@logout'])->name('infoAccount.logout');
        });
        Route::get('/gioi-thieu-cong-ty', ['uses' => 'PageController@introduce'])->name('pages.introduce');
        Route::get('/lien-he/', function(){
            return view('frontsite.pages.contact');
        })->name('404');

        Route::get('/404.html', function(){
            return view('frontsite.pages.404');
        })->name('404');

        Route::get('/clear-cache', function(){
            \Artisan::call('config:clear');
            \Artisan::call('route:clear');
            \Artisan::call('view:clear');
        })->name('clear-cache');

        // Update Route
        Route::get('/{post_slug}.html', ['uses' => 'NewsController@detail'])->name('news.detail');
        Route::get('/danh-muc-tin/{category_slug}/', ['uses' => 'NewsController@category'])->name('news.category');

        
         // Disease Route
        Route::get('/tim-hieu-benh/', ['uses' => 'DiseaseController@index'])->name('disease');
        Route::get('/{post_slug}/', ['uses' => 'DiseaseController@detail'])->name('disease.detail');
        
        
        // Account
        
        // Ajax Route
        
    });
});

Route::prefix('admincp')->group(function () {
    Route::namespace('Admin')->group(function () {
        Route::get('denied-permission', function () {
            return view('admin.pages.permission_denied');
        })->name('permission_denied');
        // login
        Route::get('/login', ['uses' => 'LoginController@login'])->name('login');
        Route::post('/login', ['uses' => 'LoginController@loginAction'])->name('loginAction');
        Route::get('/logout', ['uses' => 'LoginController@logout'])->name('logout');
        Route::get('/reset_password', ['uses' => 'LoginController@resetPassword'])->name('resetPassword');
        Route::get('/active-user', ['uses' => 'LoginController@activeUser'])->name('activeUser');
        Route::get('/active-agent', ['uses' => 'LoginController@activeAgent'])->name('activeAgent');
        Route::get('/kich-hoat-tai-khoan', ['uses' => 'LoginController@activeGuest'])->name('activeGuest');


    });




    Route::middleware(['auth'])->group(function () {


        Route::get('/dashboard', ['uses' => 'Admin\DashboardController@index'])->name('dash-board');
        Route::get('/welcome', ['uses' => 'Admin\WelcomeController@index'])->name('welcome');
        Route::get('/', ['uses' => 'Admin\WelcomeController@index'])->name('admin.index');

        Route::namespace('Admin')->group(function () {
            // my profile
            Route::get('/my-profile', ['uses' => 'MyProfileController@profile'])->name('profile');
            Route::post('/my-profile', ['uses' => 'MyProfileController@updateProfile'])->name('profile.update');

            // change password
            Route::get('/change-password', ['uses' => 'MyProfileController@changePassword'])->name('profile.changePassword');
            Route::put('/change-password', ['uses' => 'MyProfileController@changePasswordAction'])->name('profile.changePasswordAction');

            Route::namespace('CMS')->group(function () {

                Route::get('/crawler-i', ['uses' => 'CrawlerController@index'])->name('crawler.index');
                Route::get('/crawler', ['uses' => 'CrawlerController@crawl'])->name('crawler.test');
                Route::get('/crawler/ajax/list-medicine', ['uses' => 'CrawlerController@ajaxListMedicine'])->name('crawler.ajaxListMedicine');
                // Banner
                Route::get('/banner', ['uses' => 'BannersController@index'])->name('banner.list');
                Route::get('/banner/add', ['uses' => 'BannersController@add'])->name('banner.add');
                Route::post('/banner/add', ['uses' => 'BannersController@addAction'])->name('banner.add.action');
                Route::get('/banner/edit/{banner_id}', ['uses' => 'BannersController@edit'])->name('banner.edit');
                Route::post('/banner/edit/{banner_id}', ['uses' => 'BannersController@editAction'])->name('banner.edit.action');
                Route::get('/banner/delete/{banner_id}', ['uses' => 'BannersController@delete'])->name('banner.delete');
                Route::get('/banner/ajax/get-list', ['uses' => 'BannersController@ajaxGetList'])->name('banner.ajax.getList');

                // Block
                Route::get('/block', ['uses' => 'BlocksController@index'])->name('block.list');
                Route::get('/block/add', ['uses' => 'BlocksController@add'])->name('block.add');
                Route::post('/block/add', ['uses' => 'BlocksController@addAction'])->name('block.add.action');
                Route::get('/block/config/{block_id}', ['uses' => 'BlocksController@config'])->name('block.config');
                Route::post('/block/config/{block_id}', ['uses' => 'BlocksController@configAction'])->name('block.config.action');
                Route::get('/block/edit/{block_id}', ['uses' => 'BlocksController@edit'])->name('block.edit');
                Route::post('/block/edit/{block_id}', ['uses' => 'BlocksController@editAction'])->name('block.edit.action');
                Route::get('/block/delete/{block_id}', ['uses' => 'BlocksController@delete'])->name('block.delete');
                Route::get('/block/ajax/get-list', ['uses' => 'BlocksController@ajaxGetList'])->name('block.ajax.getList');

                // Page
                Route::get('/page', ['uses' => 'PageController@index'])->name('page.list');
                Route::get('/page/add', ['uses' => 'PageController@add'])->name('page.add');
                Route::post('/page/add', ['uses' => 'PageController@add'])->name('page.add.action');
                Route::get('/page/edit/{page_id}', ['uses' => 'PageController@add'])->name('page.edit');
                Route::post('/page/edit/{page_id}', ['uses' => 'PageController@add'])->name('page.edit.action');
                Route::post('/page/edit', ['uses' => 'PageController@editManyAction'])->name('page.edit.many.action');
                Route::get('/page/delete/{page_id}', ['uses' => 'PageController@delete'])->name('page.delete');
                Route::post('/page/delete', ['uses' => 'PageController@deletemany'])->name('page.delete.many');
                Route::post('/page/save', ['uses' => 'PageController@save'])->name('page.save');
                Route::get('/page/ajax/get-list', ['uses' => 'PageController@ajaxGetList'])->name('page.ajax.getList');

                // News
                Route::get('/news', ['uses' => 'NewsController@index'])->name('news.list');
                Route::get('/news/add', ['uses' => 'NewsController@add'])->name('news.add');
                Route::get('/news/edit/{news_id}', ['uses' => 'NewsController@add'])->name('news.edit');
                Route::post('/news/edit', ['uses' => 'NewsController@editManyAction'])->name('news.edit.many.action');
                Route::get('/news/delete/{news_id}', ['uses' => 'NewsController@delete'])->name('news.delete');
                Route::post('/news/delete', ['uses' => 'NewsController@deletemany'])->name('news.delete.many');
                Route::post('/news/save', ['uses' => 'NewsController@save'])->name('news.save');
                Route::get('/news/ajax/get-list', ['uses' => 'NewsController@ajaxGetList'])->name('news.ajax.getList');

                // Disease
                Route::get('/disease', ['uses' => 'DiseaseController@index'])->name('disease.list');
                Route::get('/disease/add', ['uses' => 'DiseaseController@add'])->name('disease.add');
                Route::get('/disease/edit/{news_id}', ['uses' => 'DiseaseController@add'])->name('disease.edit');
                Route::post('/disease/edit', ['uses' => 'DiseaseController@editManyAction'])->name('disease.edit.many.action');
                Route::get('/disease/delete/{news_id}', ['uses' => 'DiseaseController@delete'])->name('disease.delete');
                Route::post('/disease/delete', ['uses' => 'DiseaseController@deletemany'])->name('disease.delete.many');
                Route::post('/disease/save', ['uses' => 'DiseaseController@save'])->name('disease.save');
                Route::get('/disease/ajax/get-list', ['uses' => 'DiseaseController@ajaxGetList'])->name('disease.ajax.getList');

                // Hoạt chất
                Route::get('/ingredient', ['uses' => 'IngredientController@index'])->name('ingredient.list');
                Route::get('/ingredient/add', ['uses' => 'IngredientController@add'])->name('ingredient.add');
                Route::get('/ingredient/edit/{news_id}', ['uses' => 'IngredientController@add'])->name('ingredient.edit');
                Route::post('/ingredient/edit', ['uses' => 'IngredientController@editManyAction'])->name('ingredient.edit.many.action');
                Route::get('/ingredient/delete/{news_id}', ['uses' => 'IngredientController@delete'])->name('ingredient.delete');
                Route::post('/ingredient/delete', ['uses' => 'IngredientController@deletemany'])->name('ingredient.delete.many');
                Route::post('/ingredient/save', ['uses' => 'IngredientController@save'])->name('ingredient.save');
                Route::get('/ingredient/ajax/get-list', ['uses' => 'IngredientController@ajaxGetList'])->name('ingredient.ajax.getList');

                // subcribe Emails
                Route::get('/subcribe-emails', ['uses' => 'SubcribeEmailsController@index'])->name('subcribe_email.list');
                Route::get('/subcribe-emails/edit/{id}', ['uses' => 'SubcribeEmailsController@edit'])->name('subcribe_email.edit');
                Route::post('/subcribe-emails/edit/{id}', ['uses' => 'SubcribeEmailsController@editAction'])->name('subcribe_email.edit.action');
                Route::get('/subcribe-emails/delete/{id}', ['uses' => 'SubcribeEmailsController@delete'])->name('subcribe_email.delete');
                Route::get('/subcribe-emails/ajax/get-list', ['uses' => 'SubcribeEmailsController@ajaxGetList'])->name('subcribe_email.ajax.getList');



                // Category
                Route::get('/category', ['uses' => 'CategoryController@index'])->name('category.list');
                Route::post('/category/save', ['uses' => 'CategoryController@save'])->name('category.save');
                Route::get('/category/edit/{category_id}', ['uses' => 'CategoryController@edit'])->name('category.edit');
                Route::post('/category/edit', ['uses' => 'CategoryController@editManyAction'])->name('category.edit.many.action');
                Route::post('/category/edit/{category_id}', ['uses' => 'CategoryController@editAction'])->name('category.edit.action');
                Route::get('/category/delete/{category_id}', ['uses' => 'CategoryController@delete'])->name('category.delete');
                Route::post('/category/delete', ['uses' => 'CategoryController@deleteMany'])->name('category.deleteMany');
                Route::get('/category/ajax/get-list', ['uses' => 'CategoryController@ajaxGetList'])->name('category.ajax.getList');

                // Medicine group
                Route::get('/medicine-group', ['uses' => 'MedicineGroupController@index'])->name('medicine_group.list');
                Route::get('/medicine-group/add', ['uses' => 'MedicineGroupController@add'])->name('medicine_group.add');
                Route::post('/medicine-group/add', ['uses' => 'MedicineGroupController@addAction'])->name('medicine_group.add.action');
                Route::get('/medicine-group/detail/{group_id}', ['uses' => 'MedicineGroupController@detail'])->name('medicine_group.detail');
                Route::get('/medicine-group/edit/{group_id}', ['uses' => 'MedicineGroupController@edit'])->name('medicine_group.edit');
                Route::post('/medicine-group/edit', ['uses' => 'MedicineGroupController@editManyAction'])->name('medicine_group.edit.many.action');
                Route::post('/medicine-group/edit/{group_id}', ['uses' => 'MedicineGroupController@editAction'])->name('medicine_group.edit.action');
                Route::get('/medicine-group/delete', ['uses' => 'MedicineGroupController@deleteMany'])->name('medicine_group.delete.many');
                Route::get('/medicine-group/delete/{group_id}', ['uses' => 'MedicineGroupController@delete'])->name('medicine_group.delete');
                Route::post('/medicine-group/delete', ['uses' => 'MedicineGroupController@delete'])->name('medicine_group.delete');
                Route::get('/medicine-group/ajax/get-list', ['uses' => 'MedicineGroupController@ajaxGetList'])->name('medicine_group.ajax.getList');

                // Country
                Route::get('/country', ['uses' => 'CountryController@index'])->name('country.list');
                Route::get('/country/add', ['uses' => 'CountryController@add'])->name('country.add');
                Route::post('/country/add', ['uses' => 'CountryController@addAction'])->name('country.add.action');
                Route::get('/country/detail/{country_id}', ['uses' => 'CountryController@detail'])->name('country.detail');
                Route::get('/country/edit/{country_id}', ['uses' => 'CountryController@edit'])->name('country.edit');
                Route::post('/country/edit', ['uses' => 'CountryController@editManyAction'])->name('country.edit.many.action');
                Route::post('/country/edit/{country_id}', ['uses' => 'CountryController@editAction'])->name('country.edit.action');
                Route::get('/country/delete', ['uses' => 'CountryController@deleteMany'])->name('country.delete.many');
                Route::get('/country/delete/{country_id}', ['uses' => 'CountryController@delete'])->name('country.delete');
                Route::post('/country/delete', ['uses' => 'CountryController@delete'])->name('country.delete');
                Route::get('/country/ajax/get-list', ['uses' => 'CountryController@ajaxGetList'])->name('country.ajax.getList');

                // Brand
                Route::get('/brand', ['uses' => 'BrandController@index'])->name('brand.list');
                Route::get('/brand/add', ['uses' => 'BrandController@add'])->name('brand.add');
                Route::post('/brand/add', ['uses' => 'BrandController@addAction'])->name('brand.add.action');
                Route::get('/brand/detail/{brand_id}', ['uses' => 'BrandController@detail'])->name('brand.detail');
                Route::get('/brand/edit/{brand_id}', ['uses' => 'BrandController@edit'])->name('brand.edit');
                Route::post('/brand/edit', ['uses' => 'BrandController@editManyAction'])->name('brand.edit.many.action');
                Route::post('/brand/edit/{brand_id}', ['uses' => 'BrandController@editAction'])->name('brand.edit.action');
                Route::get('/brand/delete', ['uses' => 'BrandController@deleteMany'])->name('brand.delete.many');
                Route::get('/brand/delete/{brand_id}', ['uses' => 'BrandController@delete'])->name('brand.delete');
                Route::post('/brand/delete', ['uses' => 'BrandController@delete'])->name('brand.delete');
                Route::get('/brand/ajax/get-list', ['uses' => 'BrandController@ajaxGetList'])->name('brand.ajax.getList');

                // Package
                Route::get('/package', ['uses' => 'PackageController@index'])->name('package.list');
                Route::get('/package/add', ['uses' => 'PackageController@add'])->name('package.add');
                Route::post('/package/add', ['uses' => 'PackageController@addAction'])->name('package.add.action');
                Route::get('/package/detail/{package_id}', ['uses' => 'PackageController@detail'])->name('package.detail');
                Route::get('/package/edit/{package_id}', ['uses' => 'PackageController@edit'])->name('package.edit');
                Route::post('/package/edit', ['uses' => 'PackageController@editManyAction'])->name('package.edit.many.action');
                Route::post('/package/edit/{package_id}', ['uses' => 'PackageController@editAction'])->name('package.edit.action');
                Route::get('/package/delete', ['uses' => 'PackageController@deleteMany'])->name('package.delete.many');
                Route::get('/package/delete/{package_id}', ['uses' => 'PackageController@delete'])->name('package.delete');
                Route::post('/package/delete', ['uses' => 'PackageController@delete'])->name('package.delete');
                Route::get('/package/ajax/get-list', ['uses' => 'PackageController@ajaxGetList'])->name('package.ajax.getList');

                // Medicine
                Route::get('/medicine', ['uses' => 'MedicineController@index'])->name('medicine.list');
                Route::get('/medicine/add', ['uses' => 'MedicineController@add'])->name('medicine.add');
                Route::post('/medicine/add', ['uses' => 'MedicineController@addAction'])->name('medicine.add.action');
                Route::get('/medicine/detail/{medicine_id}', ['uses' => 'MedicineController@detail'])->name('medicine.detail');
                Route::get('/medicine/edit/{medicine_id}', ['uses' => 'MedicineController@edit'])->name('medicine.edit');
                Route::post('/medicine/edit', ['uses' => 'MedicineController@editManyAction'])->name('medicine.edit.many.action');
                Route::post('/medicine/edit/{medicine_id}', ['uses' => 'MedicineController@editAction'])->name('medicine.edit.action');
                Route::get('/medicine/delete', ['uses' => 'MedicineController@deleteMany'])->name('medicine.delete.many');
                Route::get('/medicine/delete/{medicine_id}', ['uses' => 'MedicineController@delete'])->name('medicine.delete');
                Route::post('/medicine/delete', ['uses' => 'MedicineController@delete'])->name('medicine.delete');
                Route::get('/medicine/ajax/get-list', ['uses' => 'MedicineController@ajaxGetList'])->name('medicine.ajax.getList');
                // User
                Route::get('/user', ['uses' => 'UsersController@index'])->name('user.list');
                Route::get('/user/add', ['uses' => 'UsersController@add'])->name('user.add');
                Route::post('/user/add', ['uses' => 'UsersController@addAction'])->name('user.add.action');
                Route::get('/user/detail/{user_id}', ['uses' => 'UsersController@detail'])->name('user.detail');
                Route::get('/user/edit/{user_id}', ['uses' => 'UsersController@edit'])->name('user.edit');
                Route::post('/user/edit', ['uses' => 'UsersController@editManyAction'])->name('user.edit.many.action');
                Route::post('/user/edit/{user_id}', ['uses' => 'UsersController@editAction'])->name('user.edit.action');
                Route::get('/user/delete', ['uses' => 'UsersController@deleteMany'])->name('user.delete.many');
                Route::get('/user/delete/{user_id}', ['uses' => 'UsersController@delete'])->name('user.delete');
                Route::post('/user/delete', ['uses' => 'UsersController@delete'])->name('user.delete');
                Route::get('/user/ajax/get-list', ['uses' => 'UsersController@ajaxGetList'])->name('user.ajax.getList');


                // Agent
                Route::get('/user-group', ['uses' => 'UserGroupController@index'])->name('user_group.list');
                Route::get('/user-group/add', ['uses' => 'UserGroupController@add'])->name('user_group.add');
                Route::post('/user-group/add', ['uses' => 'UserGroupController@addAction'])->name('user_group.add.action');
                Route::get('/user-group/detail/{guest_id}', ['uses' => 'UserGroupController@detail'])->name('user_group.detail');
                Route::get('/user-group/edit/{guest_id}', ['uses' => 'UserGroupController@edit'])->name('user_group.edit');
                Route::post('/user-group/edit', ['uses' => 'UserGroupController@editManyAction'])->name('user_group.edit.many.action');
                Route::post('/user-group/edit/{guest_id}', ['uses' => 'UserGroupController@editAction'])->name('user_group.edit.action');
                Route::get('/user-group/delete', ['uses' => 'UserGroupController@deleteMany'])->name('user_group.delete.many');
                Route::get('/user-group/delete/{guest_id}', ['uses' => 'UserGroupController@delete'])->name('user_group.delete');
                Route::post('/user-group/delete', ['uses' => 'UserGroupController@delete'])->name('user_group.delete');
                Route::get('/user-group/ajax/get-list', ['uses' => 'UserGroupController@ajaxGetList'])->name('user_group.ajax.getList');

                // Logs User
                Route::get('/logs-user', ['uses' => 'LogsUserController@index'])->name('logs_user.list');
                Route::get('/logs-user/detail/{id}', ['uses' => 'LogsUserController@detail'])->name('logs_user.detail');
                Route::get('/logs-user/ajax/get-list', ['uses' => 'LogsUserController@ajaxGetList'])->name('logs_user.ajax.getList');

                // Menu
                Route::get('/menus', ['uses' => 'MenuController@index'])->name('menu.index');

                // Custom Css
                Route::get('/theme-option', ['uses' => 'ThemeOptionsController@option'])->name('theme_option.index');
                Route::post('/theme-option', ['uses' => 'ThemeOptionsController@optionAction'])->name('theme_option.action');

                // Custom Css
                Route::get('/custom-css', ['uses' => 'CustomCssController@index'])->name('custom_css.index');
                Route::post('/custom-css', ['uses' => 'CustomCssController@editAction'])->name('custom_css.editAction');

                // Custom Template
                Route::get('/template', ['uses' => 'TemplateController@index'])->name('template.index');
                Route::post('/template', ['uses' => 'TemplateController@editAction'])->name('template.editAction');
                // Contacts
                Route::get('/contact', ['uses' => 'ContactController@index'])->name('contact.index');
                Route::get('/contact/ajax/get-list', ['uses' => 'ContactController@ajaxGetList'])->name('contact.ajax.getList');
                Route::get('/contact/edit/{id}', ['uses' => 'ContactController@edit'])->name('contact.edit');
                Route::post('/contact/edit', ['uses' => 'ContactController@editManyAction'])->name('contact.edit.many.action');
                Route::post('/contact/edit/{id}', ['uses' => 'ContactController@editAction'])->name('contact.edit.action');
                Route::get('/contact/delete', ['uses' => 'ContactController@deleteMany'])->name('contact.delete.many');
                Route::get('/contact/delete/{id}', ['uses' => 'ContactController@delete'])->name('contact.delete');
                Route::post('/contact/delete', ['uses' => 'ContactController@delete'])->name('contact.delete');
                Route::get('/contact/ajax/get-list', ['uses' => 'ContactController@ajaxGetList'])->name('contact.ajax.getList');
                Route::post('/contact/{id}/reply', ['uses' => 'ContactController@replyAction'])->name('contact.reply.action');

                // Comment
                Route::get('/comment', ['uses' => 'CommentController@index'])->name('comment.list');
                Route::get('/comment/add', ['uses' => 'CommentController@add'])->name('comment.add');
                Route::post('/comment/add', ['uses' => 'CommentController@addAction'])->name('comment.add.action');
                Route::get('/comment/edit/{comment_id}', ['uses' => 'CommentController@edit'])->name('comment.edit');
                Route::post('/comment/edit/{comment_id}', ['uses' => 'CommentController@editAction'])->name('comment.edit.action');
                Route::post('/comment/edit', ['uses' => 'CommentController@editManyAction'])->name('comment.edit.many.action');
                Route::get('/comment/delete/{comment_id}', ['uses' => 'CommentController@delete'])->name('comment.delete');
                Route::post('/comment/delete', ['uses' => 'CommentController@deletemany'])->name('comment.delete.many');
                Route::get('/comment/ajax/get-list', ['uses' => 'CommentController@ajaxGetList'])->name('comment.ajax.getList');

                // Orders
                Route::get('/order', ['uses' => 'OrderController@index'])->name('order.list');
                
                Route::get('/order/edit/{order_id}', ['uses' => 'OrderController@edit'])->name('order.edit');
                Route::post('/order/edit/{order_id}', ['uses' => 'OrderController@editAction'])->name('order.edit.action');
                Route::post('/order/edit', ['uses' => 'OrderController@editManyAction'])->name('order.edit.many.action');
                Route::get('/order/delete/{order_id}', ['uses' => 'OrderController@delete'])->name('order.delete');
                Route::post('/order/delete', ['uses' => 'OrderController@deletemany'])->name('order.delete.many');
                Route::get('/order/ajax/get-list', ['uses' => 'OrderController@ajaxGetList'])->name('order.ajax.getList');

                // Reviews
                Route::get('/review', ['uses' => 'ReviewController@index'])->name('review.list');
                Route::get('/review/add', ['uses' => 'ReviewController@add'])->name('review.add');
                Route::post('/review/add', ['uses' => 'ReviewController@addAction'])->name('review.add.action');
                Route::get('/review/edit/{review_id}', ['uses' => 'ReviewController@edit'])->name('review.edit');
                Route::post('/review/edit/{review_id}', ['uses' => 'ReviewController@editAction'])->name('review.edit.action');
                Route::post('/review/edit', ['uses' => 'ReviewController@editManyAction'])->name('review.edit.many.action');
                Route::get('/review/delete/{review_id}', ['uses' => 'ReviewController@delete'])->name('review.delete');
                Route::post('/review/delete', ['uses' => 'ReviewController@deletemany'])->name('review.delete.many');
                Route::get('/review/ajax/get-list', ['uses' => 'ReviewController@ajaxGetList'])->name('review.ajax.getList');

                // Setting
                Route::get('/settings-general', ['uses' => 'SettingController@general'])->name('setting.general');
                Route::post('/settings-general', ['uses' => 'SettingController@generalAction'])->name('setting.general.action');
                Route::post('/settings-login-social', ['uses' => 'SettingController@loginSocialAction'])->name('setting.login_social.action');
                Route::get('/settings-email', ['uses' => 'SettingController@email'])->name('setting.email');
                Route::get('/settings-social-login', ['uses' => 'SettingController@loginSocial'])->name('setting.login_social');
                Route::get('/settings-notification', ['uses' => 'SettingController@notification'])->name('setting.notification');
                Route::post('/settings-notification', ['uses' => 'SettingController@notificationAction'])->name('setting.notification.action');

                // Gallery
                Route::get('/gallery', ['uses' => 'GalleryController@index'])->name('gallery.index');
            });


        });
        Route::namespace('General')->group(function () {
            Route::post('/upload-image', ['uses' => 'UpLoadImageController@uploadImage'])->name('uploadImage');
            Route::post('/destroy-image', ['uses' => 'UpLoadImageController@imageDestroy'])->name('imageDestroy');
            Route::get('/language', ['uses' => 'MultiLanguageController@index'])->name('language.index');
        });
    });
});

