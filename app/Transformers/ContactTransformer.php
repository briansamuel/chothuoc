<?php
namespace App\Transformers;

class ContactTransformer
{
    /**
     *
     */
    public static function transformCollection($data)
    {
        if ($data) {
            foreach ($data as &$item) {
                $item->created_at = date("d-m-Y H:i:s", strtotime($item->created_at));
                $item->update_at = date("d-m-Y H:i:s", strtotime($item->updated_at));
            }
        }
        return $data;
    }

    /**
     *
     */
    public static function transformItem($item)
    {
        if ($item) {
            $item->created_at = date("d-m-Y H:i:s", strtotime($item->created_at));
            $item->updated_at = date("d-m-Y H:i:s", strtotime($item->updated_at));
        }

        return $item;
    }
}