<?php

namespace App\Models;


use Illuminate\Support\Facades\DB;

class MedicineModel
{

    protected static $table = 'medicines';

    public static function getAll()
    {
        $result = DB::table(self::$table)->get();
        return $result ? $result : [];
    }

    public static function getProductByCateIds($cate_ids, $limit)
    {
        return DB::table(self::$table)->whereIn('category_id', $cate_ids)->limit($limit)->get();
    }

    public static function getMany($pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = DB::table(self::$table)->skip($offset)->take($pagination['perpage']);
        if(isset($filter['name']) && $filter['name'] != ""){
            $query->where('name', 'like', "%".$filter['name']."%");
        }
        if(isset($filter['category_id']) && $filter['category_id'] != ""){
            $query->where('category_id', $filter['category_id']);
        }
        if(isset($filter['country_id']) && $filter['country_id'] != ""){
            $query->where('country_id', $filter['country_id']);
        }
        if(isset($filter['brand_id']) && $filter['brand_id'] != ""){
            $query->where('brand_id', $filter['brand_id']);
        }
        if(isset($filter['package_id']) && $filter['package_id'] != ""){
            $query->where('package_id', $filter['package_id']);
        }
        if(isset($filter['is_active']) && $filter['is_active'] != ""){
            $query->where('is_active', $filter['is_active']);
        }
        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        if(isset($sort['field']) && $sort['field'] === "created_at"){
            $query->orderBy('created_at', $sort['sort']);
        }
        return $query->get();
    }

    public static function totalRows() {
        $result = DB::table(self::$table)->count();
        return $result;
    }

    public static function findByKey($key, $value, $columns = ['*'])
    {
        $result = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $result ? $result : [];
    }

    public static function findById($id, $columns = ['*'])
    {
        $result = DB::table(self::$table)->select($columns)->where('id', $id)->first();
        return $result ? $result : [];
    }

    public static function add($data)
    {
        return DB::table(self::$table)->insertGetId($data);
    }

    public static function update($id, $data)
    {
        return DB::table(self::$table)->where('id', $id)->update($data);
    }

    public static function deleteMany($ids)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->delete();
    }

    public static function updateMany($ids, $data)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->update($data);
    }

    public static function delete($id)
    {
        return DB::table(self::$table)->where('id', $id)->delete();
    }

    public static function getListPagination($filter = [])
    {   
        $query = DB::table(self::$table);
        if(isset($filter['name']) && $filter['name'] != ""){
            $query->where('name', 'like', "%".$filter['name']."%");
        }
        if(isset($filter['category_id']) && $filter['category_id'] != ""){
            $query->where('category_id', $filter['category_id']);
        }
        if(isset($filter['country_id']) && $filter['country_id'] != ""){
            $query->where('country_id', $filter['country_id']);
        }
        if(isset($filter['brand_id']) && $filter['brand_id'] != ""){
            $query->where('brand_id', $filter['brand_id']);
        }
        if(isset($filter['package_id']) && $filter['package_id'] != ""){
            $query->where('package_id', $filter['package_id']);
        }
        if(isset($filter['is_active']) && $filter['is_active'] != ""){
            $query->where('is_active', $filter['is_active']);
        }
        if (isset($filter['created_at']) && $filter['created_at'] != "") {
            $time_filter = explode(" - ", $filter['created_at']);
            $start_time = date("Y-m-d 00:00:00", strtotime($time_filter[0]));
            $end_time = date("Y-m-d 23:59:59", strtotime($time_filter[1]));

            $query->where('created_at', '>=', $start_time);
            $query->where('created_at', '<', $end_time);
        }
        if(isset($sort['field']) && $sort['field'] === "created_at"){
            $query->orderBy('created_at', $sort['sort']);
        }
        return $query->paginate(20);
    }

}
