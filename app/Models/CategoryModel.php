<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
class CategoryModel
{
    //
    protected static $table = 'categories';

    public static function getAll()
    {
        return DB::table(self::$table)->get();
    }

    public static function getAllRoot($type = 'category_of_product')
    {
        return DB::table(self::$table)
            ->where('category_parent', 0)
            ->where('category_type', $type)
            ->get();
    }

    public static function getItemByRootId($parent_id, $limit = 1000)
    {
        return DB::table(self::$table)->where('category_parent', $parent_id)->limit($limit)->get();
    }

    public static function getItemIdByRootId($parent_id)
    {
        return DB::table(self::$table)->where('category_parent', $parent_id)->pluck('id');
    }

    public static function getAllByKey($columns = ['*'], $filter = [])
    {
        $query = DB::table(self::$table)->select($columns);
        if(isset($filter['category_name']) && $filter['category_name'] != ""){
            $query->where('category_name', 'like', "%".$filter['category_name']."%");
        }

        if(isset($filter['category_type']) && $filter['category_type'] != ""){
            $query->where('category_type', '=', $filter['category_type']);
        }

        if(isset($filter['category_parent'])){
            $query->where('category_parent', '=', $filter['category_parent']);
        }

        return $query->get();
    }

    public static function getMany($pagination, $sort, $filter)
    {
        $pagination['perpage']  = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $offset = ($pagination['page'] - 1) * $pagination['perpage'];
        $query = DB::table(self::$table)->skip($offset)->take($pagination['perpage']);

        if(isset($filter['category_name']) && $filter['category_name'] != ""){
            $query->where('category_name', 'like', "%".$filter['category_name']."%");
        }

        if(isset($filter['category_type']) && $filter['category_type'] != ""){
            $query->where('category_type', '=', $filter['category_type']);
        }

        if(isset($sort['field']) && $sort['field'] === "created_at"){
            $query->orderBy('created_at', $sort['sort']);
        }
        return $query->get();
    }

    public static function totalRows($filter) {
        $query = DB::table(self::$table);

        if(isset($filter['category_name']) && $filter['category_name'] != ""){
            $query->where('category_name', 'like', "%".$filter['category_name']."%");
        }

        if(isset($filter['category_type']) && $filter['category_type'] != ""){
            $query->where('category_type', '=', $filter['category_type']);
        }

        if(isset($filter['category_parent']) && $filter['category_parent'] != ""){
            $query->where('category_parent', '=', $filter['category_parent']);
        }


        if(isset($filter['language']) && $filter['language'] != ""){
            $query->where('language', '=', $filter['language']);
        }
        $result = $query->count();
        return $result;
    }

    public static function findByKey($key, $value, $columns = ['*'], $with = [])
    {
        $data = DB::table(self::$table)->select($columns)->where($key, $value)->first();
        return $data ? $data : [];
    }
    public static function insert($params)
    {
        return DB::table(self::$table)->insertGetId($params);

    }

    public static function update($id, $params)
    {
        return DB::table(self::$table)->where('id', $id)->update($params);

    }

    public static function deleteManyPost($ids)
    {
        return DB::table(self::$table)->whereIn('id', $ids)->delete();
    }

    public static function delete($id)
    {
        return DB::table(self::$table)->where('id', $id)->delete();

    }
}
