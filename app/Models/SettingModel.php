<?php

namespace App\Models;

use App\Events\SettingIsCreating;
use App\Services\SettingService;
use Auth;
use Illuminate\Support\Facades\DB;

class SettingModel
{
    protected static $table = 'settings';

    /**
     * Return all settings, with the setting name as key
     * @return array
     */
    public static function all()
    {
        $rawSettings = DB::table(self::$table)->orderBy('created_at', 'DESC')->get();

        $settings = [];
        foreach ($rawSettings as $setting) {
            $arr_setting = explode("::", $setting->setting_key);
            $setting_cate = $arr_setting[0];
            $setting_group = $arr_setting[1];
            $setting_name = $arr_setting[2];
            $settings[$setting_cate][$setting_group]['data'][$setting_name] = $setting;
        }

        return $settings;
    }

    public static function get($type, $lang = 'vi')
    {
        $rawSettings = DB::table(self::$table)
                            ->orderBy('created_at', 'DESC')
                            ->where('setting_key', 'like', "$type%")
                            ->get();

        $settings = [];
        foreach ($rawSettings as $setting) {
            if (in_array($setting->setting_key, SettingService::getMultipleLanguages()) && $setting->language !== $lang) {
                unset($setting);
                continue;
            }
            $arr_setting = explode("::", $setting->setting_key);
            $setting_cate = $arr_setting[0];
            $setting_group = $arr_setting[1];
            $setting_name = $arr_setting[2];
            $settings[$setting_cate."::".$setting_group."::".$setting_name] = $setting->setting_value;
        }

        return $settings;
    }

    /**
     * Create or update the settings
     * @param $settings
     * @return mixed|void
     */
    public static function createOrUpdate($settings)
    {
        foreach ($settings as $setting) {
            if ($setting_info = self::findByName($setting['setting_key'])) {
                unset($setting['created_by_user']);
                unset($setting['created_at']);
                DB::table(self::$table)->where('id', $setting_info->id)->update($setting);
                continue;
            }

            DB::table(self::$table)->insert($setting);
        }

        return true;
    }


    /**
     * Find a setting by its name
     * @param $settingName
     * @return mixed
     */
    public static function findByName($settingName, $lang = 'vi')
    {
        return DB::table(self::$table)->where('setting_key', $settingName)->where('language', $lang)->first();
    }

    /**
     * Create a setting with the given name
     * @param string $settingName
     * @param $settingValues
     * @return Setting
     */
    private function createForName($settingName, $settingValues)
    {
        $data['setting_key'] = $settingName;
        $data['setting_value'] = $settingValues;
        $data['created_by_user'] = Auth::guard('admin')->user()->email;
        $data['updated_by_user'] = Auth::guard('admin')->user()->email;
        $data['created_at'] = date("Y-m-d H:i:d");
        $data['updated_at'] = date("Y-m-d H:i:d");


        return DB::table(self::$table)->insert($data);
    }

    /**
     * Update the given setting
     * @param object setting
     * @param $settingValues
     */
    private function updateSetting($setting, $settingValues)
    {
        $setting['setting_key'] = $setting->seting_key;
        $setting['setting_value'] = $settingValues;
        $setting['updated_by_user'] = Auth::guard('admin')->user()->email;
        $setting['updated_at'] = date("Y-m-d H:i:d");
        DB::table(self::$table)->where('id', $setting->id)->update($setting);

        return $setting;
    }

    public static function deletebyCondition($conditions)
    {
        return DB::table(self::$table)->where($conditions)->delete();
    }

    public static function add($params)
    {
        return DB::table(self::$table)->insert($params);
    }

    public static function update($id, $params)
    {
        return DB::table(self::$table)->where('id', $id)->update($params);
    }
}
