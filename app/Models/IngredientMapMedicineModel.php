<?php

namespace App\Models;


use Illuminate\Support\Facades\DB;

class IngredientMapMedicineModel
{

    protected static $table = 'ingredient_map_medicine';

    public static function add($data)
    {
        return DB::table(self::$table)->insert($data);
    }

    public static function delete($id)
    {
        return DB::table(self::$table)->where('id', $id)->delete();
    }

    public static function deleteByMedicineId($medicine_id)
    {
        return DB::table(self::$table)->where('medicine_id', $medicine_id)->delete();
    }

    public static function getByMedicineId($medicine_id)
    {
        return DB::table(self::$table)->where('medicine_id', $medicine_id)->pluck('ingredient_id');
    }

}
