<?php

namespace App\Helpers;

class ArrayHelper
{
    /*
     *
     */

    public static function removeArrayNull($params)
    {
        if (!is_array($params)) {
            return $params;
        }

        if (isset($params['_token'])) {
            unset($params['_token']);
        }

        return collect($params)
            ->reject(function ($item) {
                return is_null($item);
            })
            ->flatMap(function ($item, $key) {

                return is_numeric($key)
                    ? [self::removeArrayNull($item)]
                    : [$key => self::removeArrayNull($item)];
            })
            ->toArray();
    }

    public static function removeToken($params)
    {
        if (!is_array($params)) {
            return $params;
        }

        if (isset($params['_token'])) {
            unset($params['_token']);
        }

        return $params;
    }

    public static function arrayLang() {
        return [
            'vi' => 'Tiếng Việt',
            'en' => 'Tiếng Anh',
        ];
    }

    public static function arrayPostType() {
        return [
            'news' => 'Tin tức',
            'event' => 'Sự kiện',
        ];
    }

    public static function listBlockTemplate() {
        return [
            'home_section_1' => 'Trang chủ Section 1',
            'home_section_2' => 'Trang chủ Section 2',
            'home_section_3' => 'Trang chủ Section 3',
            'home_section_4' => 'Trang chủ Section 4',
            'home_section_5' => 'Trang chủ Section 5',
            'home_section_6' => 'Trang chủ Section 6',
            'about_section_1' => 'Về chúng tôi Section 1',
            'about_section_2' => 'Về chúng tôi Section 2',
            'about_section_3' => 'Về chúng tôi Section 3',
            'about_section_4' => 'Về chúng tôi Section 4',
            'curriculum_section_1' => 'Chương trình học Section 1',
            'curriculum_section_2' => 'Chương trình học Section 2',
            'curriculum_section_3' => 'Chương trình học Section 3',
            'curriculum_section_4' => 'Chương trình học Section 4',
            'curriculum_section_5' => 'Chương trình học Section 5',
            'enrollment_section_1' => 'Ghi danh Section 1',
            'enrollment_section_2' => 'Ghi danh Section 2',
            'enrollment_section_3' => 'Ghi danh Section 3',
            'enrollment_section_4' => 'Ghi danh Section 4',
            'news_event_section_1' => 'Tin tức & Sự kiện Section 1',
            'news_event_section_2' => 'Tin tức & Sự kiện Section 2',
            'news_event_section_3' => 'Tin tức & Sự kiện Section 3',
            'news_event_section_4' => 'Tin tức & Sự kiện Section 4',
            'contact_section_1' => 'Liên hệ Section 1',
            'contact_section_2' => 'Liên hệ Section 2',
            'contact_section_3' => 'Liên hệ Section 3',
        ];
    }

    public static function listPageTemplate() {
        return [
            'about' => 'Giới thiệu',
            'curriculum' => 'Chương trình học',
            'enrollment' => 'Ghi danh',
            'contact' => 'Liên hệ'
        ];
    }
}
