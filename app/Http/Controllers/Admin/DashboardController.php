<?php

namespace App\Http\Controllers\Admin;

use App\Services\DashboardService;
use App\Services\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class DashboardController extends Controller
{
    

    public function __construct()
    {
        
        
    }

    /** 
    * ======================
    * Method:: INDEX
    * ======================
    */

    public function index()
    {
        // take 5 new user
        $data['listGuest'] = DashboardService::takeNewGuest(5);
        $data['listContact'] = DashboardService::takeNewContact(5);
        $data['listNews'] = DashboardService::takeNewNews(7);
        
        $data['totalGuest'] = DashboardService::totalGuest();
        $data['totalContact'] = DashboardService::totalContact();
        $data['totalNews'] = DashboardService::totalNews();
        $data['totalService'] = DashboardService::totalService();

        return view('admin.dash-board', $data);
    }
}
