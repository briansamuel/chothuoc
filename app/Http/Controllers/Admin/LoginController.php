<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\Message;
use App\Http\Controllers\Controller;
use App\Models\AgentModel;
use App\Models\GuestModel;
use App\Models\PasswordReset;
use App\Models\UserModel;
use App\Services\AgentService;
use App\Services\Auth\AuthService;
use App\Services\GuestService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Config;

class LoginController extends Controller
{
    protected $request;
    protected $userService;
    protected $guestService;

    function __construct(Request $request, UserService $userService)
    {
        $this->request = $request;
        $this->userService = $userService;
    }

    /**
     * ======================
     * Method:: login
     * ======================
     */

    public function login()
    {
        //check login
        $checkLogin = AuthService::checkLogin();
        if ($checkLogin) {
            return redirect()->route('welcome');
        }

        return view('admin.pages.login');
    }

    public function loginAction(Request $request)
    {
        $loginAdmin = AuthService::loginAdmin($request);

        return $loginAdmin;
    }

    /*
    * function logout
    */
    public function logout()
    {
        AuthService::logout();

        return redirect()->route('login');
    }

    public function resetPassword(Request $request)
    {
        $token = $request->input('token');
        $passwordReset = PasswordReset::where('token', $token)->first();
        if (!$passwordReset) {
            return view('admin.pages.error404');
        }
        return view('admin.pages.reset_password', ['token' => $token]);
    }

    public function activeUser(Request $request)
    {
        $active_code = $request->input('active_code');
        if ($active_code == '') {
            return view('admin.pages.error404');
        }
        $userInfo = UserModel::where('active_code', $active_code)->first();
        if (!$userInfo || $userInfo->status !== 'inactive') {
            return view('admin.pages.error404');
        }

        $active = $this->userService->activeUser($userInfo->id);
        if ($active) {
            Message::alertFlash('Bạn đã kích hoạt tài khoản thành công, vui lòng đăng nhập vào hệ thống!', 'success');
        } else {
            Message::alertFlash('Bạn đã kích hoạt tài khoản không thành công!', 'danger');
        }


        return redirect()->route('login');
    }

}
