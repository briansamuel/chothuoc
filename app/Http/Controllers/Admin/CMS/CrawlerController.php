<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Harimayco\Menu\Facades\Menu;

use App\Services\ValidationService;
use App\Services\LogsUserService;
use App\Services\IngredientService;
use App\Services\PostService;
use App\Services\CategoryService;
use App\Services\CategoryPostService;
use App\Services\BrandService;
use App\Services\PackageService;
use App\Services\CountryService;
use App\Services\MedicineService;
use Exception;
use HTMLDomParser;
use Illuminate\Support\Str;

class CrawlerController extends Controller
{



    public function __construct(Request $request, ValidationService $validator, 
    PostService $postService, CategoryService $categoryService, CategoryPostService $categoryPostService)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->postService = $postService;
        $this->categoryService = $categoryService;
        $this->categoryPostService = $categoryPostService;
    }

    /**
     * ======================
     * Method:: INDEX
     * ======================
     */
    public function crawl()
    {
        return view('admin.crawlers.index');
    }

    public function index()
    {
        // foreach (range('D', 'Z') as $char) {

        //     $url = "https://chosithuoc.com/hoat-chat/char=".$char;
        //     $html = $this->curl($url);
        //     $ingredent_link = HTMLDomParser::str_get_html($html)->find('.hoatchat-ds li a');
        //     foreach ($ingredent_link as $ingredent) {
        //         // echo $ingredent->href.'<br>';
        //         $row = IngredientService::findByKey('name', $ingredent->plaintext);
        //         if ($row) {
        //             echo 'Hoạt chất ' . $ingredent->plaintext . ' đã có sẵn <br>';

        //         } else {
        //             $data = $this->fillIngredient($ingredent->href);

        //             if ($data) {
        //                 $insert_id = IngredientService::insert($data);
        //                 if ($insert_id) {

        //                     echo 'Hoạt chất ' . $ingredent->plaintext . ' thêm thành công <br>';
        //                 } else {
        //                     echo 'Hoạt chất ' . $ingredent->plaintext . ' thêm thất bại <br>';
        //                 }
        //             }
        //         }
        //     }  
        // } 
        // for($i = 1;$i < 8; $i++) {
        //     $url = "https://chosithuoc.com/thuoc-giam-can-trang-".$i."/";

        //     $html = file_get_contents($url);
        //     if($html) {
        //         $regex = '#\<div class="itemsanpham"\>(.+?)\<\/div\>#s';
        //         preg_match_all($regex, $html, $matches);
        //         foreach($matches[0] as $match) {

        //             preg_match('/<a[^>]+href=([\'"])(?<href>.+?)\1[^>]*>/i', $match, $links);
        //             $link = $links[0];
        //             $disease_link = HTMLDomParser::str_get_html($link)->find('a')[0]->href;
        //             // $disease_link = HTMLDomParser::str_get_html($html)->find('.itemsanpham');
        //             echo $disease_link.'<br>';
        //         }
            
        //     }
        // }
        // $url = 'https://www.chosithuoc.com/enzym-ngon-tat-thanh-kids';
        // $data = $this->fillMedicince($url);
        // dd($data);
        // else {
        //     dd('Not HTML');
        // }
        // foreach ($disease_link as $key => $disease) {
        //     if ($key >= 0) {
        //         // echo $disease->href.'<br>';
        //         $title = trim($disease->find('.tieude a')[0]->plaintext);
        //         $img = $disease->find('.img img')[0]->src;
        //         $row = $this->postService->findByKey('post_title', $title);
        //         if ($row) {
        //             echo 'Thuốc ' . $title . ' đã có sẵn <br>';
        //         } else {
        //             $data = $this->fillMedicince($disease->find('.tieude a')[0]->href, $img);

        //             if ($data) {
        //                 $insert_id = $this->postService->insert($data);
        //                 if ($insert_id) {
        //                     // $data_cpr = array('category_id' => 53, 'post_id' => $insert_id);
        //                     // $status = $this->categoryPostService->insert($data_cpr);
        //                     echo 'Thuốc ' . $title . ' thêm thành công <br>';
        //                 } else {
        //                     echo 'Thuốc ' . $title . ' thêm thất bại <br>';
        //                 }
        //             }
        //         }
        //     }
        // }
        $this->addCategoryMedicine();
    }

    
    public function addCategoryMedicine() {
        $html = '<div class="sublist-inner">
        <label data-id="551" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="capecitabin">Capecitabin</span>
        </label>
        <label data-id="559" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="letrozole">Letrozole</span>
        </label>
        <label data-id="670" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="bicalutamide">Bicalutamide</span>
        </label>
        <label data-id="900" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="fucoidan">Fucoidan</span>
        </label>
        <label data-id="1485" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="thymomodulin">Thymomodulin</span>
        </label>
        <label data-id="1498" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="tamsulosin hydrochloride">Tamsulosin hydrochloride</span>
        </label>
        <label data-id="1893" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="erlotinib">Erlotinib</span>
        </label>
        <label data-id="1904" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="cyclophosphamid">Cyclophosphamid</span>
        </label>
        <label data-id="1905" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="gefitinib">Gefitinib</span>
        </label>
        <label data-id="1907" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="goserelin">Goserelin</span>
        </label>
        <label data-id="1919" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="abiraterone">Abiraterone</span>
        </label>
        <label data-id="1922" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="hydroxycarbamide">Hydroxycarbamide</span>
        </label>
        <label data-id="1924" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="temozolomide">Temozolomide</span>
        </label>
        <label data-id="1998" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="sorafenib">Sorafenib</span>
        </label>
        <label data-id="2003" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="exemestane">Exemestane</span>
        </label>
        <label data-id="2154" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="tamoxifen">Tamoxifen</span>
        </label>
        <label data-id="2719" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="leuprorelin">Leuprorelin</span>
        </label>
        <label data-id="2759" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="anastrozole">Anastrozole</span>
        </label>
        <label data-id="2765" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="fulvestrant">Fulvestrant</span>
        </label>
        <label data-id="2766" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="osimertinib">Osimertinib</span>
        </label>
        <label data-id="2786" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="aflibercept">Aflibercept</span>
        </label>
        <label data-id="2816" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="bevacizumab">Bevacizumab</span>
        </label>
        <label data-id="2818" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="trastuzumab">Trastuzumab</span>
        </label>
        <label data-id="2820" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="pertuzumab">Pertuzumab</span>
        </label>
        <label data-id="2822" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="rituximab">Rituximab</span>
        </label>
        <label data-id="2830" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="atezolizumab">Atezolizumab</span>
        </label>
        <label data-id="2839" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="paclitaxel">Paclitaxel</span>
        </label>
        <label data-id="2841" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="gemcitabine">Gemcitabine</span>
        </label>
        <label data-id="2844" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="doxorubicin">Doxorubicin</span>
        </label>
        <label data-id="2859" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="cetuximab">Cetuximab</span>
        </label>
        <label data-id="2892" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="pembrolizumab">Pembrolizumab</span>
        </label>
        <label data-id="2910" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="everolimus">Everolimus</span>
        </label>
        <label data-id="2920" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="nilotinib">Nilotinib</span>
        </label>
        <label data-id="2924" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="topotecan">Topotecan</span>
        </label>
        <label data-id="2925" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="imatinib">Imatinib</span>
        </label>
        <label data-id="2931" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="ceritinib">Ceritinib</span>
        </label>
        <label data-id="2935" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="docetaxel">Docetaxel</span>
        </label>
        <label data-id="2937" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="etoposide">Etoposide</span>
        </label>
        <label data-id="2939" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="oxaliplatin">Oxaliplatin</span>
        </label>
        <label data-id="2940" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="vinorelbine">Vinorelbine</span>
        </label>
        <label data-id="2944" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="fluorouracil">Fluorouracil</span>
        </label>
        <label data-id="2945" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="cisplatin">Cisplatin</span>
        </label>
        <label data-id="2946" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="fludarabine">Fludarabine</span>
        </label>
        <label data-id="2956" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="epirubicin">Epirubicin</span>
        </label>
        <label data-id="2959" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="irinotecan">Irinotecan</span>
        </label>
        <label data-id="3027" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="thalidomid">Thalidomid</span>
        </label>
        <label data-id="3238" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="regorafenib">Regorafenib</span>
        </label>
        <label data-id="3239" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="lenvatinib">Lenvatinib</span>
        </label>
        <label data-id="3283" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="vinblastine">Vinblastine</span>
        </label>
        <label data-id="3299" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="vincristin">Vincristin</span>
        </label>
        <label data-id="3376" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="busulfan">Busulfan</span>
        </label>
        <label data-id="3378" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="tegafur">Tegafur</span>
        </label>
        <label data-id="3379" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="gimeracil">Gimeracil</span>
        </label>
        <label data-id="3380" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="bendamustin">Bendamustin</span>
        </label>
        <label data-id="3381" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="ustekinumab">Ustekinumab</span>
        </label>
        <label data-id="3382" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="&nbsp;degarelix">&nbsp;Degarelix</span>
        </label>
        <label data-id="3383" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="golimumab">Golimumab</span>
        </label>
        <label data-id="3384" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="afatinib">Afatinib</span>
        </label>
        <label data-id="3385" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="ibrutinib">Ibrutinib</span>
        </label>
        <label data-id="3437" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="dacltasvir">Dacltasvir</span>
        </label>
        <label data-id="3438" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="crizotinib">Crizotinib</span>
        </label>
        <label data-id="3439" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="axitinib">Axitinib</span>
        </label>
        <label data-id="3440" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="cabozantinib">Cabozantinib</span>
        </label>
        <label data-id="3441" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="eltrombopag">Eltrombopag</span>
        </label>
        <label data-id="3442" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="larotrectinib">Larotrectinib</span>
        </label>
        <label data-id="3444" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="olaparib">Olaparib</span>
        </label>
        <label data-id="3445" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="palbociclib">Palbociclib</span>
        </label>
        <label data-id="3478" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="alectinib">Alectinib</span>
        </label>
        <label data-id="3479" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="brigatinib">Brigatinib</span>
        </label>
        <label data-id="3489" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="dasatinib">Dasatinib</span>
        </label>
        <label data-id="3491" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="ponatinib">Ponatinib</span>
        </label>
        <label data-id="3493" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="procarbazine">Procarbazine</span>
        </label>
        <label data-id="3496" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="venetoclax">Venetoclax</span>
        </label>
        <label data-id="3497" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="melphalan">Melphalan</span>
        </label>
        <label data-id="3499" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="anagrelide">Anagrelide</span>
        </label>
        <label data-id="3500" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="neratinib">Neratinib</span>
        </label>
        <label data-id="3503" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="chlorambucil">Chlorambucil</span>
        </label>
        <label data-id="3507" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="pipobroman">Pipobroman</span>
        </label>
        <label data-id="3508" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="lapatinib">Lapatinib</span>
        </label>
        <label data-id="3509" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="sunitinib">Sunitinib</span>
        </label>
        <label data-id="3659" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="daunorubicin">Daunorubicin</span>
        </label>
        <label data-id="3667" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="megestrol acetate">Megestrol acetate</span>
        </label>
        <label data-id="3686" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="mitomycin">Mitomycin</span>
        </label>
        <label data-id="3687" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="mercaptopurine">Mercaptopurine</span>
        </label>
        <label data-id="3709" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="pomalidomide">Pomalidomide</span>
        </label>
        <label data-id="3719" data-idnhomid="53">
            <i class="fa fa-loctren" aria-hidden="true"></i>
            <span data-text="pirfenidone">Pirfenidone</span>
        </label>
    </div>';
        
        $disease_link = HTMLDomParser::str_get_html($html)->find('span');
          foreach ($disease_link as $key => $disease) {
            if ($key >= 0) {
                // echo $disease->href.'<br>';
                $name = trim($disease->plaintext);
    
                $row = IngredientService::findByKey('name', $name);
                if ($row) {
                    $update = IngredientService::updateMedicineGroup($row->id, 28);
                    if($update) {
                        echo 'Ingredient ' . $name . ' cập nhật thành công <br>';
                    }
                    
                } else {
                    echo 'Ingredient ' . $name . ' Không tồn tại <br>';
                }
            }
        }

    }
    public function ajaxListDisease() {
        $params = $this->request->only('page');
        $data = array();
        $data['list_story'] = array();
        if (isset($params['page'])) {
            $url = $params['page'];

            $disease = $this->fillDisease($url);
            $row = $this->postService('post_title', $disease['post_title']);
            if ($row) {
                $story_status = 'Bệnh ' . $disease['post_title'] . ' đã có sẵn ';
            } else {


                if ($disease) {
                    $insert_id = $this->postService->insert($disease);
                    if ($disease) {
                        
                        $story_status = 'Bệnh ' . $disease['post_title'] . ' thêm thành công ';
                    } else {
                        $story_status = 'Bệnh ' . $disease['post_title'] . ' thêm thất bại ';
                    }
                } else {
                    $story_status = 'Bệnh ' . $disease['post_title'] . ' dữ liệu bị lỗi, vui lòng thử lại ';
                }
            }

        } else {
            $data['status'] = 'error';
            $data['message'] = '<strong class="kt-font-danger kt-font-bold">Không tìm thấy url</strong>';
        }
        //echo 'TEST';
        echo json_encode($data);
        die();
    }

    public function ajaxListMedicine() {
        $params = $this->request->only('url');
        $data = array();
        $data['list_medicine'] = array();
        if (isset($params['url'])) {
            $url = $params['url'];

            $medicince = $this->fillMedicince($url);
  
            $row = MedicineService::findByKey('name', $medicince['name']);
            $data['status'] = 'warning';
            if ($row) {
                $story_status = 'Sản phẩm ' . $medicince['name'] . ' đã có sẵn ';
            } else {


                if ($medicince) {
                    $insert_id = MedicineService::add($medicince);
                    if ($insert_id) {
                        
                        $data['status'] = 'success';
                        $story_status = 'Sản phẩm ' . $medicince['name'] . ' thêm thành công ';
                    } else {
                        $data['status'] = 'error';
                        $story_status = 'Sản phẩm ' . $medicince['name'] . ' thêm thất bại ';
                    }
                } else {
                    $data['status'] = 'error';
                    $story_status = 'Sản phẩm ' . $medicince['name'] . ' dữ liệu bị lỗi, vui lòng thử lại ';
                }
            }
            
            $data['message'] = '<strong class="kt-font-'.$data['status'].' kt-font-bold">'.$story_status.'</strong>';
        } else {
            $data['status'] = 'error';
            $data['message'] = '<strong class="kt-font-danger kt-font-bold">Không tìm thấy url</strong>';
        }
        //echo 'TEST';
        echo json_encode($data);
        die();
    }

    public function fillMedicinceCategory($url)
    {
        $html = $this->curl($url);
        $params = array();

        try {
            if ($html) {
                $tags = get_meta_tags($url);
                
                $params['category_name'] = trim(str_replace('  ',' ',$tags['title']));
                $params['category_description'] = trim(str_replace('  ',' ',$tags['description']));
                $params['category_seo_title'] = $params['category_name'];
                $params['category_seo_keyword'] = trim(str_replace('  ',' ',$tags['keywords']));
                $params['category_seo_description'] = trim(str_replace('  ',' ',$tags['description']));
                
                $params['category_type'] = 'category_of_product';
                $params['category_parent'] = 242;
                return $params;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return $e;
        }

        return false;
    }

    public function fillDiseaseCategory($url)
    {
        $html = $this->curl($url);
        $params = array();

        try {
            if ($html) {
                $params['category_name'] = HTMLDomParser::str_get_html($html)->find('.nhombenh-title')[0]->plaintext;
                $params['category_description'] = HTMLDomParser::str_get_html($html)->find('meta[name="description"]')[0]->innertext;
                $params['category_seo_title'] = HTMLDomParser::str_get_html($html)->find('.nhombenh-title')[0]->plaintext;
                $params['category_seo_keyword'] = HTMLDomParser::str_get_html($html)->find('meta[name="keywords"]')[0]->content;
                $params['category_seo_description'] = HTMLDomParser::str_get_html($html)->find('meta[name="description"]')[0]->content;
                $params['category_type'] = 'category_of_disease';
                return $params;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }

        return false;
    }

    public function addBrands($name) {
        if($name == '')
            return 0;
        $row = BrandService::findByKey('name', $name);
        if($row) {
            return $row->id;
        } else {
            $params = array('name' => $name, 'slug' => Str::slug($name));
            $insert_id = BrandService::add($params);
            if($insert_id) 
                return $insert_id;
            else
                return 0;
        }
    }

    public function addCountry($name) {
        if($name == '')
            return 0;
        $row = CountryService::findByKey('name', $name);
        if($row) {
            return $row->id;
        } else {
            $params = array('name' => $name, 'slug' => Str::slug($name));
            $insert_id = CountryService::add($params);
            if($insert_id) 
                return $insert_id;
            else
                return 0;
        }
    }

    public function addPackage($name) {
        if($name == '')
            return 0;
        $row = PackageService::findByKey('name', $name);
        if($row) {
            return $row->id;
        } else {
            $params = array('name' => $name, 'slug' => Str::slug($name));
            $insert_id = PackageService::add($params);
            if($insert_id) 
                return $insert_id;
            else
                return 0;
        }
    }

    public function fillMedicince($url)
    {
        $html = $this->curl($url);
        $params = array();
   
        
        try {
            if ($html) {
                $params['name'] = HTMLDomParser::str_get_html($html)->find('meta[name="title"]')[0]->content;
                $params['slug'] = Str::slug($params['name']);
                $params['content'] = HTMLDomParser::str_get_html($html)->find('.noidung')[0]->innertext;
                $params['instruction'] = isset(HTMLDomParser::str_get_html($html)->find('.sanpham_info .row_noidung')[2]) ? HTMLDomParser::str_get_html($html)->find('.sanpham_info .row_noidung')[2]->innertext : '';
                $params['specifications'] = isset(HTMLDomParser::str_get_html($html)->find('.thongso-sanpham')[0]->innertext) ? HTMLDomParser::str_get_html($html)->find('.thongso-sanpham')[0] : null;
                $params['keyword'] = isset(HTMLDomParser::str_get_html($html)->find('meta[name="keywords"]')[0]) ? HTMLDomParser::str_get_html($html)->find('meta[name="keywords"]')[0]->content : '';
                $params['description'] = HTMLDomParser::str_get_html($html)->find('meta[name="description"]')[0]->content;
                $params['is_active'] = 1;
                $params['price'] = isset(HTMLDomParser::str_get_html($html)->find('#loadthuoctinh_price .gia')[0]->plaintext) ? HTMLDomParser::str_get_html($html)->find('#loadthuoctinh_price .gia')[0]->plaintext : 0;
                $params['price'] = (int) str_replace('&nbsp;đ','', $params['price']) * 1000;
                $params['total'] = 100;
                $image_url = HTMLDomParser::str_get_html($html)->find('.sanpham_img img')[0]->src;
                $image = $this->grab_image($image_url, 'uploads/files/' . $params['slug'] . '.jpg');
                $params['image'] = 'http://' . config('domain.web.domain') . '/uploads/files/' . $params['slug'] . '.jpg';
                $params['package'] = '';
                $params['brand'] = '';
                $params['country'] = '';
                $specifications = array();
                $breadcrumbs = isset(HTMLDomParser::str_get_html($html)->find('#breadcrumbs li a')[3]) ? trim(HTMLDomParser::str_get_html($html)->find('#breadcrumbs li a')[3]->innertext) : trim(HTMLDomParser::str_get_html($html)->find('#breadcrumbs li a')[2]->innertext);
                $params['category_id'] = 0;
                $row_category = $this->categoryService->findByKey('category_name', $breadcrumbs);
                if($row_category) {
                    $params['category_id'] = $row_category->id;
                } else {
                    $category_params = array(
                        'category_name' => $breadcrumbs,
                        'category_slug' => Str::slug($breadcrumbs),
                        'category_description' => $breadcrumbs,
                    );
                    $category_id = $this->categoryService->insert($category_params);
                    if($category_id) {
                        $params['category_id'] = $category_id;
                    }
                }
                if($params['specifications']) {
                    $cell_tds = HTMLDomParser::str_get_html($params['specifications'])->find('table td');
                    foreach($cell_tds as $key => $cella) {
                        $cell = trim($cella->plaintext);
                        if($key % 2 == 0) {
                            $pcell = array('label' => $cell, 'value' => trim($cell_tds[$key+1]->plaintext));
                            array_push($specifications, $pcell);
                        }

                        if($cell == 'Quy cách đóng gói') {
                            $params['package'] = trim($cell_tds[$key+1]->plaintext);
                        }
                        else if($cell == 'Thương hiệu') {
                            $params['brand'] = trim($cell_tds[$key+1]->plaintext);
                        }
                        else if($cell == 'Xuất xứ') {
                            $params['country'] = trim($cell_tds[$key+1]->plaintext);
                        }
                    }
                }
                
                $params['specifications'] = json_encode($specifications, JSON_UNESCAPED_UNICODE);
                $params['brand_id'] = $this->addBrands($params['brand']);
                $params['package_id'] = $this->addPackage($params['package']);
                $params['country_id'] = $this->addCountry($params['country']);
                unset($params['package']);
                unset($params['brand']);
                unset($params['country']);
                
                return $params;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }

        return false;
    }

    public function fillNews($url, $image_url)
    {
        $html = $this->curl($url);
        $params = array();
        $user = auth()->user();
        $params['created_by_user'] = $user->id;
        $params['updated_by_user'] = $user->id;
        try {
            if ($html) {
                $params['post_title'] = HTMLDomParser::str_get_html($html)->find('meta[name="title"]')[0]->content;
                $params['post_slug'] = Str::slug($params['post_title']);
                $params['post_description'] = HTMLDomParser::str_get_html($html)->find('meta[name="description"]')[0]->content;
                $params['post_content'] = HTMLDomParser::str_get_html($html)->find('.noidung')[0]->innertext;
                $params['post_status'] = 'publish';
                $params['post_type'] = 'news';
                
                $image = $this->grab_image($image_url, 'uploads/files/' . $params['post_slug'] . '.jpg');
                $params['post_thumbnail'] = 'http://' . config('domain.web.domain') . '/uploads/files/' . $params['post_slug'] . '.jpg';
                $params['post_seo_title'] =  $params['post_title'];
                $params['post_seo_keyword'] = isset(HTMLDomParser::str_get_html($html)->find('meta[name="keywords"]')[0]) ? HTMLDomParser::str_get_html($html)->find('meta[name="keywords"]')[0]->content : '';
                $params['post_seo_description'] = HTMLDomParser::str_get_html($html)->find('meta[name="description"]')[0]->content;
                $params['language'] =  'vi';
                return $params;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }

        return false;
    }

    public function fillDisease($url)
    {
        $html = $this->curl($url);
        $params = array();
        $user = auth()->user();
        $params['created_by_user'] = $user->id;
        $params['updated_by_user'] = $user->id;
        try {
            if ($html) {
                $params['post_title'] = HTMLDomParser::str_get_html($html)->find('meta[name="title"]')[0]->content;
                $params['post_slug'] = Str::slug($params['post_title']);
                $params['post_description'] = '';
                $params['post_content'] = HTMLDomParser::str_get_html($html)->find('.noidungchitiet article')[0]->innertext;
                $params['post_status'] = 'publish';
                $params['post_type'] = 'disease';
                $image_url = HTMLDomParser::str_get_html($html)->find('article .img-wrap img')[0]->src;
                $image = $this->grab_image($image_url, 'uploads/files/' . $params['post_slug'] . '.jpg');
                $params['post_thumbnail'] = 'http://' . config('domain.web.domain') . '/uploads/files/' . $params['post_slug'] . '.jpg';
                $params['post_seo_title'] =  $params['post_title'];
                $params['post_seo_keyword'] = isset(HTMLDomParser::str_get_html($html)->find('meta[name="keywords"]')[0]) ? HTMLDomParser::str_get_html($html)->find('meta[name="keywords"]')[0]->content : '';
                $params['post_seo_description'] = HTMLDomParser::str_get_html($html)->find('meta[name="description"]')[0]->content;
                $params['language'] =  'vi';
                return $params;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }

        return false;
    }

    public function fillIngredient($url)
    {
        $html = $this->curl($url);
        $params = array();
        try {
            if ($html) {
                $params['name'] = HTMLDomParser::str_get_html($html)->find('meta[name="title"]')[0]->content;
                $params['slug'] = Str::slug($params['name']);
                $params['content'] = HTMLDomParser::str_get_html($html)->find('.noidungchitiet .noidung')[0]->innertext;
                $params['status'] = 'publish';
                $params['seo_name'] =  $params['name'];
                $params['seo_keyword'] = HTMLDomParser::str_get_html($html)->find('meta[name="keywords"]')[0]->content;
                $params['seo_description'] = HTMLDomParser::str_get_html($html)->find('meta[name="description"]')[0]->content;

                return $params;
            } else {
                return false;
            }
        } catch (Exception $e) {
            return false;
        }

        return false;
    }

    public function curl($url)
    {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //timeout in seconds
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_VERBOSE, true); // some output will go to stderr / error_log
        //curl_setopt($ch, CURLOPT_REFERER, 'https://gbanime.com/');
        //curl_setopt($ch,CURLOPT_FOLLOWLOCATION,true);
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    function grab_image($url, $saveto)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        $raw = curl_exec($ch);
        curl_close($ch);
        if (file_exists($saveto)) {
            unlink($saveto);
        }
        $fp = fopen($saveto, 'x');
        fwrite($fp, $raw);
        fclose($fp);
    }
}
