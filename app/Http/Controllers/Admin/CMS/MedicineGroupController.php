<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Helpers\ArrayHelper;
use App\Helpers\Message;
use App\Http\Controllers\Controller;
use App\Services\LogsUserService;
use App\Services\ValidationService;
use App\Services\MedicineGroupService;
use Illuminate\Http\Request;

class MedicineGroupController extends Controller
{
    protected $request;
    protected $groupService;
    protected $validator;

    function __construct(Request $request, ValidationService $validator, MedicineGroupService $groupService)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->groupService = $groupService;
    }


    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function index()
    {
        return view('admin.medicine_group.index');
    }


    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function ajaxGetList()
    {
        $params = $this->request->all();

        $result = $this->groupService->getList($params);

        return response()->json($result);
    }

    /**
     * METHOD viewInsert - VIEW ADD, EDIT NEWS
     *
     * @return void
     */

    public function add()
    {

        return view('admin.medicine_group.add');
    }

    public function addAction()
    {
        $params = $this->request->only('name', 'sort_order');
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'add_medicine_group_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all()), 400);
        }

        $add = MedicineGroupService::add($params);
        if ($add) {
            //add log
            $log['action'] = "Thêm mới 1 Medicine Group có id = " . $add;
            $log['content'] = json_encode($params);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);

            $data['success'] = true;
            $data['message'] = "Thêm mới Medicine Group thành công !!!";
        } else {
            $data['message'] = "Lỗi khi thêm mới Medicine Group !";
        }

        return response()->json($data);
    }

    public function deleteMany()
    {
        $params = $this->request->only('ids', 'total');
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $delete = $this->groupService->deleteMany($params['ids']);
        if (!$delete) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã xóa tổng cộng " . $params['total'] . " Medicine Group thành công !!!", 'success');

        //add log
        $log['action'] = "Xóa " . $params['total'] . " Medicine Group thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã xóa tổng cộng " . $params['total'] . " Medicine Group thành công !!!";
        return response()->json($data);
    }

    public function delete($id)
    {
        $detail = $this->groupService->detail($id);
        $delete = $this->groupService->delete($id);
        if($delete) {
            //add log
            $log['action'] = "Xóa Medicine Group thành công có ID = " . $id;
            $log['content'] = json_encode($detail);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);

            Message::alertFlash('Bạn đã xóa medicine group thành công', 'success');
        } else {
            Message::alertFlash('Bạn đã xóa medicine group không thành công', 'danger');
        }

        return redirect()->route("medicine_group.list");
    }

    public function edit($id)
    {
        $groupInfo = $this->groupService->detail($id);

        return view('admin.medicine_group.edit', ['groupInfo' => $groupInfo]);
    }

    public function editAction($id)
    {
        $params = $this->request->only(['name', 'sort_order']);
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'edit_medicine_group_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all(), 400));
        }

        $edit = $this->groupService->edit($id, $params);
        if (!$edit) {
            return response()->json(Message::get(13, $lang = '', []), 400);
        }

        //add log
        $log['action'] = "Cập nhập Medicine Group thành công có ID = " . $id;
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Cập nhập Medicine Group thành công !!!";
        return response()->json($data);
    }

    public function editManyAction()
    {
        $params = $this->request->only(['status', 'ids', 'total']);
        $params = ArrayHelper::removeArrayNull($params);
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $update = $this->groupService->updateMany($params['ids'], ['status' => $params['status']]);
        if (!$update) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã cập nhập tổng cộng " . $params['total'] . " Medicine Group thành công !!!", 'success');

        //add log
        $log['action'] = "Cập nhập nhiều Medicine Group thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã cập nhập tổng cộng " . $params['total'] . " Medicine Group thành công !!!";
        return response()->json($data);
    }

}
