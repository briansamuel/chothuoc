<?php

namespace App\Http\Controllers\Admin\CMS;


use App\Http\Controllers\Controller;
use App\Helpers\ArrayHelper;
use App\Helpers\Message;
use App\Services\OrderService;
use App\Services\LogsUserService;
use App\Services\ValidationService;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    //
    protected $request;
    protected $orderService;
    protected $validator;

    public function __construct(Request $request, OrderService $orderService, ValidationService $validator)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->orderService = $orderService;
    }

    public function index()
    {

        return view('admin.orders.index');
    }

    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function ajaxGetList()
    {
        $params = $this->request->all();
        $params['columns'] = ['*'];
        $result = $this->orderService->getList($params);

        return response()->json($result);
    }

    public function deleteMany()
    {
        $params = $this->request->only('ids', 'total');
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $delete = $this->orderService->deleteMany($params['ids']);
        if (!$delete) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã xóa tổng cộng " . $params['total'] . " đánh giá thành công !!!", 'success');

        //add log
        $log['action'] = "Xóa " . $params['total'] . " đánh giá thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã xóa tổng cộng " . $params['total'] . " đánh giá thành công !!!";
        return response()->json($data);
    }

    public function delete($id)
    {
        $detail = $this->orderService->detail($id);
        $delete = $this->orderService->delete($id);
        if ($delete) {
            //add log
            $log['action'] = "Xóa đánh giá thành công có ID = " . $id;
            $log['content'] = json_encode($detail);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);

            Message::alertFlash('Bạn đã xóa đánh giá thành công', 'success');
        } else {
            Message::alertFlash('Bạn đã xóa đánh giá không thành công', 'danger');
        }

        return redirect()->route("orders.list");
    }

    public function edit($id)
    {

        $data['reviewInfo'] = $this->orderService->detail($id);


        return view('admin.orders.edit', $data);


    }

    public function editAction($id)
    {
        $params = $this->request->only(['status']);
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'edit_order_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all(), 400));
        }

        $edit = $this->orderService->edit($id, $params);
        if (!$edit) {
            return response()->json(Message::get(13, $lang = '', []), 400);
        }

        //add log
        $log['action'] = "Cập nhập đánh giá thành công có ID = " . $id;
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Cập nhập đánh giá thành công !!!";
        return response()->json($data);
    }

    public function editManyAction()
    {
        $params = $this->request->only(['status', 'ids', 'total']);
        $params = ArrayHelper::removeArrayNull($params);
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $update = $this->orderService->updateMany($params['ids'], ['status' => $params['status']]);
        if (!$update) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã cập nhập tổng cộng " . $params['total'] . " đánh giá thành công !!!", 'success');

        //add log
        $log['action'] = "Cập nhập nhiều đánh giá thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã cập nhập tổng cộng " . $params['total'] . " đánh giá thành công !!!";
        return response()->json($data);
    }
}
