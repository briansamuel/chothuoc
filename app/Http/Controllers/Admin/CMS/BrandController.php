<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Helpers\ArrayHelper;
use App\Helpers\Message;
use App\Http\Controllers\Controller;
use App\Services\LogsUserService;
use App\Services\ValidationService;
use App\Services\BrandService;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    protected $request;
    protected $brandService;
    protected $validator;

    function __construct(Request $request, ValidationService $validator, BrandService $brandService)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->brandService = $brandService;
    }


    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function index()
    {
        return view('admin.brand.index');
    }


    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function ajaxGetList()
    {
        $params = $this->request->all();

        $result = $this->brandService->getList($params);

        return response()->json($result);
    }

    /**
     * METHOD viewInsert - VIEW ADD, EDIT NEWS
     *
     * @return void
     */

    public function add()
    {

        return view('admin.brand.add');
    }

    public function addAction()
    {
        $params = $this->request->only('name', 'sort_order');
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'add_brand_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all()), 400);
        }

        $add = BrandService::add($params);
        if ($add) {
            //add log
            $log['action'] = "Thêm mới 1 nhãn hiệu có id = " . $add;
            $log['content'] = json_encode($params);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);

            $data['success'] = true;
            $data['message'] = "Thêm mới nhãn hiệu thành công !!!";
        } else {
            $data['message'] = "Lỗi khi thêm mới nhãn hiệu !";
        }

        return response()->json($data);
    }

    public function deleteMany()
    {
        $params = $this->request->only('ids', 'total');
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $delete = $this->brandService->deleteMany($params['ids']);
        if (!$delete) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã xóa tổng cộng " . $params['total'] . " nhãn hiệu thành công !!!", 'success');

        //add log
        $log['action'] = "Xóa " . $params['total'] . " nhãn hiệu thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã xóa tổng cộng " . $params['total'] . " nhãn hiệu thành công !!!";
        return response()->json($data);
    }

    public function delete($id)
    {
        $detail = $this->brandService->detail($id);
        $delete = $this->brandService->delete($id);
        if($delete) {
            //add log
            $log['action'] = "Xóa nhãn hiệu thành công có ID = " . $id;
            $log['content'] = json_encode($detail);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);

            Message::alertFlash('Bạn đã xóa nhãn hiệu thành công', 'success');
        } else {
            Message::alertFlash('Bạn đã xóa nhãn hiệu không thành công', 'danger');
        }

        return redirect()->route("brand.list");
    }

    public function edit($id)
    {
        $groupInfo = $this->brandService->detail($id);

        return view('admin.brand.edit', ['groupInfo' => $groupInfo]);
    }

    public function editAction($id)
    {
        $params = $this->request->only(['name', 'sort_order']);
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'edit_brand_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all(), 400));
        }

        $edit = $this->brandService->edit($id, $params);
        if (!$edit) {
            return response()->json(Message::get(13, $lang = '', []), 400);
        }

        //add log
        $log['action'] = "Cập nhập nhãn hiệu thành công có ID = " . $id;
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Cập nhập nhãn hiệu thành công !!!";
        return response()->json($data);
    }

}
