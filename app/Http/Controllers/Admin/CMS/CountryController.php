<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Helpers\ArrayHelper;
use App\Helpers\Message;
use App\Http\Controllers\Controller;
use App\Services\LogsUserService;
use App\Services\ValidationService;
use App\Services\CountryService;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    protected $request;
    protected $countryService;
    protected $validator;

    function __construct(Request $request, ValidationService $validator, CountryService $countryService)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->countryService = $countryService;
    }


    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function index()
    {
        return view('admin.country.index');
    }


    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function ajaxGetList()
    {
        $params = $this->request->all();

        $result = $this->countryService->getList($params);

        return response()->json($result);
    }

    /**
     * METHOD viewInsert - VIEW ADD, EDIT NEWS
     *
     * @return void
     */

    public function add()
    {

        return view('admin.country.add');
    }

    public function addAction()
    {
        $params = $this->request->only('name', 'sort_order');
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'add_country_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all()), 400);
        }

        $add = CountryService::add($params);
        if ($add) {
            //add log
            $log['action'] = "Thêm mới 1 xuất sứ có id = " . $add;
            $log['content'] = json_encode($params);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);

            $data['success'] = true;
            $data['message'] = "Thêm mới xuất sứ thành công !!!";
        } else {
            $data['message'] = "Lỗi khi thêm mới xuất sứ !";
        }

        return response()->json($data);
    }

    public function deleteMany()
    {
        $params = $this->request->only('ids', 'total');
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $delete = $this->countryService->deleteMany($params['ids']);
        if (!$delete) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã xóa tổng cộng " . $params['total'] . " xuất sứ thành công !!!", 'success');

        //add log
        $log['action'] = "Xóa " . $params['total'] . " xuất sứ thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã xóa tổng cộng " . $params['total'] . " xuất sứ thành công !!!";
        return response()->json($data);
    }

    public function delete($id)
    {
        $detail = $this->countryService->detail($id);
        $delete = $this->countryService->delete($id);
        if($delete) {
            //add log
            $log['action'] = "Xóa xuất sứ thành công có ID = " . $id;
            $log['content'] = json_encode($detail);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);

            Message::alertFlash('Bạn đã xóa xuất sứ thành công', 'success');
        } else {
            Message::alertFlash('Bạn đã xóa xuất sứ không thành công', 'danger');
        }

        return redirect()->route("country.list");
    }

    public function edit($id)
    {
        $groupInfo = $this->countryService->detail($id);

        return view('admin.country.edit', ['groupInfo' => $groupInfo]);
    }

    public function editAction($id)
    {
        $params = $this->request->only(['name', 'sort_order']);
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'edit_country_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all(), 400));
        }

        $edit = $this->countryService->edit($id, $params);
        if (!$edit) {
            return response()->json(Message::get(13, $lang = '', []), 400);
        }

        //add log
        $log['action'] = "Cập nhập xuất sứ thành công có ID = " . $id;
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Cập nhập xuất sứ thành công !!!";
        return response()->json($data);
    }

    public function editManyAction()
    {
        $params = $this->request->only(['status', 'ids', 'total']);
        $params = ArrayHelper::removeArrayNull($params);
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $update = $this->countryService->updateMany($params['ids'], ['status' => $params['status']]);
        if (!$update) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã cập nhập tổng cộng " . $params['total'] . " xuất sứ thành công !!!", 'success');

        //add log
        $log['action'] = "Cập nhập nhiều xuất sứ thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã cập nhập tổng cộng " . $params['total'] . " xuất sứ thành công !!!";
        return response()->json($data);
    }

}
