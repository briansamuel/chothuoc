<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Helpers\ArrayHelper;
use App\Helpers\Message;
use App\Http\Controllers\Controller;
use App\Services\BrandService;
use App\Services\CategoryService;
use App\Services\CountryService;
use App\Services\IngredientMapMedicineService;
use App\Services\IngredientService;
use App\Services\LogsUserService;
use App\Services\PackageService;
use App\Services\ValidationService;
use App\Services\MedicineService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class MedicineController extends Controller
{
    protected $request;
    protected $medicineService;
    protected $categoryService;
    protected $countryService;
    protected $brandService;
    protected $packageService;
    protected $ingredientService;
    protected $validator;

    function __construct(Request $request, ValidationService $validator, MedicineService $medicineService, CategoryService $categoryService, CountryService $countryService, PackageService $packageService, BrandService $brandService, IngredientService $ingredientService)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->medicineService = $medicineService;
        $this->categoryService = $categoryService;
        $this->countryService = $countryService;
        $this->brandService = $brandService;
        $this->packageService = $packageService;
        $this->ingredientService = $ingredientService;
    }


    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function index()
    {
        return view('admin.medicine.index');
    }


    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function ajaxGetList()
    {
        $params = $this->request->all();

        $result = $this->medicineService->getList($params);

        return response()->json($result);
    }

    /**
     * METHOD viewInsert - VIEW ADD, EDIT NEWS
     *
     * @return void
     */

    public function add()
    {
        $categories = $this->categoryService->getAllByRoot('category_of_product');
        $countries = $this->countryService->getAll();
        $brands = $this->brandService->getAll();
        $packages = $this->packageService->getAll();
        $ingredient = $this->ingredientService->getAll();

        return view('admin.medicine.add', compact('categories', 'countries', 'brands', 'packages', 'ingredient'));
    }

    public function addAction()
    {
        $params = $this->request->only('name', 'category_id', 'country_id', 'brand_id', 'package_id', 'total', 'instruction', 'description', 'image', 'is_active', 'sort_order', 'price', 'content', 'keyword', 'specifications');
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'add_medicine_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all()), 400);
        }
        $params['slug'] = Str::slug($params['name']);
        $params['specifications'] = json_encode($params['specifications']);
        $ingredients = $this->request->input('ingredient', []);

        $add = MedicineService::add($params);
        if ($add) {
            $insert_ingredient = [];
            foreach ($ingredients as $ingredient) {
                $insert_ingredient[] = [
                    'ingredient_id' => $ingredient,
                    'medicine_id' => $add,
                    'timestamp' => time()
                ];
            }
            IngredientMapMedicineService::add($insert_ingredient);
            //add log
            $log['action'] = "Thêm mới 1 thuốc có id = " . $add;
            $log['content'] = json_encode($params);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);

            $data['success'] = true;
            $data['message'] = "Thêm mới thuốc thành công !!!";
        } else {
            $data['message'] = "Lỗi khi thêm mới thuốc !";
        }

        return response()->json($data);
    }

    public function deleteMany()
    {
        $params = $this->request->only('ids', 'total');
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $delete = $this->medicineService->deleteMany($params['ids']);
        if (!$delete) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã xóa tổng cộng " . $params['total'] . " thuốc thành công !!!", 'success');

        //add log
        $log['action'] = "Xóa " . $params['total'] . " thuốc thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã xóa tổng cộng " . $params['total'] . " thuốc thành công !!!";
        return response()->json($data);
    }

    public function delete($id)
    {
        $detail = $this->medicineService->detail($id);
        $delete = $this->medicineService->delete($id);
        if($delete) {
            IngredientMapMedicineService::deleteByMedicineId($id);
            //add log
            $log['action'] = "Xóa thuốc thành công có ID = " . $id;
            $log['content'] = json_encode($detail);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);

            Message::alertFlash('Bạn đã xóa thuốc thành công', 'success');
        } else {
            Message::alertFlash('Bạn đã xóa thuốc không thành công', 'danger');
        }

        return redirect()->route("brand.list");
    }

    public function edit($id)
    {
        $detail = $this->medicineService->detail($id);
        $specifications = json_decode($detail->specifications);
        $categories = $this->categoryService->getAllByRoot('category_of_product');
        $countries = $this->countryService->getAll();
        $brands = $this->brandService->getAll();
        $packages = $this->packageService->getAll();
        $ingredients = $this->ingredientService->getAll();
        $ingredientMedicine = IngredientMapMedicineService::getByMedicineId($id);

        return view('admin.medicine.edit', compact('detail', 'categories', 'countries', 'brands', 'packages', 'ingredients', 'ingredientMedicine', 'specifications'));
    }

    public function editAction($id)
    {
        $params = $this->request->only(['name', 'category_id', 'country_id', 'brand_id', 'package_id', 'total', 'instruction', 'description', 'image', 'is_active', 'sort_order', 'price', 'content', 'keyword', 'specifications']);
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'edit_medicine_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all(), 400));
        }
        $params['slug'] = Str::slug($params['name']);
        $params['specifications'] = json_encode($params['specifications']);
        $inputIngredient = $this->request->input('ingredient', []);

        DB::beginTransaction();
        $ingredientMedicine = IngredientMapMedicineService::getByMedicineId($id);
        if ($ingredientMedicine !== $inputIngredient) {
            $deleteOld = IngredientMapMedicineService::deleteByMedicineId($id);
            if (!$deleteOld) {
                DB::rollBack();
            }
            foreach ($inputIngredient as $item) {
                $insertPermission = ['medicine_id' => $id, 'ingredient_id' => $item, 'timestamp' => time()];
                $add_permission = IngredientMapMedicineService::add($insertPermission);
                if (!$add_permission) {
                    DB::rollback();
                }
            }
        }

        $edit = $this->medicineService->edit($id, $params);
        if (!$edit) {
            DB::rollBack();
            return response()->json(Message::get(13, $lang = '', []), 400);
        }
        DB::commit();

        //add log
        $log['action'] = "Cập nhập thuốc thành công có ID = " . $id;
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Cập nhập thuốc thành công !!!";
        return response()->json($data);
    }

}
