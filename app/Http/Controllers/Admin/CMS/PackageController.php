<?php

namespace App\Http\Controllers\Admin\CMS;

use App\Helpers\ArrayHelper;
use App\Helpers\Message;
use App\Http\Controllers\Controller;
use App\Services\LogsUserService;
use App\Services\ValidationService;
use App\Services\PackageService;
use Illuminate\Http\Request;

class PackageController extends Controller
{
    protected $request;
    protected $packageService;
    protected $validator;

    function __construct(Request $request, ValidationService $validator, PackageService $packageService)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->packageService = $packageService;
    }


    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function index()
    {
        return view('admin.package.index');
    }


    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function ajaxGetList()
    {
        $params = $this->request->all();

        $result = $this->packageService->getList($params);

        return response()->json($result);
    }

    /**
     * METHOD viewInsert - VIEW ADD, EDIT NEWS
     *
     * @return void
     */

    public function add()
    {

        return view('admin.package.add');
    }

    public function addAction()
    {
        $params = $this->request->only('name');
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'add_package_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all()), 400);
        }

        $add = PackageService::add($params);
        if ($add) {
            //add log
            $log['action'] = "Thêm mới 1 cách đóng gói có id = " . $add;
            $log['content'] = json_encode($params);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);

            $data['success'] = true;
            $data['message'] = "Thêm mới cách đóng gói thành công !!!";
        } else {
            $data['message'] = "Lỗi khi thêm mới cách đóng gói !";
        }

        return response()->json($data);
    }

    public function deleteMany()
    {
        $params = $this->request->only('ids', 'total');
        if (!isset($params['ids'])) {
            return response()->json(Message::get(26, $lang = '', []), 400);
        }
        $delete = $this->packageService->deleteMany($params['ids']);
        if (!$delete) {
            return response()->json(Message::get(12, $lang = '', []), 400);
        }

        Message::alertFlash("Bạn đã xóa tổng cộng " . $params['total'] . " cách đóng gói thành công !!!", 'success');

        //add log
        $log['action'] = "Xóa " . $params['total'] . " cách đóng gói thành công";
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Bạn đã xóa tổng cộng " . $params['total'] . " cách đóng gói thành công !!!";
        return response()->json($data);
    }

    public function delete($id)
    {
        $detail = $this->packageService->detail($id);
        $delete = $this->packageService->delete($id);
        if($delete) {
            //add log
            $log['action'] = "Xóa cách đóng gói thành công có ID = " . $id;
            $log['content'] = json_encode($detail);
            $log['ip'] = $this->request->ip();
            LogsUserService::add($log);

            Message::alertFlash('Bạn đã xóa cách đóng gói thành công', 'success');
        } else {
            Message::alertFlash('Bạn đã xóa cách đóng gói không thành công', 'danger');
        }

        return redirect()->route("package.list");
    }

    public function edit($id)
    {
        $groupInfo = $this->packageService->detail($id);

        return view('admin.package.edit', ['groupInfo' => $groupInfo]);
    }

    public function editAction($id)
    {
        $params = $this->request->only(['name']);
        $params = ArrayHelper::removeArrayNull($params);
        $validator = $this->validator->make($params, 'edit_package_fields');
        if ($validator->fails()) {
            return response()->json(Message::get(1, $lang = '', $validator->errors()->all(), 400));
        }

        $edit = $this->packageService->edit($id, $params);
        if (!$edit) {
            return response()->json(Message::get(13, $lang = '', []), 400);
        }

        //add log
        $log['action'] = "Cập nhập cách đóng gói thành công có ID = " . $id;
        $log['content'] = json_encode($params);
        $log['ip'] = $this->request->ip();
        LogsUserService::add($log);

        $data['success'] = true;
        $data['message'] = "Cập nhập cách đóng gói thành công !!!";
        return response()->json($data);
    }

}
