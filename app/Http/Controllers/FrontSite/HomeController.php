<?php
namespace App\Http\Controllers\FrontSite;

use App\Services\CategoryService;
use Illuminate\Http\Request;
use Harimayco\Menu\Facades\Menu;
use App\Services\Auth\AuthService;
use App\Services\MedicineService;
use App\Services\IngredientService;
use App\Services\PostService;
use Illuminate\Support\Facades\Auth;
use function GuzzleHttp\json_encode;
use App\Services\OrderService;
use App\Services\CommentService;
use App\Services\ReviewService;
use Exception;

class HomeController
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    /**
     * ======================
     * Method:: INDEX
     * ======================
     */

    public function index()
    {
       
    
        $medicineByCategory = $this->categoryService->getAllByRoot('category_of_product');
        foreach ($medicineByCategory as &$category) {
            $category->products = $this->categoryService->getProductByRootId($category->id);
        }
        return view('frontsite.home.index', compact('medicineByCategory'));
    }

    public function ajaxAction(Request $r) {

        $request = $r->only('action', 'email', 'password', 'remember', '_token', 'id');
        $action = $request['action'];
        switch($action) {
            case 'login':
                return view('frontsite.pages.login');
                break;
            case 'register':
                return view('frontsite.pages.register');
                break;   
             case 'loginquery':
                return $this->loginQuery($r);
                break;
            case 'getloginuserinfo':
                return $this->getloginuserinfo();
                break;
            case 'viewdonhangchitiet':
                return $this->ajaxOrderDetail($r);
                break;
            case 'comment':
                    return $this->ajaxComment($r);
                break;            
            default:
                 return view('frontsite.pages.register');
                break;     
        }
    }

    public function getloginuserinfo() {
        $userInfo = Auth::guard('user')->user();
        return $userInfo;
    }

    // Ajax send Comment in Product
    public function ajaxComment($request) {
        $input = $request->only('idtype', 'hoten', 'email', 'dienthoai', 'noidung');
        $params['medicine_id'] = isset($input['idtype']) ? $input['idtype'] : '';
        $params['name_guest'] = isset($input['hoten']) ? $input['hoten'] : '';
        $params['email_guest'] = isset($input['email']) ? $input['email'] : '';
        $params['phone_number_guest'] = isset($input['dienthoai']) ? $input['dienthoai'] : '';
        $params['comment_content'] = isset($input['noidung']) ? $input['noidung'] : '';
        $params['language'] = 'vi';
        $data['message'] = '';
        $data['status'] = 'error';
        $insert = CommentService::add($params);
        if($insert) {
            $data['message'] = 'Gửi bình luận thành công';
            $data['status'] = 'success';
        }

        return json_encode($data, TRUE);
    }

    public function ajaxOrderDetail($request) {
        if($request['id']) {
            $order = OrderService::findById($request['id']);
            if($order) {
                $products = json_decode($order->products);
                $total_price = json_decode($order->price);
                return view('frontsite.accounts.elements.ajax_orders', compact('products', 'total_price'));
            }
            return false;
        } else {
            return false;
        }
    }

    public function loginQuery($request) {

        $data = AuthService::loginUser($request);
        return json_encode($data);
    }

    public function ajaxSearch(Request $r) {
       
        $request = $r->only('value', 'idloai');
        $type = (int) $request['idloai'];
        $q = $request['value'];
        $data = [];
        switch($type) {
            case 1:
                $query['name'] = $q;
                $products = MedicineService::getMany(['page' => 1],[], $query);
                if(count($products) > 0) {
                    $data['message'] = '';
                    $data['status'] = 'success';
                    $data['result'] = html_entity_decode(view('frontsite.ajax.product-search', compact('products'))->render());
                } else {
                    $data['message'] = 'Không tìm thấy kết quả nào.';
                    $data['status'] = 'empty';
                    $data['result'] = null;
                }
                
               
                break;
            case 5:
                $query['post_title'] = $q;
                $query['post_type'] = 'disease';
                $posts = PostService::getMany(['*'], ['page' => 1],[], $query);
                if(count($posts) > 0) {
                    $data['message'] = '';
                    $data['status'] = 'success';
                    $data['result'] = html_entity_decode(view('frontsite.ajax.disease-search', compact('posts'))->render());
                } else {
                    $data['message'] = 'Không tìm thấy kết quả nào.';
                    $data['status'] = 'empty';
                    $data['result'] = null;
                }
                break;   
             case 0:
                $query['name'] = $q;
                $ingredients = IngredientService::getMany(['*'],['page' => 1],[], $query);
                if(count($ingredients) > 0) {
                    $data['message'] = '';
                    $data['status'] = 'success';
                    $data['result'] = html_entity_decode(view('frontsite.ajax.ingredient-search', compact('ingredients'))->render());
                } else {
                    $data['message'] = 'Không tìm thấy kết quả nào.';
                    $data['status'] = 'empty';
                    $data['result'] = null;
                }
                break;               
            default:
                $data['message'] = 'Không tìm thấy kết quả nào.';
                $data['status'] = 'empty';
                $data['result'] = null;
                break;     
        }

        return json_encode($data, TRUE);
    }

    public function ajaxRatingShow(Request $r) {
        $request = $r->only('id');
        $product = MedicineService::findByKey('id', $request['id']);
        $data['message'] = '';
        $data['status'] = 'error';
        if($product) {
            $reviews = ReviewService::getMany(['*'], ['page' => 1, 'perpage' => 100], null, ['medicine_id' => $request['id']]);
            $data['message'] = '';
            $data['status'] = 'success';
            $data['result'] = html_entity_decode(view('frontsite.medicines.elements.modalReview', compact('product', 'reviews')));
        }
        
        return json_encode($data, TRUE);
    }

    public function ajaxRatingExecute(Request $r) {
        $request = $r->only('id', 'value', 'nhanxet');
        $params['medicine_id'] = isset($request['id']) ? $request['id'] : 0;
        $params['rating_review'] = isset($request['value']) ? $request['value'] : 0;
        $params['review_content'] = isset($request['nhanxet']) ? $request['nhanxet'] : '';
       
        $data['message'] = '';
        $data['status'] = 'error';
        try {
            $params['name_guest'] =  Auth::guard('user')->user()->full_name;
            $params['created_by_guest'] =  Auth::guard('user')->user()->id;
            $reviews = ReviewService::insert($params);
            if($reviews) {
                $data['message'] = 'Đánh giá sản phẩm thành công';
                $data['status'] = 'success';
            }

            return json_encode($data, TRUE);
        } catch(Exception $e) {
            dd($e);
        }

        
    }
}
