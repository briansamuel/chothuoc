<?php

namespace App\Http\Controllers\Frontsite;

use App\Http\Controllers\Controller;
use App\Services\ValidationService;
use App\Services\IngredientService;


use App\Services\LogsUserService;
use Illuminate\Http\Request;
use App\Helpers\Message;
use App\Helpers\ArrayHelper;
class IngredientController extends Controller
{
    //
    function __construct(Request $request, ValidationService $validator, IngredientService $ingredientService)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->ingredientService = $ingredientService;
        
    }
    
    
    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function index()
    {   
        // Test
        return view('frontsite.ingredients.index');
    }

    public function detail($slug) {
        

        $ingredient = $this->ingredientService->findByKey('slug', $slug);
        if(!$ingredient) {
            redirect('404');
        }

        return view('frontsite.ingredients.detail', ['ingredient' => $ingredient]);
    }

    public function character($character) {
        $ingredients = $this->ingredientService->searchByCharacter($character);

        return view('frontsite.ingredients.character', ['ingredients' => $ingredients, 'character' => $character]);
    }
    
}
