<?php
namespace App\Http\Controllers\FrontSite;

use App\Services\CountryService;
use App\Services\MedicineService;
use App\Services\MedicineGroupService;
use App\Services\IngredientService;
use App\Services\BrandService;
use App\Services\PackageService;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use function GuzzleHttp\json_decode;

class ProductsController
{
    protected $request;
    protected $medicineService;
    protected $medicineGroupService;
    protected $countryService;
    protected $ingredientService;
    protected $brandService;
    protected $packageService;

    public function __construct(Request $request, MedicineService $medicineService, MedicineGroupService $medicineGroupService, IngredientService $ingredientService, CountryService $countryService, BrandService $brandService, PackageService $packageService, CategoryService $categoryService)
    {
        $this->request = $request;
        $this->medicineService = $medicineService;
        $this->medicineGroupService = $medicineGroupService;
        $this->ingredientService = $ingredientService;
        $this->countryService = $countryService;
        $this->brandService = $brandService;
        $this->packageService = $packageService;
        $this->categoryService = $categoryService;
    }

    /**
     * ======================
     * Method:: INDEX
     * ======================
     */

    public function index()
    {
        $params = $this->request->all();
        $medicineGroups = $this->medicineGroupService->getAll();
        foreach ($medicineGroups as &$group) {
            $group->ingredients = $this->ingredientService->getByGroupId($group->id);
        }
        $medicines = $this->medicineService->getListFrontEnd($params);
        $countries = $this->countryService->getAll();
        $brands = $this->brandService->getAll();
        $packages = $this->packageService->getAll();
        if($this->request->ajax() || isset($params['idnhomid']) ) {
            
            return view('frontsite.medicines.elements.filterItem', compact('medicines'));
        }
        return view('frontsite.medicines.index', compact('medicineGroups', 'medicines', 'countries','brands', 'packages'));
    }

    public function filterProduct() {
        $input = $this->request->all();
        $params = [];
        if(isset($input['idnhomid'])) {
            $params[$input['idnhomid']] = (int) $input['idnhomnd'];
        }
        $medicines = $this->medicineService->getListFrontEnd($params);
        return view('frontsite.medicines.elements.filterItem', compact('medicines'));
    }

    public function resetFilterProduct() {
        $input = $this->request->all();
        if(isset($input['action'])) {
            $action = $input['action'];
            if($action == 'delallfilter') {

            }
        }
        return back();
    }

    public function detail($slug) {

        $medicine = $this->medicineService->findByKey('slug', $slug);
        
        if($medicine) {
            $breadcrumbs = array(
                array(
                    'title' => 'Danh mục',
                    'slug' => 'san-pham',

                )
            );
            $category = $this->categoryService->findByKey('id', $medicine->category_id, ['*']);
            $medicine->specifications = json_decode($medicine->specifications);
            $related_products = [];
            if($category) {
                $breadcrumb = array(
                    'title' => $category->category_name,
                    'slug' => 'san-pham/'.$category->category_slug,
                );
                array_push($breadcrumbs, $breadcrumb);
                $related_products = $this->medicineService->getProductByCateIds(array($medicine->category_id), 10);
                
            }
            if(Auth::guard('user')) {
                $this->viewProduct($medicine);
            }
            return view('frontsite.medicines.detail', compact('medicine', 'breadcrumbs', 'related_products'));
        }
    }

    public function category($slug) {

        $params = [];
        $category =  $this->categoryService->findByKey('category_slug', $slug);
        if($category) {
            $params['category_id'] = $category->id;
        }
       
        $medicineGroups = $this->medicineGroupService->getAll();
        foreach ($medicineGroups as &$group) {
            $group->ingredients = $this->ingredientService->getByGroupId($group->id);
        }
        $medicines = $this->medicineService->getListFrontEnd($params);
        $countries = $this->countryService->getAll();
        $brands = $this->brandService->getAll();
        $packages = $this->packageService->getAll();
        if($this->request->ajax() || isset($params['idnhomid']) ) {
            
            return view('frontsite.medicines.elements.filterItem', compact('medicines'));
        }
        return view('frontsite.medicines.index', compact('medicineGroups', 'medicines', 'countries','brands', 'packages'));
    }
    public function viewProduct($product) {
        $viewed_products = session()->get('viewed_products') ? session()->get('viewed_products') : [];

        foreach($viewed_products as $key => $viewed_product) {
            if($product->id == $viewed_product->id) {
                return false;
            }
        }
        array_push($viewed_products, $product);
        session()->put('viewed_products', $viewed_products);
    }
}
