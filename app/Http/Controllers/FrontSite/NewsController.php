<?php

namespace App\Http\Controllers\Frontsite;

use App\Http\Controllers\Controller;
use App\Services\ValidationService;
use App\Services\PostService;
use App\Services\CategoryService;
use App\Services\CategoryPostService;

use App\Services\LogsUserService;
use Illuminate\Http\Request;
use App\Helpers\Message;
use App\Helpers\ArrayHelper;
class NewsController extends Controller
{
    //
    function __construct(Request $request, ValidationService $validator, PostService $postService, CategoryPostService $categoryPostService)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->postService = $postService;
        
        $this->categoryPostService = $categoryPostService;
    }
    
    
    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function index()
    {   
        $html_posts = '';
        $posts_1 = $this->postService->takeNewByCategory(5, array('post_type' => 'news', 'post_status' => 'publish'), 49);
        $row_home_1 = view('frontsite.news.elements.row_home', ['posts' => $posts_1, 'title' => 'Làm đẹp', 'slug' => 'lam-dep']);
        $posts_2 = $this->postService->takeNewByCategory(5, array('post_type' => 'news', 'post_status' => 'publish'), 50);
        $row_home_2 = view('frontsite.news.elements.row_home', ['posts' => $posts_2, 'title' => 'Giảm cân', 'slug' => 'giam-can']);
        $posts_3 = $this->postService->takeNewByCategory(5, array('post_type' => 'news', 'post_status' => 'publish'), 51);
        $row_home_3 = view('frontsite.news.elements.row_home', ['posts' => $posts_3, 'title' => 'Xương khớp', 'slug' => 'xuong-khop']);
        $posts_4 = $this->postService->takeNewByCategory(5, array('post_type' => 'news', 'post_status' => 'publish'), 52);
        $row_home_4 = view('frontsite.news.elements.row_home', ['posts' => $posts_4, 'title' => 'Sinh lý', 'slug' => 'sinh-ly']);
        $posts_5 = $this->postService->takeNewByCategory(5, array('post_type' => 'news', 'post_status' => 'publish'),53);
        $row_home_5 = view('frontsite.news.elements.row_home', ['posts' => $posts_5, 'title' => 'Sức khỏe tổng hợp', 'slug' => 'suc-khoe-tong-hop']);
        $posts_6 = $this->postService->takeNewByCategory(5, array('post_type' => 'news', 'post_status' => 'publish'),54);
        $row_home_6 = view('frontsite.news.elements.row_home', ['posts' => $posts_6, 'title' => 'Chương trình khuyến mãi', 'slug' => 'chuong-trinh-khuyen-mai']);
        $posts_7 = $this->postService->takeNewByCategory(5, array('post_type' => 'news', 'post_status' => 'publish'),55);
        $row_home_7 = view('frontsite.news.elements.row_home', ['posts' => $posts_7, 'title' => 'Thị trường thuốc', 'slug' => 'thi-truong-thuoc']);
        $html_posts = $row_home_1.$row_home_2.$row_home_3.$row_home_4.$row_home_5.$row_home_6.$row_home_7;
        $breadcrumbs = array(
            array(
                'slug' => 'tin-tuc',
                'title' => 'Tin tức',
            )
        );
        
        return view('frontsite.news.index', ['html_posts' => $html_posts, 'breadcrumbs' => $breadcrumbs]);
    }

    public function detail($post_slug) {
        
        $params = array(
            'post_slug' => $post_slug,
            'post_type' => 'news',
            'post_status' => 'publish',
        );

        $post = $this->postService->findByMultiKey($params);
        if(!$post) {
            redirect('404');
        }
        $categoryPost = $this->categoryPostService->findByKey('post_id', $post->id);
        $breadcrumb_category = null;
        $posts_related = null;
        if($categoryPost) {
            $category = CategoryService::findByKey('id', $categoryPost->category_id);
            $posts_related_ids = $this->categoryPostService->getManyByKey('post_id', 
            array('category_id' => $categoryPost->category_id),
            array('perpage' => 10, 'page' => 1));
            $posts_related = $posts_related_ids->map(function ($item){
                return $this->postService->findByKey('id', $item->post_id);
            });
            
            if($category) {
                $breadcrumb_category = array(
                    'slug' => $category->category_slug,
                    'title' => $category->category_name,
                );
            }
        }
        $breadcrumbs = array(
            array('slug' => 'tin-tuc',
            'title' => 'Tin tức',
            )
        );
        if($breadcrumb_category) {
            array_push($breadcrumbs, $breadcrumb_category);
        }
        return view('frontsite.news.detail', ['post' => $post, 'breadcrumbs' => $breadcrumbs, 'posts_related' => $posts_related]);
    }

    
}
