<?php

namespace App\Http\Controllers\Frontsite;

use App\Http\Controllers\Controller;
use App\Services\ValidationService;
use App\Services\PostService;
use App\Services\CategoryService;
use App\Services\CategoryPostService;

use App\Services\LogsUserService;
use Illuminate\Http\Request;
use App\Helpers\Message;
use App\Helpers\ArrayHelper;
class DiseaseController extends Controller
{
    //
    function __construct(Request $request, ValidationService $validator, PostService $postService, CategoryPostService $categoryPostService)
    {
        $this->request = $request;
        $this->validator = $validator;
        $this->postService = $postService;
        
        $this->categoryPostService = $categoryPostService;
    }
    
    
    /**
     * METHOD index - View List News
     *
     * @return void
     */

    public function index()
    {   

  
        $posts = $this->postService->takeNew(200, array('post_type' => 'disease', 'post_status' => 'publish'));
        
        
        $breadcrumbs = array(
            array(
                'slug' => 'tim-hieu-benh',
                'title' => 'Tìm hiểu bệnh',
            )
        );
        $categories = CategoryService::getAllByRoot('category_of_disease');
        return view('frontsite.diseases.index', ['posts' => $posts, 'breadcrumbs' => $breadcrumbs, 'categories' => $categories]);
    }

    public function detail($post_slug) {
        
        $params = array(
            'post_slug' => $post_slug,
            'post_type' => 'disease',
            'post_status' => 'publish',
        );
        $breadcrumbs = array(
            array(
                'slug' => 'tim-hieu-benh',
                'title' => 'Tìm hiểu bệnh',
            )
        );
        $post = $this->postService->findByMultiKey($params);
        if(!empty($post)) {
            $categoryPost = $this->categoryPostService->findByKey('post_id', $post->id);
            $breadcrumb_category = null;
            $posts_related = null;
            if($categoryPost) {
                $category = CategoryService::findByKey('id', $categoryPost->category_id);
                $posts_related_ids = $this->categoryPostService->getManyByKey('post_id', 
                array('category_id' => $categoryPost->category_id),
                array('perpage' => 10, 'page' => 1));
                
                
                if($category) {
                    $breadcrumb_category = array(
                        'slug' => $category->category_slug,
                        'title' => $category->category_name,
                    );
                }
            }
            
        
            if($breadcrumb_category) {
                array_push($breadcrumbs, $breadcrumb_category);
            }
            
            return view('frontsite.diseases.detail', ['post' => $post, 'breadcrumbs' => $breadcrumbs]);
        } else {
            $category = CategoryService::findByKey('category_slug', $post_slug);
            if(!empty($category)) {
                $breadcrumb_category = array(
                    'slug' => $category->category_slug,
                    'title' => $category->category_name,
                );
                if($breadcrumb_category) {
                    array_push($breadcrumbs, $breadcrumb_category);
                }
                $posts = $this->postService->takeNewByCategory(30, array('post_type' => 'news', 'post_status' => 'publish'), $category->id);
                $categories = CategoryService::getAllByRoot('category_of_disease');
                return view('frontsite.diseases.category', ['posts' => $posts, 'breadcrumbs' => $breadcrumbs, 'category' => $category, 'categories' => $categories]);

            } else {

                return redirect()->route('404');

            }
        }
        
    }

    
}
