<?php
namespace App\Http\Controllers\FrontSite;

use App\Services\CategoryService;
use Illuminate\Http\Request;
use Harimayco\Menu\Facades\Menu;
use App\Services\Auth\AuthService;
use App\Services\MedicineService;
use App\Services\IngredientService;
use App\Services\PostService;
use Illuminate\Support\Facades\Auth;
use function GuzzleHttp\json_encode;
use App\Services\UserService;
use App\Services\ValidationService;
use App\Services\OrderService;
class AccountController
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService,  ValidationService $validator)
    {
        $this->categoryService = $categoryService;
        $this->validator = $validator;
    }

    /**
     * ======================
     * Method:: INDEX
     * ======================
     */

    public function index()
    {
       
      $user = Auth::guard('user')->user();  
      return view('frontsite.accounts.index', compact('user'));
    }

    // Update Account POST
    public function updateAccount(Request $request) {
        $params = $request->only('full_name', 'phone', 'gender', 'address', 'pharmacy_name', 'pharmacy_mst', 'old_password', 'password' , 'confirm_password', 'doimatkhau');
        $user = Auth::guard('user')->user();
        $validator = $this->validator->make($params, 'update_my_profile_fields');
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors(['error' => $validator->errors()->all()]);
        }
        if(isset($params['doimatkhau']) && $params['doimatkhau'] == 1) {
            if($user->password == $params['old_password']) {
                if($params['old_password'] == $params['comfirm_password']) {
                    $user_update = UserService::updateUserFrontsite($params);
                    if($user_update) {
                        return redirect()->route('infoAccount.index');
                    }
                } else {
                    $validator->getMessageBag()->add('comfirm_password', 'Xác nhận password không giống nhau');
                }
    
            } else {
                $validator->getMessageBag()->add('old_password', 'Bạn đã nhập sai mật khẩu hiện tại');
            }
        } else {
            unset($params['password']);
            $user_update = UserService::updateUserFrontsite($params);
            if($user_update) {
                return redirect()->route('infoAccount.index');
            }
        }
        

        return redirect()->back()->withInput()->withErrors(['error' => $validator->errors()->all()]);
    }

    public function viewed()
    {
       
        $medicines = session()->get('viewed_products') ? session()->get('viewed_products') : [];
        
        $user = Auth::guard('user')->user();  
        return view('frontsite.accounts.product_viewed', compact('user', 'medicines'));
    }

    public function liked()
    {
       
    
      return view('frontsite.accounts.index');
    }

    public function willbuy()
    {
       
    
      return view('frontsite.accounts.index');
    }

    public function order()
    {
       
        $user = Auth::guard('user')->user();
        $query['user_id'] =  $user->id;
        $orders = OrderService::getMany(['*'], array('page' => 1), null, $query);
        return view('frontsite.accounts.orders', compact('user','orders'));
    }

    public function logout()
    {
        dd('TEST');
       
        // Auth::guard('user')->logout();
        
        // return redirect()->route('home');
    }


    
}
