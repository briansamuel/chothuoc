<?php
namespace App\Http\Controllers\FrontSite;

use App\Services\CategoryService;
use Illuminate\Http\Request;
use Harimayco\Menu\Facades\Menu;
use App\Services\Auth\AuthService;
use App\Services\DistrictService;
use App\Services\MedicineService;
use function GuzzleHttp\json_encode;
use App\Services\ProvinceService;
use App\Services\DistristService;
use App\Services\OrderService;
use App\Services\WardService;
use App\Services\ValidationService;

class CartController
{
    protected $categoryService;

    public function __construct(MedicineService $medicineService)
    {
        $this->medicineService = $medicineService;
        session()->start();
        session()->put('cart', []);
    }

    /**
     * ======================
     * Method:: INDEX
     * ======================
     */

    public function index()
    {
        $cart = $this->productsCart();
        $provines = ProvinceService::getAll(['*']);
        $cart['provines'] = $provines;
        return view('frontsite.cart.index', $cart);
    }

    public function ajaxAdress(Request $r) {
        $request = $r->only('name','id');
        if(isset($request['name'])) {
            if($request['name'] == 'quanhuyen') {
                $districts = DistrictService::getMany(['*'], ['_province_id' => $request['id']]);
                return view('frontsite.cart.elements.district', compact('districts'));
            } else  if($request['name'] == 'phuongxa') {
                $wards = WardService::getMany(['*'], ['_district_id' => $request['id']]);
                return view('frontsite.cart.elements.ward', compact('wards'));
            }
            
        }
    }
    public function checkout(Request $r) {
        $params = $r->only('fullname','phone', 'email', 'address', 'tinhthanh', 'quanhuyen', 'phuongxa', 'note');
        if(isset($params['tinhthanh'])) {
            $province = ProvinceService::findByKey('id', $params['tinhthanh']);
            if($province) {
                $params['city'] = $province->_name;
                unset($params['tinhthanh']);
            }
        }

        if(isset($params['quanhuyen'])) {
            $district = DistrictService::findByKey('id', $params['quanhuyen']);
            if($district) {
                $params['district'] = $district->_name;
                unset($params['quanhuyen']);
            }
        }

        if(isset($params['phuongxa'])) {
            $ward = WardService::findByKey('id', $params['phuongxa']);
            if($ward) {
                $params['ward'] = $ward->_name;
                unset($params['phuongxa']);
            }
        }

        $cart = $this->productsCart();
        $params['products'] = json_encode($cart['products']);
        $params['price'] = $cart['total_price'];
        $params['ip'] = $r->ip();
        $order = OrderService::insert($params);
        if($order) {
            return redirect()->route('checkoutSuccess');
        } else {
            return back();
        }

    }

    public function checkoutSuccess() {

        session()->put('cart', []);
        return view('frontsite.cart.checkout');
    }

    public function addToCart(Request $r) {
        
        
        $request = $r->only('masp','id', 'masp', 'method', 'sl');
        if(isset($request['masp'])) {
            $id_product = $request['masp'];
            $quantity = isset($request['sl']) ? $request['sl'] : 1;
            $method = $request['method'];
            switch($method) {
                case 'add':
                    $this->addItem($id_product, $quantity);
                    $cart = $this->productsCart();
                    return view('frontsite.elements.popup_cart', $cart);
                    break;
                case 'delete':
                    $this->deleteItem($id_product);
                    break;
                case 'update':
                    $this->updateQuality($id_product, $quantity);
                    $cart = $this->productsCart();
                    return view('frontsite.cart.elements.update_cart', $cart);
                    break;
                default :
                    $this->addItem($id_product, $quantity);
                    break;
            }
            
           
        }

        
        
       
    }
    public function productsCart() {
        $cart = session()->get('cart');
        $data = [];
        $products = [];
        $total_price = 0;
        foreach($cart as $key => $item) {
            $product = $this->medicineService->findByKey('id', $item['id']);
            if($product) {
                $product->quantity = (int) $item['quantity'];
                $product->total_price = $product->quantity * $product->price;
                array_push($products, $product);
                $total_price += $product->total_price;
            }
        }

        $data['products'] = $products;
        $data['total_price'] = $total_price;
        return $data;
    }
    public function addItem($id_product, $quantity) {
        $cart = session()->get('cart');

        $existCart = false;
        foreach($cart as $key => $item) {
            if($item['id'] == $id_product) {
                $quantity = (int) $item['quantity'] + 1;
                $this->updateQuality($id_product, $quantity);
                $existCart = true;
            }
        }
        if(!$existCart) {
            $item = array(
                'id' => $id_product,
                'quantity' => $quantity,
            );
            session()->push('cart', $item);
        }
        
    }
    
    public function deleteItem($id_product) {
        $cart = session()->get('cart');
        foreach($cart as $key => $item) {
            if($item['id'] == $id_product) {
                unset($cart[$key]);
            }
        }
        session()->put('cart', $cart);

    }

    public function updateQuality($id, $quantity) {
        $cart = session()->get('cart');
        foreach($cart as $key => &$item) {
            if($item['id'] == $id) {
                $item['quantity'] = $quantity;
            }
        }
        session()->put('cart', $cart);

    }
   
}
