<?php
namespace App\Services;

use App\Models\CategoryModel;
use App\Transformers\AgencyTransformer;
use Illuminate\Support\Str;
class CategoryService
{
    public static function getAll()
    {
        return CategoryModel::getAll();
    }

    public static function getAllByRoot($type)
    {
        $roots = CategoryModel::getAllRoot($type);
        foreach ($roots as $root) {
            $root->item = CategoryModel::getItemByRootId($root->id, 7);
        }

        return $roots;
    }

    public function getProductByRootId($cate_id, $limit = 20)
    {
        $ids = $this->getItemIdByRootId($cate_id);
		array_push($ids,$cate_id);
        return MedicineService::getProductByCateIds($ids, $limit);
    }

    public function getItemIdByRootId($root_id)
    {
        $ids = [];
        $result = CategoryModel::getItemIdByRootId($root_id);
        foreach ($result as $id) {
            $ids[] = $id;
        }

        return $ids;
    }

    public static function totalRows($filter)
    {
        return CategoryModel::totalRows($filter);
	}

	public static function getAllByKey($columns = ['*'], $filter = [])
	{
		$result = CategoryModel::getAllByKey($columns, $filter);
        return $result ? $result : [];
	}



	public static function getMany($pagination, $sort, $filter)
	{
		$result = CategoryModel::getMany($pagination, $sort, $filter);
        return $result ? $result : [];
    }

	public static function findByKey($key, $value)
	{
        $result = CategoryModel::findByKey($key, $value);
        return $result ? $result : [];
	}

	
	public function insert($params)
	{
		$insert['category_name'] = $params['category_name'];
		$insert['category_slug'] = isset($params['category_slug']) ? $params['category_slug'] : Str::slug($params['category_name']);
		$insert['category_description'] = $params['category_description'];
		$insert['category_seo_title'] = isset($params['category_seo_title']) ? $params['category_seo_title'] : '';
		$insert['category_seo_description'] = isset($params['category_seo_description']) ? $params['category_seo_description'] : '';
		$insert['category_seo_keyword'] = isset($params['category_seo_keyword']) ? $params['category_seo_keyword'] : '';
		$insert['category_thumbnail'] = isset($params['category_thumbnail']) ? $params['category_thumbnail'] : '';
		$insert['category_icon'] = isset($params['category_icon']) ? $params['category_icon'] : '';
		$insert['category_parent'] = isset($params['category_parent']) ? $params['category_parent'] : 0;
		$insert['category_type'] = isset($params['category_type']) ? $params['category_type'] : 'category_of_news';
		$insert['created_at'] = isset($params['created_at']) ? $params['created_at'] : date("Y-m-d H:i:s");
		$insert['updated_at'] = date("Y-m-d H:i:s");
		return CategoryModel::insert($insert);
	}
	public function update($id, $params)
	{
		$update['category_name'] = $params['category_name'];
		$update['category_slug'] = isset($params['category_slug']) ? $params['category_slug'] : Str::slug($params['category_title']);
		$update['category_description'] = $params['category_description'];
		$update['category_seo_title'] = isset($params['category_seo_title']) ? $params['category_seo_title'] : '';
		$update['category_seo_description'] = isset($params['category_seo_description']) ? $params['category_seo_description'] : '';
		$update['category_seo_keyword'] = isset($params['category_seo_keyword']) ? $params['category_seo_keyword'] : '';
		$update['category_thumbnail'] = isset($params['category_thumbnail']) ? $params['category_thumbnail'] : '';
		$update['category_icon'] = isset($params['category_icon']) ? $params['category_icon'] : '';
		$update['category_parent'] = isset($params['category_parent']) ? $params['category_parent'] : 0;
		$update['updated_at'] = date("Y-m-d H:i:s");
		return CategoryModel::update($id, $update);
	}

	public function deleteMany($ids)
    {
        return CategoryModel::deleteManyPost($ids);
	}

	public function delete($id)
	{
		return CategoryModel::delete($id);
	}

	public function getList(array $params)
    {
        $pagination = $params['pagination'];
        $sort = isset($params['sort']) ? $params['sort'] : [];
        $query = isset($params['query']) ? $params['query'] : [];
		$total = self::totalRows($query);
        $result = CategoryModel::getMany($pagination, $sort, $query);
		foreach($result as $index => $item) {
			$total_post = CategoryPostService::countByKey('post_id', $item->id);
			$item->post_count = $total_post;
		}
		$data['data'] = $result;

        $data['meta']['page'] = isset($pagination['page']) ? $pagination['page'] : 1;
        $data['meta']['perpage'] = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $data['meta']['total'] = $total;
        $data['meta']['pages'] = ceil($total / $data['meta']['perpage']);
		$data['meta']['rowIds'] = self::getListIDs($result);
        return $data;
	}

	public function getListIDs($data) {

		$ids = array();

		foreach($data as $row) {
			array_push($ids, $row->id);
		}

		return $ids;
	}
}
