<?php

namespace App\Services;

use App\Models\OrderModel;
use App\Transformers\ContactTransformer;

class OrderService
{

    public static function getAll($columns, $filter)
    {
        $result = OrderModel::getAll($columns, $filter);
        return $result;
    }

    public static function getMany($colum, $pagination, $sort, $query)
    {
        $result = OrderModel::getMany($colum, $pagination, $sort, $query);
        return $result;
    }

    public static function totalRows()
    {
        $result = OrderModel::totalRows();
        return $result;
    }

    public static function insert($params)
    {

        
        $insert['fullname'] = $params['fullname'];
        $insert['phone'] = isset($params['phone']) ? $params['phone'] : '';
        $insert['email'] = isset($params['email']) ? $params['email'] : '';
        $insert['address'] = isset($params['address']) ? $params['address'] : '';
        $insert['city'] = isset($params['city']) ? $params['city'] : '';
        $insert['district'] = isset($params['district']) ? $params['district'] : '';
        $insert['ward'] = isset($params['ward']) ? $params['ward'] : '';
        $insert['products'] = isset($params['products']) ? $params['products'] : json_encode([]);
        $insert['price'] = isset($params['price']) ? $params['price'] : '';
        $insert['note'] = isset($params['note']) ? $params['note'] : '';
        $insert['ip'] = isset($params['ip']) ? $params['ip'] : '';
        $insert['status'] = isset($params['status']) ? $params['status'] : 'pending';
        $insert['user_id'] = isset($params['user_id']) ? $params['user_id'] : 0;
        $insert['created_at'] = date("Y-m-d H:i:s");
        $insert['updated_at'] = date("Y-m-d H:i:s");
        return OrderModel::insert($insert);
    }

    public function update($id, $params)
    {

        $params['updated_at'] = date("Y-m-d H:i:s");
        return OrderModel::update($id, $params);
    }

    public function edit($id, $params)
    {

        $update['status'] = $params['status'];
        $update['updated_at'] = date("Y-m-d H:i:s");
        return OrderModel::update($id, $update);
    }


    public function deleteMany($ids)
    {
        return OrderModel::deleteMany($ids);
    }

    public function updateMany($ids, $data)
    {
        return OrderModel::updateMany($ids, $data);
    }

    public function delete($ids)
    {
        return OrderModel::delete($ids);
    }

    public static function findById($id) {
        $detail = OrderModel::findById($id);
        return $detail;
    }

    public function detail($id)
    {
        $detail = OrderModel::findById($id);
        $medicine = MedicineService::findByKey('id', $detail->medicine_id);
        if ($medicine) {
            $detail->medicine_name = $medicine->name;
        } else {
            $detail->medicine_name = 'Không tìm thấy thuốc';
        }
        return ContactTransformer::transformItem($detail);
    }

    public function getList(array $params)
    {
        $pagination = $params['pagination'];
        $sort = isset($params['sort']) ? $params['sort'] : [];
        $query = isset($params['query']) ? $params['query'] : [];
        $column = isset($params['column']) ? $params['column'] : ['*'];
        $total = self::totalRows($query);

        $result = OrderModel::getMany($column, $pagination, $sort, $query);
        
        $data['data'] = $result;
        $data['meta']['page'] = isset($pagination['page']) ? $pagination['page'] : 1;
        $data['meta']['perpage'] = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $data['meta']['total'] = $total;
        $data['meta']['pages'] = ceil($total / $data['meta']['perpage']);
        $data['meta']['rowIds'] = self::getListIDs($result);

        return $data;
    }

    public function getListIDs($data)
    {

        $ids = array();

        foreach ($data as $row) {
            array_push($ids, $row->id);
        }

        return $ids;
    }

    public static function takeNew($quantity, $filter)
    {
        return OrderModel::takeNew($quantity, $filter);
    }

}
