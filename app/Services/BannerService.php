<?php

namespace App\Services;

use App\Models\BannerModel;
use Illuminate\Support\Facades\App;

class BannerService
{

    public static function totalRows()
    {
        $result = BannerModel::totalRows();
        return $result;
    }

    public static function add($params)
    {
        $params['created_at'] = date("Y-m-d H:i:s");
        $params['updated_at'] = date("Y-m-d H:i:s");
        return BannerModel::add($params);
    }

    public function edit($id, $params)
    {
        $params['updated_at'] = date("Y-m-d H:i:s");
        return BannerModel::edit($id, $params);
    }

    public function delete($ids)
    {
        return BannerModel::delete($ids);
    }

    public function detail($id)
    {
        return BannerModel::findById($id);
    }

    public function getList(array $params)
    {
        $total = self::totalRows();
        $pagination = $params['pagination'];
        $sort = isset($params['sort']) ? $params['sort'] : [];
        $query = isset($params['query']) ? $params['query'] : [];

        $result = BannerModel::getMany($pagination, $sort, $query);

        $data['data'] = $result;
        $data['meta']['page'] = isset($pagination['page']) ? $pagination['page'] : 1;
        $data['meta']['perpage'] = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $data['meta']['total'] = $total;
        $data['meta']['pages'] = ceil($total / $data['meta']['perpage']);
        $data['meta']['rowIds'] = self::getListIDs($result);

        return $data;
    }

    public static function getForFrontEnd($type = 'home')
    {
        $lang = App::getLocale();
        $result = BannerModel::getForFrontEnd($type, $lang);
        if (!$result) {
            $result = BannerModel::getForFrontEnd($type, 'vi');
        }

        return $result;
    }

    public static function getForFrontEndByTypeValue($slug)
    {
        $lang = App::getLocale();
        $result = BannerModel::getForFrontEndBySlug($slug, $lang);
        if (!$result) {
            $result = BannerModel::getForFrontEndBySlug($slug, 'vi');
        }

        return $result;
    }

    public function getListIDs($data)
    {

        $ids = array();

        foreach ($data as $row) {
            array_push($ids, $row->id);
        }

        return $ids;
    }

}
