<?php

namespace App\Services;

use App\Models\BrandModel;

class BrandService
{
    public function getAll() {
        return BrandModel::getAll();
    }

    public static function totalRows() {
        return BrandModel::totalRows();
    }

    public static function add($params)
    {
        $params['created_at'] = date("Y-m-d H:i:s");
        $params['updated_at'] = date("Y-m-d H:i:s");
        return BrandModel::add($params);
    }

    public function edit($id, $params)
    {
        $params['updated_at'] = date("Y-m-d H:i:s");
        return BrandModel::update($id, $params);
    }

    public static function findByKey($key, $value)
	{
        $result = BrandModel::findByKey($key, $value);
        return $result ? $result : [];
    }
    
    public function deleteMany($ids)
    {
        return BrandModel::deleteMany($ids);
    }

    public function updateMany($ids, $data)
    {
        return BrandModel::updateMany($ids, $data);
    }

    public function delete($ids)
    {
        return BrandModel::delete($ids);
    }

    public function detail($id)
    {
        return BrandModel::findById($id);
    }

    public function getList(array $params)
    {
        $total = self::totalRows();
        $pagination = $params['pagination'];
        $sort = isset($params['sort']) ? $params['sort'] : [];
        $query = isset($params['query']) ? $params['query'] : [];

        $result = BrandModel::getMany($pagination, $sort, $query);

        $data['data'] = $result;
        $data['meta']['page'] = isset($pagination['page']) ? $pagination['page'] : 1;
        $data['meta']['perpage'] = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $data['meta']['total'] = $total;
        $data['meta']['pages'] = ceil($total / $data['meta']['perpage']);
        $data['meta']['rowIds'] = self::getListIDs($result);

        return $data;
    }

    public function getListIDs($data) {

        $ids = array();

        foreach($data as $row) {
            array_push($ids, $row->id);
        }

        return $ids;
    }

}
