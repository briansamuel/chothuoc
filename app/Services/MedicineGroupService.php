<?php

namespace App\Services;

use App\Models\MedicineGroupModel;

class MedicineGroupService
{

    public static function totalRows() {
        $result = MedicineGroupModel::totalRows();
        return $result;
    }

    public function findByKey($key, $value, $column = ['*'])
    {
        return MedicineGroupModel::findByKey($key, $value, $column);
    }


    public static function add($params)
    {
        $params['created_at'] = date("Y-m-d H:i:s");
        $params['updated_at'] = date("Y-m-d H:i:s");
        return MedicineGroupModel::add($params);
    }

    public function edit($id, $params)
    {
        $params['updated_at'] = date("Y-m-d H:i:s");
        return MedicineGroupModel::update($id, $params);
    }

    public function deleteMany($ids)
    {
        return MedicineGroupModel::deleteMany($ids);
    }

    public function updateMany($ids, $data)
    {
        return MedicineGroupModel::updateMany($ids, $data);
    }

    public function delete($ids)
    {
        return MedicineGroupModel::delete($ids);
    }

    public function detail($id)
    {
        return MedicineGroupModel::findById($id);
    }

    public function getAll()
    {
        return MedicineGroupModel::getAll();
    }

    public function getAllRoot()
    {
        return MedicineGroupModel::getAllRoot();
    }

    public function getList(array $params)
    {
        $total = self::totalRows();
        $pagination = $params['pagination'];
        $sort = isset($params['sort']) ? $params['sort'] : [];
        $query = isset($params['query']) ? $params['query'] : [];

        $result = MedicineGroupModel::getMany($pagination, $sort, $query);

        $data['data'] = $result;
        $data['meta']['page'] = isset($pagination['page']) ? $pagination['page'] : 1;
        $data['meta']['perpage'] = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $data['meta']['total'] = $total;
        $data['meta']['pages'] = ceil($total / $data['meta']['perpage']);
        $data['meta']['rowIds'] = self::getListIDs($result);

        return $data;
    }

    public function getListIDs($data) {

        $ids = array();

        foreach($data as $row) {
            array_push($ids, $row->id);
        }

        return $ids;
    }

}
