<?php

namespace App\Services;

use App\Models\BlockModel;
use Illuminate\Support\Facades\App;

class BlockService
{

    public static function totalRows($filter = [])
    {
        $result = BlockModel::totalRows($filter);
        return $result;
    }

    public static function add($params)
    {
        $params['created_at'] = date("Y-m-d H:i:s");
        $params['updated_at'] = date("Y-m-d H:i:s");
        return BlockModel::add($params);
    }

    public function edit($id, $params)
    {
        $params['updated_at'] = date("Y-m-d H:i:s");
        return BlockModel::edit($id, $params);
    }

    public function delete($ids)
    {
        return BlockModel::delete($ids);
    }

    public function detail($id)
    {
        return BlockModel::findById($id);
    }

    public function getList(array $params)
    {
        
        $pagination = $params['pagination'];
        $sort = isset($params['sort']) ? $params['sort'] : [];
        $query = isset($params['query']) ? $params['query'] : [];

        $result = BlockModel::getMany($pagination, $sort, $query);
        $total = self::totalRows($query);
        $data['data'] = $result;
        $data['meta']['page'] = isset($pagination['page']) ? $pagination['page'] : 1;
        $data['meta']['perpage'] = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $data['meta']['total'] = $total;
        $data['meta']['pages'] = ceil($total / $data['meta']['perpage']);
        $data['meta']['rowIds'] = self::getListIDs($result);

        return $data;
    }

    public static function getForFrontEnd($slug)
    {
        $lang = App::getLocale();
        $result = BlockModel::getForFrontEnd($slug, $lang);
        if (!$result) {
            $result = BlockModel::getForFrontEnd($slug, 'vi');
        }

        return isset($result->content) ? json_decode($result->content) : '';
    }

    public function getListIDs($data)
    {

        $ids = array();

        foreach ($data as $row) {
            array_push($ids, $row->id);
        }

        return $ids;
    }

    public static function checkSlugExist($slug)
    {
        return BlockModel::checkSlugExist($slug);
    }

}
