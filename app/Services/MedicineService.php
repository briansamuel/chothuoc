<?php

namespace App\Services;

use App\Models\MedicineModel;

class MedicineService
{

    public static function totalRows() {
        return MedicineModel::totalRows();
    }

    public static function getProductByCateIds($cate_ids, $limit) {
        return MedicineModel::getProductByCateIds($cate_ids, $limit);
    }

    public static function add($params)
    {
        $params['created_at'] = date("Y-m-d H:i:s");
        $params['updated_at'] = date("Y-m-d H:i:s");
        return MedicineModel::add($params);
    }

    public function edit($id, $params)
    {
        $params['updated_at'] = date("Y-m-d H:i:s");
        return MedicineModel::update($id, $params);
    }

    public static function findByKey($key, $value)
	{
        $result = MedicineModel::findByKey($key, $value);
        return $result ? $result : [];
    }

    public function deleteMany($ids)
    {
        return MedicineModel::deleteMany($ids);
    }

    public function updateMany($ids, $data)
    {
        return MedicineModel::updateMany($ids, $data);
    }

    public function delete($ids)
    {
        return MedicineModel::delete($ids);
    }

    public function detail($id)
    {
        return MedicineModel::findById($id);
    }

    public function getList(array $params)
    {
        $total = self::totalRows();
        $pagination = $params['pagination'];
        $sort = isset($params['sort']) ? $params['sort'] : [];
        $query = isset($params['query']) ? $params['query'] : [];

        $result = MedicineModel::getMany($pagination, $sort, $query);

        $data['data'] = $result;
        $data['meta']['page'] = isset($pagination['page']) ? $pagination['page'] : 1;
        $data['meta']['perpage'] = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $data['meta']['total'] = $total;
        $data['meta']['pages'] = ceil($total / $data['meta']['perpage']);
        $data['meta']['rowIds'] = self::getListIDs($result);

        return $data;
    }

    public static function getMany($pagination, $sort, $query) {
        $result = MedicineModel::getMany($pagination, $sort, $query);
        return $result ? $result : [];
    }
    public function getListIDs($data) {

        $ids = array();

        foreach($data as $row) {
            array_push($ids, $row->id);
        }

        return $ids;
    }

    public function getListFrontEnd($filter)
    {
        return MedicineModel::getListPagination($filter);
    }

}
