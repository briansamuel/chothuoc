<?php
namespace App\Services\Admin;

use App\Exceptions\CheckException;
use App\Services\Auth\AuthService;
use App\Services\UserService;

class MyProfileService
{
    public static function changePassword(string $password, string $new_password)
    {
        if ( !AuthService::checkCurrentPassword($password)) {
            throw new CheckException('password_not_valid', 20, []);
        }

        $update = UserService::updatePassword($new_password);
        if ($update) {
            $result = [
                'success' => true,
                'message' => 'Thay đổi mật khẩu thành công!'
            ];
            return $result;
        }

        throw new CheckException('update_password_not_success', 11, ['res' => $update]);
    }
}