<?php
namespace App\Services;

use App\Models\IngredientModel;
use App\Transformers\AgencyTransformer;
use Illuminate\Support\Str;

class IngredientService
{
    public function getAll() {
        return IngredientModel::getAll();
    }

    public function getByGroupId($group_id)
    {
        return IngredientModel::getByGroupId($group_id);
    }

	public static function totalRows($params) {
        return IngredientModel::totalRows($params);
	}

	public static function getMany($columns = ['*'],$pagination, $sort, $filter)
	{
		$result = IngredientModel::getMany($columns,$pagination, $sort, $filter);
        return $result ? $result : [];
	}


	public static function findByKey($key, $value)
	{
        $result = IngredientModel::findByKey($key, $value);
        return $result ? $result : [];
	}

	public static function findByMultiKey($filter)
	{
        $result = IngredientModel::findByMultiKey($filter);
        return $result ? $result : [];
	}

	public static function searchByCharacter($character)
	{
        $result = IngredientModel::search( $character, []);
        return $result ? $result : [];
	}

	public static function insert($params)
	{
		$insert['name'] = $params['name'];
		$insert['slug'] = isset($params['slug']) ? Str::slug($params['slug']) : Str::slug($params['name']);
		$insert['content'] = $params['content'];
		$insert['seo_name'] = isset($params['seo_name']) ? $params['seo_name'] : '';
		$insert['seo_description'] = isset($params['seo_description']) ? $params['seo_description'] : '';
		$insert['seo_keyword'] = isset($params['seo_keyword']) ? $params['seo_keyword'] : '';
		$insert['thumbnail'] = isset($params['thumbnail']) ? $params['thumbnail'] : '';
		$insert['medicine_group_id'] = isset($params['medicine_group_id']) ? $params['medicine_group_id'] : 0;
		$insert['status'] = $params['status'];
		$insert['created_at'] = isset($params['created_at']) ? $params['created_at'] : date("Y-m-d H:i:s");
		$insert['updated_at'] = date("Y-m-d H:i:s");
		return IngredientModel::insert($insert);
	}
	public static function update($id, $params)
	{
		$update['name'] = $params['name'];
		$update['slug'] = isset($params['slug']) ? $params['slug'] : Str::slug($params['name']);

		$update['content'] = $params['content'];
		$update['seo_name'] = isset($params['seo_name']) ? $params['seo_name'] : '';
		$update['seo_description'] = isset($params['seo_description']) ? $params['seo_description'] : '';
		$update['seo_keyword'] = isset($params['seo_keyword']) ? $params['seo_keyword'] : '';
		$update['thumbnail'] = isset($params['thumbnail']) ? $params['thumbnail'] : '';
		$update['medicine_group_id'] = isset($params['medicine_group_id']) ? $params['medicine_group_id'] : 0;
		$update['status'] = $params['status'];
		$update['created_at'] = isset($params['created_at']) ? $params['created_at'] : date("Y-m-d H:i:s");
		$update['updated_at'] = date("Y-m-d H:i:s");
		return IngredientModel::update($id, $update);
	}

	public static function updateMedicineGroup($id, $medicine_group_id)
	{

		$update['medicine_group_id'] = isset($medicine_group_id) ? $medicine_group_id : 0;
		
		return IngredientModel::update($id, $update);
	}


	public static function updateMany($ids, $data)
    {
        return IngredientModel::updateManyPost($ids, $data);
	}

	public static function deleteMany($ids)
    {
        return IngredientModel::deleteManyPost($ids);
	}

	public static function delete($id)
	{
		return IngredientModel::delete($id);
	}

	public function getList(array $params)
    {
		$pagination = $params['pagination'];
        $sort = isset($params['sort']) ? $params['sort'] : [];
        $query = isset($params['query']) ? $params['query'] : [];
		$total = self::totalRows($query);

		$column = ['id', 'name', 'status', 'created_at'];
        $result = IngredientModel::getMany($column, $pagination, $sort, $query);

        $data['data'] = $result;
        $data['meta']['page'] = isset($pagination['page']) ? $pagination['page'] : 1;
        $data['meta']['perpage'] = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $data['meta']['total'] = $total;
        $data['meta']['pages'] = ceil($total / $data['meta']['perpage']);
		$data['meta']['rowIds'] = self::getListIDs($result);
        return $data;
	}

	public function getListIDs($data) {

		$ids = array();

		foreach($data as $row) {
			array_push($ids, $row->id);
		}

		return $ids;
	}

}
