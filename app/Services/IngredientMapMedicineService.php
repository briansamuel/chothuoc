<?php

namespace App\Services;

use App\Models\IngredientMapMedicineModel;

class IngredientMapMedicineService
{

    public static function add($params)
    {
        return IngredientMapMedicineModel::add($params);
    }

    public static function getByMedicineId($medicine_id)
    {
        $arr = [];
        $result = IngredientMapMedicineModel::getByMedicineId($medicine_id);
        foreach ($result as $item) {
            $arr[] = $item;
        }

        return $arr;
    }

    public static function deleteByMedicineId($medicine_id)
    {
        return IngredientMapMedicineModel::deleteByMedicineId($medicine_id);
    }

}
