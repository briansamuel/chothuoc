<?php

namespace App\Services\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthPermissionService
{

    //CI instance
    private $CI;
    private $request;
    private $admin = 'no';
    private $is_root = 'no';
    private $requireRoot = 'no';
    //current user's permissions
    private $authobj;

    private $MyProfileController = array(
        'MyProfileController' => 'Quản lý toàn quyền(Tài Khoản => Thông tin tài khoản)',
        'MyProfileController/profile' => 'Xem thông tin cá nhân',
        'MyProfileController/updateProfile' => 'Cập nhập thông tin cá nhân',
        'MyProfileController/changePassword' => 'Thay đổi mật khẩu',
        'MyProfileController/changePasswordAction' => 'Action thay đổi mật khẩu',
    );

    private $UsersController = array(
        'UsersController' => 'Quản lý toàn quyền(Tài Khoản => Tài Khoản Hệ Thống)',
        'UsersController/index' => 'Danh sách hệ thống',
        'UsersController/ajaxGetList' => 'Action Danh sách hệ thống',
        'UsersController/add' => 'Tạo mới 1 tài khoản hệ thống',
        'UsersController/addAction' => 'Action xử lý tạo mới 1 tài khoản hệ thống',
        'UsersController/edit' => 'Sửa thông tin 1 tài khoản hệ thống',
        'UsersController/editAction' => 'Action xử lý sửa thông tin 1 tài khoản hệ thống',
        'UsersController/editManyAction' => 'Action xử lý sửa nhiều thông tin tài khoản hệ thống',
        'UsersController/delete' => 'Xóa thông tin 1 tài khoản hệ thống',
        'UsersController/deleteMany' => 'Xóa nhiều thông tin tài khoản hệ thống',
    );

    private $UserGroupController = array(
        'UserGroupController' => 'Quản lý toàn quyền(Tài Khoản => Quản lý phân quyền)',
        'UserGroupController/index' => 'Danh sách phân quyền',
        'UserGroupController/ajaxGetList' => 'Action Danh sách phân quyền',
        'UserGroupController/add' => 'Tạo mới 1 tài khoản phân quyền',
        'UserGroupController/addAction' => 'Action xử lý tạo mới 1 phân quyền',
        'UserGroupController/edit' => 'Sửa thông tin 1 phân quyền',
        'UserGroupController/editAction' => 'Action xử lý sửa thông tin 1 phân quyền',
        'UserGroupController/editManyAction' => 'Action xử lý sửa nhiều thông tin phân quyền',
        'UserGroupController/delete' => 'Xóa thông tin 1 phân quyền',
        'UserGroupController/deleteMany' => 'Xóa nhiều thông tin phân quyền',
    );

    private $LogsUserController = array(
        'LogsUserController' => 'Quản lý toàn quyền(Tài Khoản => Logs User)',
        'LogsUserController/index' => 'Danh sách Logs User',
        'LogsUserController/ajaxGetList' => 'Action Danh sách Logs User',
    );

    // Group quyền tin tức
    private $NewsController = array(
        'NewsController' => 'Quản lý toàn quyền (Tin tức)',
        'NewsController/index' => 'Danh sách tin tức',
        'NewsController/add' => 'Trang Thêm và Sửa tin tức',
        'NewsController/save' => 'Action Lưu dữ liệu',
        'NewsController/delete' => 'Xóa tin tức',
    );

    // Group quyền tin tức
    private $DeseaseController = array(
        'DeseaseController' => 'Quản lý toàn quyền (Bệnh)',
        'DeseaseController/index' => 'Danh sách tin tức',
        'DeseaseController/add' => 'Trang Thêm và Sửa Bệnh',
        'DeseaseController/save' => 'Action Lưu dữ liệu Bệnh',
        'DeseaseController/delete' => 'Xóa Bệnh',
    );

    // Group quyền tin tức
    private $IngredientController = array(
        'IngredientController' => 'Quản lý toàn quyền (Hoạt chất)',
        'IngredientController/index' => 'Danh sách hoạt chất',
        'IngredientController/add' => 'Trang Thêm và Sửa hoạt chất',
        'IngredientController/save' => 'Action Lưu dữ liệu hoạt chất',
        'IngredientController/delete' => 'Xóa hoạt chất',
    );

    // Group banner
    private $BannersController = array(
        'BannersController' => 'Quản lý toàn quyền (Banner)',
        'BannersController/index' => 'Danh sách banner',
        'BannersController/add' => 'Tạo mới banner',
        'BannersController/addAction' => 'Action Tạo mới banner',
        'BannersController/edit' => 'Sửa thông tin 1 banner',
        'BannersController/editAction' => 'Action xử lý sửa thông tin 1 banner',
        'BannersController/editManyAction' => 'Action xử lý sửa nhiều banner',
        'BannersController/delete' => 'Xóa 1 banner',
        'BannersController/deleteMany' => 'Xóa nhiều banner',
    );

    // Group brand
    private $BrandsController = array(
        'BrandsController' => 'Quản lý toàn quyền (Brand)',
        'BrandsController/index' => 'Danh sách brand',
        'BrandsController/add' => 'Tạo mới brand',
        'BrandsController/addAction' => 'Action Tạo mới brand',
        'BrandsController/edit' => 'Sửa thông tin 1 brand',
        'BrandsController/editAction' => 'Action xử lý sửa thông tin 1 brand',
        'BrandsController/editManyAction' => 'Action xử lý sửa nhiều brand',
        'BrandsController/delete' => 'Xóa 1 brand',
        'BrandsController/deleteMany' => 'Xóa nhiều brand',
    );


    // Group thư viện
    private $GalleryController = array(
        'GalleryController' => 'Quản lý toàn quyền (Thư viện)',
        'GalleryController/index' => 'Quản lý thư viện',
    );

    // Group thư viện
    private $MultiLanguageController = array(
        'MultiLanguageController' => 'Quản lý toàn quyền (Đa ngôn ngữ)',
        'MultiLanguageController/index' => 'Quản lý đa ngôn ngữ',
    );
    private $MenusController = array(
        'MenusController' => 'Quản lý toàn quyền (Menu)',
        'MenusController/index' => 'Quản lý Menu',
    );

    private $ThemeOptionsController = array(
        'ThemeOptionsController' => 'Quản lý toàn quyền (Tùy biến)',
        'ThemeOptionsController/index' => 'Tùy biến',
        'ThemeOptionsController/editAction' => 'Action Tùy biến',
        'ThemeOptionsController/editAction' => 'Action Tùy biến',
    );

    private $MedicineGroupController = array(
        'MedicineGroupController' => 'Quản lý toàn quyền(Nhóm thuốc => Danh sách nhóm thuốc)',
        'MedicineGroupController/index' => 'Danh sách nhóm thuốc',
        'MedicineGroupController/ajaxGetList' => 'Action Danh sách nhóm thuốc',
        'MedicineGroupController/add' => 'Tạo mới 1 tài khoản nhóm thuốc',
        'MedicineGroupController/addAction' => 'Action xử lý tạo mới 1 nhóm thuốc',
        'MedicineGroupController/edit' => 'Sửa thông tin 1 nhóm thuốc',
        'MedicineGroupController/editAction' => 'Action xử lý sửa thông tin 1 nhóm thuốc',
        'MedicineGroupController/editManyAction' => 'Action xử lý sửa nhiều thông tin nhóm thuốc',
        'MedicineGroupController/delete' => 'Xóa thông tin 1 nhóm thuốc',
        'MedicineGroupController/deleteMany' => 'Xóa nhiều thông tin nhóm thuốc',
    );

    private $CountryController = array(
        'CountryController' => 'Quản lý toàn quyền(Phân loại hàng => Danh sách nơi xuất sứ)',
        'CountryController/index' => 'Danh sách nơi xuất sứ',
        'CountryController/ajaxGetList' => 'Action Danh sách nơi xuất sứ',
        'CountryController/add' => 'Tạo mới 1 tài khoản nơi xuất sứ',
        'CountryController/addAction' => 'Action xử lý tạo mới 1 nơi xuất sứ',
        'CountryController/edit' => 'Sửa thông tin 1 nơi xuất sứ',
        'CountryController/editAction' => 'Action xử lý sửa thông tin 1 nơi xuất sứ',
        'CountryController/editManyAction' => 'Action xử lý sửa nhiều thông tin nơi xuất sứ',
        'CountryController/delete' => 'Xóa thông tin 1 nơi xuất sứ',
        'CountryController/deleteMany' => 'Xóa nhiều thông tin nơi xuất sứ',
    );

    private $BrandController = array(
        'BrandController' => 'Quản lý toàn quyền(Phân loại hàng => Danh sách nhãn hiệu)',
        'BrandController/index' => 'Danh sách nhãn hiệu',
        'BrandController/ajaxGetList' => 'Action Danh sách nhãn hiệu',
        'BrandController/add' => 'Tạo mới 1 nhãn hiệu',
        'BrandController/addAction' => 'Action xử lý tạo mới 1 nhãn hiệu',
        'BrandController/edit' => 'Sửa thông tin 1 nhãn hiệu',
        'BrandController/editAction' => 'Action xử lý sửa thông tin 1 nhãn hiệu',
        'BrandController/editManyAction' => 'Action xử lý sửa nhiều thông tin nhãn hiệu',
        'BrandController/delete' => 'Xóa thông tin 1 nhãn hiệu',
        'BrandController/deleteMany' => 'Xóa nhiều thông tin nhãn hiệu',
    );

    private $ReviewController = array(
        'ReviewController' => 'Quản lý toàn quyền(Phân loại hàng => Danh sách đánh giá)',
        'ReviewController/index' => 'Danh sách đánh giá',
        'ReviewController/ajaxGetList' => 'Action Danh sách đánh giá',
        'ReviewController/edit' => 'Sửa thông tin 1 đánh giá',
        'ReviewController/editAction' => 'Action xử lý sửa thông tin 1 đánh giá',
        'ReviewController/editManyAction' => 'Action xử lý sửa nhiều thông tin đánh giá',
        'ReviewController/delete' => 'Xóa thông tin 1 đánh giá',
        'ReviewController/deleteMany' => 'Xóa nhiều thông tin đánh giá',
    );

    private $OrderController = array(
        'OrderController' => 'Quản lý toàn quyền(Đơn hàng)',
        'OrderController/index' => 'Danh sách đơn hàng',
        'OrderController/ajaxGetList' => 'Action Danh sách đơn hàng',
        'OrderController/edit' => 'Sửa thông tin 1 đơn hàng',
        'OrderController/editAction' => 'Action xử lý sửa thông tin 1 đơn hàng',
        'OrderController/editManyAction' => 'Action xử lý sửa nhiều thông tin đơn hàng',
        'OrderController/delete' => 'Xóa thông tin 1 đơn hàng',
        'OrderController/deleteMany' => 'Xóa nhiều thông tin đơn hàng',
    );

    private $MedicineController = array(
        'MedicineController' => 'Quản lý toàn quyền(Thuốc => Danh sách thuốc)',
        'MedicineController/index' => 'Danh sách thuốc',
        'MedicineController/ajaxGetList' => 'Action Danh sách thuốc',
        'MedicineController/add' => 'Tạo mới 1 thuốc',
        'MedicineController/addAction' => 'Action xử lý tạo mới 1 thuốc',
        'MedicineController/edit' => 'Sửa thông tin 1 thuốc',
        'MedicineController/editAction' => 'Action xử lý sửa thông tin 1 thuốc',
        'MedicineController/editManyAction' => 'Action xử lý sửa nhiều thông tin thuốc',
        'MedicineController/delete' => 'Xóa thông tin 1 thuốc',
        'MedicineController/deleteMany' => 'Xóa nhiều thông tin thuốc',
    );

    /*
     * construct
     */

    public function __construct(Request $request, $requireRoot = false)
    {
        $this->CI = new \stdClass();

        $this->request = $request;
        $this->requireRoot = $requireRoot;

        //current permission
        $this->authobj = json_decode(Auth::user()->permission);
        $this->authobj = is_array($this->authobj) ? $this->authobj : array();
    }

    function check()
    {
        //is root
        if (Auth::user()->is_root == 1) {

            return true;
        }
        //only root
        if ($this->requireRoot && Auth::user()->is_root != 1) {
            return false;
        }
        //lấy controller - function hiện tại qua router
        $currentAction = \Route::currentRouteAction();
        list($controllers, $method) = explode('@', $currentAction);
        // $controller now is "App\Http\Controllers\FooBarController"
        $controller = preg_replace('/.*\\\/', '', $controllers);
        if($controller === 'WelcomeController') {
            return true;
        }
        //tên controller/function
        $function = $controller . '/' . $method;
        //full access controller
        if (in_array($controller, $this->authobj)) {
            return true;
        }
        //can access
        if (in_array($function, $this->authobj)) { //|| in_array($method, $this->ignores)) {
            return true;
        }
        //no permission
        return false;
    }

    /*
     * get list controller
     */

    function listController()
    {
        $this->controllers = array(
            'MyProfileController' => $this->MyProfileController,
            'UsersController' => $this->UsersController,
            'UserGroupController' => $this->UserGroupController,
            'LogsUserController' => $this->LogsUserController,
            'NewsController' => $this->NewsController,
            'DeseaseController' => $this->DeseaseController,
            'IngredientController' => $this->IngredientController,
            'BannersController' => $this->BannersController,
            'BrandsController' => $this->BrandsController,
            'MenusController' => $this->MenusController,
            'GalleryController' => $this->GalleryController,
            'MultiLanguageController' => $this->MultiLanguageController,
            'ThemeOptionsController' => $this->ThemeOptionsController,
            'MedicineGroupController' => $this->MedicineGroupController,
            'CountryController' => $this->CountryController,
            'BrandController' => $this->BrandController,
            'MedicineController' => $this->MedicineController,
            'ReviewController' => $this->ReviewController,
            'OrderController' => $this->OrderController,
        );
        return $this->controllers;
    }

    /*
     * return admin
     */

    public function isAdmin()
    {
        if ($this->admin == 'yes') {
            return true;
        } else {
            return false;
        }
    }

    /*
     * check function show in header
     */

    function checkHeader($controller)
    {
        if (Auth::user()->is_root == 1) {
            return true;
        }
        foreach ($this->authobj as $permission) {
            if (strpos($controller, $permission) === 0) {
                return true;
            }
        }
        return false;
    }

    public function isHeader($controller)
    {
        if (Auth::user()->is_root == 1) {
            return true;
        }
        if (in_array($controller, $this->authobj)) {
            return true;
        } else {
            return false;
        }
    }

    public function getListPermission($encode = true)
    {
        $permissions = [];
        foreach ($this->listController() as $key => $value) {
            //neu co 1 chuc nang dc lua chon
            if ($this->request->input($key, '')) {
                $permission = $this->request->input($key, '');
                //neu la toan quyen
                if ($permission[0] == $key) {
                    $permissions[] = $key;
                } else {
                    foreach ($permission as $x) {
                        $permissions[] = $x;
                    }
                }
            }
        }

        return $encode ? json_encode($permissions) : $permissions;
    }

}

?>
