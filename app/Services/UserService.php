<?php

namespace App\Services;

use App\Models\UserModel;
use App\Notifications\ActiveUserRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class UserService
{

    public static function totalRows() {
        $result = UserModel::totalRows();
        return $result;
    }

    public static function add($params)
    {
        $insert['username'] = $params['username'];
        $insert['password'] = bcrypt($params['password']);
        $insert['email'] = $params['email'];
        $insert['full_name'] = $params['full_name'];
        $insert['active_code'] = Str::random(60);
        $insert['avatar'] = $params['url_avatar'];
        $insert['is_root'] = 0;
        $insert['status'] = 'inactive';
        $insert['group_id'] = isset($params['group_id']) ? implode(",", $params['group_id']) : '';
        $insert['created_at'] = date("Y-m-d H:i:s");
        $insert['updated_at'] = date("Y-m-d H:i:s");
        return UserModel::insert($insert);
    }

    public static function edit($id, $params)
    {
        
        $params['group_id'] = isset($params['group_id']) ? implode(",", $params['group_id']) : '';
        $params['updated_at'] = date("Y-m-d H:i:s");
        return UserModel::updateUser($id, $params);
    }

    public function deleteMany($ids)
    {
        return UserModel::deleteManyUser($ids);
    }

    public function updateMany($ids, $data)
    {
        return UserModel::updateManyUser($ids, $data);
    }

    public function delete($ids)
    {
        return UserModel::deleteUser($ids);
    }

    public function detail($id)
    {
        return UserModel::findById($id);
    }

    public static function getUserInfoByEmail(string $email)
    {
        return UserModel::findByKey('email', $email);
    }

    public static function updatePasswordByEmail(string $email, string $password)
    {
        return UserModel::where('email', $email)->update([
            'password' => bcrypt($password),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }

    public static function updateProfile(array $params)
    {
        $user = Auth::guard('admin')->user();

        $user->full_name = $params['full_name'];
        $user->avatar = isset($params['avatar']) ? $params['avatar'] : $user->avatar;

        return $user->save();
    }

    public static function updatePassword(string $password)
    {
        $user = Auth::guard('admin')->user();

        $user->password = bcrypt($password);

        return $user->save();
    }

    public static function updateUserFrontsite(array $params) {
        $user = Auth::guard('user')->user();

        $user->full_name = $params['full_name'];
        $user->phone = $params['phone'];
        $user->gender = $params['gender'];
        $user->address = $params['address'];
        $user->pharmacy_name = isset($params['pharmacy_name']) ? $params['pharmacy_name'] : '';
        $user->pharmacy_mst = isset($params['pharmacy_mst']) ? $params['pharmacy_mst'] : '';
        if(isset($params['password'])) {
            $user->password = bcrypt($params['password']);
        }
        return $user->save();

    }
    public function getList(array $params)
    {
        $total = self::totalRows();
        $pagination = $params['pagination'];
        $sort = isset($params['sort']) ? $params['sort'] : [];
        $query = isset($params['query']) ? $params['query'] : [];

        $result = UserModel::getMany($pagination, $sort, $query);

        $data['data'] = $result;
        $data['meta']['page'] = isset($pagination['page']) ? $pagination['page'] : 1;
        $data['meta']['perpage'] = isset($pagination['perpage']) ? $pagination['perpage'] : 20;
        $data['meta']['total'] = $total;
        $data['meta']['pages'] = ceil($total / $data['meta']['perpage']);
        $data['meta']['rowIds'] = self::getListIDs($result);

        return $data;
    }

    public function getListIDs($data) {

        $ids = array();

        foreach($data as $row) {
            array_push($ids, $row->id);
        }

        return $ids;
    }

    public function sendMailActiveUser($email)
    {
        $user = UserService::getUserInfoByEmail($email);

        $user->notify(new ActiveUserRequest($user->active_code));

        return response()->json([
            'success' => true,
            'message' => 'Chúng tôi đã gửi email kích hoạt tài khoản đến email ' . $email . '!'
        ]);
    }

    public function activeUser($id) {
        $data['status'] = 'active';
        $data['email_verified_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        return UserModel::updateUser($id, $data);
    }

}
