<?php
return [
    'google_translate_api_key'=> 'AIzaSyAMMeGEeYSevUQYg9ZtSDC7PWuATS3fAVw',
    'trans_functions' => [
        'trans',
        'trans_choice',
        'Lang::get',
        'Lang::choice',
        'Lang::trans',
        'Lang::transChoice',
        '@lang',
        '@choice',
        '__',
        '\$trans.get',
        '\$t'
    ],
];
