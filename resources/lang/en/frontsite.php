<?php

return array (
  'news' => 
  array (
    'load_more' => 'Xem thêm',
    'all' => 'Tất cả',
    'type_news' => 'Tin tức',
    'type_event' => 'Sự kiện',
    'new_feature' => 'Mới nhất',
    'new_and_event' => 'Tin tức & Sự kiện',
  ),
);
