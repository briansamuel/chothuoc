<?php

return array (
  'web' => array (
    'slogan' => 'Chợ thuốc sĩ',
  ),
  'home' => 
  array (
    'title' => 'Pharmacy Market',
    'title_ab' => 'Pharmacy Market',
    'keyword' => 'Pharmacy Market',
    'description' => 'Pharmacy Market',
  ),
  'news' => 
  array (
    'title' => 'Tin tức',
    'title_ab' => 'Tin tức',
    'keyword' => 'Tin tức ',
    'description' => 'Tin tức',
  ),

  'products' => 
  array (
    'title' => 'Sản phẩm',
    'title_ab' => 'Sản phẩm',
    'keyword' => 'Sản phẩm',
    'description' => 'Sản phẩm',
  ),

  'ingredient' => 
  array (
    'title' => 'Hoạt chất',
    'title_ab' => 'Hoạt chất',
    'keyword' => 'Hoạt chất',
    'description' => 'Hoạt chất',
  ),

  'disease' => 
  array (
    'title' => 'Tìm hiểu bệnh',
    'title_ab' => 'Tìm hiểu bệnh',
    'keyword' => 'Tìm hiểu bệnh',
    'description' => 'Tìm hiểu bệnh',
  ),

  'account' => 
      array (
        'title' => 'Thông tin tài khoản',
        'title_ab' => 'Thông tin tài khoản',
        'keyword' => 'Thông tin tài khoản',
        'description' => 'Thông tin tài khoản',
  ),
  'account_order' => 
      array (
        'title' => 'Quản lý đơn hàng',
        'title_ab' => 'Quản lý đơn hàng',
        'keyword' => 'Quản lý đơn hàng',
        'description' => 'Quản lý đơn hàng',
  ),
  'viewed_product' => array (
      'title' => 'Sản phẩm đã xem',
      'title_ab' => 'Sản phẩm đã xem',
      'keyword' => 'Sản phẩm đã xem',
      'description' => 'Sản phẩm đã xem',
  ),

  'liked_product' => array (
    'title' => 'Sản phẩm yêu thích',
    'title_ab' => 'Sản phẩm yêu thích',
    'keyword' => 'Sản phẩm yêu thích',
    'description' => 'Sản phẩm yêu thích',
  ),

  'willbuy_product' => array (
    'title' => 'Sản phẩm mua sau',
    'title_ab' => 'Sản phẩm mua sau',
    'keyword' => 'Sản phẩm ymua sau',
    'description' => 'Sản phẩm mua sau',
  ),

  'cart' => array (
    'title' => 'Giỏ hàng',
    'title_ab' => 'Giỏ hàng',
    'keyword' => 'Giỏ hàng',
    'description' => 'Giỏ hàng',
  ),
);
