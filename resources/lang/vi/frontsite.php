<?php

return array (
  'header' => 
    array (
      'site_name' => 'ILO',
      'show_by' => 'Chương trình bởi',
      'performed_by' => 'Thực hiện bởi',
    ),
    'footer' => array(
      'company_name' => 'CÔNG TY TNHH THƯƠNG MẠI DỊCH VỤ HEALING',
      'address' => 'Tầng 1 Tòa nhà AN PHÚ PLAZA, 117-119 Lý Chính Thắng, P7, Quận 3, HCM',
      'email' => 'pharmacymarket.com@gmail.com',
      'tel_1' => '0378218368',
      'tel_2' => '',
      'MST' => '0314475861',
      'time_word' => 'từ t2 đến t7(8h-17h)',
      'STK' => '0911000007569',
      'bank_name' => 'VCB',
      'bank_account' => 'LÂM QUANG MƯỜI' 

    ),
  
);
