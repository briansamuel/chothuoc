@extends('admin.index')
@section('page-header', 'Menu')
@section('page-sub_header', 'Quản lý Menu')
@section('style')

@endsection
@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            {!! Menu::render() !!}
        </div>
    </div>
@endsection
@section('script')
    {!! Menu::scripts() !!}
@endsection