@extends('admin.index')
@section('page-header', 'Nhãn hiệu')
@section('page-sub_header', 'Danh sách nhãn hiệu')
@section('style')

@endsection
@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-line-chart"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                Danh sách nhãn hiệu
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <a href="" class="btn btn-clean btn-icon-sm">
                    <i class="la la-long-arrow-left"></i>
                    Back
                </a>
                &nbsp;
                <div class="dropdown dropdown-inline">
                    <a href="{{route('brand.add')}}" class="btn btn-brand btn-icon-sm"><i class="flaticon2-plus"></i> Thêm nhãn hiệu</a>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">

        <!--begin: Search Form -->
        <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
            <div class="row align-items-center">
                <div class="col-xl-12 order-2 order-xl-1">
                    <div class="row align-items-center">
                        <div class="col-md-3 kt-margin-b-20-tablet-and-mobile">
                            <label>Name:</label>
                            <div class="kt-input-icon kt-input-icon--left">
                                <input type="text" class="form-control" placeholder="Vui lòng nhập từ khóa..." id="kt_form_name">
                                <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                    <span><i class="la la-search"></i></span>
                                </span>
                            </div>
                        </div>
                        <div class="col-md-3 kt-margin-b-20-tablet-and-mobile" id="kt_form_daterange1">
                            <div class="form__group kt-form__group--inline">
                                <div class="kt-form__label">
                                    <label>Theo ngày tạo:</label>
                                </div>
                                <input type='text' class="form-control" id="kt_daterangepicker_1"  readonly placeholder="Chọn thời gian" value=""/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--begin: Selected Rows Group Action Form -->
        <div class="kt-form kt-form--label-align-right kt-margin-t-20 collapse" id="kt_datatable_group_action_form1">
            <div class="row align-items-center">
                <div class="col-xl-12">
                    <div class="kt-form__group kt-form__group--inline">
                        <div class="kt-form__label kt-form__label-no-wrap">
                            <label class="kt-font-bold kt-font-danger-">Chọn
                                <span id="kt_datatable_selected_number1">0</span> dòng:</label>
                                {{csrf_field()}}
                        </div>

                        <div class="kt-form__control">
                            <div class="btn-toolbar">
                                <button class="btn btn-sm btn-danger" type="button" id="kt_datatable_delete_all">Xóa Tất Cả</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--end: Selected Rows Group Action Form -->
    </div>
    <div class="kt-portlet__body kt-portlet__body--fit">
        @include('admin.elements.alert_flash')
        <!--begin: Datatable -->
        <div class="kt-datatable" id="ajax_data"></div>

        <!--end: Datatable -->
    </div>
</div>
@endsection
@section('script')
    <script src="assets/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
    <script src="assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>
    <script src="assets/js/pages/custom/brand/list-datatable.js?v1" type="text/javascript"></script>
@endsection
