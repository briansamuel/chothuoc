@extends('admin.index')
@section('page-header', 'Block')
@section('page-sub_header', 'Config Block')
@section('style')
    <link rel="stylesheet" href="admin/plugins/fancybox/jquery.fancybox.min.css"/>
    <style type="text/css">
        .remove_image {
            position: absolute;
            right: 10%;
            top: 5%;
        }

        .remove_image i {
            font-size: 20px;
        }
    </style>
@endsection
@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        {{$blockInfo->name}} </h3>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <form id="kt_edit_form">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                        <div class="kt-portlet">
                            <div class="kt-portlet__head kt-portlet__head--right">
                                <div class="kt-portlet__head-label ">
                                    <span class="kt-font-danger"><i class="fa fa-star"></i> Bắt buộc phải nhập / chọn nội dung</span>
                                </div>
                            </div>
                            <!--begin::Form-->
                            <div class="kt-form">
                                <div class="kt-portlet__body">
                                    <div class="form-group row">
                                        <label for="title" class="col-12 col-lg-12 col-xl-3 col-form-label">Tiêu
                                            đề:</label>
                                        <div class="col-12 col-lg-12 col-xl-9">
                                            {{csrf_field()}}
                                            <input type="hidden" name="_id" id="_id" value="{{$blockInfo->id}}"/>
                                            <input class="form-control" type="text" value="{{isset($config->title) ? $config->title : ''}}"
                                                   id="title" name="title"
                                                   placeholder="Tiêu đề bắt buộc phải nhập nội dung">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="content" class="col-12 col-lg-12 col-xl-3 col-form-label">Nội dung:</label>
                                        <div class="col-12 col-lg-12 col-xl-9">
                                            <textarea class="form-control" name="content" id="content" cols="30" rows="10">{{isset($config->content) ? $config->content : ''}}</textarea>
                                        </div>
                                    </div>
                                    <div class="kt-portlet" id="kt_portlet_tools_gallery">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">
                                                    Thư viện hình ảnh Khách Sạn
                                                </h3>
                                            </div>
                                            <div class="kt-portlet__head-toolbar">
                                                <div class="kt-portlet__head-group">
                                                    <a href="#" data-ktportlet-tool="toggle" class="btn btn-sm btn-icon btn-clean btn-icon-md" aria-describedby="tooltip_gggrqlstux"><i class="la la-angle-down"></i></a>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-form ">
                                            <div class="kt-portlet__body">
                                                <div id="preview_gallery" class="preview_gallery sortable sortable-dragging sortable-placeholder row form-group">
                                                    @if(is_object($config))
                                                        @foreach($config->gallery as $image)
                                                            <div class="col-xl-2 col-lg-3 col-md-4 col-6">
                                                                <img class="img-fluid img-thumbnail" src="{{ $image }}" alt="">
                                                                <a class="remove_image" href="javascript:;" onClick="delete_image(this)"><i class="flaticon-circle"></i></a>
                                                                <input name="gallery[]" type="hidden" value="{{ $image }}" />
                                                            </div>
                                                        @endforeach
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="kt-portlet__foot kt-align-right p-2">
                                            <div>
                                                <a data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=gallery_select&lang=vi&akey=@filemanager_get_key()&fldr=khach-san" class="iframe-btn btn btn-primary" data-fancybox data-fancybox data-type="iframe" href="javascript:;">
                                                    Upload Image
                                                </a>
                                                <input type="hidden" id="gallery_select" name="gallery_select">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <label for="content" class="col-12 col-lg-12 col-xl-3 col-form-label"></label>
                                        <button type="button" class="btn btn-primary" id="btn_edit"><i
                                                    class="la la-save"></i> Lưu dữ liệu
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!-- end:: Content -->
    </div>
@endsection
@section('script')
    <!--end::Page Vendors -->
    <script src="assets/js/pages/crud/forms/widgets/bootstrap-datetimepicker.js" type="text/javascript"></script>
    <script src="admin/js/pages/block/config/config-home-block-4.js?v1.1" type="text/javascript"></script>
    <script src="admin/plugins/fancybox/jquery.fancybox.min.js"></script>
    <script type="text/javascript" src="admin/plugins/fancybox/jquery.observe_field.js"></script>


    <script>
        $('.iframe-btn').fancybox({
            'iframe': {
                'css': {
                    'width': '90%',
                    'height': '90%',
                }
            },
        });

        $(document).ready(function() {

            $("#host_thumbnail").observe_field(1, function() {
                // alert('Change observed! new value: ' + this.value );
                $('#preview_thumbnail').attr('src', this.value).show();

            });

            $("#gallery_select").observe_field(1, function() {
                // alert('Change observed! new value: ' + this.value );

                var list = this.value;
                if(list != '') {
                    var n = list.includes("[");
                    if (n) {
                        var array = JSON.parse(list);
                        console.log(array);
                        array.forEach((num, index) => {
                            console.log(num);
                            $('#preview_gallery').append('<div class="col-xl-2 col-lg-3 col-md-4 col-6"><img class="img-fluid img-thumbnail" src="' + num + '" alt=""><a class="remove_image" href="javascript:;" onClick="delete_image(this)"><i class="flaticon-circle"></i></a><input name="gallery[]" type="hidden" value="' + num + '" /></div>');

                        });
                    } else {
                        $('#preview_gallery').append('<div class="col-xl-2 col-lg-3 col-md-4 col-6"><a class="remove_image" href="javascript:;" onClick="delete_image(this)"><i class="flaticon-circle"></i></a><img class="img-fluid img-thumbnail" src="' + list + '" alt=""><input name="gallery[]" type="hidden" value="' + list + '" /></div>');
                    }

                    $("#preview_gallery").sortable();
                }

            });

        });

        function delete_image(e) {
            e.parentNode.parentNode.removeChild(e.parentNode);
            document.getElementById("gallery_select").value = '';
        }
    </script>
@endsection