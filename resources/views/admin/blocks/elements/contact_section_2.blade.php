@extends('admin.index')
@section('page-header', 'Block')
@section('page-sub_header', 'Config Block')
@section('style')
<link rel="stylesheet" href="admin/plugins/fancybox/jquery.fancybox.min.css" />
@endsection
@section('content')
<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

    <!-- begin:: Subheader -->
    <div class="kt-subheader   kt-grid__item" id="kt_subheader">
        <div class="kt-container  kt-container--fluid ">
            <div class="kt-subheader__main">
                <h3 class="kt-subheader__title">
                    {{$blockInfo->name}} </h3>
            </div>
        </div>
    </div>

    <!-- end:: Subheader -->

    <!-- begin:: Content -->
    <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
        <form id="kt_edit_form">
            <div class="row">
                <div class="col-md-12 col-lg-12">
                    <div class="kt-portlet">
                        <div class="kt-portlet__head kt-portlet__head--right">
                            <div class="kt-portlet__head-label ">
                                <span class="kt-font-danger"><i class="fa fa-star"></i> Bắt buộc phải nhập / chọn nội dung</span>
                            </div>
                        </div>
                        <!--begin::Form-->
                        <div class="kt-form">
                            <div class="kt-portlet__body">
                                <div class="form-group row">
                                    <label for="title" class="col-12 col-lg-12 col-xl-3 col-form-label">Tiêu
                                        đề:</label>
                                    <div class="col-12 col-lg-12 col-xl-9">
                                        {{csrf_field()}}
                                        <input type="hidden" name="_id" id="_id" value="{{$blockInfo->id}}" />
                                        <input class="form-control" type="text" value="{{isset($config->title) ? $config->title : ''}}" id="title" name="title" placeholder="Tiêu đề bắt buộc phải nhập nội dung">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="sub_title" class="col-12 col-lg-12 col-xl-3 col-form-label">Tiêu đề phụ:</label>
                                    <div class="col-12 col-lg-12 col-xl-9">
                                        <input class="form-control" type="text" value="{{isset($config->sub_title) ? $config->sub_title : ''}}" id="sub_title" name="sub_title" placeholder="Tiêu đề phụ bắt buộc phải nhập nội dung">
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label class="col-md-3 control-label">Step</label>
                                    <div class="col-md-9">
                                        <div id="div_slider">
                                            @if(isset($config->place))
                                            @foreach($config->place as $key=>$place)
                                            <div class="row m-1 row_slider" id="div_slider_{{$key}}">

                                                <div class="col-md-5">
                                                    <input class="form-control" placeholder="Nhập tên cơ sở vào đây" name="place[{{$key}}][name]" id="name_{{$key}}" value="{{isset($place->name) ? $place->name : ''}}" />
                                                </div>
                                                <div class="col-md-6">
                                                    <input class="form-control" placeholder="Nhập địa chỉ cơ sở vào đây" name="place[{{$key}}][address]" id="address_{{$key}}" value="{{isset($place->address) ? $place->address : ''}}" />
                                                </div>

                                                @if($key !== 0)
                                                <div class="col-md-1">
                                                    <a href="javascript:;" class="btn btn-danger delete_row" style="width: 43px; height:39px;">
                                                        <i class="flaticon2-delete"></i>
                                                    </a>
                                                </div>
                                                @endif
                                                <div class="col-md-4 mt-2">
                                                    <input class="form-control" placeholder="Nhập số điện thoại" name="place[{{$key}}][hotline]" id="hotline_{{$key}}" value="{{isset($place->hotline) ? $place->hotline : ''}}" />
                                                </div>
                                                <div class="col-md-4 mt-2">
                                                    <input class="form-control" placeholder="Nhập email cơ sở vào đây" name="place[{{$key}}][email]" id="email_{{$key}}" value="{{isset($place->email) ? $place->email : ''}}" />
                                                </div>
                                                <div class="col-md-4 mt-2">
                                                    <select id="city_{{$key}}" class="form-control" name="place[{{$key}}][city]">
                                                          <option value="TPHCM" {{isset($place->city) && $place->city == 'TPHCM'  ? 'selected' : ''}}> Hồ Chí Minh</option>
                                                          <option value="HN" {{isset($place->city) && $place->city == 'HN'  ? 'selected' : ''}}> Hà Nội</option>         
                                                    </select>
                                                    
                                                </div>
                                                <div class="col-md-6 mt-2">
                                                    <input class="form-control" placeholder="Nhập số vĩ độ cơ sở vào đây" name="place[{{$key}}][lat]" id="lat_{{$key}}" value="{{isset($place->lat) ? $place->lat : ''}}" />
                                                </div>
                                                <div class="col-md-6 mt-2">
                                                    <input class="form-control" placeholder="Nhập kinh độ cơ sở vào đây" name="place[{{$key}}][long]" id="long_{{$key}}" value="{{isset($place->long) ? $place->long : ''}}" />
                                                </div>
                                            </div>
                                            @endforeach
                                            @else
                                            <div class="row m-1" id="div_slider_0">
                                                <div class="col-md-5">
                                                    <input class="form-control" placeholder="Nhập tên cơ sở vào đây" name="place[0][name]" id="name_0"  />
                                                </div>
                                                <div class="col-md-6">
                                                    <input class="form-control" placeholder="Nhập địa chỉ cơ sở vào đây" name="place[0][address]" id="address_0" />
                                                </div>
                                                <div class="col-md-4 mt-2">
                                                    <input class="form-control" placeholder="Nhập số điện thoại" name="place[0][hotline]" id="hotline_0" />
                                                </div>
                                                <div class="col-md-4 mt-2">
                                                    <input class="form-control" placeholder="Nhập email cơ sở vào đây" name="place[0][email]" id="email_0" />
                                                </div>
                                                <div class="col-md-4 mt-2">
                                                    <select id="city_0" class="form-control" name="place[0][city]">
                                                          <option value="TPHCM"> Hồ Chí Minh</option>
                                                          <option value="HN"> Hà Nội</option>         
                                                    </select>
                                                    
                                                </div>
                                                <div class="col-md-6 mt-2">
                                                    <input class="form-control" placeholder="Nhập số vĩ độ cơ sở vào đây" name="place[0][lat]" id="lat_0" />
                                                </div>
                                                <div class="col-md-6 mt-2">
                                                    <input class="form-control" placeholder="Nhập kinh độ cơ sở vào đây" name="place[0][long]" id="long_0" />
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                        <hr>
                                        <a href="javascript:;" class="btn btn-success mt-repeater-add pull-right" onclick="add_attribute()">
                                            <i class="fa fa-plus"></i> Thêm Place
                                        </a>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="content" class="col-12 col-lg-12 col-xl-3 col-form-label"></label>
                                    <button type="button" class="btn btn-primary" id="btn_edit"><i class="la la-save"></i> Lưu dữ liệu
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <!-- end:: Content -->
</div>
@endsection
@section('script')
<!--end::Page Vendors -->
<script src="assets/js/pages/crud/forms/widgets/bootstrap-datetimepicker.js" type="text/javascript"></script>
<script src="admin/js/pages/block/config/config-contact-block-2.js?v1.4" type="text/javascript"></script>
<script src="admin/plugins/fancybox/jquery.fancybox.min.js"></script>
<script src="assets/js/pages/crud/file-upload/dropzonejs.js?v2" type="text/javascript"></script>
<script src="admin/plugins/fancybox/jquery.observe_field.js"></script>
<script src="http://keenthemes.com/preview/metronic/theme/assets/global/plugins/jquery-repeater/jquery.repeater.js" type="text/javascript"></script>

<script>
    $("#upload").dropzone({
        url: "/upload-image",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: 5,
        addRemoveLinks: !0,
        sending: function(file, xhr, formData) {
            formData.append("_token", "{{ csrf_token() }}");
        },
        removedfile: function(file) {
            var name = $("#image_1").val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                },
                type: 'POST',
                url: "destroy-image",
                data: {
                    filename: name
                },
                success: function(data) {
                    $("#image_1").val('');
                },
                error: function(e) {
                    console.log(e);
                }
            });
            var fileRef;
            return (fileRef = file.previewElement) != null ?
                fileRef.parentNode.removeChild(file.previewElement) : void 0;
        },
        success: function(file, response) {
            if (response.success) {

            } else {
                alert('Có lỗi xảy ra, vui lòng thử lại sau');
            }
        },
        error: function(file, response) {
            alert('Có lỗi xảy ra, vui lòng thử lại sau');
        }
    });

    $('.iframe-btn').fancybox({
        'iframe': {
            'css': {
                'width': '90%',
                'height': '90%',
            }
        },
    });
</script>

<script>
    var total = {{isset($config->place) ? count($config->place) : 0}};

    function add_attribute() {
        total++;
        let container_html = `
                <div class="row m-1 row_slider">
                    <div class="col-md-5">
                        <input class="form-control" placeholder="Nhập tên cơ sở ${total} vào đây" name="place[${total}][name]" id="name_${total}"  />
                    </div>
                    <div class="col-md-6">
                        <input class="form-control" placeholder="Nhập địa chỉ cơ sở ${total} vào đây" name="place[${total}][address]" id="address_${total}" />
                    </div>
                    
                    <div class="col-md-1">
                        <a href="javascript:;" class="btn btn-danger delete_row" style="width: 43px; height:39px;">
                            <i class="flaticon2-delete"></i>
                        </a>
                    </div>
                    <div class="col-md-4 mt-2">
                        <input class="form-control" placeholder="Nhập số điện thoại cơ sở ${total} vào đây" name="place[${total}][hotline]" id="hotline_${total}" />
                    </div>
                    <div class="col-md-4 mt-2">
                        <input class="form-control" placeholder="Nhập email cơ sở ${total} vào đây" name="place[${total}][email]" id="email_${total}" />
                    </div>
                    <div class="col-md-4 mt-2">
                        <select id="city_${total}" class="form-control" name="place[${total}][city]">
                            <option value="TPHCM"> Hồ Chí Minh</option>
                            <option value="HN"> Hà Nội</option>
                        </select>
                    </div>
                    <div class="col-md-6 mt-2">
                        <input class="form-control" placeholder="Nhập số vĩ độ cơ sở ${total} vào đây" name="place[${total}][lat]" id="lat_${total}" />
                    </div>
                    <div class="col-md-6 mt-2">
                        <input class="form-control" placeholder="Nhập kinh độ cơ sở ${total} vào đây" name="place[${total}][long]" id="long_${total}" />
                    </div>
                </div>
            `;
        $("#div_slider").append(container_html);
    }

    $(document).on('click', 'a.delete_row', function() {
        if (!window.confirm('Bạn có chắc muốn xóa slider này đi không?')) return true;

        $(this).closest('.row_slider').remove();
    });
</script>

@endsection