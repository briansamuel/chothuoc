@extends('admin.index')
@section('page-header', 'bệnh')
@section('page-sub_header', 'Danh sách bệnh')
@section('style')

@endsection
@section('content')

<div class="row">

    <div class="kt-portlet" data-ktportlet="true">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="kt-font-brand flaticon2-line-chart"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    CRAWL DATA
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <a href="{{ route('news.add') }}" class="btn btn-brand kt-btn">
                        <i class="flaticon2-plus"></i>
                        CRAWL DATA CHOTHUOC
                    </a>

                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12 col-lg-9">
                <div class="kt-portlet">

                    <!--begin::Form-->
                    <div class="kt-form ">
                        <div class="kt-portlet__body">
                            <div class="kt-portlet__foot kt-align-right p-2">
                                <div>
                                    <button id="btn-clear-console" type="button" class="btn btn-primary disabled " disabled><i class="la la-desktop"></i> Xóa Màn Hình</button>
                                    <button id="btn-stop-crawl" type="button" class="btn btn-primary disabled" disabled><i class="la la-stop"></i> Tạm dừng</button>
                                    <button id="btn-restart" type="button" class="btn btn-primary disabled" disabled><i class="la la-share-alt-square"  ></i> Tiếp tục lấy</button>
                                    <button id="btn-crawl-story" type="button" class="btn btn-primary"><i class="la la-hand-o-right"></i> Lấy bệnh</button>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="story_description" class="col-12 col-lg-12 col-xl-3 col-form-label">Trang bệnh cần lấy</label>
                                <div class="col-12 col-lg-12 col-xl-9">
                                    <textarea class="form-control" id="page_story" rows="8" name="page_story"></textarea>

                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="story_description" class="col-12 col-form-label"><strong>Trang hiện tại :</strong> <span class="current-page kt-font-danger kt-font-italic"></span></label>
                                <label for="story_description" class="col-12 col-form-label"><strong>Kết quả :</strong> <span class="result-message"></span></label>
                                <div class="col-12 ">
                                    <label  class="col-12 col-form-label"><strong>Danh sách bệnh :</strong></label>
                                    <div id="list_story" class="kt-section">

                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-striped progress-bar-animated  bg-success" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-3">
                <div class="kt-portlet">

                    <!--begin::Form-->
                    <div class="kt-form ">
                        <div class="kt-portlet__body">

                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

    </div>
    @endsection
    @section('script')
    <!--begin::Page Vendors(used by this page) -->


    <!--end::Page Vendors -->
    <script src="admin/js/pages/crawlers/index.js" type="text/javascript"></script>


    <!-- <script src="assets/js/pages/crud/metronic-datatable/advanced/record-selection.js" type="text/javascript"></script> -->

    @endsection