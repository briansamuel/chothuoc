@extends('admin.index')
@section('page-header', 'Bình luận')
@section('page-sub_header', 'Cập nhập bình luận')
@section('style')

@endsection
@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Bình luận </h3>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <div class="row">
                <div class="col-md-9">
                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Bình luận
                                </h3>
                            </div>
                        </div>

                        <!--begin::Form-->
                        <form class="kt-form">
                            <div class="kt-portlet__body">
                                <div class="form-group form-group-xs row">
                                    <label class="col-4 col-form-label">Thời gian:</label>
                                    <div class="col-8">
                                        <span class="form-control-plaintext kt-font-bolder">{{$commentInfo->created_at}}</span>
                                    </div>
                                </div>
                                <div class="form-group form-group-xs row">
                                    <label class="col-4 col-form-label">Thuốc:</label>
                                    <div class="col-8">
                                        <span class="form-control-plaintext kt-font-bolder">{{$commentInfo->medicine_name}}</span>
                                    </div>
                                </div>
                                <div class="form-group form-group-xs row">
                                    <label class="col-4 col-form-label">Người bình luận:</label>
                                    <div class="col-8">
                                        <span class="form-control-plaintext kt-font-bolder">{{$commentInfo->name_guest}}</span>
                                    </div>
                                </div>
                                <div class="form-group form-group-xs row">
                                    <label class="col-4 col-form-label">Số điện thoại:</label>
                                    <div class="col-8">
                                        <span class="form-control-plaintext kt-font-bolder">{{$commentInfo->phone_number_guest}}</span>
                                    </div>
                                </div>
                                <div class="form-group form-group-xs row">
                                    <label class="col-4 col-form-label">Email:</label>
                                    <div class="col-8">
                                        <span class="form-control-plaintext kt-font-bolder">{{$commentInfo->email_guest}}</span>
                                    </div>
                                </div>

                                <div class="form-group form-group-xs row">
                                    <label class="col-4 col-form-label">Nội dung:</label>
                                    <div class="col-8">
                                        <textarea rows="5" class="form-control" disabled="">{{$commentInfo->comment_content}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <!--end::Form-->
                    </div>

                </div>
                <div class="col-md-3">
                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Cập Nhập
                                </h3>
                            </div>
                        </div>

                        <!--begin::Form-->
                        <form class="kt-form" id="kt_edit_form">
                            <div class="kt-portlet__body">
                                <div class="form-group">
                                    <label>Status:</label>
                                    {{csrf_field()}}
                                    <input type="hidden" name="_id" id="_id" value="{{$commentInfo->id}}" />
                                    <select class="form-control kt-select2" id="comment_status" name="comment_status">
                                        <option value="pending" {{$commentInfo->comment_status === 'pending' ? 'selected' : ''}}>Chờ duyệt</option>
                                        <option value="publish" {{$commentInfo->comment_status === 'publish' ? 'selected' : ''}}>Đã duyệt</option>
                                        <option value="block" {{$commentInfo->comment_status === 'block' ? 'selected' : ''}}>Chặn</option>
                                    </select>
                                </div>
                                <div class="kt-form__actions">
                                    <button type="button" id="btn_edit" class="btn btn-primary">Cập nhập</button>
                                    <button type="reset" class="btn btn-secondary">Hủy bỏ</button>
                                </div>
                            </div>
                        </form>

                        <!--end::Form-->
                    </div>
                </div>
            </div>
        </div>

        <!-- end:: Content -->
</div>
@endsection
@section('vendor-script')

    <script src="assets/plugins/custom/tinymce/tinymce.bundle.js" type="text/javascript"></script>
@endsection
@section('script')
    <!--end::Page Vendors -->
    <script src="admin/js/pages/comments/edit-comment.js?v4" type="text/javascript"></script>
    <script src="assets/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
    <script src="assets/js/pages/crud/forms/widgets/bootstrap-datepicker.js" type="text/javascript"></script>

@endsection
