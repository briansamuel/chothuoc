@extends('admin.index')
@section('page-header', 'Danh mục')
@section('page-sub_header', 'Thêm Danh Mục')
@section('style')
    <link rel="stylesheet" href="admin/plugins/fancybox/jquery.fancybox.min.css"/>
@endsection
@section('content')
<div class="row">
    @if ($errors->any())
    @foreach ($errors->all() as $error)
    <div class="col-12">
        <div class="alert alert-solid-danger alert-bold" role="alert">
            <div class="alert-text">{{ $error }}</div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>
    </div>
    @endforeach
    @endif

    <div class="col-12">
        @include('admin.elements.alert_flash')
    </div>
</div>
<div class="row">

    <div class="col-4">
        <form method="POST" action="{{ route('category.save')}}">
            <input type="hidden" name="category_type" id="category_type" value="{{ Request()->query('type') ? Request()->query('type') : 'category_of_news' }}">
            <div class="kt-portlet" data-ktportlet="true">
                <div class="kt-portlet__body">

                    {{ csrf_field()}}
                    <div class="form-group">
                        <label>Tên danh mục:</label>
                        <input type="text" class="form-control" placeholder="Nhập tên danh mục" name="category_name">
                    </div>
                    <div class="form-group">
                        <label>Slug danh mục:</label>
                        <input type="text" class="form-control" placeholder="Nhập slug danh mục" name="category_slug">
                    </div>
                    <div class="form-group">
                        <label>Danh mục cha:</label>
                        <select class="form-control kt-selectpicker" name="category_parent">
                            <option value="0">Để trống</option>
                            @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Mô tả:</label>
                        <textarea type="text" class="form-control" placeholder="" rows="5" name="category_description"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Thumbnail:</label>
                        <a id="div_image"
                           data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=category_thumbnail&lang=vi&akey=@filemanager_get_key()"
                           class="iframe-btn" data-fancybox data-fancybox data-type="iframe"
                           href="javascript:;">
                                <img style="width:200px" id="preview_thumbnail" class="img-fluid"
                                     src="admin/images/upload-thumbnail.png">
                        </a>
                        <input type="hidden" name="category_thumbnail" id="category_thumbnail" value="">
                    </div>
                    <div class="form-group">
                        <label>Icon:</label>
                        <a id="div_image"
                           data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=category_icon&lang=vi&akey=@filemanager_get_key()"
                           class="iframe-btn" data-fancybox data-fancybox data-type="iframe"
                           href="javascript:;">
                                <img style="width:200px" id="preview_icon" class="img-fluid"
                                     src="admin/images/upload-thumbnail.png">
                        </a>
                        <input type="hidden" name="category_icon" id="category_icon" value="">
                    </div>

                </div>
                <div class="kt-portlet__foot kt-align-right p-3">
                    <div>
                        <button type="submit" class="btn btn-primary"><i class="la la-save"></i> Thêm danh mục</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="col-8">
        <div class="kt-portlet" data-ktportlet="true">
            <div class="kt-portlet__head kt-portlet__head--lg">
                <div class="kt-portlet__head-label">
                    <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                    </span>
                    <h3 class="kt-portlet__head-title">
                        TÌM KIẾM CATEGORY
                    </h3>
                </div>

            </div>
            <form class="kt-form" method="POST">
                <div class="kt-portlet__body">

                    <!--begin: Search Form -->
                    <div class="kt-form kt-form--label-right kt-margin-t-20 kt-margin-b-10">
                        <div class="row align-items-center">
                            <div class="col-xl-12 order-2 order-xl-1">
                                <div class="row align-items-center">
                                    <div class="col-md-12 kt-margin-b-20-tablet-and-mobile">

                                        <div class="kt-input-icon kt-input-icon--left">
                                            <input type="text" class="form-control" placeholder="Vui lòng nhập từ khóa..." id="category_name">
                                            <span class="kt-input-icon__icon kt-input-icon__icon--left">
                                                <span><i class="la la-search"></i></span>
                                            </span>
                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>

                    <!--end: Search Form -->

                    <!--begin: Selected Rows Group Action Form -->
                    <div class="kt-form kt-form--label-align-right kt-margin-t-20 collapse" id="kt_datatable_group_action_form1">
                        <div class="row align-items-center">
                            <div class="col-xl-12">
                                <div class="kt-form__group kt-form__group--inline">
                                    <div class="kt-form__label kt-form__label-no-wrap">
                                        <label class="kt-font-bold kt-font-danger-">Chọn
                                            <span id="kt_datatable_selected_number1">0</span> dòng:</label>
                                        {{csrf_field()}}
                                    </div>
                                    <div class="kt-form__control">
                                        <div class="btn-toolbar">

                                            <button class="btn btn-sm btn-danger" type="button" id="kt_datatable_delete_all">Xóa Tất Cả</button>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!--end: Selected Rows Group Action Form -->
                </div>
                <div class="kt-portlet__body kt-portlet__body--fit">

                    <!--begin: Datatable -->
                    <div class="kt-datatable" id="server_category_selection"></div>

                    <!--end: Datatable -->
                </div>

            </form>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="assets/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
    <script src="assets/js/pages/crud/forms/widgets/bootstrap-select.js" type="text/javascript"></script>
    <script src="admin/js/pages/news/categories.js?v1" type="text/javascript"></script>
    <script src="assets/js/pages/crud/file-upload/dropzonejs.js?v1" type="text/javascript"></script>
    <script src="admin/plugins/fancybox/jquery.fancybox.min.js"></script>
    <script src="admin/plugins/fancybox/jquery.observe_field.js"></script>
    <script>
        $("#upload").dropzone({
            url: "/upload-image",
            paramName: "file",
            maxFiles: 1,
            maxFilesize: 5,
            addRemoveLinks: !0,
            sending: function (file, xhr, formData) {
                formData.append("_token", "{{ csrf_token() }}");
            },
            removedfile: function (file) {
                var name = $("#image").val();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    type: 'POST',
                    url: "destroy-image",
                    data: {filename: name},
                    success: function (data) {
                        $("#image").val('');
                    },
                    error: function (e) {
                        console.log(e);
                    }
                });
                var fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },
            success: function (file, response) {
                if (response.success) {
                    $("#image").val(response.url);
                } else {
                    alert('Có lỗi xảy ra, vui lòng thử lại sau');
                }
            },
            error: function (file, response) {
                alert('Có lỗi xảy ra, vui lòng thử lại sau');
            }
        });

        $('.iframe-btn').fancybox({
            'iframe': {
                'css': {
                    'width': '90%',
                    'height': '90%',
                }
            },
        });

        $(document).ready(function () {

            $("#category_icon").observe_field(1, function () {
                $('#preview_icon').attr('src', this.value).show();
            });

            $("#category_thumbnail").observe_field(1, function () {
                $('#preview_thumbnail').attr('src', this.value).show();
            });

        });
    </script>

@endsection
