@extends('admin.index')
@section('page-header', 'Danh mục tin tức')
@section('page-sub_header', 'Thêm Danh Mục')
@section('style')
    <link rel="stylesheet" href="admin/plugins/fancybox/jquery.fancybox.min.css"/>
@endsection
@section('content')
<div class="row">
    @if ($errors->any())
    @foreach ($errors->all() as $error)
    <div class="col-12">
        <div class="alert alert-solid-danger alert-bold" role="alert">
            <div class="alert-text">{{ $error }}</div>
            <div class="alert-close">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true"><i class="la la-close"></i></span>
                </button>
            </div>
        </div>
    </div>
    @endforeach
    @endif

    <div class="col-12">
        @include('admin.elements.alert_flash')
    </div>
</div>
<div class="row">

    <div class="col-xl-8 col-sm-12 col-md-12">
        <form method="POST" action="{{ route('category.save')}}">
        
            <div class="kt-portlet" data-ktportlet="true">
                <div class="kt-portlet__body">

                    {{ csrf_field()}}
                    <input type="hidden" name="id" value="{{ $category->id }}" />
                    <input type="hidden" name="category_type" value="category_of_news" />
                    <div class="form-group">
                        <label>Tên danh mục:</label>
                        <input type="text" class="form-control" placeholder="Nhập tên danh mục" name="category_name" value="{{ $category->category_name }}">
                    </div>
                    <div class="form-group">
                        <label>Slug danh mục:</label>
                        <input type="text" class="form-control" placeholder="Nhập slug danh mục" name="category_slug" value="{{ $category->category_slug }}">
                    </div>
                    <div class="form-group">
                        <label>Danh mục cha:</label>
                        <select class="form-control kt-selectpicker" name="category_parent">
                            <option value="0">Để trống</option>
                            @foreach($categories as $category_i)
                            <option value="{{ $category_i->id }}">{{ $category_i->category_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Mô tả:</label>
                        <textarea type="text" class="form-control" placeholder="" rows="5" name="category_description" >{{ $category->category_description }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>Thumbnail:</label>
                        <div class="col-12">
                            <a id="div_image"
                               data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=category_thumbnail&lang=vi&akey=@filemanager_get_key()"
                               class="iframe-btn" data-fancybox data-fancybox data-type="iframe"
                               href="javascript:;">
                                @if(isset($category->category_thumbnail) && $category->category_thumbnail !== '')
                                    <img style="width:200px" id="preview_thumbnail" class="img-fluid"
                                         src="{{ $category->category_thumbnail }}">
                                @else
                                    <img style="width:200px" id="preview_thumbnail" class="img-fluid"
                                         src="admin/images/upload-thumbnail.png">
                                @endif
                            </a>
                            <input type="hidden" name="category_thumbnail" id="category_thumbnail" value="{{$category->category_thumbnail}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Icon:</label>
                        <div class="col-12">
                            <a id="div_image"
                               data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=category_icon&lang=vi&akey=@filemanager_get_key()"
                               class="iframe-btn" data-fancybox data-fancybox data-type="iframe"
                               href="javascript:;">
                                @if(isset($category->category_icon) && $category->category_icon !== '')
                                    <img style="width:200px" id="preview_icon" class="img-fluid"
                                         src="{{ $category->category_icon }}">
                                @else
                                    <img style="width:200px" id="preview_icon" class="img-fluid"
                                         src="admin/images/upload-thumbnail.png">
                                @endif
                            </a>
                            <input type="hidden" name="category_icon" id="category_icon" value="{{$category->category_icon}}">
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__foot kt-align-right p-3">
                    <div>
                        <button type="submit" class="btn btn-primary"><i class="la la-save"></i> Lưu danh mục</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

</div>
@endsection
@section('script')
<!-- <script src="admin/js/news.js" type="text/javascript"></script> -->
<script src="assets/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
<script src="assets/js/pages/crud/forms/widgets/bootstrap-select.js" type="text/javascript"></script>
<script src="admin/js/pages/news/categories.js" type="text/javascript"></script>
<script src="assets/js/pages/crud/file-upload/dropzonejs.js?v1" type="text/javascript"></script>
<script src="admin/plugins/fancybox/jquery.fancybox.min.js"></script>
<script src="admin/plugins/fancybox/jquery.observe_field.js"></script>
<script>
    $("#upload").dropzone({
        url: "/upload-image",
        paramName: "file",
        maxFiles: 1,
        maxFilesize: 5,
        addRemoveLinks: !0,
        sending: function (file, xhr, formData) {
            formData.append("_token", "{{ csrf_token() }}");
        },
        removedfile: function (file) {
            var name = $("#image").val();
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': "{{csrf_token()}}"
                },
                type: 'POST',
                url: "destroy-image",
                data: {filename: name},
                success: function (data) {
                    $("#image").val('');
                },
                error: function (e) {
                    console.log(e);
                }
            });
            var fileRef;
            return (fileRef = file.previewElement) != null ?
                fileRef.parentNode.removeChild(file.previewElement) : void 0;
        },
        success: function (file, response) {
            if (response.success) {
                $("#image").val(response.url);
            } else {
                alert('Có lỗi xảy ra, vui lòng thử lại sau');
            }
        },
        error: function (file, response) {
            alert('Có lỗi xảy ra, vui lòng thử lại sau');
        }
    });

    $('.iframe-btn').fancybox({
        'iframe': {
            'css': {
                'width': '90%',
                'height': '90%',
            }
        },
    });

    $(document).ready(function () {

        $("#category_icon").observe_field(1, function () {
            $('#preview_icon').attr('src', this.value).show();
        });

        $("#category_thumbnail").observe_field(1, function () {
            $('#preview_thumbnail').attr('src', this.value).show();
        });

    });
</script>

@endsection
