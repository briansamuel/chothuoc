@extends('admin.index')
@section('page-header', 'Thuốc')
@section('page-sub_header', 'Cập nhập thuốc')
@section('style')
    <link rel="stylesheet" href="admin/plugins/fancybox/jquery.fancybox.min.css" />
@endsection
@section('content')
    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

        <!-- begin:: Subheader -->
        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
            <div class="kt-container  kt-container--fluid ">
                <div class="kt-subheader__main">
                    <h3 class="kt-subheader__title">
                        Thuốc </h3>
                </div>
            </div>
        </div>

        <!-- end:: Subheader -->

        <!-- begin:: Content -->
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <form class="kt_form" id="kt_edit_form">
            <div class="row">
                <!--begin::Form-->
                <div class="col-md-8 col-lg-9">

                    <!--begin::Portlet-->
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Cập nhập thuốc
                                </h3>
                            </div>
                        </div>

                        <div class="kt-portlet__body">
                            <div class="form-group">
                                <label>Name:</label>
                                {{csrf_field()}}
                                <input type="hidden" name="_id" id="_id" value="{{$detail->id}}" />
                                <input type="text" class="form-control" placeholder="Nhập tên nhóm" name="name" value="{{$detail->name}}">
                            </div>
                            <div class="form-group">
                                <label>Giá:</label>
                                <input type="text" class="form-control" placeholder="Nhập giá thuốc" name="price" value="{{$detail->price}}">
                            </div>
                            <div class="form-group">
                                <label>Hoạt chất:</label>
                                <div style="height: 200px; overflow: scroll;">
                                    @foreach($ingredients as $ingredient)
                                        <label class="kt-checkbox kt-checkbox--success">
                                            <input type="checkbox" value="{{$ingredient->id}}" name="ingredient[]" {{in_array($ingredient->id, $ingredientMedicine) ? 'checked' : ''}}> {{$ingredient->name}}
                                            <span></span>
                                        </label>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Trích dẫn:</label>
                                <textarea class="form-control tox-target" name="instruction" id="instruction" placeholder="Vui lòng nhập trích dẫn" >{{$detail->instruction}}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Mô tả:</label>
                                <textarea class="form-control tox-target" name="content" id="content" placeholder="Vui lòng nhập mô tả">{{$detail->content}}</textarea>
                            </div>
                            <div class="form-group">
                                <label>Chọn danh mục</label>
                                @if(isset($categories) && count($categories) > 0)
                                    <select name="category_id" class="form-control" id="kt_multipleselectsplitter_1">
                                        @forelse($categories as $category)
                                            <optgroup label="{{$category->category_name}}">
                                                @forelse($category->item as $category_item)
                                                    <option value="{{$category_item->id}}" {{$detail->category_id == $category_item->id ? 'selected' : ''}}>{{$category_item->category_name}}</option>
                                                @empty
                                                @endforelse
                                            </optgroup>
                                        @empty
                                            <option value="">Không có danh mục nào</option>
                                        @endforelse
                                    </select>
                                @else
                                    <span class="form-control">Không có danh mục nào</span>
                                @endif
                            </div>

                            <div class="form-group row">
                                <label class="col-md-3 control-label">Thông số kỹ thuật</label>
                                <div class="col-md-12">
                                    <div id="div_specifications">
                                        @if(isset($specifications))
                                            @foreach($specifications as $key => $specification)
                                            <div class="row row_specifications" id="div_specifications_{{$key}}">
                                                <div class="col-md-5" style="margin-top:5px">
                                                    <input class="form-control" placeholder="Nhập tên thông số vào đây" name="specifications[{{$key}}][label]" id="label_{{$key}}" value="{{isset($specification->label) ? $specification->label : ''}}"/>
                                                </div>
                                                <div class="col-md-6" style="margin-top:5px">
                                                    <input class="form-control" placeholder="Nhập giá trị thông số vào đây" name="specifications[{{$key}}][value]" id="value_{{$key}}" value="{{isset($specification->value) ? $specification->value : ''}}"/>
                                                </div>
                                            </div>
                                            <hr>
                                            @endforeach
                                        @else
                                            <div class="row row_specifications" id="div_specifications_0">
                                                <div class="col-md-5" style="margin-top:5px">
                                                    <input class="form-control" placeholder="Nhập tên thông số vào đây" name="specifications[0][label]" id="label_0" />
                                                </div>
                                                <div class="col-md-6" style="margin-top:5px">
                                                    <input class="form-control" placeholder="Nhập giá trị thông số vào đây" name="specifications[0][value]" id="value_0" />
                                                </div>
                                            </div>
                                            <hr>
                                        @endif
                                    </div>
                                    <a href="javascript:;" class="btn btn-success mt-repeater-add pull-right" onclick="add_attribute()">
                                        <i class="fa fa-plus"></i> Thêm thông số
                                    </a>
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Từ khóa (SEO):</label>
                                <input type="text" name="keyword" id="keyword" class="form-control" value="{{$detail->keyword}}" placeholder="Vui lòng nhập từ khóa">
                            </div>
                            <div class="form-group">
                                <label>Mô tả (SEO):</label>
                                <textarea class="form-control" name="description" id="description" placeholder="Vui lòng nhập mô tả">{{$detail->description}}</textarea>
                            </div>
                        </div>
                    </div>
                    <!--end::Portlet-->
                </div>
                <div class="col-md-4 col-lg-3">
                    <div class="kt-portlet">
                        <div class="kt-portlet__body p-3">
                            <div class="form-group row mb-3">
                                <label>Nhãn hiệu:</label>
                                <select name="brand_id" class="form-control">
                                    <option value="">Vui lòng chọn</option>
                                    @foreach($brands as $brand)
                                        <option value="{{$brand->id}}" {{$detail->brand_id == $brand->id ? 'selected' : ''}}>{{$brand->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group row mb-3">
                                <label>Xuất sứ:</label>
                                <select name="country_id" class="form-control">
                                    <option value="">Vui lòng chọn</option>
                                    @foreach($countries as $country)
                                        <option value="{{$country->id}}" {{$detail->country_id == $country->id ? 'selected' : ''}}>{{$country->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group row mb-3">
                                <label>Đóng gói:</label>
                                <select name="package_id" class="form-control">
                                    <option value="">Vui lòng chọn</option>
                                    @foreach($packages as $package)
                                        <option value="{{$package->id}}" {{$detail->package_id == $package->id ? 'selected' : ''}}>{{$package->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group row mb-3">
                                <label>Kích hoạt:</label>
                                <select name="is_active" class="form-control">
                                    <option value="1" {{$detail->is_active === 1 ? 'selected' : ''}}>Yes</option>
                                    <option value="0" {{$detail->is_active === 0 ? 'selected' : ''}}>No</option>
                                </select>
                            </div>
                            <div class="form-group row mb-3">
                                <label>Số lượng:</label>
                                <input type="text" class="form-control" name="total" placeholder="Vui lòng nhập số lượng" value="{{$detail->total}}" />
                            </div>
                            <div class="form-group row mb-3">
                                <label>Ưu tiên:</label>
                                <input type="text" class="form-control" name="sort_order" value="{{$detail->sort_order}}"/>
                            </div>
                            <div class="form-group row mb-3">
                                <a id="div_image"
                                   data-src="@filemanager_get_resource(dialog.php)?type=1&field_id=image&lang=vi&akey=@filemanager_get_key()"
                                   class="iframe-btn" data-fancybox data-fancybox data-type="iframe"
                                   href="javascript:;">
                                    @if(isset($detail->image) && $detail->image != '')
                                        <img id="preview_thumbnail" class="img-fluid"
                                             src="{{ $detail->image }}">
                                    @else
                                        <img id="preview_thumbnail" class="img-fluid"
                                             src="admin/images/upload-thumbnail.png">
                                    @endif
                                </a>
                                <input type="hidden" name="image" id="image" value={{$detail->image}}>
                            </div>
                        </div>
                        <div class="kt-portlet__foot kt-align-right p-2">
                            <div>
                                <button type="submit" id="btn_edit" class="btn btn-primary"><i class="la la-save"></i> Lưu dữ liệu</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>

        <!-- end:: Content -->
    </div>
@endsection
@section('vendor-script')
    <script src="assets/plugins/custom/tinymce/tinymce.bundle.js" type="text/javascript"></script>
@endsection
@section('script')
    <script src="assets/js/pages/custom/medicine/edit-medicine.js?v1.2" type="text/javascript"></script>
    <script src="assets/js/pages/crud/forms/widgets/select2.js" type="text/javascript"></script>
    <script src="assets/js/pages/crud/forms/widgets/bootstrap-daterangepicker.js" type="text/javascript"></script>
    <script src="assets/js/pages/crud/forms/widgets/bootstrap-multipleselectsplitter.js" type="text/javascript"></script>
    <script src="assets/js/pages/crud/file-upload/dropzonejs.js?v2" type="text/javascript"></script>
    <script src="admin/plugins/fancybox/jquery.fancybox.min.js"></script>
    <script src="admin/plugins/fancybox/jquery.observe_field.js"></script>

    <script>
        ! function(t) {
            var e = {};

            function n(i) {
                if (e[i]) return e[i].exports;
                var r = e[i] = {
                    i: i,
                    l: !1,
                    exports: {}
                };
                return t[i].call(r.exports, r, r.exports, n), r.l = !0, r.exports
            }
            n.m = t, n.c = e, n.d = function(t, e, i) {
                n.o(t, e) || Object.defineProperty(t, e, {
                    enumerable: !0,
                    get: i
                })
            }, n.r = function(t) {
                "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(t, Symbol.toStringTag, {
                    value: "Module"
                }), Object.defineProperty(t, "__esModule", {
                    value: !0
                })
            }, n.t = function(t, e) {
                if (1 & e && (t = n(t)), 8 & e) return t;
                if (4 & e && "object" == typeof t && t && t.__esModule) return t;
                var i = Object.create(null);
                if (n.r(i), Object.defineProperty(i, "default", {
                    enumerable: !0,
                    value: t
                }), 2 & e && "string" != typeof t)
                    for (var r in t) n.d(i, r, function(e) {
                        return t[e]
                    }.bind(null, r));
                return i
            }, n.n = function(t) {
                var e = t && t.__esModule ? function() {
                    return t.default
                } : function() {
                    return t
                };
                return n.d(e, "a", e), e
            }, n.o = function(t, e) {
                return Object.prototype.hasOwnProperty.call(t, e)
            }, n.p = "", n(n.s = 677)
        }({
            677: function(t, e, n) {
                "use strict";
                var i = {
                    init: function() {
                        tinymce.init({
                            selector: "#content",
                            toolbar: !1,
                            statusbar: !1,
                            height: 200,
                            setup: function (editor) {
                                editor.on('change', function () {
                                    tinymce.triggerSave();
                                });
                            }
                        }), tinymce.init({
                            selector: "#instruction",
                            toolbar: !1,
                            statusbar: !1,
                            height: 200,
                            setup: function (editor) {
                                editor.on('change', function () {
                                    tinymce.triggerSave();
                                });
                            }
                        })
                    }
                };
                jQuery(document).ready((function() {
                    i.init()
                }))
            }
        });

        $("#upload").dropzone({
            url: "/upload-image",
            paramName: "file",
            maxFiles: 1,
            maxFilesize: 5,
            addRemoveLinks: !0,
            sending: function(file, xhr, formData) {
                formData.append("_token", "{{ csrf_token() }}");
            },
            removedfile: function(file)
            {
                var name = $("#image").val();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                    },
                    type: 'POST',
                    url: "destroy-image",
                    data: {filename: name},
                    success: function (data){
                        $("#image").val('');
                    },
                    error: function(e) {
                        console.log(e);
                    }});
                var fileRef;
                return (fileRef = file.previewElement) != null ?
                    fileRef.parentNode.removeChild(file.previewElement) : void 0;
            },
            success: function(file, response) {
                if(response.success) {
                    $("#image").val(response.url);
                } else {
                    alert('Có lỗi xảy ra, vui lòng thử lại sau');
                }
            },
            error: function(file, response) {
                alert('Có lỗi xảy ra, vui lòng thử lại sau');
            }
        });

        $('.iframe-btn').fancybox({
            'iframe': {
                'css': {
                    'width': '90%',
                    'height': '90%',
                }
            },
        });

        $(document).ready(function () {

            $("#image").observe_field(1, function () {
                $('#preview_thumbnail').attr('src', this.value).show();
            });

        });
    </script>

    <script>
        var total = {{isset($specifications) ? count($specifications) : 0}};
        function add_attribute() {
            total++;
            let container_html = `
                <div class="row" id="div_specifications_${total}">
                    <div class="col-md-5" style="margin-top:5px">
                        <input class="form-control" placeholder="Nhập tên thông số vào đây" name="specifications[${total}][label]" id="label_${total}" />
                    </div>
                    <div class="col-md-6" style="margin-top:5px">
                        <input class="form-control" placeholder="Nhập giá trị thông số vào đây" name="specifications[${total}][value]" id="value_${total}" />
                    </div>
                    <div class="col-md-1">
                        <a href="javascript:;" class="btn btn-danger delete_row" style="width: 43px; height:39px;">
                            <i class="flaticon2-delete"></i>
                        </a>
                    </div>
                </div>
                <hr>
            `;
            $("#div_specifications").append(container_html);
        }

        $(document).on('click', 'a.delete_row', function () {
            if(!window.confirm('Bạn có chắc muốn xóa dòng này đi không?')) return true;

            $(this).closest('.row_specifications').remove();
        });
    </script>
@endsection
