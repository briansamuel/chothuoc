<div class="nhombenh">
    <div class="inner">
        <div class="nhombenh-title">
            <h2>Tìm hiểu theo nhóm bệnh</h2>
        </div><!-- /.nhombenh-title -->
        <div class="nhombenh-container">
            @foreach($categories as $category)
            <a href="{{ $category->category_slug }}" title="{{ $category->category_name }}" class="nhombenh-item">
                {{ $category->category_name }}
                <span></span>
            </a><!-- /.nhombenh-item -->
            @endforeach
            
        </div><!-- /.nhombenh-container -->
    </div><!-- /.inner -->
</div><!-- /.nhombenh -->