@extends('frontsite.index')
@section('title', __('seo.disease.title'))
@section('title_ab', __('seo.disease.title_ab'))
@section('description', __('seo.disease.description'))
@section('keywords', __('seo.disease.keyword'))
@section('body_class', '_benh')
@section('style')

@endsection
@section('content')
<div id="wapper_main">
    <div class="content_main">
        <div class="content_content">
            @include('frontsite.elements.breadcrumb')
            <div class="benh-hot">
                <div class="inner">
                    <div class="nhombenh-title">
                        <h2>Các bệnh được quan tâm</h2>
                    </div><!-- /.nhombenh-title -->
                    <div class="benh-container">
                        @foreach($posts as $post)
                        <a href="/{{ $post->post_slug }}" title="{{ $post->post_title }}" class="benh-inner">
                            {{ $post->post_title }}
                        </a><!-- /.benh-inner -->
                        @endforeach
                        
                    </div><!-- /.benh-container -->
                </div><!-- /.inner -->
            </div><!-- /.benh-hot -->
            
            @include('frontsite.diseases.elements.group-disease')
            <div class="clear"></div>
            <div class="clear"></div>
        </div>
    </div>
</div>
@endsection
@section('script')


@endsection