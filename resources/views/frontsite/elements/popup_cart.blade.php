<!-- Modal content-->
<div class="modal-content">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Giỏ hàng của bạn</h4>
    </div>
    <div class="modal-body">

        <div class="giohang_group">
            @foreach($products as $product)
            <div class="product_box">

                <div class="product_info">
                    <div class="name">
                        <div title="Xóa sản phẩm này" class="deletesp" onclick="MuaHang('/modules/giohang/giohang_ajax.php?op=&masp={{ $product->id }}&method=del','loaddatacart')">x</div><a href="/san-pham/{{ $product->name }}" target="_blank" title="{{ $product->name }}">{{ $product->name }}</a>
                    </div>
                    <div class="product_img">
                        <img onerror="xulyloi(this)" src="{{ $product->image }}" alt="{{ $product->name }}" width="70" />
                    </div>

                    <div class="item">
                        <ul>
                            <li><input onchange="MuaHang('/modules/giohang/giohang_ajax.php?op=&masp={{ $product->id }}&method=update&sl='+this.value,'loaddatacart')" onkeyup="MuaHang('/modules/giohang/giohang_ajax.php?op=&masp={{ $product->id }}&method=update&sl='+this.value,'loaddatacart')" class="txt_soluong" name="C{{ $product->id }}" type="number" min="1" max="100000" value="{{ $product->quantity }}" size="4" maxlength="4" /></li>
                            <li>&nbsp; X {{number_format($product->price)}} đ = {{number_format($product->price*$product->quantity)}} đ</li>
                            <li class="li1">
                                <div title="Xóa thuộc tính này" class="deletesp" onclick="MuaHang('/modules/giohang/giohang_ajax.php?op=&masp={{ $product->id }}&method=delkho','loaddatacart')">x</div>
                            </li>
                        </ul>
                        <div class="clear"></div>
                        <div>
                            <ul class="ul-thuoctinhkho"></ul>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div class="product_line_nhe"></div>
            @endforeach
        </div>
        <div class="tongtien_giohang">
            Tổng tiền: {{number_format($total_price)}} đ
        </div>
        <div class="xemgiohang btn btn-success"><a href="/cart.htm" title="Thanh toán">Thanh toán</a></div>
        <div class="clear"></div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
    </div>
</div>