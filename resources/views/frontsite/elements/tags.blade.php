
<!-- Tags for post -->
<ul class="tags">
    <li class="tags_label"><i class="fa fa-tags"></i>Tags</li>
    @if(isset($tags) && count($tags) > 0)
    foreach($tags as $key => $tag)
    <li class="tagitem">
        <a href="https://www.chosithuoc.com/{{ $tag->slug }}.tag" title="{{ $tag->name }}">{{ $tag->name }}</a>
    </li>
    
    @endif
</ul>
<div class="clear"></div>