<div xmlns:v="http://rdf.data-vocabulary.org/" id="breadcrumbs">
    <ul itemprop="breadcrumb">
        <li typeof="v:Breadcrumb"><a property="v:title" rel="v:url" href="//{{ config('domain.web.domain') }}" title=""><label><i class="fa fa-home fa-2x"></i></label></a></li>
        @if(isset($breadcrumbs))
        @foreach($breadcrumbs as $key => $breadcrumb)
        <li typeof="v:Breadcrumb"><a property="v:title" rel="v:url" href="{{ $breadcrumb['slug'] }}" title="{{ $breadcrumb['title'] }}">{{ $breadcrumb['title'] }}</a></li>
        @endforeach
        @endif
    </ul>
</div>