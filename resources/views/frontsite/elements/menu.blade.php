<div id="wapper_menu">
<div class="_bgcolormain">
    <div class="ws-container">
        <div class="menu_content _bgcolormain">
            <div class="menuresponsive _bgcolormain" data-link="application/files/cotmobile.php">
                <div class="menuitemmain"><span class="open-menu-icon"></span>
                    <div class="clear"></div>
                </div>
            </div>
            <div id="drop-menu">
                <a href="/" id="menu-dropper__" title="Logo cst new">
                    <img src="uploads/quangcao/Logo-cst-new-.png" class="img-responsive" alt="Logo cst new" width="" height="" />
                </a>
            </div>
            <div id="cssmenu">
                <ul>
                    <li>
                        <a href="san-pham/" title="Danh mục"><span class="menu-link-name">Danh
                                mục</span></a>
                        <ul>
                            @foreach($categories as $category)
                            <li>
                                <a href="{{$category->category_slug}}/" title="{{$category->category_name}}">
                                    <span class="ws-icon" style="background-image:url('{{$category->category_icon}}');"></span><span class="menu-link-name">{{$category->category_name}}</span></a>
                                <ul>
                                    @foreach($category->item as $sub_cate)
                                    <li>
                                        <a href="{{$sub_cate->category_slug}}-{{$sub_cate->id}}/" title="{{$sub_cate->category_name}}"><span class="menu-link-name">{{$sub_cate->category_name}}</span></a>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                            @endforeach
                        </ul>
                    </li>
                    <li>
                        <a href="tim-hieu-benh/" title="Tìm hiểu bệnh"><span class="menu-link-name">Tìm hiểu bệnh</span></a>
                        <ul>
                            <li>
                                <a href="ung-thu/" title="Ung thư">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-cancer-35.png');"></span><span class="menu-link-name">Ung thư</span></a></li>
                            <li>
                                <a href="da-lieu/" title="Da liễu">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-da-lieu-35.png');"></span><span class="menu-link-name">Da liễu</span></a></li>
                            <li>
                                <a href="tim-mach/" title="Tim mạch">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-heart-beat-35.png');"></span><span class="menu-link-name">Tim mạch</span></a></li>
                            <li>
                                <a href="than-kinh/" title="Thần Kinh">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-brain-35.png');"></span><span class="menu-link-name">Thần Kinh</span></a></li>
                            <li>
                                <a href="ho-hap/" title="Hô Hấp">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-lungs-35.png');"></span><span class="menu-link-name">Hô Hấp</span></a></li>
                            <li>
                                <a href="co-xuong-khop/" title="Cơ, Xương khớp">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-strong-35.png');"></span><span class="menu-link-name">Cơ, Xương khớp</span></a></li>
                            <li>
                                <a href="tai-mui-hong/" title="Tai, Mũi, Họng">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-tai-mui-hong-35.png');"></span><span class="menu-link-name">Tai, Mũi, Họng</span></a></li>
                            <li>
                                <a href="tieu-hoa-gan-mat-tuy/" title="Tiêu hóa, gan mật, tụy">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-stomach-35.png');"></span><span class="menu-link-name">Tiêu hóa, gan mật, tụy</span></a></li>
                            <li>
                                <a href="san-phu/" title="Sản phụ">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-mom-35.png');"></span><span class="menu-link-name">Sản phụ</span></a></li>
                            <li>
                                <a href="than-nieu-nam-khoa/" title="Thận niệu, nam khoa">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-kidney-35.png');"></span><span class="menu-link-name">Thận niệu, nam khoa</span></a></li>
                            <li>
                                <a href="truyen-nhiem/" title="Truyền nhiễm">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-virus-35.png');"></span><span class="menu-link-name">Truyền nhiễm</span></a></li>
                            <li>
                                <a href="mat/" title="Mắt">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-eye-35.png');"></span><span class="menu-link-name">Mắt</span></a></li>
                            <li>
                                <a href="chung-ngua/" title="Chủng ngừa">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-tim-chung-35.png');"></span><span class="menu-link-name">Chủng ngừa</span></a></li>
                            <li>
                                <a href="tam-than-tam-ly/" title="Tâm thần, tâm lý">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-mentaly-35.png');"></span><span class="menu-link-name">Tâm thần, tâm lý</span></a></li>
                            <li>
                                <a href="di-truyen-mien-dich-di-ung/" title="Di truyền, miễn dịch, dị ứng">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-adn-35.png');"></span><span class="menu-link-name">Di truyền, miễn dịch, dị ứng</span></a>
                            </li>
                            <li>
                                <a href="noi-tiet/" title="Nội tiết">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-Endocrine-35.png');"></span><span class="menu-link-name">Nội tiết</span></a></li>
                            <li>
                                <a href="rang-ham-mat/" title="Răng hàm mặt">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-teeth-35.png');"></span><span class="menu-link-name">Răng hàm mặt</span></a></li>
                            <li>
                                <a href="dinh-duong/" title="Dinh dưỡng">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-nutrition-35.png');"></span><span class="menu-link-name">Dinh dưỡng</span></a></li>
                            <li>
                                <a href="huyet-hoc/" title="Huyết học">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-blood-35.png');"></span><span class="menu-link-name">Huyết học</span></a></li>
                            <li>
                                <a href="tre-em/" title="Trẻ em">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-baby-35.png');"></span><span class="menu-link-name">Trẻ em</span></a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="hoat-chat/" title="Hoạt chất"><span class="menu-link-name">Hoạt chất</span></a></li>
                    <li>
                        <a href="tin-tuc/" title="Tin tức"><span class="menu-link-name">Tin
                                tức</span></a>
                        <ul>
                            <li>
                                <a href="lam-dep/" title="Làm đẹp">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-beauty-35.png');"></span><span class="menu-link-name">Làm đẹp</span></a></li>
                            <li>
                                <a href="giam-can/" title="Giảm cân">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-diet-35.png');"></span><span class="menu-link-name">Giảm cân</span></a></li>
                            <li>
                                <a href="xuong-khop/" title="Xương khớp">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-joints-35.png');"></span><span class="menu-link-name">Xương khớp</span></a></li>
                            <li>
                                <a href="sinh-ly/" title="Sinh lý">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-strong-35.png');"></span><span class="menu-link-name">Sinh lý</span></a></li>
                            <li>
                                <a href="suc-khoe-tong-hop/" title="Sức khỏe tổng hợp">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-dietary-supplement-35.png');"></span><span class="menu-link-name">Sức khỏe tổng hợp</span></a></li>
                            <li>
                                <a href="chuong-trinh-khuyen-mai-739/" title="Chương trình khuyến mãi">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-gift-35.png');"></span><span class="menu-link-name">Chương trình khuyến mãi</span></a></li>
                            <li>
                                <a href="thi-truong-thuoc/" title="Thị trường thuốc">
                                    <span class="ws-icon" style="background-image:url('uploads/danhmuc/icon-drugstore.png');"></span><span class="menu-link-name">Thị trường thuốc</span></a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="nha-thuoc/" title="Nhà Thuốc"><span class="menu-link-name">Nhà Thuốc</span></a></li>
                </ul>
            </div>
            @if(Auth::guard('user')->check())
            <div class="dropdown ">
                <button class=" btn-signup dropdown-toggle btn-user-login" type="button" data-toggle="dropdown"><i class="fa fa-user"></i> Tài khoản
                <span class="caret"></span></button>
                <ul class="dropdown-menu dropdown-menu-right dropdown-user">
                    <li><a href="thong-tin-tai-khoan.htm"><i class="fa fa-user"></i> Thông tin tài khoản</a></li>
                    <li><a href="quan-ly-don-hang.htm"><i class="fa fa-cart-plus"></i> Quản lý đơn hàng</a></li>

                    <li><a href="san-pham-da-xem.htm"><i class="fa fa-eye"></i> Sản phẩm đã xem</a></li>
                    <li><a href="san-pham-yeu-thich.htm"><i class="fa fa-star"></i> Sản phẩm yêu thích</a></li>
                    <li><a href="san-pham-mua-sau.htm"><i class="fa fa-shopping-cart"></i> Sản phẩm mua sau</a></li>
                    <li><a href="javascript:if(confirm('Thoát khỏi tài khoản?')){location.href='logout.htm';} "><i class="fa fa-sign-out"></i> Thoát</a></li>
                </ul>
            </div>
            @else
            <div class="btn-user-login user-desktop">
                <a href="#" data-toggle="modal" data-target="#myModal" title="Đăng ký" onclick="Get_Data('ajax/?action=register','loaddatacart');">Đăng ký
                </a>/
                <a href="#" data-toggle="modal" data-target="#myModal" title="Đăng nhập" onclick="Get_Data('ajax/?action=login','loaddatacart');">Đăng nhập
                </a>
            </div>

            <div class="dropdown user-mobile">
                <button class=" btn-signup dropdown-toggle  btn-user-login" type="button" data-toggle="dropdown"><i class="fa fa-user"></i> Tài khoản
                    <span class="caret"></span></button>
                <ul class="dropdown-menu dropdown-menu-right dropdown-user">
                    <li><a href="#" data-toggle="modal" data-target="#myModal" title="Đăng ký" onclick="Get_Data('ajax/?action=register','loaddatacart');"><i class="fa fa-user"></i> Đăng ký</a></li>
                    <li><a href="#" data-toggle="modal" data-target="#myModal" title="Đăng nhập" onclick="Get_Data('ajax/?action=login','loaddatacart');"><i class="fa fa-cart-plus"></i> Đăng nhập</a></li>
                </ul>
            </div>
            @endif
            <div class="search-icon-mobile">
                <i class="fa fa-search" aria-hidden="true"></i>
            </div><!-- /.search-icon-mobile -->
        </div>
    </div>
</div>
</div><!-- /._bgcolormain -->
