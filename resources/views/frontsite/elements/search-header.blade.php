<div class="search-head-container">

    <div class="ws-container" style="background: url('uploads/images/banner-sp.png') no-repeat left top; background-size: cover;">
        <div class="search-header">
            <div class="inner">
                <form action="tim-kiem.htm" method="POST">
                    <div class="head">
                        <ul class="list-search-type">
                            <li class="active"><a href="javascript:void(0);" data-value="1" class="searchtype" title="">Tìm thuốc</a></li>
                            <li><a href="javascript:void(0);" data-value="5" class="searchtype" title="">Tìm
                                    bệnh</a></li>
                            <li><a href="javascript:void(0);" data-value="0" class="searchtype" title="">Tìm
                                    hoạt chất</a></li>
                            <input type="hidden" name="searchtype" value="1" />
                        </ul>
                    </div><!-- /.head -->
                    <div class="body">
                        <div class="input-group">
                            <input type="text" name="key" class="form-control" placeholder="Bạn muốn tìm gì?" autocomplete="off">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-tim">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span><!-- /.input-group-btn -->
                        </div><!-- /.input-group -->
                    </div><!-- /.body -->
                </form>
                <div id="resultSearch" class="qa-list"></div>
                <span class="hint">
                    <p class="keyword-popular">Từ khoá phổ biến: <a href="thuoc-bo-than/" target="_blank">Thuốc bổ thận</a>&nbsp;&nbsp;<a href="thuoc-giam-can">Thuốc giảm cân</a>&nbsp;&nbsp;<a href="thuoc-bo-nao/">Thuốc bổ não</a>&nbsp;&nbsp;<a href="thuoc-bo-khop/">Thuốc bổ khớp</a>&nbsp;&nbsp;<a href="tim-mach/">Bệnh tim mạch</a><a href="benh-ung-thu/">Bệnh ung thư</a></p>
                    <style type="text/css">
                        .keyword-popular {
                            font-size: 13px;
                            color: #000;
                            font-weight: bold
                        }

                        .keyword-popular a {
                            border: 1px solid #a42429;
                            text-decoration: none;
                            color: #000;
                            padding: 3px 12px;
                            font-weight: 400;
                            line-height: 18px;
                            margin: 2px;
                            display: inline-block
                        }

                        .keyword-popular a:hover {
                            color: #000;
                            background: #f7f7f7
                        }
                    </style>
                </span><!-- /.hint -->
            </div><!-- /.inner -->
        </div><!-- /.search-header -->
    </div><!-- /.ws-container -->
</div><!-- /.search-head-container -->
