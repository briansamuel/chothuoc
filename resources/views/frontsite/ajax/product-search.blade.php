<ul>
    @foreach($products as $key => $product)
    <li class="clearfix item-ls">
        <div class="image">
            <a href="san-pham/{{ $product->slug }}" title="">
                <img src="{{ $product->image }}" alt="{{ $product->name }}" class="img-thumbnail" width="50" onerror="this.style.display='none';">
            </a>
        </div>

        <div class="name">
            <a href="san-pham/{{ $product->slug }}" title="{{ $product->name }}">
                {{ $product->name }}
            </a>
            <br>
            <br>
            <span class="gia">
                Giá: {{number_format($product->price)}} đ
            </span>
        </div>

    </li>
    @endforeach
</ul>