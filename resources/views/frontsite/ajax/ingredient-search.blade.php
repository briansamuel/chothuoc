<ul>
    @foreach($ingredients as $ingredient)
    <li class="clearfix item-ls">
        

        <div class="name">
            <a href="{{ $ingredient->slug }}.hc" title="{{ $ingredient->name }}">
               {{ $ingredient->name }}
            </a>
            <br>
            <br>
        </div>
    </li>
    @endforeach
</ul>