<ul>
    @foreach($posts as $post)
    <li class="clearfix item-ls">
        <div class="image">
            <a href="{{ $post->post_slug }}" title="">
                <img src="{{ $post->post_thumbnail }}" alt="{{ $post->post_title }}" class="img-thumbnail" width="50" onerror="this.style.display='none';">
            </a>
        </div>

        <div class="name">
            <a href="{{ $post->post_slug }}" title="{{ $post->post_title }}">
               {{ $post->post_title }}
            </a>
            <br>
            <br>
        </div>
    </li>
    @endforeach
</ul>