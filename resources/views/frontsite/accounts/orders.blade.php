@extends('frontsite.index')
@section('title', __('seo.account_order.title_ab'))
@section('title_ab', __('seo.account_order.title_ab'))
@section('description', __('seo.account_order.description'))
@section('keywords', __('seo.account_order.keyword'))
@section('body_class', '_thanhvien')
@section('style')

@endsection
@section('content')
<div id="wapper_main">
    <div class="content_main">
        <div class="content_content">
            <div class="group-thanhvien">

                @include('frontsite.accounts.elements.menu')

                <div class="col-data">
                    <div class="titleuser"><i class="fa fa-user"></i> Danh sách đơn hàng</div>
                    <table class="table table-responsive">
                        <tbody>
                            <tr>
                                <th>STT</th>
                                <th>Mã Đơn hàng</th>
                                <th>Số tiền</th>
                                <th>Ngày đặt</th>
                                <th>Trạng thái</th>
                                <th>Thao tác</th>
                            </tr>
                            @if(isset($orders))
                            @foreach($orders as $key => $order)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>02{{ $order->id }}</td>
                                <td>{{ $order->price }}</td>
                                <td>{{ $order->created_at }}</td>
                                <td>{{ $order->status }}</td>
                                <td>
                                    <a href="#" data-target="#modaldonhangchitiet" data-toggle="modal" onclick="Get_Data('ajax/?action=viewdonhangchitiet&amp;id={{ $order->id }}','loaddonhangchitiet');" class="btn  btn-default">Xem chi tiết</a>
                                </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>

                    <div id="modaldonhangchitiet" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content" id="loaddonhangchitiet">

                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
@endsection
@section('script')


@endsection