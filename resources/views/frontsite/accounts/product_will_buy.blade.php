@extends('frontsite.index')
@section('title', __('seo.willbuy_product.title_ab'))
@section('title_ab', __('seo.willbuy_product.title_ab'))
@section('description', __('seo.willbuy_product.description'))
@section('keywords', __('seo.willbuy_product.keyword'))
@section('body_class', '_thanhvien')
@section('style')

@endsection
@section('content')
<div id="wapper_main">
    <div class="content_main">
        <div class="content_content">
            <div class="group-thanhvien">

                @include('frontsite.accounts.elements.menu')

                <div class="col-data">
                    <div class="titleuser"><i class="fa fa-user"></i> Sản phẩm đã xem</div>
                    @if(isset($medicines))
                    @foreach($medicines as $medicine)
                    <div class="itemsanpham">
                        <div class="img">
                            <a href="/san-pham/{{$medicine->slug}}" title="{{$medicine->name}}">
                                <img onerror="xulyloi(this);" src="{{$medicine->image}}" alt="{{$medicine->name}}">
                            </a>
                            <div class="icon_group">
                                <div class="iconsp icon_view">
                                    <a href="/san-pham/{{$medicine->slug}}" title="Thông tin sản phẩm">
                                        <i class="fa fa-info"></i>
                                    </a>
                                </div>
                                <div class="iconsp icon_cart">

                                    <a href="#" title="Đặt hàng" data-toggle="modal" data-target="#myModal" onclick="MuaHang('modules/giohang/giohang_ajax.php?id={{$medicine->id}}&amp;masp={{$medicine->id}}&amp;method=add','loaddatacart');">
                                        <i class="fa fa-cart-plus"></i>
                                    </a>
                                </div>
                                @if(Auth::guard('user'))
                                <div class="iconsp icon_view icon_yeuthich">
                                    <a onclick="Ajax_noreturn_swal('ajax/?action=sanphamyeuthich&amp;idsp={{$medicine->id}}','Đã thêm vào sản phẩm yêu thích');" href="javascript:" title="Sản phẩm yêu thích">
                                    <i class="fa fa-star"></i>
                                    </a>
                                </div>
                                @endif
                            </div>


                            <div class="masp_hover">
                                <div class="masp_hover_masp" data-toggle="tooltip" title="Mã số">
                                    <span>Mã số:</span> {{$medicine->id}}
                                </div>
                                <div class="masp_hover_view" data-toggle="tooltip" title="Số lần xem">
                                    <i class="fa fa-eye"></i> {{$medicine->count_view}}
                                </div>
                            </div><!-- /.masp_hover -->
                        </div><!-- /.img -->
                        <p class="tieude">
                            <a href="/san-pham/{{$medicine->slug}}" title="{{$medicine->name}}">{{$medicine->name}}</a>
                        </p>
                        <div class="rating-wrap">
                            <i class="fa fa-star "></i>
                            <i class="fa fa-star "></i>
                            <i class="fa fa-star "></i>
                            <i class="fa fa-star "></i>
                            <i class="fa fa-star "></i></div><!-- /.rating-wrap -->
                        <div class="gia" data-toggle="tooltip">{{number_format($medicine->price)}}&nbsp;đ</div>
                        <div class="giagoc" data-toggle="tooltip">{{number_format($medicine->price)}}&nbsp;đ</div>
                    </div><!-- /.itemsanpham -->
                    @endforeach
                    @endif
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
@endsection
@section('script')


@endsection