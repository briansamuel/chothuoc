@extends('frontsite.index')
@section('title', __('seo.account.title_ab'))
@section('title_ab', __('seo.account.title_ab'))
@section('description', __('seo.account.description'))
@section('keywords', __('seo.account.keyword'))
@section('body_class', '_thanhvien')
@section('style')

@endsection
@section('content')
<div id="wapper_main">
    <div class="content_main">
        <div class="content_content">
            <div class="group-thanhvien">
               
                @include('frontsite.accounts.elements.menu')

                <div class="col-data">
                    <div class="titleuser"><i class="fa fa-user"></i> Thông tin tài khoản</div>
                    <form action="" method="POST">
                        @csrf
                        <div class="row_user">
                            <div class="label-user">Họ và tên <span class="require">(*)</span></div>
                            <div class="input-user">
                                <input required="" name="full_name" class="form-control" value="{{ $user->full_name }}">
                            </div>
                        </div>
                        <div class="row_user">
                            <div class="label-user">Email <span class="require">(*)</span></div>
                            <div class="input-user">
                                <input readonly="" required="" name="email" class="form-control" value="{{ $user->email }}">
                            </div>
                        </div>
                        <div class="row_user">
                            <div class="label-user">Số điện thoại <span class="require">(*)</span></div>
                            <div class="input-user">
                                <input required="" name="phone" class="form-control" value="{{ $user->phone }}">
                            </div>
                        </div>
                        <div class="row_user">
                            <label><input type="checkbox" name="dknhathuoc" id="dknhathuoc" value="1"> Nhập thông tin doanh nghiệp để biết giá sỉ <br></label>&nbsp;&nbsp;&nbsp;<div class="btn btn-default"><i class="fa fa-times-circle cc_pointer" style="color:red;"></i> Chờ duyệt</div>
                        </div>
                        <div id="boxnhathuoc" style="display:none; ">
                            <div class="row_user">
                                <div class="label-user">Tên nhà thuốc <span class="require">(*)</span></div>
                                <div class="input-user">
                                    <textarea placeholder="Nhập tên nhà thuốc/ tên doanh nghiệp" class="form-control" name="pharmacy_name"></textarea>
                                </div>
                            </div>
                            <div class="row_user">
                                <div class="label-user">MST/Giấy ĐKKD <span class="require">(*)</span></div>
                                <div class="input-user">
                                    <input placeholder="MST/Giấy ĐKKD" name="pharmacy_mst" class="form-control" value="">
                                </div>
                            </div>
                        </div>
                        <div class="row_user">
                            <input type="hidden" name="gioitinh" id="load_gioitinh" value="{{ $user->gender }}">

                            <i class="fa fa-mars"></i> Giới tính:
                            <label><input onclick="document.getElementById('load_gioitinh').value=0;" type="radio" checked="checked" name="gender" value="0">Nam</label>

                            <label><input onclick="document.getElementById('load_gioitinh').value=1;" type="radio" name="gender" value="1">Nữ</label>
                        </div>

                        <div class="row_user">
                            <div class="label-user">Địa chỉ <span class="require">(*)</span></div>
                            <div class="input-user">
                                <input required="" name="address" class="form-control" value="{{ $user->address }}">
                            </div>
                        </div>

                        <div class="row_user">
                            <div class="label-user">Mật khẩu <span class="require">(*)</span></div>
                            <div class="input-user">
                                <input type="password" placeholder="Mật khẩu" name="old_password" class="form-control" value="">
                            </div>
                        </div>
                        <div class="row_user">
                            <label><input type="checkbox" name="doimatkhau" id="dkdoimatkhau" value="1"> Đổi mật khẩu <br></label>
                        </div>

                        <div id="boxdoimatkhau" style="display:none;">
                            <div class="row_user">
                                <div class="label-user">Mật khẩu mới <span class="require">(*)</span></div>
                                <div class="input-user">
                                    <input type="password" placeholder="Mật khẩu mới" name="password" class="form-control" value="">
                                </div>
                            </div>
                            <div class="row_user">
                                <div class="label-user">Nhập lại mật khẩu mới <span class="require">(*)</span></div>
                                <div class="input-user">
                                    <input type="password" placeholder="Nhập lại mật khẩu mới" name="comfirm_password" class="form-control" value="">
                                </div>
                            </div>
                        </div>
                        <br><br>
                        <div class="row_user" style="text-align:center;">
                            <button class="btn btn-primary align-center cc_pointer" value="1" type="submit"><i class="fa fa-send"></i> Cập nhật </button>
                        </div>
                        <input type="hidden" name="updatethanhvien" id="updatethanhvien" value="1">
                    </form>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
@endsection
@section('script')


@endsection