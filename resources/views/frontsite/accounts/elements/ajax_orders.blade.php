<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
    <h4 style="color:red;"><span class="glyphicon glyphicon-lock"></span>Chi tiết đơn hàng </h4>
</div>
<div class="modal-body">
    <table class="table table-responsive">
        <tr>
            <th>STT</th>
            <th>Tên sản phẩm</th>
            <th>Đơn giá</th>
            <th>Số lượng</th>
            <th>Thành tiền</th>
        </tr>
        @if(isset($products))
        @foreach($products as $key => $product)
        <tr>
            
            <td>{{ $key+1 }}</td>
            <td>{{ $product->name }}</td>
            <td>{{ number_format($product->price) }} vnđ</td>
            <td>{{ $product->quantity }}</td>
            <td>{{ number_format($product->price*$product->quantity) }} vnđ</td>
        </tr>
        @endforeach
        @endif
        <tr style="font-weight:bold;">
            <td colspan="3">Tổng tiền</td>
            <td colspan="2">{{ number_format($total_price) }} vnđ</td>
        </tr>
    </table>
</div>
<div class="modal-footer">
    <button type="submit" class="btn btn-default btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Đóng</button>
</div>