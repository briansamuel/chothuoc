<div class="col-menu">
    <div class="avata">
        <img src="https://chosithuoc.com/application/templates/images/avatar.png" alt="Avata">
        <p>&nbsp;</p>
        <p class="labeluser">Tài khoản của</p>
        <p>DINH VAN VU</p>
    </div>
    <ul class="ul-user">
        <li><a href="thong-tin-tai-khoan.htm"><i class="fa fa-user"></i> Thông tin tài khoản</a></li>
        <li><a href="quan-ly-don-hang.htm"><i class="fa fa-cart-plus"></i> Quản lý đơn hàng</a></li>

        <li><a href="san-pham-da-xem.htm"><i class="fa fa-eye"></i> Sản phẩm đã xem</a></li>
        <li><a href="san-pham-yeu-thich.htm"><i class="fa fa-star"></i> Sản phẩm yêu thích</a></li>
        <li><a href="san-pham-mua-sau.htm"><i class="fa fa-shopping-cart"></i> Sản phẩm mua sau</a></li>
        <li><a href="javascript:if(confirm('Thoát khỏi tài khoản?')){location.href='logout.htm';} "><i class="fa fa-sign-out"></i> Thoát</a></li>
    </ul>

</div>