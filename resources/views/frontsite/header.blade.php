<div id="top_wrapper" class="top-container">
    <div class="ws-container">
        <div class="top-inner clearfix">
            <div class="top-item top-a">
                <p><a href="ung-pho-virus-corona" style="color:#1e91cf" target="_blank"><strong>Ứng phó Virus Corona</strong></a></p>

            </div><!-- /.top-item -->
            <div class="top-item top-b">
                <ul class="clearfix">
                    <li>
                        <a href="gioi-thieu-cong-ty/" title="Giới thiệu">Giới thiệu</a>
                    </li>
                    <li>
                        <a href="lien-he/" title="Liên hệ">Liên hệ</a>
                    </li>
                    <li>
                        <a href="cart.htm" title="Giỏ hàng của bạn">
                            <i class="fa fa-shopping-cart fa-lg" aria-hidden="true"></i>
                            <strong>
                            @if(session()->has('cart'))
                                {{ count(session()->get('cart')) }}
                            @else 
                                0
                            @endif
                            </strong>
                        </a>
                    </li>
                </ul>
            </div><!-- /.top-item -->
        </div><!-- /.top-inner -->
    </div><!-- /.ws-container -->
</div><!-- /#top_wrapper.top-container -->

@include('frontsite.elements.menu')
@include('frontsite.elements.search-header')
