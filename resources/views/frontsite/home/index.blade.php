@extends('frontsite.index')
@section('title', __('seo.home.title'))
@section('title_ab', __('seo.home.title_ab'))
@section('description', __('seo.home.description'))
@section('keywords', __('seo.home.keyword'))
@section('body_class', '_home')
@section('style')

@endsection
@section('content')
@include('frontsite.elements.slide')
<div id="wapper_main">
    <div class="content_main">
        <div class="content_content">
            <h1 class="display_none">Chợ Sỉ Thuốc, Thuốc Sỉ, Chợ Sỉ Thuốc Tây - Giá Sỉ, Chất lượng tốt, Thuốc Gì Cũng</h1>
            <h2 class="display_none">Chợ Sỉ Thuốc, Thuốc Sỉ, Chợ Sỉ Thuốc Tây - Giá Sỉ, Chất lượng tốt, Thuốc Gì Cũng</h2>
            <div class="danhmuc-didong clearfix">
                <ul class="clearfix">
                    @foreach($categories as $category)
                    <li>
                        <a href="danh-muc/{{$category->category_slug}}" title="{{$category->category_name}}">

                            <span class="ws-icon">
                                <img src="{{$category->category_icon}}" width="20px" onerror="xulyloi(this)" class="img-responsive" alt="">
                            </span>
                            <span class="menu-link-name">
                                {{$category->category_name}}
                            </span><!-- /.menu-link-name -->
                        </a>
                    </li>
                    @endforeach
                </ul>
            </div><!-- /.danhmuc-didong -->
            <div class="benhqt-container cc_cursor">

                <div class="benh-group">
                    <div class="benh-title clearfix">
                        <h3>Bệnh được quan tâm</h3>
                        <a href="tim-hieu-benh/" class="close cc_pointer">Xem tất cả các bệnh
                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                        </a><!-- /.close -->
                    </div><!-- /.benh-title -->
                    <div class="benh-container">
                        <a href="hoi-chung-down" title="Hội chứng Down" class="benh-inner">
                            Hội chứng Down
                        </a><!-- /.benh-inner -->
                        <a href="dai-thao-duong" title="Đái tháo đường" class="benh-inner">
                            Đái tháo đường
                        </a><!-- /.benh-inner -->
                        <a href="thieu-mau" title="Thiếu máu" class="benh-inner">
                            Thiếu máu
                        </a><!-- /.benh-inner -->
                        <a href="chung-quen" title="Chứng quên" class="benh-inner">
                            Chứng quên
                        </a><!-- /.benh-inner -->
                        <a href="benh-viem-khop" title="Bệnh viêm khớp" class="benh-inner">
                            Bệnh viêm khớp
                        </a><!-- /.benh-inner -->
                        <a href="dau-lung" title="Đau lưng" class="benh-inner">
                            Đau lưng
                        </a><!-- /.benh-inner -->
                        <a href="duc-thuy-tinh-the" title="Đục thủy tinh thể" class="benh-inner">
                            Đục thủy tinh thể
                        </a><!-- /.benh-inner -->
                        <a href="giun-san" title="Giun sán" class="benh-inner">
                            Giun sán
                        </a><!-- /.benh-inner -->
                        <a href="roi-loan-nhip-tim" title="Rối loạn nhịp tim" class="benh-inner">
                            Rối loạn nhịp tim
                        </a><!-- /.benh-inner -->
                        <a href="dau-bụng-kinh" title="Đau bụng kinh" class="benh-inner">
                            Đau bụng kinh
                        </a><!-- /.benh-inner -->
                        <a href="polyp-co-tu-cung" title="Polyp Cổ tử cung" class="benh-inner">
                            Polyp Cổ tử cung
                        </a><!-- /.benh-inner -->
                        <a href="say-thai" title="Sảy thai" class="benh-inner">
                            Sảy thai
                        </a><!-- /.benh-inner -->
                        <a href="sa-sut-tri-tue-mach-mau" title="Sa sút trí tuệ mạch máu" class="benh-inner">
                            Sa sút trí tuệ mạch máu
                        </a><!-- /.benh-inner -->
                        <a href="viem-mang-nao" title="Viêm màng não" class="benh-inner">
                            Viêm màng não
                        </a><!-- /.benh-inner -->
                        <a href="u-xo-tu-cung" title="U xơ tử cung" class="benh-inner">
                            U xơ tử cung
                        </a><!-- /.benh-inner -->
                        <a href="u-nang-buong-trung" title="U nang buồng trứng" class="benh-inner">
                            U nang buồng trứng
                        </a><!-- /.benh-inner -->
                        <a href="benh-ung-thu-hodgkin" title="Bệnh ung thư Hodgkin" class="benh-inner">
                            Bệnh ung thư Hodgkin
                        </a><!-- /.benh-inner -->
                        <a href="gan-nhiem-mo" title="Gan nhiễm mỡ" class="benh-inner">
                            Gan nhiễm mỡ
                        </a><!-- /.benh-inner -->
                    </div><!-- /.benh-container -->
                </div><!-- /.benh-group -->

                <div class="hoatchat-group">
                    <div class="benh-title clearfix">
                        <h3>Hoạt chất thông dụng</h3>
                        <a href="hoat-chat/" class="close">Xem tất cả
                            <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                        </a><!-- /.close -->
                    </div><!-- /.benh-title -->

                    <div class="benh-container">
                        <a href="acid-clavulanic.hc" title="Acid clavulanic" class="benh-inner">
                            Acid clavulanic
                        </a><!-- /.benh-inner -->
                        <a href="penicilin.hc" title="Penicilin" class="benh-inner">
                            Penicilin
                        </a><!-- /.benh-inner -->
                        <a href="methicilin.hc" title="Methicilin" class="benh-inner">
                            Methicilin
                        </a><!-- /.benh-inner -->
                        <a href="ampicilin.hc" title="Ampicilin" class="benh-inner">
                            Ampicilin
                        </a><!-- /.benh-inner -->
                        <a href="amoxicilin.hc" title="Amoxicilin" class="benh-inner">
                            Amoxicilin
                        </a><!-- /.benh-inner -->
                        <a href="cloxacilline.hc" title="Cloxacilline" class="benh-inner">
                            Cloxacilline
                        </a><!-- /.benh-inner -->
                        <a href="piperacillin.hc" title="Piperacillin" class="benh-inner">
                            Piperacillin
                        </a><!-- /.benh-inner -->
                        <a href="cefadroxil.hc" title="Cefadroxil" class="benh-inner">
                            Cefadroxil
                        </a><!-- /.benh-inner -->
                        <a href="cephalexin.hc" title="Cephalexin" class="benh-inner">
                            Cephalexin
                        </a><!-- /.benh-inner -->
                        <a href="cefalothin.hc" title="Cefalexin" class="benh-inner">
                            Cefalexin
                        </a><!-- /.benh-inner -->
                    </div><!-- /.benh-container -->
                </div><!-- /.hoatchat-group -->
            </div><!-- /.benhqt-container -->
            <div class="clear"></div>
            <div>
                <div class="banner_home banner_home0 cc_cursor"><a href="salehadariki.tags" title="Banenr home" target="_blank"><img class="owl-lazy lazy" width="" height="" onerror="xulyloi(this);" data-original="uploads/quangcao/Banenr-home-.png" alt="Banenr home" title="Banenr home"></a></div>
                <div class="banner_home banner_home1 cc_cursor"><a href="salesinhlynam.tags" title="Banner home 2" target="_blank"><img class="owl-lazy lazy" width="" height="" onerror="xulyloi(this);" data-original="uploads/quangcao/Banner-home-2-.png" alt="Banner home 2" title="Banner home 2"></a></div>
                <div class="banner_home banner_home2 cc_cursor"><a href="salebaocaosu.tags" title="Banner home 3" target="_blank"><img class="owl-lazy lazy" width="" height="" onerror="xulyloi(this);" data-original="uploads/quangcao/Banner-home-3-.png" alt="Banner home 3" title="Banner home 3"></a></div>
            </div>
            <div class="clear"></div>
            @foreach($medicineByCategory as $category)
            @if($category->id != 81)
            <div class="rowx" id="category-{{ $category->id }}">
                <div class="rowx_content">
                    <div class="title">
                        <div class="titleitem">
                            <span class="img"><img src="{{$category->category_icon}}" title="{{$category->category_name}}"></span>
                            <a href="{{env('APP_URL')}}/{{$category->category_slug}}/" title="{{$category->category_name}}">{{$category->category_name}}</a>
                        </div>
                        <div class="title_listitem">
                            <ul class="danhmuclienquan">
                                @foreach($category->item as $key => $sub_cate)
                                @if($key < 5 && strlen($sub_cate->category_name) < 30)
                                <li><a href="danh-muc/{{$sub_cate->category_slug}}/" title="{{$sub_cate->category_name}}">{{$sub_cate->category_name}}</a></li>
                                @endif
                                @endforeach
                            </ul>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clearfix"></div><!-- /.clearfix -->
                    <div class="owl-home-wrap cc_cursor">
                        <div class="catimg cc_cursor">
                            <div class="owl-banhome owl-carousel owl-theme">
                                <div class="banhome-item">
                                    <a href="danh-muc/{{$category->category_slug}}/" title="Banner home {{ $category->category_name }}">
                                        <img src="{{ $category->category_thumbnail }}" alt="Banner home {{ $category->category_name }}" class="img-responsive">
                                    </a>
                                </div><!-- /.banhome-item -->
                            </div><!-- /.owl-banhome owl-carousel owl-theme -->
                        </div><!-- /.catimg -->
                        <div class="owl_home cc_cursor">
                            @foreach($category->products as $product)
                            <div class="itemsanpham">
                                <div class="img">
                                    <a href="/san-pham/{{$product->slug}}" title="{{$product->name}}">
                                        <img onerror="xulyloi(this);" src="{{$product->image}}" alt="{{$product->name}}">
                                    </a>
                                    <div class="icon_group">
                                        <div class="iconsp icon_view">
                                            <a href="/san-pham/{{$product->slug}}" title="Thông tin sản phẩm">
                                                <i class="fa fa-info"></i>
                                            </a>
                                        </div>
                                        <div class="iconsp icon_cart">

                                            <a href="#" title="Đặt hàng" data-toggle="modal" data-target="#myModal" onclick="MuaHang('modules/giohang/giohang_ajax.php?id={{$product->id}}&amp;masp={{$product->id}}&amp;method=add','loaddatacart');">
                                                <i class="fa fa-cart-plus"></i>
                                            </a>
                                        </div>
                                        @if(Auth::guard('user'))
                                        <div class="iconsp icon_view icon_yeuthich">
                                            <a onclick="Ajax_noreturn_swal('ajax/?action=sanphamyeuthich&amp;idsp={{$product->id}}','Đã thêm vào sản phẩm yêu thích');" href="javascript:" title="Sản phẩm yêu thích">
                                            <i class="fa fa-star"></i>
                                            </a>
                                        </div>
                                        @endif
                                    </div>


                                    <div class="masp_hover">
                                        <div class="masp_hover_masp" data-toggle="tooltip" title="Mã số">
                                            <span>Mã số:</span> {{$product->id}}
                                        </div>
                                        <div class="masp_hover_view" data-toggle="tooltip" title="Số lần xem">
                                            <i class="fa fa-eye"></i> {{$product->count_view}}
                                        </div>
                                    </div><!-- /.masp_hover -->
                                </div><!-- /.img -->
                                <p class="tieude">
                                    <a href="/san-pham/{{$product->slug}}" title="{{$product->name}}">{{$product->name}}</a>
                                </p>
                                <div class="rating-wrap">
                                    <i class="fa fa-star "></i>
                                    <i class="fa fa-star "></i>
                                    <i class="fa fa-star "></i>
                                    <i class="fa fa-star "></i>
                                    <i class="fa fa-star "></i></div><!-- /.rating-wrap -->
                                <div class="gia" data-toggle="tooltip">{{number_format($product->price)}}&nbsp;đ</div>
                                <div class="giagoc" data-toggle="tooltip">{{number_format($product->price)}}&nbsp;đ</div>
                            </div><!-- /.itemsanpham -->
                            @endforeach
                        </div>
                    </div><!-- /.owl-home-wrap -->
                </div>
            </div>
            
            <div class="clear"></div>
            <div></div>
            <div class="clear"></div>
            @endif
            @endforeach
            <div class="clear"></div>
        </div>
    </div>
</div>
@endsection
@section('script')


@endsection
