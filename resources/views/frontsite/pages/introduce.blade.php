@extends('frontsite.index')
@section('title', 'Giới thiệu PharmacyMarket')
@section('title_ab', 'Giới thiệu PharmacyMarket')
@section('description', 'Giới thiệu PharmacyMarket')
@section('keywords', 'Giới thiệu PharmacyMarket')
@section('body_class', '_noidung _chitiet')
@section('style')

@endsection
@section('content')
<div id="wapper_main">
    <div class="content_main">
        <div class="content_content">
            
            @include('frontsite.elements.breadcrumb')
            <div class="clear"></div>
            <div class="noidungchitiet">
                <h1 class="title">{{ $page->page_title }}</h1>
                <div class="ngaycapnhat">{{ date("d/m/Y - H:i A", strtotime($page->created_at)) }}</div>
                @include('frontsite.elements.social')
 
                <div class="noidung">
                    {!! $page->page_content !!}
                </div>
                <div class="clear"></div>
                
                
                @include('frontsite.elements.social')
  
                <br>
               
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
@endsection
@section('script')


@endsection