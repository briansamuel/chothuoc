@extends('frontsite.index')
@section('title', 'Giới thiệu PharmacyMarket')
@section('title_ab', 'Giới thiệu PharmacyMarket')
@section('description', 'Giới thiệu PharmacyMarket')
@section('keywords', 'Giới thiệu PharmacyMarket')
@section('body_class', '_noidung _chitiet')
@section('style')

@endsection
@section('content')
<div id="wapper_main">
    <div class="content_main">
        <div class="content_content">
            <div xmlns:v="http://rdf.data-vocabulary.org/" id="breadcrumbs">
                <ul itemprop="breadcrumb">
                    <li typeof="v:Breadcrumb"><a property="v:title" rel="v:url" href="https://chosithuoc.com/" title=""><label><i class="fa fa-home fa-2x"></i></label></a></li>
                    <li typeof="v:Breadcrumb"><a property="v:title" rel="v:url" href="https://chosithuoc.com/lien-he/" title="Liên hệ">Liên hệ</a></li>
                </ul>
            </div>
            <div class="lienheform clearfix">
                <div class="thongtin clearfix">
                    <div class="contact-left cc_cursor">
                        <p class="header-contact">&nbsp;THÔNG TIN CÔNG TY</p>

                        <p><strong>Đại diện:&nbsp;</strong>{{ __('frontsite.footer.company_name') }}<br>
                            <strong>Địa chỉ:</strong>&nbsp;{{ __('frontsite.footer.address') }}<br>
                            <strong>Mã số doanh nghiệp:</strong> {{ __('frontsite.footer.mst') }}<br>
                            <strong>Thời gian làm việc:</strong> {{ __('frontsite.footer.time_work') }}<br>
                            <strong>Tell: </strong>{{ __('frontsite.footer.tel_1') }}<br>
                            <strong>Email:</strong>&nbsp;{{ __('frontsite.footer.email') }}<br>
                            </p>
                    </div>

                    <div class="contact-right">
                        <p class="header-contact">&nbsp;THÔNG TIN thanh toán</p>

                        <p><strong>Ngân hàng: {{ __('frontsite.footer.bank_name') }}</strong><br>
                            <strong>Số tài khoản:</strong>&nbsp;{{ __('frontsite.footer.STK') }}<br>
                            <strong>Người đại diện:</strong>&nbsp;{{ __('frontsite.footer.bank_account') }}<br>
                            &nbsp;</p>

                        <p><br>
                            &nbsp;</p>
                    </div>
                    <style type="text/css">
                        .header-contact {
                            text-transform: uppercase;
                            font-size: 20px;
                            font-weight: bold;
                            border-left: 5px solid #007AC3;
                            padding-left: 5px;
                            margin-bottom: 10px
                        }

                        .contact-right,
                        .contact-left {
                            width: 100%%;
                            padding-bottom: 20px
                        }

                        .contact-right,
                        .contact-left p {
                            line-height: 30px;
                        }

                        .contact-left {}

                        .contact-right {
                            ;
                            text-align: left
                        }
                    </style>

                </div>
                <div class="form clearfix">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title text-center"><label for="lienhe">Thông tin liên hệ</label></h3>
                        </div>
                        <div class="panel-body box-lienhe cc_cursor">
                            <form action="" method="post">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">&nbsp;Họ tên</div>
                                        <input type="text" name="hoten" class="form-control" id="" placeholder="Vui lòng nhập họ và tên" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">&nbsp;Điện thoại</div>
                                        <input type="text" name="dienthoai" class="form-control" id="" placeholder="Vui lòng nhập số điện thoại" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">&nbsp;Email</div>
                                        <input type="text" name="email" class="form-control" id="" placeholder="Vui lòng nhập email" value="">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">&nbsp;Captcha</div>
                                        <input style="height:47px;" type="text" name="captcha" class="form-control" id="" placeholder="Vui lòng nhập captcha">
                                        <div class="input-group-addon">
                                            <img src="https://chosithuoc.com/plugin/captcha/image.php" id="img-captcha">
                                            <a onclick="$('#img-captcha').attr('src', 'https://chosithuoc.com/plugin/captcha/image.php?rand=' + Math.random())" title="reload">
                                                <img src="https://chosithuoc.com/application/templates/images/reload.png" style="width: 30px; height: 30px; cursor: pointer;">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">&nbsp;Tiêu đề</div>
                                        <input type="text" class="form-control" name="tieude" id="" placeholder="Vui lòng nhập tiêu đề">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">&nbsp;nội dung</div>
                                        <textarea class="form-control" name="noidung" rows="3" placeholder="Vui lòng nhập nội dung"></textarea>
                                    </div>
                                </div>
                                <div class="text-center">
                                    <input type="submit" name="" class="btn btn-primary btn-lg" value="Gửi">
                                    <input type="reset" name="reset" class="btn btn-default btn-lg" value="Nhập lại">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="lienhemaps">
                <style>
                    .likbando {
                        font-size: 12px;
                    }

                    .likbando a {
                        color: #000;
                    }

                    .likbando a:hover {
                        color: #09F;
                    }
                </style>
                <script type="text/javascript" src="http://code.google.com/apis/gears/gears_init.js">
                </script>
                <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;language=vi"></script>
                <script type="text/javascript">
                    var map;

                    function initialize() {
                        var myLatlng = new google.maps.LatLng(10.806897, 106.668999)
                        var myOptions = {
                            zoom: 15,
                            center: myLatlng,
                            mapTypeId: google.maps.MapTypeId.ROADMAP
                        }
                        map = new google.maps.Map(document.getElementById("div_id"), myOptions);
                        // Bi?n text ch?a n?i dung s? du?c hi?n th?
                        var text;
                        text = '<div style="width:280px; color:#000; font-size:12px; padding-right:10px;"><b>Công ty TNHH Thương mại Vinacost</b><br /><b>Address: </b>86/15 Phổ Quang - Phường 2 - Tân Bình - Tp HCM.</div>'
                        var infowindow = new google.maps.InfoWindow({
                            content: text,
                            size: new google.maps.Size(100, 50),
                            position: myLatlng
                        });
                        infowindow.open(map);
                        var marker = new google.maps.Marker({
                            position: myLatlng,
                            map: map,
                            title: "B?n d? hu?ng d?n du?ng di"
                        });
                    }
                </script>
                <div id="div_id" class="css_maps"></div>
                <script>
                    window.onload = initialize();
                </script>
            </div>
            <div class="clear"></div>
            <div class="clear"></div>
        </div>
    </div>
</div>
@endsection
@section('script')


@endsection