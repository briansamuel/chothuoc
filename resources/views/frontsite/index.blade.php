<!DOCTYPE html>
<html xmlns="https://www.w3.org/1999/xhtml">

<!-- Mirrored from pharmacymarket.vn/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 27 Jul 2020 07:57:43 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
	<base href="//{{ config('domain.web.domain') }}/" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="robots" content="index,follow,snippet,archive" />
	<meta name="title" content="@yield('title_ab') - {{ __('seo.web.slogan') }}" />
	<meta name="description" content="@yield('description')" />
	<meta name="keywords" content="@yield('keywords')" />
	<meta name="author" content="//{{ config('domain.web.domain') }}" />
	<meta property="og:title" content="@yield('title_ab') - {{ __('seo.web.slogan') }}" />
	<meta property="og:url" content="index.html" />
	<meta property="og:description" content="@yield('description')" />
	<meta property="og:image" content="@yield('image')" />
	<meta http-equiv="content-language" content="vi" />
	<meta http-equiv="REFRESH" content="1800" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="DC.title" content="Trang chủ" />
	<meta name="geo.region" content="VN-SG" />
	<meta name="geo.position" content="" />
	<meta name="ICBM" content="" />
	<meta name="google-site-verification" content="BDS7nBai45n6gdSGlLPKBFOq49yN0EoVl31FD-4C4q0" />
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<link href="feed" rel="alternate" title="Dòng thông tin pharmacymarket.vn" type="application/rss+xml" />
	<title>@yield('title') - Giá Sỉ, Chất lượng tốt, Thuốc Gì Cũng</title>
	<!--font-awesome & bootstrap-->
	<link rel="stylesheet" href="//{{ config('domain.web.domain') }}/application/templates/css/bootstrap-awesome-animate-hover.min.css">
	<!--owl slideshow version 2.0-->
	<link rel="stylesheet" href="//{{ config('domain.web.domain') }}/plugin/owl.carousel/owl.carousel.css">
	<link rel="stylesheet" href="//{{ config('domain.web.domain') }}/plugin/owl.carousel/owl.theme.default.min.css">
	<!-- script -->

	<script type="text/javascript" src="//{{ config('domain.web.domain') }}/application/templates/js/jquery-lib-webso.min.js"></script>
	<script type="application/javascript" src="//{{ config('domain.web.domain') }}/application/templates/js/bootstrap-carousel-sweetalert.min.js"></script>
	<script src="//{{ config('domain.web.domain') }}/plugin/owl.carousel/owl.carousel.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="//{{ config('domain.web.domain') }}/application/templates/js/jquery.lazyload.min.js"></script>
	<!-- <link rel="stylesheet" href="//{{ config('domain.web.domain') }}/plugin/elevatezoom-master/jquery.fancybox.css">
	<script src="//{{ config('domain.web.domain') }}/plugin/elevatezoom-master/jquery.elevatezoom.js"></script>
	<script src="//{{ config('domain.web.domain') }}/plugin/elevatezoom-master/jquery.elevatezoom.min.js" type="text/javascript"></script>
	<script src="//{{ config('domain.web.domain') }}/plugin/elevatezoom-master/jquery.fancybox.pack.js" type="text/javascript"></script> -->
	<script src="//{{ config('domain.web.domain') }}/plugin/sweet-alert/sweetalert.min.js" type="text/javascript"></script>
	<!-- Menumain -->
	<script src="//{{ config('domain.web.domain') }}/plugin/menu/main.js" type="text/javascript"></script>

	<!-- Sticky -->
	<script src="//{{ config('domain.web.domain') }}/plugin/sticky/jquery.sticky.js"></script>

	<script src="//{{ config('domain.web.domain') }}/plugin/star-rating-svg/dist/jquery.star-rating-svg.js"></script>
	<link rel="stylesheet" href="//{{ config('domain.web.domain') }}/plugin/star-rating-svg/src/css/star-rating-svg.css">
	<link rel="stylesheet" href="//{{ config('domain.web.domain') }}/plugin/icheck/skins/minimal/blue.css">
	<script src="//{{ config('domain.web.domain') }}/plugin/icheck/icheck.min.js"></script>
	<!-- load -->
	<script type="application/javascript" src="//{{ config('domain.web.domain') }}/application/templates/js/load.js"></script>
	<!-- MatchHeight -->
	<script src="//{{ config('domain.web.domain') }}/plugin/matchheight/jquery.matchHeight-min.js" type="text/javascript"></script>

	<!-- css -->
	<link href="//{{ config('domain.web.domain') }}/application/templates/css/css.css" rel="stylesheet" type="text/css" />
	<link href="//{{ config('domain.web.domain') }}/application/templates/css/css3.css" rel="stylesheet" type="text/css" />
	<link href="//{{ config('domain.web.domain') }}/application/templates/css/responsive.css" rel="stylesheet" type="text/css" />
	<!--comment-->
	<link href="//{{ config('domain.web.domain') }}/application/templates/css/comment.css" rel="stylesheet" type="text/css" />

	<!--select2-->
	<script src="//{{ config('domain.web.domain') }}/plugin/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
	<script src="//{{ config('domain.web.domain') }}/plugin/select2/dist/js/i18n/vi.js" type="text/javascript"></script>
	<link href="//{{ config('domain.web.domain') }}/plugin/select2/dist/css/select2.min.css" ," rel="stylesheet" type="text/css" />

	<!-- ICON -->
	<link rel="icon" href="//{{ config('domain.web.domain') }}/uploads/danhmuc/favicon-cst.png" type="image/x-icon" />
	
	<style type="text/css" media="screen">
		._bgcolormain {
			background-color: #1E91CF;
		}
	</style>
	
	<!--End of Tawk.to Script-->
	<link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;display=swap" rel="stylesheet">

	<style type="text/css">
		.designby_info {
			text-align: left;
			color: #000
		}

		.designby {
			display: none
		}

		.thongso-sanpham table tr td:first-child {
			background: #efefef;
		}

		.nhomnutmua .inner .btn-themgiohang {
			background: #1e91cf !important;
			color: #fff !important
		}

		.noidung table {
			margin: 10px !important
		}

		.noidung table td,
		.noidung table th {
			padding: 5px !important;
		}

		.tab-noidungchitiet .row-banner {
			border: none !important;
		}

		.tab-noidungchitiet .row-banner img {
			border: 1px solid #d5d5d5;
		}

		.hotline_fix {
			display: none
		}

		.btn {
			border-radius: 0 !important
		}

		body {
			background-color: #F5F5F5;
			margin: 0px;
		}

		.keyword-popular a {
			background: #fff;
			color: #1f90cf !important;
			border: 1px solid #1f90cf !important;
			border-radius: 3px;
		}

		.search-header .head>.list-search-type>li.active>a {
			background-color: #1e91cf !important;
		}

		.box-register {
			background: #fff;
		}

		._bgcolormain {
			background: none
		}

		._bgcolormain .ws-container {
			background: #1e91cf
		}

		.keyword-popular {
			color: #1f90cf !important;
			font-size: 13px !important;
		}

		.popup_top {
			background: #f8981c !important
		}

		._benh _ .group_bottom_menu {
			display: none
		}

		_chitiet .group_bottom_menu {
			display: none
		}

		._bgcolormain .ws-container {
			background-image: url();
		}

		.owl-slide .owl-nav [class*="owl-"] {
			width: 22px !important;
			border-radius: 4px !important;
			background-color: none !important;
		}

		#drop-menu a img {
			max-height: 50px !important;
		}

		#drop-menu {
			overflow: visible !important;
		}

		.button-chatcustom {
			width: 86%;
			background: #1e91cf;
			border: none;
			height: 40px;
			margin: 10px auto;
			display: block;
		}

		.button-chatcustom a {
			color: #fff !important;
			font-size: 16px;
			font-weight: bold;
		}

		.button-chatcustom img {
			width: 20px
		}

		.group_bottom .noidung_bottom a {
			line-height: 20px !important;
		}

		.bottom_content .group_bottom {
			padding: 15px 10px 10px !important;
		}

		.slide_content img {
			width: 838px
		}

		@media (max-width: 576px) {
			.fb-customerchat {
				display: none;
			}
		}

		.zalome {
			position: fixed;
			bottom: 15px;
			left: 5px;
			z-index: 1000
		}

		.zalo-ico:before {
			top: 2px;
			left: 36px;
			z-index: 4;
			content: '';
			width: 8px;
			height: 8px;
			border-radius: 100%;
			position: absolute;
			border: 1px solid #fff;
			background-color: #7ed321;
		}

		.thongtin-khuyenmai a:visited {
			text-decoration: none;
			color: #1c91cf !important;
		}

		.thongtin-khuyenmai .fa {
			color: #1c91cf
		}
	</style>

</head>

<body class="@yield('body_class')">
	
	<div id="wapper_wapper_wapper" class="home">
		<div id="wapper_col_left">
		</div>
		<div id="wapper_wapper">
			@include('frontsite.header')
			@yield('content')
			@include('frontsite.footer')
			<div class="wapper_mark"></div>
		</div>
	</div>
	<ul class="clearfix menu-bottom">

		<li>
			<a href="index.html" title="HomePage"><i class="fa fa-home"></i></a>
		</li>


		<li><a href="sms:0909 54 6070" target="_blank" title="Click to SMS">

				<i class="fa fa-comments"></i> sms</a>

		</li>

		<li>

			<a href="tel:0909 54 6070" title="clickToCall"><i class="fa fa-phone"></i></a>

		</li>

		<li>
			<a href="http://zalo.me/{{ __('frontsite.footer.tel_1') }}0" target="_blank" title="Chat Zalo">
				<img src="application/templates/images/zalo.png" class="img-responsive" alt="zalo">
			</a>
		</li>
		<li>
			<a data-toggle2="tooltip" data-toggle="modal" data-target="#myModal" href="javascript:" onclick="Get_Data('ajax/indexe58b.html?action=bando','loaddatacart');" class="fixed-scroll-mail" href="#">
				<div class="iconcatagory"><i class="fa fa-map-marker"></i> </div>
			</a>
		</li>
	</ul>

	<div class="contact-fixed-scroll">

		<a data-toggle2="tooltip" data-toggle="modal" data-target="#myModal" href="javascript:" onclick="Get_Data('ajax?action=bando','loaddatacart');" class="contact-fixed-item fixed-scroll-map" title="Xem địa chỉ doanh nghiệp" href="javascript:">
		</a>
		<a class="contact-fixed-item fixed-scroll-phone" title="Gọi cho chúng tôi" href="tel:{{ __('frontsite.footer.tel_1') }}">
		</a>
		<a target="_blank" class="contact-fixed-item fixed-scroll-zalo" title="Chát zalo với chúng tôi" href="https://zalo.me/{{ __('frontsite.footer.tel_1') }}">
		</a>


	</div>
	<div id="notification"></div>
	<div id="loadingcss3"><i class="fa fa-spinner fa-pulse"></i></div>
	<div class="hotline_fix">
		<div class="hotline_icon" onclick="showhideclass('hotline_content');""><i class=" fa fa-phone fa-3x"></i></div>
		<div class="hotline_content">
			<p><a title="Nhấn để gọi" href="tel:{{ __('frontsite.footer.tel_1') }}">{{ __('frontsite.footer.tel_1') }}</a></p>
		</div>
	</div>
	<script>
		jQuery(window).load(function() {
			jQuery('#nhanvienyte').modal('show');
		});
	</script>
	<div class="modal fade" id="nhanvienyte" data-backdrop="static" data-keyboard="false">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-body">
					<div class="form-group">
						<p style="font-weight:400;font-size:14px"><span style="font-size:14px;">Vui lòng xác nhận bạn là
								dược sĩ, bác sĩ, chuyên viên y khoa... có nhu cầu tìm hiểu về sản phẩm. Thông tin mô tả
								tại đây chỉ mang tính chất trợ giúp người đọc hiểu hơn về sản phẩm, không nhằm mục đích
								quảng cáo.&nbsp; Việc sử dụng thuốc phải tuân theo hướng dẫn của Y bác sĩ và dược sĩ có
								chuyên môn.</span></p>
					</div><!-- /.form-group -->
					<div class="group-button">
						<button type="button" data-dismiss="modal" class="btn btn-success" onClick="Ajax_noreturn('ajax/?action=nhanvienyte');">
							Xác nhận </button>
						<button type="button" class="btn btn-default">
							<a href="lien-he/index.html" title="Bỏ qua">
								Bỏ qua </a>
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<a href="#" class="go-top">
		<img src="application/templates/images/btn_top.png" alt="Back to top" title="Back to top" />
	</a>
	<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog" id="loaddatacart"></div>
	</div>

	<div id="rootPath" class="hide" data-url="//{{ config('domain.web.domain') }}/"></div>
	
	
</body>

</html>