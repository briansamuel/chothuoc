@extends('frontsite.index')
@section('title', __('seo.products.title'))
@section('title_ab', __('seo.products.title_ab'))
@section('description', __('seo.products.description'))
@section('keywords', __('seo.products.keyword'))
@section('body_class', '_sanpham')
@section('style')

@endsection
@section('content')
<div id="wapper_main">
    <div class="content_main">
        <div class="content_content">
            <div id="colcontent">
                <div xmlns:v="http://rdf.data-vocabulary.org/" id="breadcrumbs">
                    <ul itemprop="breadcrumb">
                        <li typeof="v:Breadcrumb"><a property="v:title" rel="v:url" href="/" title=""><label><i class="fa fa-home fa-2x"></i></label></a></li>
                        <li typeof="v:Breadcrumb"><a property="v:title" rel="v:url" href="/danh-muc/" title="Danh mục">Danh mục</a></li>
                    </ul>
                </div>
                <div class="head-loctren clearfix">
                    <h3 class="loctren-name">Chọn nhóm thuốc</h3>
                    <!-- /.loctren-name -->
                    <div class="filter-other" id="filterMobile" data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-filter" aria-hidden="true"></i>
                        Bộ lọc
                    </div><!-- /.filter-other -->
                </div><!-- /.head-loctren -->
                <ul class="loctren-list">
                    @foreach($medicineGroups as $group)
                    <li>
                        <div class="loctren-inner">
                            <span class="loctren-title">{{$group->name}}</span>
                            <div class="sublist" data-choose="multiple">
                                <div class="clearfix head">
                                    <div>
                                        <span>{{$group->name}}</span>
                                        <span class="close">×</span>
                                    </div>
                                    <input type="text" class="loctren-tim form-control">
                                </div><!-- /.clearfix -->
                                <div class="sublist-inner">
                                    @foreach($group->ingredients as $ingredient)
                                    <label data-id="{{$ingredient->id}}" data-idnhomid="{{$ingredient->medicine_group_id}}">
                                        <i class="fa fa-loctren" aria-hidden="true"></i>
                                        <span data-text="{{$ingredient->name}}">{{$ingredient->name}}</span>
                                    </label>
                                    @endforeach
                                </div><!-- /.sublist-inner -->
                            </div><!-- /.sublist -->
                        </div>
                    </li>
                    @endforeach
                </ul><!-- /.loctren-list -->
                <div class="filter-list">

                </div><!-- /.filter-list -->
                <h1 class="title"><a href="/san-pham/" title="Danh mục">Danh mục</a></h1>
                <div class="noidungdanhmuc"></div>
                <div class="clearfix" id="loaditemsanpham">
                    @foreach($medicines as $medicine)
                    <div class="itemsanpham">
                        <div class="img">
                            <a href="/san-pham/{{$medicine->slug}}" title="{{$medicine->name}}">
                                <img onerror="xulyloi(this);" src="{{$medicine->image}}" alt="{{$medicine->name}}">
                            </a>
                            <div class="icon_group">
                                <div class="iconsp icon_view">
                                    <a href="/san-pham/{{$medicine->slug}}" title="Thông tin sản phẩm">
                                        <i class="fa fa-info"></i>
                                    </a>
                                </div>
                                <div class="iconsp icon_cart">

                                    <a href="#" title="Đặt hàng" data-toggle="modal" data-target="#myModal" onclick="MuaHang('modules/giohang/giohang_ajax.php?id={{$medicine->id}}&amp;masp={{$medicine->id}}&amp;method=add','loaddatacart');">
                                        <i class="fa fa-cart-plus"></i>
                                    </a>
                                </div>
                                @if(Auth::guard('user'))
                                <div class="iconsp icon_view icon_yeuthich">
                                    <a onclick="Ajax_noreturn_swal('ajax/?action=sanphamyeuthich&amp;idsp={{$medicine->id}}','Đã thêm vào sản phẩm yêu thích');" href="javascript:" title="Sản phẩm yêu thích">
                                    <i class="fa fa-star"></i>
                                    </a>
                                </div>
                                @endif
                            </div>


                            <div class="masp_hover">
                                <div class="masp_hover_masp" data-toggle="tooltip" title="Mã số">
                                    <span>Mã số:</span> {{$medicine->id}}
                                </div>
                                <div class="masp_hover_view" data-toggle="tooltip" title="Số lần xem">
                                    <i class="fa fa-eye"></i> {{$medicine->count_view}}
                                </div>
                            </div><!-- /.masp_hover -->
                        </div><!-- /.img -->
                        <p class="tieude">
                            <a href="/san-pham/{{$medicine->slug}}" title="{{$medicine->name}}">{{$medicine->name}}</a>
                        </p>
                        <div class="rating-wrap">
                            <i class="fa fa-star "></i>
                            <i class="fa fa-star "></i>
                            <i class="fa fa-star "></i>
                            <i class="fa fa-star "></i>
                            <i class="fa fa-star "></i></div><!-- /.rating-wrap -->
                        <div class="gia" data-toggle="tooltip">{{number_format($medicine->price)}}&nbsp;đ</div>
                        <div class="giagoc" data-toggle="tooltip">{{number_format($medicine->price)}}&nbsp;đ</div>
                    </div><!-- /.itemsanpham -->
                    @endforeach
                    <div class="clearfix"></div>
                    <ul class="pagination">
                        {{$medicines->links('frontsite.medicines.elements.pagination')}}
                    </ul>
                </div><!-- /#loaditemsanpham -->
                <div class="noidungdanhmuc"></div>
                <div class="clear"></div>
            </div>
            <div id="col1">
                <div class="group_tin" style="background-color:#FFFFFF;">
                    <div class="group_tin_tieude" style="background-color:#FFFFFF;">&nbsp;Danh mục sản phẩm</div>
                    <div class="group_tin_noidung">
                        <div class="danhmuccol">
                            <ul>
                                @foreach($categories as $category)
                                <li>
                                    <a href="/danh-muc/{{$category->category_slug}}/" title="{{$category->category_name}}"><span class="menu-link-name">{{$category->category_name}}</span></a>
                                    <ul>
                                        @foreach($category->item as $sub_cate)
                                        <li>
                                            <a href="/danh-muc/{{$sub_cate->category_slug}}/" title="{{$sub_cate->category_name}}"><span class="menu-link-name">{{$sub_cate->category_name}}</span></a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                @include('frontsite.medicines.elements.filterWidget')
                <div class="noidung_danhmuc"></div>
            </div>
            <div class="clear">
            </div>

        </div>
    </div>
</div>
@endsection
@section('script')


@endsection
