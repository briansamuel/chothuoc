<div class="modal-content">
    <div class="panel-body">
        <span class="close">
            <a href="javascript:void(0);" data-dismiss="modal">×</a>
        </span><!-- /.close -->
        <div class="rating-form">
            <form id="rating_form" action="" method="POST" onsubmit="return validateMyForm(event);">
                <h4 class="form-group"><strong>Đánh giá sản phẩm:</strong> {{ $product->name }}</h4>

                <div class="form-group">
                    <label>Khách hàng đánh giá:</label>
                    <div class="prod-rating">
                       <input type="hidden" name="rating_value">
                        <input type="hidden" name="id" value="{{ $product->id }}">

                    <script>
                        $(function() {
                            $(".prod-rating").starRating({
                                starSize: 30,
                                useFullStars: true,
                                disableAfterRate: false,
                                callback: function(currentRating, $el) {
                                    // make a server call here
                                    $('#enterRating').removeAttr('disabled');
                                    $('input[name=rating_value]').val(currentRating);
                                    if (currentRating < 1) {
                                        $('#enterRating').attr('disabled', 'disabled');
                                    }
                                }
                            });
                        });
                    </script>
                </div><!-- /.form-group -->

                <div class="form-group">
                    <label>Nhận xét:</label>
                    <textarea class="form-control" rows="5" name="nhanxet"></textarea>
                </div><!-- /.form-group -->
                <div class="form-group">
                    <button type="submit" data-loading-text="<i class='fa fa-refresh fa-spin fa-fw'></i> Đang gửi" id="enterRating" class="btn btn-danger" disabled=""> Gửi</button>
                </div><!-- /.form-group -->

            </form>
        </div><!-- /.rating-form -->
    </div><!-- /.panel-body -->
</div><!-- /.modal-content -->