<div class="lienquan-container clearfix">
    <div class="title_tinlienquan">Bạn cũng sẽ thích</div>
    <div class="owl-lienquan owl-carousel owl-theme">
        @foreach($related_products as $key => $medicine)
        @include('frontsite.medicines.elements.item-product')
        @endforeach
    </div><!-- /.owl-lienquan -->
</div><!-- /.lienquan-container -->