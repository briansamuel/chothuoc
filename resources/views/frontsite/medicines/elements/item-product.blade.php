<div class="itemsanpham">
    <div class="img">
        <a href="/san-pham/{{$medicine->slug}}" title="{{$medicine->name}}">
            <img onerror="xulyloi(this);" src="{{$medicine->image}}" alt="{{$medicine->name}}">
        </a>
        <div class="icon_group">
            <div class="iconsp icon_view">
                <a href="/san-pham/{{$medicine->slug}}" title="Thông tin sản phẩm">
                    <i class="fa fa-info"></i>
                </a>
            </div>
            <div class="iconsp icon_cart">

                <a href="#" title="Đặt hàng" data-toggle="modal" data-target="#myModal" onclick="MuaHang('https://www.chosithuoc.com/modules/giohang/giohang_ajax.php?id=11831&amp;masp=11831&amp;method=add','loaddatacart');">
                    <i class="fa fa-cart-plus"></i>
                </a>
            </div>
        </div>


        <div class="masp_hover">
            <div class="masp_hover_masp" data-toggle="tooltip" title="Mã số">
                <span>Mã số:</span> {{$medicine->id}}
            </div>
            <div class="masp_hover_view" data-toggle="tooltip" title="Số lần xem">
                <i class="fa fa-eye"></i> {{$medicine->count_view}}
            </div>
        </div><!-- /.masp_hover -->
    </div><!-- /.img -->
    <p class="tieude">
        <a href="/san-pham/{{$medicine->slug}}" title="{{$medicine->name}}">{{$medicine->name}}</a>
    </p>
    <div class="rating-wrap">
        <i class="fa fa-star "></i>
        <i class="fa fa-star "></i>
        <i class="fa fa-star "></i>
        <i class="fa fa-star "></i>
        <i class="fa fa-star "></i></div><!-- /.rating-wrap -->
    <div class="gia" data-toggle="tooltip">{{number_format($medicine->price)}}&nbsp;đ</div>
    <div class="giagoc" data-toggle="tooltip">{{number_format($medicine->price)}}&nbsp;đ</div>
</div><!-- /.itemsanpham -->