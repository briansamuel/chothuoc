<div class="rating-container">
    <div class="rating-inner">
        <div class="head clearfix">
            <h3 class="pull-left">Đánh giá và nhận xét</h3>
            <button type="button" onclick="showRating('{{  $medicine->id }}',this);" class="btn btn-default pull-right h3">
                Đánh giá sản phẩm này
            </button>
        </div><!-- /.head -->
        <div class="body">
            <div class="rating-review clearfix">
                <div class="review-inner">
                    <div class="col-a">
                        <div class="top">
                            0 <i class="fa fa-star"></i>
                        </div><!-- /.top -->
                        <div class="bottom">
                            0 Khách hàng đánh giá &amp; <br>0 Nhận xét
                        </div><!-- /.bottom -->
                    </div><!-- /.col-a -->
                    <div class="col-b">
                        <div class="char-rating">
                            <ul>
                                <li class="col char-item five">
                                    <div class="star-lbl">
                                        <span>5</span>
                                        <span><i class="fa fa-star"></i></span>
                                    </div>
                                    <div class="process">
                                        <div class="frame">
                                            <span class="percent" style="width: 0%"></span>
                                        </div>
                                    </div>
                                    <div class="statis">
                                        0
                                    </div>
                                </li>
                                <li class="col char-item four">
                                    <div class="star-lbl">
                                        <span>4</span>
                                        <span><i class="fa fa-star"></i></span>
                                    </div>
                                    <div class="process">
                                        <div class="frame">
                                            <span class="percent" style="width: 0%"></span>
                                        </div>
                                    </div>
                                    <div class="statis">
                                        0
                                    </div>
                                </li>
                                <li class="col char-item three">
                                    <div class="star-lbl">
                                        <span>3</span>
                                        <span><i class="fa fa-star"></i></span>
                                    </div>
                                    <div class="process">
                                        <div class="frame">
                                            <span class="percent" style="width: 0%"></span>
                                        </div>
                                    </div>
                                    <div class="statis">
                                        0
                                    </div>
                                </li>
                                <li class="col char-item two">
                                    <div class="star-lbl">
                                        <span>2</span>
                                        <span><i class="fa fa-star"></i></span>
                                    </div>
                                    <div class="process">
                                        <div class="frame">
                                            <span class="percent" style="width: 0%"></span>
                                        </div>
                                    </div>
                                    <div class="statis">
                                        0
                                    </div>
                                </li>
                                <li class="col char-item one">
                                    <div class="star-lbl">
                                        <span>1</span>
                                        <span><i class="fa fa-star"></i></span>
                                    </div>
                                    <div class="process">
                                        <div class="frame">
                                            <span class="percent" style="width: 0%"></span>
                                        </div>
                                    </div>
                                    <div class="statis">
                                        0
                                    </div>
                                </li>
                            </ul>
                        </div><!-- /.char-rating -->
                    </div><!-- /.col-b -->
                </div><!-- /.review-inner -->

            </div><!-- /.rating-review -->
            <div class="review-list">
            </div><!-- /.review-list -->
        </div><!-- /.body -->
    </div><!-- /.rating-inner -->
</div><!-- /.rating-container -->