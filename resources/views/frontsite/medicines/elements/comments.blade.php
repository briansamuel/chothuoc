<div class="ws-container">
    <div class="titlecomment">Bình luận của bạn</div>
    <form action="" method="POST" accept-charset="utf-8" class="frm-comment" id="frm-comment">
        <input type="hidden" name="idtype" id="this_product_idtype" value="{{  $medicine->id }}" placeholder="">
        <input type="hidden" name="idparent" id="this_product_idparent" value="" placeholder="">
        <div class="comment_col1">
            <div class="form-group icon-input">
                <input type="text" require="" name="hoten" class="form-control ws-required ws-rq-hoten" placeholder="Vui lòng nhập họ tên...">
                <span class="require">*</span>
            </div>

            <div class="form-group icon-input">
                <input type="text" data-wserror-email="Vui lòng nhập vào 1 Email" name="email" class="form-control ws-rq-email" placeholder="Vui lòng nhập email...">
            </div>

            <div class="form-group icon-input">
                <input type="text" name="dienthoai" class="form-control ws-required ws-required-number ws-rq-dienthoai" data-wserror-phone="Vui lòng nhập vào 1 số điện thoại" placeholder="Vui lòng nhập số điện thoại...">
            </div>
        </div>
        <div class="comment_col2">
            <div class="form-group icon-input">
                <textarea require="" name="noidung" class="form-control ws-rq-noidung" rows="3" placeholder="Vui lòng nhập bình luận..."></textarea>
                <span class="require">*</span>
            </div>
        </div>
        <div class="clear"></div>
        <div class="btngroup">
            <button type="button" id="" data-loading-text="<i class='fa fa-spinner fa-spin fa-lg fa-fw'></i>" class="btnSendComment btn btn-primary btn-center">

                Gửi bình luận
            </button>
            <button type="reset" class="btn btn-default btn-center">

                Nhập lại
            </button>
        </div>
    </form>
    <div class="clear"></div>
</div>