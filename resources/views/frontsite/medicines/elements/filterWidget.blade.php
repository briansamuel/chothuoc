<div class="filter-container height-filter">
    <div class="filter-wrap height-filter">
        <div class="phannhom-content">
            <div class="phannhom-item">
                <h3>Khách hàng đánh giá</h3>
                <div class="ndlocnhanh-content">
                    @for($i = 1; $i < 6; $i++) <div class="rate-line " data-tag="" data-star="{{$i}}">
                        <div class="rating-wrap">
                            <i class="fa fa-star {{$i > 0 ? 'active' : ''}}"></i>
                            <i class="fa fa-star {{$i > 1 ? 'active' : ''}}"></i>
                            <i class="fa fa-star {{$i > 2 ? 'active' : ''}}"></i>
                            <i class="fa fa-star {{$i > 3 ? 'active' : ''}}"></i>
                            <i class="fa fa-star {{$i > 4 ? 'active' : ''}}"></i></div><!-- /.rating-wrap --><span>( ít nhất {{$i}} sao )</span>
                </div>
                @endfor
            </div><!-- /.ndlocnhanh-content -->
        </div><!-- /.phannhom-item -->


        <div class="phannhom-item">
            <h3>Xuất xứ</h3>
            <div class="ndlocnhanh-content">
                <ul class="ulloc">
                    @foreach($countries as $country)
                    <li class="click_filter ">
                        <i class="item-check"></i>
                        <a href="javascript:void(0);" title="{{$country->name}}">
                            {{$country->name}}
                            <input type="hidden" name="this_value_nhomid" value="country_id">
                            <input type="hidden" name="this_value_nhomnd" value="{{$country->id}}">
                            <input type="hidden" name="this_link_loc" value="application/files/locNhanh_ajax.php">
                        </a>
                    </li>
                    @endforeach
                </ul><!-- /.ulloc -->
            </div><!-- /.ndlocnhanh-content -->
        </div><!-- /.phannhom-item -->
        <div class="phannhom-item">
            <h3>Thương hiệu</h3>
            <div class="ndlocnhanh-content">
                <ul class="ulloc">
                    @foreach($brands as $brand)
                    <li class="click_filter ">
                        <i class="item-check"></i>
                        <a href="javascript:void(0);" title="">
                            {{$brand->name}}
                            
                            <input type="hidden" name="this_value_nhomid" value="brand_id">
                            <input type="hidden" name="this_value_nhomnd" value="{{$brand->id}}"">
                            <input type="hidden" name="this_link_loc" value="application/files/locNhanh_ajax.php">
                        </a>
                    </li>
                    @endforeach
                </ul><!-- /.ulloc -->
            </div><!-- /.ndlocnhanh-content -->
        </div><!-- /.phannhom-item -->
        <div class="phannhom-item">
            <h3>Quy cách đóng gói</h3>
            <div class="ndlocnhanh-content">
                <ul class="ulloc">
                    @foreach($packages as $package)
                    <li class="click_filter ">
                        <i class="item-check"></i>
                        <a href="javascript:void(0);" title="{{$package->name}}">
                            {{$package->name}}
                           
                            <input type="hidden" name="this_value_nhomid" value="package_id">
                            <input type="hidden" name="this_value_nhomnd" value="{{$package->id}}"">
                            <input type="hidden" name="this_link_loc" value="application/files/locNhanh_ajax.php">
                        </a>
                    </li>
                    @endforeach
                </ul><!-- /.ulloc -->
            </div><!-- /.ndlocnhanh-content -->
        </div><!-- /.phannhom-item -->
    </div><!-- /.phannhom-content -->
</div><!-- /.filter-wrap -->
</div><!-- /.filter-container -->