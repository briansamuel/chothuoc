@extends('frontsite.index')
@section('title', $medicine->name )
@section('title_ab', $medicine->name )
@section('description', $medicine->description)
@section('keywords', $medicine->keyword)
@section('style')

@endsection
@section('content')
<div id="wapper_main">
    <div class="content_main">
        <div class="content_content">
            @include('frontsite.elements.breadcrumb')
            <div class="noidungchitiet">
                <div class="sanpham_group clearfix">
                    <div class="sanpham_img chitiet-item">
                        <div class="noidung_img1">
                            <a href="#" data-image="{{ $medicine->image }}" data-zoom-image="{{ $medicine->image }}">
                                <img id="img_01" src="{{ $medicine->image }}" alt="{{ $medicine->name }}" title="{{ $medicine->name }}">
                            </a>

                        </div>

                    </div>
                    <div class="sanpham_info chitiet-item">
                        <h1 class="title">{{ $medicine->name }}</h1>
                        <div class="clear"></div>
                        <div class="row_chitiet">
                            <div class="ct_label">Số lượt mua:</div>
                            <div class="row_noidung">{{ $medicine->bought }}</div>
                            <div class="clear"></div>
                        </div>
                        <div class="row_chitiet">
                            <div class="ct_label">Mã sản phẩm:</div>
                            <div class="row_noidung">{{ $medicine->id }}</div>
                            <div class="clear"></div>
                        </div>
                        <div class="row_chitiet">
                            <div class="row_noidung">
                                {!! $medicine->instruction !!}
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div id="loadthuoctinh_price"></div>
                        <div class="nhomnutmua">
                            <div class="group_btn_dathang">
                                <span class="soluongsp input-group-addon">Số lượng</span>
                                <input class="form-control" name="slspct" id="slspct" value="1" type="number" min="1" maxlength="5" size="10">
                            </div>
                            <div class="inner">
                                <a href="#" title="" rel="nofollow" class="btn btn-default btn-themgiohang" data-toggle="modal" data-target="#myModal" onclick="MuaHang('modules/giohang/giohang_ajax.php?masp={{  $medicine->id }}&amp;method=add&amp;sl='+document.getElementById('slspct').value+'&amp;item=1','loaddatacart');">
                                    Thêm vào giỏ hàng</a>
                                <a href="javascript:void(0);" title="" rel="nofollow" class="btn btn-default btn-muangay" onclick="MuaHang('modules/giohang/giohang_ajax.php?masp={{  $medicine->id }}&amp;method=add&amp;sl='+document.getElementById('slspct').value+'&amp;item=1','loaddatacart');window.location.href = 'cart.htm'">
                                    Mua ngay</a>
                            </div><!-- /.inner -->
                        </div><!-- /.nhomnutmua -->
                        <div class="clear"></div>

                        <div class="thongtin-khuyenmai">
                            <div class="title">
                                <h3>Tìm đối tác kinh doanh</h3>
                            </div><!-- /.title -->
                            <div class="inner">
                                <ul style="font-size:14px">
                                    <li><i class="fa fa-check-circle"></i> Đăng ký tài khoản Doanh nghiệp (Quầy thuốc, Nhà Thuốc, Phòng khám, Doanh Nghiệp)&nbsp;<strong><a href="huong-dan-dang-ky">Hướng dẫn đăng ký</a> </strong>xem giá sỉ</li>
                                    <li><i class="fa fa-check-circle"></i> Tìm hiểu các loại bệnh phổ biến <strong><a href="/tim-hieu-benh/">Tìm hiểu bệnh</a></strong></li>
                                    <li><i class="fa fa-check-circle"></i> Tìm hiểu thông tin các <strong><a href="/hoat-chat/">Hoạt chất</a> </strong>thông dụng</li>
                                </ul>
                            </div><!-- /.inner -->
                        </div><!-- /.thongtin-khuyenmai -->
                        <form id="yeucaugoilai" name="yeucaugoilai" class="hidden-md hidden-lg yeucaumobile">
                            <div class="group_btn_dathang yeucaugoilai" id="contentycgoilai">
                                <div class="ycgoilai" id="ycgoilai">Yêu cầu shop gọi lại</div>
                                <input class="form-control" name="txtgoilai" id="txtgoilai" value="" placeholder="Nhập số điện thoại" type="number" minlength="8" maxlength="11" size="10">
                                <div class="btn btn-primary btn-dathang">
                                    <a title="Gửi" href="#" onclick="goilai('modules/sanpham/goilai.php','contentycgoilai','{{  $medicine->id }}','thuoc-sandoz-alprazolam-tablets-usp-1mg-gg-258','Số điện thoại không đúng','Gửi thông tin thành công'); return false;"><i class="fa fa-send"></i> Gửi
                                    </a>
                                    <input type="hidden" name="idsp_hi" id="idsp_hi" value="{{  $medicine->id }}">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="thongtindathang chitiet-item">
                        <div class="cot-giaohang">
                            <div class="inner">
                                <div class="r r1">
                                    <p><img alt="" src="uploads/images/shipping.png"> Giao hàng toàn quốc: Viettel Post, VNPost, GHN, GHTK</p>

                                    <p><img alt="" src="uploads/images/icon-freeship.png"> Giao hàng nhanh tại Tp HCM</p>

                                    <p><img alt="" src="uploads/images/icon-return.png"> Đổi trả miễn phí trong vòng 7 ngày</p>

                                    <p><button class="button-chatcustom"><a href="javascript:void(Tawk_API.toggle())"><img alt="" src="uploads/images/icon-chat.png"> Chat với tư vấn viên</a></button></p>
                                </div><!-- /.r1 -->
                                <div class="r r2">
                                    <p><img alt="" src="uploads/images/icon-support.png"> Hotline: <span style="color:#ff0000;"><span style="font-size:16px;"><strong>028.6686.3399</strong></span></span></p>

                                    <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Di động: <span style="color:#ff0000;"><span style="font-size:16px;"><strong>0909.54.6070</strong></span></span></p>
                                </div><!-- /.r2 -->
                                <div class="r dangkymail">
                                    Bạn chưa có tài khoản?
                                    <button type="button" class="btn-dangky" data-toggle="modal" data-target="#myModal" title="Đăng ký" onclick="Get_Data('ajax/?action=register','loaddatacart');">
                                        Đăng ký</button>
                                </div><!-- /.r dangkymail -->
                                <div class="r ycgl">
                                    <form id="yeucaugoilai" name="yeucaugoilai">
                                        <div class="group_btn_dathang yeucaugoilai" id="contentycgoilai2">
                                            <div class="ycgoilai" id="ycgoilai2">Yêu cầu shop gọi lại</div>

                                            <div class="input-group">
                                                <input class="form-control" name="txtgoilai2" id="txtgoilai2" value="" placeholder="Nhập số điện thoại" type="number" minlength="8" maxlength="11" size="10">
                                                <div class="input-group-btn">
                                                    <div class="btn-yeucaugoilai btn">
                                                        <a title="Gửi" href="#" onclick="goilai2('modules/sanpham/goilai.php','contentycgoilai2','{{  $medicine->id }}','thuoc-sandoz-alprazolam-tablets-usp-1mg-gg-258','Số điện thoại không đúng','Gửi thông tin thành công'); return false;"> Gửi
                                                        </a>
                                                        <input type="hidden" name="idsp_hi" id="idsp_hi" value="{{  $medicine->id }}">
                                                    </div>
                                                </div><!-- /.input-group-btn -->
                                            </div><!-- /.input-group -->
                                        </div>
                                    </form>
                                </div><!-- /.r yeucaugoilai -->
                            </div><!-- /.inner -->
                        </div><!-- /.cot-giaohang -->

                        <div class="mangxahoi">
                            <div class="fb-like" data-href="thuoc-sandoz-alprazolam-tablets-usp-1mg-gg-258" data-layout="button_count" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
                        </div><!-- /.mangxahoi -->

                    </div><!-- /.thongtindathang -->
                </div><!-- /.sanpham_group -->
                <div class="clear"></div>
                @include('frontsite.medicines.elements.related-product')
                <div class="clearfix"></div><!-- /.clearfix -->
                <div role="tabpanel" class="tab-noidungchitiet">
                    <div class="row-tabs noidungct-item">
                        <div class="thongso-sanpham">
                            <div class="head"><strong>Thông số sản phẩm</strong></div><!-- /.head -->
                            <table class="table table-bordered table-hover">
                                <tbody>
                                    @foreach($medicine->specifications as $key => $specification)
                                    <tr>
                                        <td>{{ $specification->label }}</td>
                                        <td>
                                            <a href="#" title="{{ $specification->value }}">
                                                {{ $specification->value }} </a>
                                        </td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div><!-- /.thongso-sanpham -->
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#chitiet" aria-controls="chitiet" role="tab" data-toggle="tab">Thông tin sản phẩm</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#hoidap" aria-controls="hoidap" role="tab" data-toggle="tab">Khách hàng hỏi đáp</a>
                            </li>
                            <li role="presentation" class="">
                                <a href="#danhgia" aria-controls="danhgia" role="tab" data-toggle="tab">Khách hàng đánh giá</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="chitiet">
                                <div class="noidung">
                                    {!! $medicine->content !!}
                                </div>
                            </div>
                            <div class="tab-pane" id="hoidap">
                                @include('frontsite.medicines.elements.comments')
                                <!-- .ws-container -->
                            </div><!-- /#hoidap.tab-pane -->
                            <div role="tabpanel" class="tab-pane" id="danhgia">
                                @include('frontsite.medicines.elements.reviews')
                            </div>
                        </div>
                    </div><!-- /.row-tabs -->
                    <div class="row-banner noidungct-item">
                        <div>
                            <div class=""><a href="dong-trung-ha-thao-cordyceps-1500mg-chai-60-vien" title="Banner Quảng cáo" target="_blank"><img class="owl-lazy lazy img-responsive" width="336px" height="" onerror="xulyloi(this);" data-original="uploads/quangcao/Banner-Quang-cao-.png" alt="Banner Quảng cáo" title="Banner Quảng cáo"></a></div>
                        </div>
                    </div><!-- /.row-banner -->
                </div><!-- /.tab-noidungchitiet -->
                <div class="clear"></div>
                <div class="group_btn_dathang" style="display:none;">
                    <span class="soluongsp input-group-addon">Số lượng</span>
                    <input class="form-control" name="slspct2" id="slspct2" value="1" type="number" min="1" maxlength="5" size="10">
                    <div class="btn btn-primary btn-dathang">

                        <a href="#" title="Đặt hàng" data-toggle="modal" data-target="#myModal" onclick="MuaHang('modules/giohang/giohang_ajax.php?masp={{  $medicine->id }}&amp;method=add&amp;sl='+document.getElementById('slspct').value+'&amp;item=1','loaddatacart');">
                            <i class="fa fa-cart-plus fa-lg"></i> Đặt mua
                        </a>
                    </div>
                </div>
                <div class="clear"></div>
                @include('frontsite.elements.tags')
                <div class="clear"></div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div class="clear"></div>
        </div>
    </div>
</div>
@endsection
@section('script')

@endsection