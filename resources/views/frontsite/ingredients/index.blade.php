@extends('frontsite.index')
@section('title', __('seo.ingredient.title'))
@section('title_ab', __('seo.ingredient.title_ab'))
@section('description', __('seo.ingredient.description'))
@section('keywords', __('seo.ingredient.keyword'))
@section('body_class', '_hoatchat')
@section('style')

@endsection
@section('content')
<div id="wapper_main">
    <div class="content_main">
        <div class="content_content">

            @include('frontsite.ingredients.elements.search-form')
            <div class="hoatchat-research">
                <h3>Tìm kiếm theo bảng chữ cái</h3>
                <ul>
                    <li data-value="A">
                        <a href="hoat-chat/char=A" rel="nofollow" title="A">A</a>
                    </li>
                    <li data-value="B">
                        <a href="hoat-chat/char=B" rel="nofollow" title="B">B</a>
                    </li>
                    <li data-value="C">
                        <a href="hoat-chat/char=C" rel="nofollow" title="C">C</a>
                    </li>
                    <li data-value="D">
                        <a href="hoat-chat/char=D" rel="nofollow" title="D">D</a>
                    </li>
                    <li data-value="E">
                        <a href="hoat-chat/char=E" rel="nofollow" title="E">E</a>
                    </li>
                    <li data-value="F">
                        <a href="hoat-chat/char=F" rel="nofollow" title="F">F</a>
                    </li>
                    <li data-value="G">
                        <a href="hoat-chat/char=G" rel="nofollow" title="G">G</a>
                    </li>
                    <li data-value="H">
                        <a href="hoat-chat/char=H" rel="nofollow" title="H">H</a>
                    </li>
                    <li data-value="I">
                        <a href="hoat-chat/char=I" rel="nofollow" title="I">I</a>
                    </li>
                    <li data-value="J">
                        <a href="hoat-chat/char=J" rel="nofollow" title="J">J</a>
                    </li>
                    <li data-value="K">
                        <a href="hoat-chat/char=K" rel="nofollow" title="K">K</a>
                    </li>
                    <li data-value="L">
                        <a href="hoat-chat/char=L" rel="nofollow" title="L">L</a>
                    </li>
                    <li data-value="M">
                        <a href="hoat-chat/char=M" rel="nofollow" title="M">M</a>
                    </li>
                    <li data-value="N">
                        <a href="hoat-chat/char=N" rel="nofollow" title="N">N</a>
                    </li>
                    <li data-value="O">
                        <a href="hoat-chat/char=O" rel="nofollow" title="O">O</a>
                    </li>
                    <li data-value="P">
                        <a href="hoat-chat/char=P" rel="nofollow" title="P">P</a>
                    </li>
                    <li data-value="Q">
                        <a href="hoat-chat/char=Q" rel="nofollow" title="Q">Q</a>
                    </li>
                    <li data-value="R">
                        <a href="hoat-chat/char=R" rel="nofollow" title="R">R</a>
                    </li>
                    <li data-value="S">
                        <a href="hoat-chat/char=S" rel="nofollow" title="S">S</a>
                    </li>
                    <li data-value="T">
                        <a href="hoat-chat/char=T" rel="nofollow" title="T">T</a>
                    </li>
                    <li data-value="U">
                        <a href="hoat-chat/char=U" rel="nofollow" title="U">U</a>
                    </li>
                    <li data-value="V">
                        <a href="hoat-chat/char=V" rel="nofollow" title="V">V</a>
                    </li>
                    <li data-value="W">
                        <a href="hoat-chat/char=W" rel="nofollow" title="W">W</a>
                    </li>
                    <li data-value="X">
                        <a href="hoat-chat/char=X" rel="nofollow" title="X">X</a>
                    </li>
                    <li data-value="Y">
                        <a href="hoat-chat/char=Y" rel="nofollow" title="Y">Y</a>
                    </li>
                    <li data-value="Z">
                        <a href="hoat-chat/char=Z" rel="nofollow" title="Z">Z</a>
                    </li>
                </ul>
            </div><!-- /.hoatchat-research -->
            <div class="hoatchat-thongdung">
                <div class="inner clearfix">
                    <h3 class="text-uppercase">Các hoạt chất thông dụng nhất</h3>
                    <ul>
                        <li>
                            <a href="acid-clavulanic.hc" title="Acid clavulanic">Acid clavulanic</a>
                        </li>
                        <li>
                            <a href="penicilin.hc" title="Penicilin">Penicilin</a>
                        </li>
                        <li>
                            <a href="methicilin.hc" title="Methicilin">Methicilin</a>
                        </li>
                        <li>
                            <a href="ampicilin.hc" title="Ampicilin">Ampicilin</a>
                        </li>
                        <li>
                            <a href="amoxicilin.hc" title="Amoxicilin">Amoxicilin</a>
                        </li>
                        <li>
                            <a href="cloxacilline.hc" title="Cloxacilline">Cloxacilline</a>
                        </li>
                        <li>
                            <a href="piperacillin.hc" title="Piperacillin">Piperacillin</a>
                        </li>
                        <li>
                            <a href="cefadroxil.hc" title="Cefadroxil">Cefadroxil</a>
                        </li>
                        <li>
                            <a href="cephalexin.hc" title="Cephalexin">Cephalexin</a>
                        </li>
                        <li>
                            <a href="cefalothin.hc" title="Cefalexin">Cefalexin</a>
                        </li>
                        <li>
                            <a href="cephazolin.hc" title="Cephazolin">Cephazolin</a>
                        </li>
                        <li>
                            <a href="cefaclor.hc" title="Cefaclor">Cefaclor</a>
                        </li>
                        <li>
                            <a href="ceftriaxone.hc" title="Ceftriaxone">Ceftriaxone</a>
                        </li>
                        <li>
                            <a href="cefuroxime.hc" title="Cefuroxime">Cefuroxime</a>
                        </li>
                        <li>
                            <a href="cefixime-856.hc" title="Cefixime">Cefixime</a>
                        </li>
                        <li>
                            <a href="ceftazidim.hc" title="Ceftazidim">Ceftazidim</a>
                        </li>
                        <li>
                            <a href="cefotaxim.hc" title="Cefotaxim">Cefotaxim</a>
                        </li>
                        <li>
                            <a href="cefpodoxim.hc" title="Cefpodoxim">Cefpodoxim</a>
                        </li>
                        <li>
                            <a href="tetracyclin.hc" title="Tetracyclin">Tetracyclin</a>
                        </li>
                        <li>
                            <a href="doxycyline.hc" title="Doxycyline">Doxycyline</a>
                        </li>
                        <li>
                            <a href="clotetracyclin.hc" title="Clotetracyclin">Clotetracyclin</a>
                        </li>
                        <li>
                            <a href="oxytetracyclin.hc" title="Oxytetracyclin">Oxytetracyclin</a>
                        </li>
                        <li>
                            <a href="minocyclin.hc" title="Minocyclin">Minocyclin</a>
                        </li>
                        <li>
                            <a href="amikacin.hc" title="Amikacin">Amikacin</a>
                        </li>
                        <li>
                            <a href="tobramycin.hc" title="Tobramycin">Tobramycin</a>
                        </li>
                        <li>
                            <a href="neomycin.hc" title="Neomycin">Neomycin</a>
                        </li>
                        <li>
                            <a href="gentamicin.hc" title="Gentamicin">Gentamicin</a>
                        </li>
                        <li>
                            <a href="kanamycin.hc" title="Kanamycin">Kanamycin</a>
                        </li>
                        <li>
                            <a href="streptomycin.hc" title="Streptomycin">Streptomycin</a>
                        </li>
                        <li>
                            <a href="azithromycin.hc" title="Azithromycin">Azithromycin</a>
                        </li>
                        <li>
                            <a href="roxithromycin.hc" title="Roxithromycin">Roxithromycin</a>
                        </li>
                        <li>
                            <a href="erythromycin.hc" title="Erythromycin">Erythromycin</a>
                        </li>
                        <li>
                            <a href="rovamycin.hc" title="Rovamycin">Rovamycin</a>
                        </li>
                        <li>
                            <a href="clarithromycin.hc" title="Clarithromycin">Clarithromycin</a>
                        </li>
                        <li>
                            <a href="spiramycin.hc" title="Spiramycin">Spiramycin</a>
                        </li>
                        <li>
                            <a href="lincomycin.hc" title="Lincomycin">Lincomycin</a>
                        </li>
                        <li>
                            <a href="clindamycin.hc" title="Clindamycin">Clindamycin</a>
                        </li>
                        <li>
                            <a href="nalidixic-acid.hc" title="Nalidixic acid">Nalidixic acid</a>
                        </li>
                        <li>
                            <a href="lomefloxacin.hc" title="Lomefloxacin">Lomefloxacin</a>
                        </li>
                        <li>
                            <a href="ciprofloxacin.hc" title="Ciprofloxacin">Ciprofloxacin</a>
                        </li>
                        <li>
                            <a href="norfloxacin.hc" title="Norfloxacin">Norfloxacin</a>
                        </li>
                        <li>
                            <a href="ofloxacin.hc" title="Ofloxacin">Ofloxacin</a>
                        </li>
                        <li>
                            <a href="levofloxacin.hc" title="Levofloxacin">Levofloxacin</a>
                        </li>
                        <li>
                            <a href="gatifloxacin.hc" title="Gatifloxacin">Gatifloxacin</a>
                        </li>
                        <li>
                            <a href="moxifloxacin.hc" title="Moxifloxacin">Moxifloxacin</a>
                        </li>
                        <li>
                            <a href="pefloxacin.hc" title="Pefloxacin">Pefloxacin</a>
                        </li>
                        <li>
                            <a href="sparfloxacin.hc" title="Sparfloxacin">Sparfloxacin</a>
                        </li>
                        <li>
                            <a href="sulfaguanidin.hc" title="Sulfaguanidin">Sulfaguanidin</a>
                        </li>
                        <li>
                            <a href="levothyroxine.hc" title="Levothyroxine">Levothyroxine</a>
                        </li>
                        <li>
                            <a href="orciprenalin.hc" title="Orciprenalin">Orciprenalin</a>
                        </li>
                        <li>
                            <a href="cefdinir.hc" title="Cefdinir">Cefdinir</a>
                        </li>
                        <li>
                            <a href="cefepim.hc" title="Cefepim">Cefepim</a>
                        </li>
                        <li>
                            <a href="linezolid.hc" title="Linezolid">Linezolid</a>
                        </li>
                    </ul>
                </div><!-- /.inner -->
            </div><!-- /.hoatchat-thongdung -->
            <div class="clear"></div>
        </div>
    </div>
</div>
@endsection
@section('script')


@endsection