@extends('frontsite.index')
@section('title', $ingredient->name ? $ingredient->name : __('seo.ingredient.title_ab'))
@section('title_ab', $ingredient->seo_name ? $ingredient->seo_name : __('seo.ingredient.title_ab'))
@section('description', $ingredient->seo_description ? $ingredient->seo_description : __('seo.ingredient.seo_description'))
@section('keywords', $ingredient->seo_keyword ? $ingredient->seo_keyword : __('seo.ingredient.keyword'))
@section('image', $ingredient->thumbnail ?? $ingredient->thumbnail)
@section('body_class', '_hoatchat _chitiet')
@section('style')

@endsection
@section('content')
<div id="wapper_main">
    <div class="content_main">
        <div class="content_content">

            <section class="hoatchat-chitiet">
                <div class="topbar">
                    <strong>Thông tin</strong> {{ $ingredient->name }}
                </div><!-- /.topbar -->
                <div class="conts noidungchitiet">
                    <div class="noidung"> 
                        {!! $ingredient->content !!}
                    </div><!-- /.noidung -->
                </div><!-- /.conts -->
                <strong>
                    <p style="font-size:13px;padding-left:10px"><i>Lưu ý: Dùng thuốc theo chỉ định của Bác sĩ. Tuyệt đối không được tự ý dùng thuốc kê đơn mà không có sự hướng dẫn của y bác sĩ và người có chuyên môn.</i></p>
                </strong>
            </section>
            <div class="noidungchitiet">
            </div><!-- /.noidungchitiet -->
            <div class="clear"></div>
        </div>
    </div>
</div>
@endsection
@section('script')


@endsection