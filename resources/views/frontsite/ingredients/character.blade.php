@extends('frontsite.index')
@section('title', __('seo.ingredient.title'))
@section('title_ab', __('seo.ingredient.title_ab'))
@section('description', __('seo.ingredient.description'))
@section('keywords', __('seo.ingredient.keyword'))
@section('body_class', '_hoatchat')
@section('style')

@endsection
@section('content')
<div id="wapper_main">
    <div class="content_main">
        <div class="content_content">

            <aside class="hoatchat-group-search">
                <div class="hoatchat-timkiem">
                    <form action="hoat-chat/" data-url="hoat-chat/" onsubmit="return timHoatChat(this);">
                        <input type="text" name="char" class="hoatchat-input" autocomplete="off" placeholder="Nhập tên hoạt chất cần tìm" value="{{ $character }}" required="">
                        <button type="submit">
                            <i class="fa fa-search" aria-hidden="true"></i>
                        </button>
                    </form>
                </div><!-- /.timkiem -->
                <img alt="" height="150" src="uploads/images/banner-hoat-chat(1).png" width="900">

            </aside><!-- /.hoatchat-group-search -->
            <div class="hoatchat-research">
                <h3>Tìm kiếm theo bảng chữ cái</h3>
                <ul>
                    <li data-value="A">
                        <a href="hoat-chat/char=A" rel="nofollow" title="A">A</a>
                    </li>
                    <li data-value="B">
                        <a href="hoat-chat/char=B" rel="nofollow" title="B">B</a>
                    </li>
                    <li data-value="C">
                        <a href="hoat-chat/char=C" rel="nofollow" title="C">C</a>
                    </li>
                    <li data-value="D">
                        <a href="hoat-chat/char=D" rel="nofollow" title="D">D</a>
                    </li>
                    <li data-value="E">
                        <a href="hoat-chat/char=E" rel="nofollow" title="E">E</a>
                    </li>
                    <li data-value="F">
                        <a href="hoat-chat/char=F" rel="nofollow" title="F">F</a>
                    </li>
                    <li data-value="G">
                        <a href="hoat-chat/char=G" rel="nofollow" title="G">G</a>
                    </li>
                    <li data-value="H">
                        <a href="hoat-chat/char=H" rel="nofollow" title="H">H</a>
                    </li>
                    <li data-value="I">
                        <a href="hoat-chat/char=I" rel="nofollow" title="I">I</a>
                    </li>
                    <li data-value="J">
                        <a href="hoat-chat/char=J" rel="nofollow" title="J">J</a>
                    </li>
                    <li data-value="K">
                        <a href="hoat-chat/char=K" rel="nofollow" title="K">K</a>
                    </li>
                    <li data-value="L">
                        <a href="hoat-chat/char=L" rel="nofollow" title="L">L</a>
                    </li>
                    <li data-value="M">
                        <a href="hoat-chat/char=M" rel="nofollow" title="M">M</a>
                    </li>
                    <li data-value="N">
                        <a href="hoat-chat/char=N" rel="nofollow" title="N">N</a>
                    </li>
                    <li data-value="O">
                        <a href="hoat-chat/char=O" rel="nofollow" title="O">O</a>
                    </li>
                    <li data-value="P">
                        <a href="hoat-chat/char=P" rel="nofollow" title="P">P</a>
                    </li>
                    <li data-value="Q">
                        <a href="hoat-chat/char=Q" rel="nofollow" title="Q">Q</a>
                    </li>
                    <li data-value="R">
                        <a href="hoat-chat/char=R" rel="nofollow" title="R">R</a>
                    </li>
                    <li data-value="S">
                        <a href="hoat-chat/char=S" rel="nofollow" title="S">S</a>
                    </li>
                    <li data-value="T">
                        <a href="hoat-chat/char=T" rel="nofollow" title="T">T</a>
                    </li>
                    <li data-value="U">
                        <a href="hoat-chat/char=U" rel="nofollow" title="U">U</a>
                    </li>
                    <li data-value="V">
                        <a href="hoat-chat/char=V" rel="nofollow" title="V">V</a>
                    </li>
                    <li data-value="W">
                        <a href="hoat-chat/char=W" rel="nofollow" title="W">W</a>
                    </li>
                    <li data-value="X">
                        <a href="hoat-chat/char=X" rel="nofollow" title="X">X</a>
                    </li>
                    <li data-value="Y">
                        <a href="hoat-chat/char=Y" rel="nofollow" title="Y">Y</a>
                    </li>
                    <li data-value="Z">
                        <a href="hoat-chat/char=Z" rel="nofollow" title="Z">Z</a>
                    </li>
                </ul>
            </div><!-- /.hoatchat-research -->
            <ul class="hoatchat-ds ">
                <li class="title">
                    <h3>{{ $character }}</h3>
                </li>
                @foreach($ingredients as $ingredient)
                <li>
                    <a href="{{ $ingredient->slug }}.hc" title="{{ $ingredient->name }}">{{ $ingredient->name }}</a>
                </li>
                @endforeach
            </ul>
            <div class="clear"></div>
        </div>
    </div>
</div>
@endsection
@section('script')


@endsection