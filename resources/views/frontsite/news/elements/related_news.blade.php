<div class="title_tinlienquan"><i class="fa fa-chevron-circle-right"></i>Tin cùng chuyên mục<h2></h2>
</div>
@if(isset($posts_related))
<ul class="tinlienquan">
    @foreach($posts_related as $key => $post)
    <li>
    <a href="{{ $post->post_slug }}.html" title="{{ $post->post_title }}"><i class="fa fa-angle-right fa-lg"></i> {{ $post->post_title }}</a>
    </li>
    @endforeach
</ul>
<div class="clear"></div>
@endif