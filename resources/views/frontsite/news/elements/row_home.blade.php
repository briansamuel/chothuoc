<div class="row_home">
    <div class="home_content70 ">
        <h2 class="title">
            <div><a class="_colormain" href="{{ $slug }}/" title="{{ $title }}">{{ $title }}</a></div>
        </h2>
        <div class="addxemthem wow fadeInLeft" data-wow-delay="0.5s"><a class="_colormain" href="{{ $slug }}/" title="Xem thêm">+</a></div>
        <div class="clear"></div>
        @if(count($posts) > 0)
        <div class="nhomtin_col1 wow fadeInLeft" data-wow-delay="0.5s">
            <div class="itemtin">
                <div class="img"><a href="{{ $posts[0]->post_slug }}.html" title="{{ $posts[0]->post_title }}">
                    <img src="{{ $posts[0]->post_thumbnail }}" alt="{{ $posts[0]->post_title }}"></a>
                </div>
                <div class="description cc_cursor">
                    <p class="titletin"><a href="{{ $posts[0]->post_slug }}.html" title="{{ $posts[0]->post_title }}">{{ $posts[0]->post_title }}</a></p>
                    <p>{{ $posts[0]->post_description }}</p>
                </div>
            </div>
        </div>
        <div class="nhomtin_col2">
            @foreach($posts as $key => $post)
            @if($key > 0)
            <div class="itemtin  wow fadeInUp" data-wow-delay="0.2s">
                <div class="img"><a href="{{ $post->post_slug }}.html" title="{{ $post->post_title }}">
                    <img src="{{ $post->post_thumbnail }}" alt="{{ $post->post_title }}"></a></div>
                <p class="titletin">
                    <a href="{{ $post->post_slug }}.html" title="{{ $post->post_title }}">{{ $post->post_title }}</a></p>
            </div>
            <div class="clear"></div>
            @endif
            @endforeach
           
            
        </div>
        @endif
    </div>
</div>