@extends('frontsite.index')
@section('title', $post->post_title ? $post->post_title : __('seo.news.title_ab'))
@section('title_ab', $post->post_seo_title ? $post->post_seo_title : __('seo.news.title_ab'))
@section('description', $post->post_seo_description ? $post->post_seo_description : __('seo.news.post_seo_description'))
@section('keywords', $post->post_seo_keyword ? $post->post_seo_keyword : __('seo.news.keyword'))
@section('image', $post->post_thumbnail ?? $post->post_thumbnail)
@section('body_class', '_noidung _chitiet')
@section('style')

@endsection
@section('content')
<div id="wapper_main">
    <div class="content_main">
        <div class="content_content">
            
            @include('frontsite.elements.breadcrumb')
            <div class="clear"></div>
            <div class="noidungchitiet">
                <h1 class="title">{{ $post->post_title }}</h1>
                <div class="ngaycapnhat">{{ date("d/m/Y - H:i A", strtotime($post->created_at)) }}</div>
                @include('frontsite.elements.social')
 
                <div class="noidung">
                    {!! $post->post_content !!}
                </div>
                <div class="clear"></div>
                @include('frontsite.elements.tags')
                
                @include('frontsite.elements.social')
  
                <br>
                @include('frontsite.news.elements.related_news')
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
@endsection
@section('script')


@endsection