@extends('frontsite.index')
@section('title', __('seo.news.title'))
@section('title_ab', __('seo.news.title_ab'))
@section('description', __('seo.news.description'))
@section('keywords', __('seo.news.keyword'))
@section('body_class', '_noidung')
@section('style')

@endsection
@section('content')
<div id="wapper_main">
    <div class="content_main">
        <div class="content_content">
            <div xmlns:v="http://rdf.data-vocabulary.org/" id="breadcrumbs">
                <ul itemprop="breadcrumb">
                    <li typeof="v:Breadcrumb"><a property="v:title" rel="v:url" href="https://chosithuoc.com/" title=""><label><i class="fa fa-home fa-2x"></i></label></a></li>
                    <li typeof="v:Breadcrumb"><a property="v:title" rel="v:url" href="https://chosithuoc.com/tin-tuc/" title="Tin tức">Tin tức</a></li>
                </ul>
            </div>
            <div class="clear"></div>
            {!! $html_posts !!}
            <div class="clear"></div>
            <div class="clear"></div>
        </div>
    </div>
</div>
@endsection
@section('script')


@endsection