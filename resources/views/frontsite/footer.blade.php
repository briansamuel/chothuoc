<div id="wapper_bottom">
    <div class="ws-container">
        <div class="bottom_content">

            <div class="group_bottom group_bottom_menu">
                <div class="title_bottom title-sp">
                    <a href="danh-muc/my-pham" title="Mỹ phẩm">
                        Mỹ phẩm
                    </a>
                </div>
                <div class="noidung_bottom">
                    <ul>
                        <li><a href="danh-muc/cham-soc-da-mat" title="Chăm sóc da mặt">Chăm sóc da mặt</a>
                        </li>
                        <li><a href="danh-muc/cham-soc-body" title="Chăm sóc Body">Chăm sóc Body</a></li>
                        <li><a href="danh-muc/sua-rua-mat-tri-mun" title="Sữa rửa mặt trị mụn">Sữa rửa mặt
                                trị mụn</a></li>
                        <li><a href="danh-muc/nuoc-xit-khoang" title="Nước xịt khoáng">Nước xịt khoáng</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="group_bottom group_bottom_menu">
                <div class="title_bottom title-sp">
                    <a href="danh-muc/thuoc-tay-y-dong-y" title="Thuốc tây y, Đông y">
                        Thuốc tây y, Đông y
                    </a>
                </div>
                <div class="noidung_bottom">
                    <ul>
                        <li><a href="danh-muc/thuoc-khang-sinh" title="Thuốc kháng sinh, Kháng nấm">Thuốc
                                kháng sinh, Kháng nấm</a></li>
                        <li><a href="danh-muc/thuoc-tim-mach" title="Thuốc tim mạch & Huyết áp">Thuốc tim
                                mạch & Huyết áp</a></li>
                        <li><a href="danh-muc/thuoc-ho" title="Thuốc Hô Hấp">Thuốc Hô Hấp</a></li>
                        <li><a href="danh-muc/thuoc-bao-tu-da-day" title="Thuốc Tiêu Hóa, gan mật">Thuốc
                                Tiêu Hóa, gan mật</a></li>
                    </ul>
                </div>
            </div>
            <div class="group_bottom group_bottom_menu">
                <div class="title_bottom title-sp">
                    <a href="danh-muc/thuoc-xuong-khop" title="Thuốc xương khớp">
                        Thuốc xương khớp
                    </a>
                </div>
                <div class="noidung_bottom">
                    <ul>
                        <li><a href="danh-muc/glucosamine" title="Glucosamine">Glucosamine</a></li>
                        <li><a href="danh-muc/sun-ca-map" title="Sụn cá mập">Sụn cá mập</a></li>
                        <li><a href="danh-muc/calcium" title="Calcium">Calcium</a></li>
                    </ul>
                </div>
            </div>
            <div class="group_bottom group_bottom_menu">
                <div class="title_bottom title-sp">
                    <a href="danh-muc/thuoc-giam-can" title="Thuốc giảm cân">
                        Thuốc giảm cân
                    </a>
                </div>
                <div class="noidung_bottom">
                    <ul>
                        <li><a href="danh-muc/thuoc-giam-can-cua-my" title="Thuốc giảm cân của mỹ">Thuốc
                                giảm cân của mỹ</a></li>
                        <li><a href="danh-muc/thuoc-giam-can-thai-lan" title="Thuốc giảm cân thái lan">Thuốc giảm cân thái lan</a></li>
                        <li><a href="danh-muc/thuoc-giam-can-nhat-ban" title="Thuốc giảm cân nhật bản">Thuốc giảm cân nhật bản</a></li>
                        <li><a href="danh-muc/kem-tan-mo" title="Kem tan mỡ">Kem tan mỡ</a></li>
                        <li><a href="danh-muc/tra-giam-can" title="Trà giảm cân">Trà giảm cân</a></li>
                    </ul>
                </div>
            </div>
            <div class="group_bottom group_bottom_menu">
                <div class="title_bottom title-sp">
                    <a href="danh-muc/thuoc-bo-than" title="Thuốc bổ thận">
                        Thuốc bổ thận
                    </a>
                </div>
                <div class="noidung_bottom">
                    <ul>
                        <li><a href="danh-muc/thuoc-sinh-ly-nam" title="Thuốc sinh lý nam">Thuốc sinh lý
                                nam</a></li>
                        <li><a href="danh-muc/thuoc-sinh-ly-nu" title="Thuốc sinh  lý nữ">Thuốc sinh lý
                                nữ</a></li>
                        <li><a href="danh-muc/thuoc-vo-sinh" title="Thuốc vô sinh">Thuốc vô sinh</a></li>
                        <li><a href="danh-muc/gel-boi-tron" title="Gel bôi trơn">Gel bôi trơn</a></li>
                    </ul>
                </div>
            </div>
            <div class="group_bottom group_bottom_menu">
                <div class="title_bottom title-sp">
                    <a href="danh-muc/thuc-pham-chuc-nang" title="Thực phẩm chức năng">
                        Thực phẩm chức năng
                    </a>
                </div>
                <div class="noidung_bottom">
                    <ul>
                        <li><a href="danh-muc/collagen" title="Collagen">Collagen</a></li>
                        <li><a href="danh-muc/nhau-thai-cuu" title="Nhau thai cừu">Nhau thai cừu</a></li>
                        <li><a href="danh-muc/vitamin-e" title="Vitamin E">Vitamin E</a></li>
                        <li><a href="danh-muc/omega-3" title="Omega 3">Omega 3</a></li>
                    </ul>
                </div>
            </div>
            <div class="group_bottom group_bottom_menu">
                <div class="title_bottom title-sp">
                    <a href="thuc-pham-cao-cap" title="Sữa & Thực phẩm cao cấp">
                        Sữa & Thực phẩm cao cấp
                    </a>
                </div>
                <div class="noidung_bottom">
                    <ul>
                        <li><a href="danh-muc/hat-mac-ca" title="Hạt mắc ca">Hạt mắc ca</a></li>
                        <li><a href="danh-muc/hat-hanh-nhan" title="Hạt hạnh nhân">Hạt hạnh nhân</a></li>
                        <li><a href="danh-muc/qua-oc-cho" title="Quả óc chó">Quả óc chó</a></li>
                        <li><a href="danh-muc/hat-chia" title="Hạt chia">Hạt chia</a></li>
                    </ul>
                </div>
            </div>
            <div class="group_bottom group_bottom_menu">
                <div class="title_bottom title-sp">
                    <a href="danh-muc/thiet-bi-y-te" title="Thiết bị y tế">
                        Thiết bị y tế
                    </a>
                </div>
                <div class="noidung_bottom">
                    <ul>
                        <li><a href="danh-muc/may-do-huyet-ap" title="Máy đo huyết áp">Máy đo huyết áp</a>
                        </li>
                        <li><a href="danh-muc/may-do-duong-huyet" title="Máy đo đường huyết">Máy đo đường
                                huyết</a></li>
                        <li><a href="danh-muc/may-massage" title="Máy massage">Máy massage</a></li>
                        <li><a href="danh-muc/can-suc-khoe" title="Cân sức khỏe">Cân sức khỏe</a></li>
                    </ul>
                </div>
            </div>
            <div class="group_bottom">
                <div class="title_bottom">
                    <a href="#" title="Tài khoản">Tài khoản</a>
                </div><!-- /.title_bottom -->
                <div class="noidung_bottom">
                    <ul>
                        <li>
                            <a href="#" title="" data-toggle="modal" data-target="#myModal" onclick="Get_Data('ajax/?action=login','loaddatacart');" rel="nofollow">
                                Đăng nhập
                            </a>
                        </li>
                        <li>
                            <a href="#" title="" data-toggle="modal" data-target="#myModal" onclick="Get_Data('ajax/?action=register','loaddatacart');" rel="nofollow">
                                Đăng ký
                            </a>
                        </li>
                        <li>
                            <a href="#" title="" data-toggle="modal" data-target="#myModal" onclick="Get_Data('ajax/?action=checkorder','loaddatacart');" rel="nofollow">
                                Kiểm tra đơn hàng
                            </a>
                        </li>
                    </ul>
                </div><!-- /.noidung_bottom -->
            </div><!-- /.group_bottom -->
            <div class="group_bottom">
                <div class="title_bottom">
                    <a href="#" title="Về chúng tôi">
                        Về chúng tôi</a>
                </div><!-- /.title_bottom -->
                <div class="noidung_bottom">
                    <ul>
                        <li>
                            <a href="gioi-thieu" title="Giới thiệu">
                                Giới thiệu
                            </a>
                        </li>
                        <li>
                            <a href="index.html" title="">

                            </a>
                        </li>
                        <li>
                            <ul style="display:block">
                                <li><a href="tuyen-dung" target="_blank">Tuyển dụng</a></li>
                                <li><a href="chinh-sach-giao-hang" target="_blank">Chính sách giao
                                        hàng</a></li>
                                <li><a href="chinh-sach-doi-tra" target="_blank">Chính sách đổi trả</a>
                                </li>
                                <li><a href="chinh-sach-bao-mat" target="_blank">Chính sách bảo mật</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div><!-- /.noidung_bottom -->
            </div><!-- /.group_bottom -->
            <div class="group_bottom">
                <div class="title_bottom"><a href="#" title="Mạng xã hội">
                        Mạng xã hội</a></div><!-- /.title_bottom -->
                <div class="noidung_bottom">
                    <p><a href="#" target="_blank"><img alt="Facebook Page" height="30" src="uploads/images/icon-facebook.png" style="margin-right:5px" width="30" /></a><a href="#" target="_blank"><img alt="Google Plus" height="30" src="uploads/images/icon-google.png" style="margin-right:5px" width="30" /></a><a href="https://www.youtube.com/channel/UCVq3fbZ7MfXsQaBCQ1yNbCw" target="_blank"><img alt="Youtube" height="35" src="uploads/images/icon-youtube.png" style="margin-right:5px" width="35" /></a><br />
                        <img alt="" src="uploads/images/dathongbao(1).png" width="120" /></p>

                </div><!-- /.noidung_bottom -->
            </div><!-- /.group_bottom -->
            <div class="group_bottom">
                <div class="noidung_bottom">
                    <p><strong>{{ __('frontsite.footer.company_name') }}</strong><br />
                        <b>Địa chỉ: </b>{{ __('frontsite.footer.address') }}<br />
                        <strong>Email:</strong> {{ __('frontsite.footer.email') }}<br />
                        <strong>Tel:</strong> <span style="color:#1e91cf;"><strong>{{ __('frontsite.footer.tel_1') }}</strong></span>

                    <table align="center">
                        <tbody>
                            <tr>
                                <td><img alt="ios" src="uploads/images/ios-icon.png" width="150" /></td>
                                <td><a href="#" target="_blank"><img alt="ios" src="uploads/images/android.png" width="150" /></a></td>
                            </tr>
                        </tbody>
                    </table>

                </div><!-- /.noidung_bottom -->
            </div><!-- /.group_bottom -->
        </div>
    </div><!-- /.ws-container -->
</div>

@include('frontsite.elements.copyright')