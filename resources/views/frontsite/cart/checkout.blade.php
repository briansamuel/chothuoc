@extends('frontsite.index')
@section('title', __('seo.cart.title_ab'))
@section('title_ab', __('seo.cart.title_ab'))
@section('description', __('seo.cart.seo_description'))
@section('keywords', __('seo.cart.keyword'))
@section('body_class', '_giohang _thanhtoan')
@section('style')

@endsection
@section('content')
<div id="wapper_main">
    <div class="content_main">
        <div class="content_content">
            <div class="panel panel-default">
                <div class="panel-heading panel-heading-edit"><i class="fa fa-shopping-cart fa-2x"></i> Thông báo</div>
                <div class="panel-body">
                    <p style="text-align:center"><span style="color:#ff0000"><span style="font-size:20px"><strong>Chúc mừng quý khách đã mua hàng thành công!</strong></span></span></p>

                    <p style="text-align:center">&nbsp;</p>

                    <p style="text-align:center"><strong><img alt="" src="https://chosithuoc.com/uploads/images/THANKS.JPG" width="461"></strong></p>

                    <p style="text-align:center">&nbsp;</p>

                    <p style="text-align:center"><span style="font-size:20px">Chúng tôi sẽ liên hệ với Qúy Anh, Chị trong thời gian sớm nhất.</span></p>

                    <p style="text-align:center">&nbsp;</p>

                    <p style="text-align:center"><span style="font-size:20px">Kính chúc Anh Chị sức khỏe, thành công và hạnh phúc.</span></p>

                    <p style="text-align:center">&nbsp;</p>

                    <p style="text-align:center"><span style="font-size:20px">Trân trọng!</span></p>

                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
@endsection
@section('script')


@endsection