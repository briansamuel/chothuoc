<div class="payment clearfix">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="panel-title">
                <div class="payment-title cc_cursor">
                    <i class="fa fa-credit-card"></i><!-- /.fa -->
                    Chọn hình thức thanh toán</div>
            </div><!-- /.panel-title -->
        </div><!-- /.panel-heading -->
        <div class="panel-body">
            <div class="form-group">
                <label class="icheck-wrap">
                    <div class="iradio_minimal-blue checked" style="position: relative;"><input type="radio" name="payment" value="0" checked="" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"><ins class="iCheck-helper cc_pointer" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                    <span>Thanh toán tiền mặt khi nhận hàng (COD)</span>
                </label>
            </div><!-- /.form-group -->
            <div class="form-group">
                <label class="icheck-wrap cc_pointer">
                    <div class="iradio_minimal-blue" style="position: relative;"><input type="radio" name="payment" value="1" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
                    <span>Chuyển khoản ngân hàng</span>
                </label>
            </div><!-- /.form-group -->
            <div class="payment-content hightlight">
                <pre>
                   
                    <p><strong>Ngân hàng: {{ __('frontsite.footer.bank_name') }}</strong><br>
                            <strong>Số tài khoản:</strong>&nbsp;{{ __('frontsite.footer.STK') }}<br>
                            <strong>Người đại diện:</strong>&nbsp;{{ __('frontsite.footer.bank_account') }}<br>
                            &nbsp;</p>
                    <p></p>
                </pre>
            </div><!-- /.payment-content -->
        </div><!-- /.panel-body -->
    </div><!-- /.panel panel-default -->
</div><!-- /.payment clearfix -->