<div class="input-group">
    <div class="input-group-addon"><i class="fa fa-bell fa-lg"></i> Phường/ Xã</div>
    <select name="phuongxa" id="phuongxa" onchange="Ajax_noreturn('modules/giohang/giohang_tinhthanh.php?id='+this.value)" class="form-control select2">
        <option value="">Vui lòng chọn</option>
        @foreach($wards as $key => $ward)
        <option value="{{ $ward->id }}">{{ $ward->_name }}</option>
        @endforeach
    </select>
</div>