<div class="input-group">
    <div class="input-group-addon"><i class="fa fa-bell fa-lg"></i> Quận/huyện</div>
    <select onchange="Get_Data('modules/giohang/giohang_tinhthanh.php?name=phuongxa&amp;id='+this.value,'loadphuongxa')" name="quanhuyen" id="quanhuyen" class="form-control select2">
        <option value="">Vui lòng chọn</option>
        @foreach($districts as $key => $district)
        <option value="{{ $district->id }}">{{ $district->_name }}</option>
        @endforeach
    </select>
</div>