<div class="panel panel-default">
    <div class="panel-heading">
        <div class="panel-title">
            <div class="payment-title">
                <i class="fa fa-shopping-cart"></i>
                Giỏ hàng của bạn</div>
        </div><!-- /.panel-title -->
    </div><!-- /.panel-heading -->
    <div class="panel-body" id="loaddata_giohang">
        <div class="giohang_group">
            @if(isset($products))
            @foreach($products as $key => $product)
            <div class="product_box">

                <div class="product_info">
                    <div class="name">
                        <div title="Xóa sản phẩm này" class="deletesp" onclick="MuaHang('modules/giohang/giohang_ajax.php?op=giohang&amp;masp={{ $product->id }}&amp;method=del','loaddata_giohang')">x</div><a href="{{ $product->slug }}" target="_blank" title="{{ $product->name }}">{{ $product->name }}</a>
                    </div>
                    <div class="product_img">
                        <img onerror="xulyloi(this)" src="{{ $product->image }}" alt="{{ $product->name }}" width="70">
                    </div>

                    <div class="item">
                        <ul>
                            <li><input onchange="MuaHang('modules/giohang/giohang_ajax.php?op=giohang&amp;masp={{ $product->id }}&amp;idkho={{ $product->id }}&amp;method=update&amp;sl='+this.value,'loaddata_giohang')" onkeyup="MuaHang('modules/giohang/giohang_ajax.php?op=giohang&amp;masp={{ $product->id }}&amp;idkho={{ $product->id }}&amp;method=update&amp;sl='+this.value,'loaddata_giohang')" class="txt_soluong" name="C{{ $product->id }}" type="number" min="1" max="100000" value="{{ $product->quantity }}" size="4" maxlength="4"></li>
                            <li>&nbsp; x {{number_format($product->price)}} đ = {{number_format($product->price*$product->quantity)}} đ</li>
                            <li class="li1">
                                <div title="Xóa thuộc tính này" class="deletesp" onclick="MuaHang('modules/giohang/giohang_ajax.php?op=giohang&amp;masp={{ $product->id }}&amp;idkho={{ $product->id }}&amp;method=delkho','loaddata_giohang')">x</div>
                            </li>
                        </ul>
                        <div class="clear"></div>
                        <div>
                            <ul class="ul-thuoctinhkho"></ul>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
            <div class="product_line_nhe"></div>
            @endforeach
           @endif
        </div>
        <div class="tongtien_giohang">
            @if(isset($total_price))
            Tổng tiền: {{number_format($total_price)}} đ
            @endif
        </div>
        <div class="xemgiohang btn btn-success"><a href="/" title="Tiếp tục mua hàng">Tiếp tục mua hàng</a></div>
        <div class="clear"></div>
    </div>
</div>