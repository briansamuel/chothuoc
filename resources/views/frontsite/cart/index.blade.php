@extends('frontsite.index')
@section('title', __('seo.cart.title_ab'))
@section('title_ab', __('seo.cart.title_ab'))
@section('description', __('seo.cart.seo_description'))
@section('keywords', __('seo.cart.keyword'))
@section('body_class', '_giohang')
@section('style')

@endsection
@section('content')
<div id="wapper_main">
    <div class="content_main">
        <div class="content_content">

            <div class="group_giohang cc_cursor">
                <div class="cot_dssp cc_cursor">

                    @include('frontsite.cart.elements.cart')


                </div>
                <div class="cot_form cc_cursor">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default cc_cursor">
                            <div class="panel-heading">
                                <h4 class="panel-title cc_cursor">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                                        <i class="fa fa-user fa-2x"></i> Mua hàng không cần đăng ký</a>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse  in">
                                <div class="panel-body">
                                    <form action="{{ route('checkout.index') }}" method="post" name="checkout" id="checkout">
                                        @csrf
                                        <div class="form-group form_giohang">
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-mobile fa-lg"></i> Điện thoại <span>(*)</span></div>
                                                <input class="form-control" name="phone" id="phone" value="">
                                                <input type="hidden" value="Vui lòng nhập số điện thoại" name="dienthoai_alert">
                                            </div>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-user-md fa-lg"></i> Họ tên <span>(*)</span></div>
                                                <input class="form-control" name="fullname" id="fullname" value="">
                                                <input type="hidden" value="Vui lòng nhập họ tên" name="hoten_alert">
                                            </div>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-envelope-o fa-lg"></i> Email</div>
                                                <input class="form-control" name="email" id="email" value="">
                                            </div>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-map-marker fa-lg"></i> Địa chỉ <span>(*)</span></div>
                                                <input class="form-control" name="address" id="address" value="">
                                                <input type="hidden" value="Vui lòng nhập địa chỉ nhận hàng" name="diachi_alert">
                                            </div>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-bell fa-lg"></i> Tỉnh thành</div>
                                                <select onchange="Get_Data('modules/giohang/giohang_tinhthanh.php?name=quanhuyen&amp;id='+this.value,'loadquanhuyen')" name="tinhthanh" id="tinhthanh" class="form-control select2">
                                                    <option value="">Vui lòng chọn</option>
                                                    @foreach($provines as $provine)
                                                    <option value="{{ $provine->id }}">{{ $provine->_name }}</option>    
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div id="loadquanhuyen"></div>
                                            <div id="loadphuongxa"></div>
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-comments fa-lg"></i> Ghi chú</div>
                                                <textarea class="form-control" rows="5" id="note" name="note"></textarea>
                                            </div>
                                            <input class="btn btn-primary btn_submit cc_pointer" type="submit" name="thanhtoan_giohang" id="thanhtoan_giohang" value="Đặt hàng">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <style>
                .content_main {
                    background-color: transparent;
                }
            </style>
            <div class="clear"></div>
        </div>
    </div>
</div>
@endsection
@section('script')


@endsection