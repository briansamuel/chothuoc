<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->default(0);
            $table->string('fullname');
            $table->string('email');
            $table->string('phone');
            $table->text('address');
            $table->string('ward');
            $table->string('district');
            $table->string('city');
            $table->text('products');
            $table->integer('price')->default(0);
            $table->enum('method_payment', ['bank', 'cod']);
            $table->text('note');
            $table->string('ip');
            $table->enum('status', ['paid', 'pending', 'comfirm'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
