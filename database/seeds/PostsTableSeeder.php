<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('posts')->insert([
            'post_title' => Str::random(20),
            'post_slug' => Str::random(20),
            'post_description' => Str::random(16),
            'post_content' => Str::random(100),
            'post_status' => 'draft',
            'post_author' => 'Admin',
            'post_type' => 'news',
            'language' => 'vi',
            'created_by_user' => 1,
            'updated_by_user' => 1,
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s")
        ]);
    }
}
