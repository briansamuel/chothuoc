var _link = $("#rootPath").data('url');
function timHoatChat(n) {
	var t = $(n),
	i = t.data('url');
	if( t.find('[name=char]').length == 0 || t.find('[name=char]').val().trim() == "" ){
		return !1;
	}
	else {
		i+= 'char=' + t.find('[name=char]').val();
		window.location.href = i;
		return !1;
	}
}
$( window ).on( "load", function () {
	var is_url = window.location.href;
	$('.danhmuccol>ul>li').each(function (index, element) {
		// element == this
		$(this).removeClass('active');
		var url = $(this).find('a').attr('href');
		if ( is_url == url ) {
			$(this).addClass('active');
		}
	});
} );
$(function() {
	var _link = $("#rootPath").data('url');
	$('.payment input').iCheck({
		checkboxClass: 'icheckbox_minimal-blue',
		radioClass: 'iradio_minimal-blue',
		increaseArea: '20%' // optional
	});
	$('.payment input').on('ifChanged', function(e) {
		var val;
		if ( $(this).prop('checked') ){
			val = $(this).val();
		}
		if (val == 1) {
			$('.payment-content').show();
		}
		else {
			$('.payment-content').hide();
		}
		if ( val ) {
			$.get('ajax/', {
				value: val,
				action: 'payment'
			}, function (res) {
			});
		}
	});
});

var select2opt = {
    amdLanguageBase: "./i18n/vi.js",
    language: "vi",
    containerCssClass: "form-control-select2"
}
$(function(){
    if ( jQuery().select2 ) {
        $('select.select2').select2(select2opt);
    }

    var csrf_token = $('meta[name="csrf-token"]').attr('content');
    $.ajaxPrefilter(function(options, originalOptions, jqXHR){
        if (options.type.toLowerCase() === "post") {
            // initialize `data` to empty string if it does not exist
            options.data = options.data || "";

            // add leading ampersand if `data` is non-empty
            options.data += options.data?"&":"";

            // add _token entry
            options.data += "_token=" + encodeURIComponent(csrf_token);
        }
    });
});



$(function() {
	$('#filterMobile').on('click', function() {
		var contents = $('#col1 > .filter-container').html();
		$('#loaddatacart').html(`
			<div class="modal-content modal-filter">
				<a href="#" title="close" class="close" data-dismiss="modal">
					<i class="fa fa-check" aria-hidden="true"></i>
				</a>
			`+contents+`</div>
		`);
	});
});
$(function() {
	if( $(window).width() <= 768 ) {
		$('.loctren-list').on('click', function() {
			$(this).addClass('full');
		});
	}
});
$(function() {
	$('#myModal').scroll(function() {
		var a = $('#myModal').scrollTop();
		$('#myModal .close').css('top',a+'px');
	});
});
$(function() {
	$('.search-icon-mobile').on('click', function() {
		var $this = $(this);
		if($this.children('.fa').hasClass('fa-search')) {
			$this.children('.fa').addClass('fa-times').removeClass('fa-search');
		}
		else {
			$this.children('.fa').removeClass('fa-times').addClass('fa-search');
		}
		$('.search-header').slideToggle();
	});
});
$(function() {
	$('.loctren-tim').on('keydown keypress keyup change', function() {
		var $this = $(this)
			$val = $this.val().toLowerCase();
		$this.parent('.head').next('.sublist-inner').find('label').hide();
		$this.parent('.head').next('.sublist-inner').find("span").filter(function() {
			return $(this).data("text").indexOf($val) >= 0;
		}).parent('label').show();
	});
});
$(function() {
	$('.danhmucmobile li.has-sub > .fa').live('click', function() {
		var $this = $(this);
		var $li = $(this).parent();
		$li.parent('ul').not($li).removeClass('active');
		$(this).parent('.has-sub').toggleClass('active');
		if($this.hasClass('fa-angle-down')) {
			$this.removeClass('fa-angle-down').addClass('fa-angle-up');
		}
		else {
			$this.removeClass('fa-angle-up').addClass('fa-angle-down');
		}
	});
});
$(function() {
	var _link = $("#rootPath").data('url');
	$('.sublist').on('click', function(e) {
		e.stopPropagation();
	});
	// CLick
	$('.sublist-inner > label').on('click', function() {
		var $this    = $(this),
		    $val     = $this.data('id'),
		    text     = $this.children('span').text(),
		    idnhomid = $this.data('idnhomid'),
		    choose   = $this.closest('.sublist').data('choose');
		if( $this.closest('.sublist').data('choose') == 'single' ) {
			$this.parent('.sublist-inner').find('label').not($this).removeClass('check');
		}
		$this.toggleClass('check');
		if( choose == 'single' ) {
			$('.filter-list').find('[data-idtype='+idnhomid+']').remove();
		}
		if( $(".filter-list").children('a').size() ){
			if( !$(".filter-list").find('.delall').length ) {
				$(".filter-list").append(`
					<a href="javascript:;" class="delall" onclick="DeleteFilterAll()">
						<span>Xóa tất cả</span>
						<i class="fa fa-close" aria-hidden="true"></i>
					</a>
				`);
			}
		}
		if( $this.hasClass('check') ) {
			$(".filter-list").append(`
				<a href="javascript:;" id="`+$val+`" data-idtype="`+idnhomid+`" onclick="DeleteFilter(this, '`+$val+`')">
					<span>`+text+`</span>
					<i class="fa fa-close" aria-hidden="true"></i>
				</a>
			`);
		}
		else {
			$(".filter-list").find('#'+$val).remove();
			if($(".filter-list").children('a').size() < 2 ) {
				$(".filter-list").find('.delall').remove();
			}
		}
		$.get(_link + "application/files/locNhanh_ajax.php", {
			noidungnhom: $val,
			phannhom   : idnhomid,
			check      : (!$this.hasClass('check') ? 'uncheck' : 'check'),
			flag       : 'loctren'
		}, function(data) {
			$('#loaditemsanpham').html(data)
		});
	});
	//
	$('.loctren-list > li').on('click', function() {
		$this = $(this);
		$('.loctren-list').children().not($this).find('.sublist').hide();
		//
		if( $this.find('.sublist').is(':visible') ){
			$this.find('.sublist').slideUp();
			$('.loctren-tim').val('');
			$('.sublist-inner > label').removeAttr('style');
		}
		else {
			$this.find('.sublist').slideDown();
		}
	});
	$('.sublist .close').on('click', function() {
		$('.sublist').slideUp();
		$('.loctren-tim').val('');
		$('.sublist-inner > label').removeAttr('style');
	});
});
// XOA FILTER STAR
function DeleteFilterStar(t) {
	var _link = $("#rootPath").data('url');
	$.post(_link + "ajax/", {
		action: 'delfilterstar'
	}, function(responsive) {
		if( responsive ){
			setTimeout(function() {
				location.reload();
			}, 1000);
		}
	});
}
// XOA ALL FILTER
function DeleteFilterAll() {
    var link = $('body').find('input[name=this_link_loc]').val();
    $.post(link, {
        action: "delallfilter"
    },
    function (data, textStatus, jqXHR) {
        if( data == 'success' ) {
            location.reload();
        }
    }
    );
}
// XOA FILTER
function DeleteFilter(t, i){
    var link   = $('body').find('input[name=this_link_loc]').val();
    var idtype = $(t).data('idtype');
    $.post(link, {
        action: "delfilter",
        i     : i,
        idtype: idtype
    },
    function (data, textStatus, jqXHR) {
        if ( data == 'success' )
			setTimeout(function () {
				location.reload();
			}, 5e2);
    }
    );
}
$(function ($) {
	$('#cssmenu > ul > li').hover(function () {
		$('.wapper_mark').css({
			position       : 'fixed',
			backgroundColor: '#000',
			opacity        : 0.7,
			display        : 'block',
			zIndex         : 97
		});
	}, function () {
		$('.wapper_mark').removeAttr('style');
	});
});
$(function() {
	if ($("._sanpham._chitiet .row-banner > div").length) {
		$("._sanpham._chitiet .row-banner > div").sticky({
			bottomSpacing: ($('#wapper_design').height() + 95)
		});
	}
});
jQuery(function () {
	$(window).click(function() {
	//Hide the menus if visible
		$('#resultSearch').html('');
	});
	$('#resultSearch').click(function(event){
		event.stopPropagation();
	});
});
$(function() {
	var _link = $("#rootPath").data('url');
	var typingTimer,
		doneTypingInterVal = 800,
		$inputLiveSearch = $("[name=key]"),
		$resultLiveSearch = null;
	//on keydown, clear the countdown
	$inputLiveSearch.on('keydown', function () {
		clearTimeout(typingTimer);
		if( !$(".qa-list").children(".loading").length ) {
			$(".qa-list").append(`
				<div class="loading">
					<i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
				</div>
			`);
			$(".qa-list").css({
				'position': 'relative'
			});
			$(".qa-list .loading").css({
				'position': 'absolute',
				'top': 0,
				'left': 0,
				'width': '100%',
				'height': '100%',
				'text-align': 'center',
				'background': 'rgba(0,0,0,0.18)',
				'display': 'block'
			});
			$(".qa-list .loading .fa").css({
				'position': 'absolute',
				'top': '35%'
			});
		}
	});
	$inputLiveSearch.on('keyup', function() {
		var thjs   = $(this),
		    val    = $.trim(thjs.val());
		clearTimeout(typingTimer);
		typingTimer = setTimeout(hamTimKiem, doneTypingInterVal);
	});
	function hamTimKiem() {
		var
			$thjs = $inputLiveSearch,
			val = $.trim($thjs.val()),
			idloai = $("[name='searchtype']").val();
		if( val && idloai ) {
			$.get(_link+"ajax/common/search/", {
				value: val,
				idloai: idloai
			},
				function (data, textStatus, jqXHR) {
					if( data ) {
						if( data.status == 'success' ) {
							$(".qa-list").html(data.result);
						}else {
							$(".qa-list").html(`
								<span class="alert alert-danger" style="display:block;">
								`+data.message+`
								</span>
							`);
							$(".qa-list .loading").remove();
						}
					}
				},
				"json"
			);
		}
	}
});
$(function() {
	$(".boxlink > a").click(function(e) {
		e.preventDefault();
		var id = $(this).attr('href');
		$('html, body').animate({
			scrollTop: $(id).offset().top - 110
		}, 1000);
	});
	var _link = $("#rootPath").data('url');
	$('.rate-line').live('click', function(){
		var $this = $(this),
			val = $this.data('star'),
			tag = $this.data('tag'),
			text = val + ' sao';
		$this.parent().children().removeClass('choosed');
		$this.toggleClass('choosed');
		$('.filter-list').find('.rated').remove();
		$(".filter-list").append(`
			<a href="javascript:;" class="rated" onclick="DeleteFilterStar(this)">
				<span>`+text+`</span>
				<i class="fa fa-close" aria-hidden="true"></i>
			</a>
		`);
		if( $(".filter-list").children('a').size() ){
			if( !$(".filter-list").find('.delall').length ) {
				$(".filter-list").append(`
					<a href="javascript:;" class="delall" onclick="DeleteFilterAll()">
						<span>Xóa tất cả</span>
						<i class="fa fa-close" aria-hidden="true"></i>
					</a>
				`);
			}
		}
		$.get(_link + "application/files/locNhanh_ajax.php", {
			value: val,
			tag: tag
		}, function(res) {
			$('.noidungdanhmuc').next('.pagination').hide();
			if( res ) {
				$('#loaditemsanpham').html(res);
			}
		});
	})
	// Loai tim kiem
	$('.searchtype').on('click', function() {
		var $this = $(this);
		var val = $this.data('value');
		$('.list-search-type').find('.active').removeClass('active');
		$this.parent('li').addClass('active');
		$('.list-search-type [name=searchtype]').val(val);
		$('[name=key]').val('');
	});
});
$(function() {
	var _link = $("#rootPath").data('url');
	$('#checkOrderBtn').live('click', function() {
		var frm   = $('#checkOrder');
		var input = frm.find('input[name=madonhang]');
		var btn   = $(this);
		if( input.val() ){
			$("#checkOrderBody").html(`
				<i class="fa fa-cog fa-spin fa-3x fa-fw"></i>
			`);
			$.post(_link + "ajax/", {
				action: "checkorder_process",
				value: input.val()
			}, function(res){
				if( res ){
					if( input.parent().hasClass('has-error') ){
						input.parent().removeClass('has-error');
					}
					$("#checkOrderBody").html(res);
				}
				else {
					$("#checkOrderBody").html(`
						<div class="alert alert-danger">
							<strong class="h4">Đơn hàng không tồn tại</strong>
						</div>
					`);
				}
			});
		}
		else {
			if( !input.parent().hasClass('has-error') ){
				input.wrap(`<div class="has-error"></div>`);
			}
		}
	});
});
$(function() {
	var _link = $("#rootPath").data('url');
	if( $.isFunction($.fn.starRating) ){
		$('.starr').starRating({
			strokeColor: '#894A00',
			strokeWidth: 10,
			starSize: 25,
			readOnly: true,
			initialRating: 4
		});
		$('.js-star').starRating({
			strokeColor: '#894A00',
			strokeWidth: 10,
			starSize: 16,
			useFullStars: true,
			initialRating: 0
		});
		$('.rating-dm').starRating({
			strokeColor: '#894A00',
			strokeWidth: 10,
			starSize: 20,
			useFullStars: true,
			disableAfterRate: false,
			callback: function( currentRating, $el ) {
				// Check Login
				$.get(_link + "ajax/", {
					action: 'checklogin'
				}, function(res) {
					if( res ) {
						var id = $($el).data('id');
						$.get(_link + "ajax/", {
							action: "ratingcat",
							value: currentRating,
							id: id
						}, function(res) {
							if( !res ){
								swal('', '', 'error');
							}
							else {
								swal('', '', 'success');
							}
						});
					}
					else {
						$.get($("#rootPath").data('url') + 'ajax/', {
                            action: 'login',
                        }, function(data) {
                            $('#myModal').modal('show');
                            $('#loaddatacart').html(data);
                        });
                        return false;
					}
				});
			}
		});
	}
	else {
		console.warn('starRating not found');
	}
});
$(function () {
    dislike = function(i, t) {
        if( i ) {
            var thjs = window.event.target;
            if( $(this).parent().hasClass('clicked') ) {
                return false;
            }
            $.post($("#rootPath").data('url') + "ajax/common/rating/dislike/", {
                method: 'dislike',
                id: i
            }, function(response) {
                if( response ) {
                    if( response.status == 'login' ) {
                        $.get($("#rootPath").data('url') + 'ajax/', {
                            method: 'login',
                        }, function(data) {
                            $('#myModal').modal('show');
                            $('#loaddatacart').html(data);
                        });
                        return false;
                    }
                    if( response.status == 'like' ) {
                        $(t).addClass('clicked');
                    }
                    else {
                        $(t).removeClass('clicked');
                    }
                    $(t).html(response.result);
                    if( response.message ) {
                        swal(response.message, '', response.status);
                    }
                }
            }, 'json');
        }
        else {
            swal('Oops!', '', 'error');
        }
    },
    like = function(i, t) {
        if( i ) {
            var thjs = window.event.target;
            if( $(this).parent().hasClass('clicked') ) {
                return false;
            }
            $.post($("#rootPath").data('url') + "ajax/common/rating/like/", {
                method: 'like',
                id: i
            }, function(response) {
                if( response ) {
                    if( response.status == 'login' ) {
                        $.get($("#rootPath").data('url') + 'ajax/', {
                            method: 'login',
                        }, function(data) {
                            $('#myModal').modal('show');
                            $('#loaddatacart').html(data);
                        });
                        return false;
                    }
                    if( response.status == 'like' ) {
                        $(t).addClass('clicked');
                    }
                    else {
                        $(t).removeClass('clicked');
                    }
                    $(t).html(response.result);
                    if( response.message ) {
                        swal(response.message, '', response.status);
                    }
                }
            }, 'json');
        }
        else {
            swal('Oops!', '', 'error');
        }
    },
    showRating = function(i, t) {
        $.ajax({
            type: "POST",
            url: $("#rootPath").data('url') + "ajax/common/rating/show",
            data: {
                id: i
            },
            dataType: "json",
            success: function (response) {
                if( response ) {
                    if( response.status == 'login' ) {
                        $.get($("#rootPath").data('url') + 'ajax/', {
                            action: 'login',
                        }, function(data) {
                            $('#myModal').modal('show');
                            $('#loaddatacart').html(data);
                        });
                    }
                    if( response.status == 'success' && response.result.length ) {
                        $('#myModal').modal('show');
                        $('#loaddatacart').html(response.result);
                    }
                    if( response.status && response.message ) {
                        swal(response.message, '', response.status);
                    }
                }
            }
        });
    },
    validateMyForm = function(e) {
        e.preventDefault();
        var frm     = '#rating_form';
        var val     = $(frm).find('[name=rating_value]').val();
        var id      = $(frm).find('[name=id]').val();
        var nhanxet = $(frm).find('[name=nhanxet]').val();
        var btn     = '#enterRating';
        if( val > 0 && id ) {
            $(btn).button('loading');
            $.post($("#rootPath").data('url') + "ajax/common/rating/execute", {
                id: id,
                value: val,
                nhanxet: nhanxet
            }, function(data) {
                if( data && data.message && data.status ) {
					swal(data.message, '', data.status);
                }
				else {
					swal('', '', 'success');
				}
                setTimeout(function() {
                    $('#myModal').modal('hide');
                }, 1e3);
            }, 'json');
        }
    }
});
/////////////////////////
// JavaScript Document //
/////////////////////////
function bootstrap_alert(message, alerttype) {
    $('#notification').append('<div id="alertdiv" class="alert ' + alerttype + '" role="alert"><a class="close" data-dismiss="alert">×</a><span>' + message + '</span></div>')
    setTimeout(function () {
        $("#alertdiv").remove();
    }, 3000);
}
$(document).ready(function () {
    var _link = $("#rootPath").data('url');
    // SETUP AJAX
    $.ajaxSetup({
        cache: false,
        beforeSend: besendfunc,
        complete: complefunc,
        crossDomain: true,
        xhrFields: {
            withCredentials: true
        }
    });
    if ($.isFunction($.fn.lazyload)) {
        $("img.lazy").lazyload({
            effect: "fadeIn"
        });
    }
    $('#dknhathuoc').live('click',function() {
        if($('#dknhathuoc').is(":checked")){
            $("#boxnhathuoc").show(300);
        }else{
            $("#boxnhathuoc").hide(300);
        }
    });

    $('#dkdoimatkhau').live('click',function() {
        if($('#dkdoimatkhau').is(":checked")){
            $("#boxdoimatkhau").show(300);
        }else{
            $("#boxdoimatkhau").hide(300);
        }
    });
    function besendfunc() {
        if ($("body").find("#ws-wating").length) {
            //return $("#ws-wating").fadeIn("fast");
        }
    }
    function complefunc() {
        if ($("body").find("#ws-wating").length) {
            return $("#ws-wating").fadeOut("fast");
        }
    }
    $(".itemsl").click(function(){
        $(this).parent().find('.itemsl').removeClass("active");
        $(this).addClass("active");
    });
    $('.btnSendComment').on('click', function() {
        //$(this).button('loading');
        var _idtype      = $(this).parent().parent().find('#this_product_idtype');
        var _idparent    = $(this).parent().parent().find('#this_product_idparent');
        var _hoten       = $(this).parent().parent().find('.ws-rq-hoten');
        var _email       = $(this).parent().parent().find('.ws-rq-email');
        var _dienthoai   = $(this).parent().parent().find('.ws-rq-dienthoai');
        var _noidung     = $(this).parent().parent().find('.ws-rq-noidung');
        if (!_hoten.val().length) {
            // Chua nhap Email
            $('.btnSendComment').button('reset');
            $(_hoten).focus();
            swal('Vui lòng nhập họ tên!', '', 'warning');
        }else if (!_noidung.val().length) {
            $('.btnSendComment').button('reset');
            $(_noidung).focus();
            swal('Vui lòng nhập nội dung!', '', 'warning');
        }else{
          $.getJSON(_link + 'ajax/?action=comment', {
              value       : 'comment',
              idtype      : _idtype.val(),
              idparent    : _idparent.val(),
              hoten       : _hoten.val(),
              email       : _email.val(),
              dienthoai   : _dienthoai.val(),
              noidung     : _noidung.val(),
          }, function(json, textStatus) {
              if (json.status != 'success') {
                  if (json.status == 'exists') {
                      // Email ton tai
                      $('.btnSendComment').button('reset');
                      $(_hoten).val('');
                      swal(json.message, '', 'error');
                  }
              } else {
                  // Dang ky thanh cong
                  $('.btnSendComment').button('reset');
                  $(_hoten).val('');
                  $(_email).val('');
                  $(_dienthoai).val('');
                  $(_noidung).val('');
                  // $(this).parent('li').find('#replace_binhluan:first').hide(300);
                  swal(json.message, '', 'success');
              }
          });
        }
    });
    $(".traloibinhluan").click(function(){
        $(this).parent("li").find("#replace_binhluan:first").toggle(300);
    });
    if ($.isFunction($.fn.countdown)) {
        if ( $(".countdown").length ) {
            $("[data-time]").each(function(i, e) {
                var finalDate = $(e).data("time");
                if ( $(e).attr('data-time') ) {
                    $(e).countdown(finalDate, function(event) {
                        $(this).text(event.strftime("%D ngày %H:%M:%S"));
                    })
                    .on('update.countdown', function() {
                        // hanh dong khi dang chay
                    })
                    .on('finish.countdown', function() {
                        // Hanh dong khi hoan thanh
                        $.post(_link+'ajax/?method=expire', {
                            method: 'expire',
                            id: $(e).data('id')
                        }, function(data, textStatus, xhr) {
                            $(e).closest('.productBox').parent('.productItem').slideUp('slow');
                            // remove cate
                            $(e).parent('.clock').remove();
                        }, 'json');
                    });
                }
            });
        }
    }
    $(".menuitemmain").click(function () {
        var link = $(".menuresponsive").data("link");
        if ($("#wapper_col_left").hasClass('openleft')) {
            $("#wapper_col_left").removeClass('openleft');
        } else {
            $("#wapper_col_left").addClass('openleft');
            if ($("#wapper_col_left").html().length) {
                $.ajax({
                    url: _link+'application/files/cotmobile.php',
                    type: 'GET',
                    success: function (html) {
                        console.log(html);
                        $("#wapper_col_left").html(html);
						$('.danhmucmobile li ul').parent().addClass('has-sub');
						$('.danhmucmobile li ul').parent().append(`
							<i class="fa fa-angle-down" aria-hidden="true"></i>
						`);
                    }
                });
            }
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
        }
        if ($("#wapper_wapper").hasClass('openleft')) {
            $("#wapper_wapper").removeClass('openleft');
        } else {
            $("#wapper_wapper").addClass('openleft');
            $("#wapper_wapper").css("height", $("#wapper_wapper").innerHeight());
            $("#wapper_wapper_wapper").css("height", $("#wapper_wapper").innerHeight());
        }
    });
    $(".wapper_mark").click(function () {
        $("#wapper_col_left").removeClass('openleft');
        $("#wapper_wapper").removeClass('openleft');
        $("#wapper_wapper").css("height", "auto");
        $("#wapper_wapper_wapper").css("height", "auto");
    });
    var nav = $('.danhmuccol');
    // them i.fa
    nav.find('li ul').parent().addClass('hasChild');
    nav.find('li li ul').parent().addClass('hasSubChild');
    nav.find('li ul').parent().append('<i class="fa fa-caret-down"></i>');
    nav.find('i.fa-caret-down').live('click', function (e) {
        e.preventDefault();
        var $this = $(this);
        var $li = $this.parent();
        //set active state to clicked menu item:
        $li.addClass('hasChild').siblings().removeClass('active');
        //look if there is a UL after the A, if so it contains submenu:
        if ($this.prev().is('ul')) {
            $li.toggleClass('active');
        }
    });
    $(window).scroll(function () {
        if ($(this).scrollTop() > 200) {
            $('.go-top').fadeIn(200);
            $('.go-top').addClass('hien');
        } else {
            $('.go-top').fadeOut(200);
            $('.go-top').removeClass('hien');
        }
    });
    $('.go-top').click(function (event) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 300);
    });
    $(window).scroll(function () {
        if ($(this).scrollTop() > 250) {
            $("#wapper_mobile_fix").addClass("showtagbar");
        } else {
            $("#wapper_mobile_fix").removeClass("showtagbar");
        }
    });
    var num = $('#wapper_banner').innerHeight() + 430;
    $(window).bind('scroll', function () {
        if ($(window).scrollTop() > num) {
            $('#wapper_menu').addClass('fixed');
        } else {
            $('#wapper_menu').removeClass('fixed');
        }
    });
    $(".ndlocnhanh-content li").click(function (event) {
        event.preventDefault();
        $(this).siblings().removeClass('choosed');
        $(this).addClass('choosed');
    });
    $(".closepopuptop").click(function (event) {
        $(".popup_top").hide(300);
    });
    /** Send Filter */
    $(".click_filter").live('click', function () {
        var idnhomid = $(this).find('input[name=this_value_nhomid]').val();
        console.log(idnhomid);
        var idnhomnd = $(this).find('input[name=this_value_nhomnd]').val();
        var idlink = $(this).find('input[name=this_link_loc]').val();
        
		var text = $(this).children('a').attr('title');
		var clickID = $(this).find("input[name=this_value_nhomid]").val();
		$(".filter-list").find('a').each(function (index, element) {
			// element == this
			var item = $(element).children('span').text();
			if( item == text) {
				$(element).remove();
			}
			var i = $(element).attr('onclick').split(',');
			var y = $.trim(i[i.length-1].slice(0,-1));
			if( clickID == y ){
				$(element).remove();
			}
		});
		if( $.trim($(".filter-list").text()).length == 0 ){
			$(".filter-list").append(`
				<a href="javascript:;" class="delall" onclick="DeleteFilterAll()">
					<span>Xóa tất cả</span>
					<i class="fa fa-close" aria-hidden="true"></i>
				</a>
			`);
		}
		$(".filter-list").append(`
			<a href="javascript:;" onclick="DeleteFilter(this, '`+idnhomid+`')">
				<span>`+text+`</span>
				<i class="fa fa-close" aria-hidden="true"></i>
			</a>
		`);
        $("#loadingcss3").show();
        $.ajax({
            url: idlink,
            type: 'GET',
            data: {
                idnhomid: idnhomid,
                idnhomnd: idnhomnd
            },
            async: true,
            cache: false,
            success: function (idata) {
				$('html,body').animate({
					scrollTop: $('#colcontent').offset().top
				}, 600);
                setTimeout(function () {
                    $("#loadingcss3").hide();
                    $("#loaditemsanpham").html(idata);
                }, 300);
            },
            error: function (request, status, error) {
                swal('Error', '', 'error');
            }
        });
        // e.preventDefault();
    });
    $(".owl-slide").owlCarousel({
        autoplay: 5000,
        items: 1,
        loop: $(".owl-slide").find('.owl-item').size() ? true : false,
        nav: true,
        lazyLoad: true,
        dots: false,
		navText: [
			'<i class="fa fa-angle-left"></i>',
            '<i class="fa fa-angle-right"></i>'
		]
    });
    $(".owl-banhome").owlCarousel({
        autoplay: 5000,
        items: 1,
        nav: false,
        lazyLoad: true,
        dots: false,
		navText: [
			'<i class="fa fa-angle-left"></i>',
            '<i class="fa fa-angle-right"></i>'
		]
    });
    $(".owl-lienquan").owlCarousel({
        autoplay: 20000,
        autoplaySpeed: 1000,
        autoplayTimeout: 5000,
        loop: $(".owl-lienquan").find('.owl-item').size() ? true : false,
		lazyLoad: true,
		margin: 10,
        nav: false,
        dots: false,
        animateOut: 'slideOutDown',
        animateIn: 'flipInX',
        navText: [
            '<i class="fa fa-angle-left"></i>',
            '<i class="fa fa-angle-right"></i>'
        ],
        responsiveClass: true,
        responsive: {
            0: {
                items: 2,
            },
            640: {
                items: 3,
            },
            1000: {
                items: 4,
            },
            1200: {
                items: 6,
            },
        }
    });
    $(".owl-home").owlCarousel({
        autoplay: 20000,
        autoplaySpeed: 1000,
        autoplayTimeout: 5000,
        items: 2,
        loop: $(".owl-home").find('.owl-item').size() ? true : false,
        nav: true,
        dots: false,
        animateOut: 'slideOutDown',
        animateIn: 'flipInX',
        navText: [
            '<i class="fa fa-angle-left"></i>',
            '<i class="fa fa-angle-right"></i>'
        ],
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            640: {
                items: 2,
                nav: true
            }
        }
    });
    $(".owl_home").owlCarousel({
        autoplay: 20000,
        items: 5,
        loop: true,
		margin: 10,
        nav: true,
        lazyLoad: true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1,
                nav: true
            },
            360: {
                items: 2,
                nav: true
            },
            768: {
                items: 3,
                nav: true
            },
            1024: {
                items: 4,
                nav: true
            },
            1200: {
                items: 5,
                nav: true
            }
        },
        navText: [
            '<i class="fa fa-chevron-left fa-2x"></i>',
            '<i class="fa fa-chevron-right fa-2x"></i>'
        ]
    });
    // tooltip
    $('[data-toggle="tooltip"]').tooltip();
    // require gio hang
    $("#thanhtoan_giohang").click(function () {
       
        if ($("#checkout input[name=phone]").val().length == 0) {
            $("#checkout input[name=phone]").focus();
            swal('', $("#checkout input[name=dienthoai_alert]").val(), 'warning');
            return false;
        } else if ($("#checkout input[name=fullname]").val().length == 0) {
            $("#checkout input[name=fullname]").focus();
            swal('', $("#checkout input[name=hoten_alert]").val(), 'warning');
            return false;
        } else if ($("#checkout input[name=address]").val().length == 0) {
            $("#checkout input[name=address]").focus();
            swal('', $("#checkout input[name=diachi_alert]").val(), 'warning');
            return false;
        } else {
            return true;
        }
    });
    // zoom anh
    var widthdevice = $(window).width();
    if (widthdevice > 768 && jQuery().elevateZoom) {
        $("#img_01").elevateZoom({
            gallery: "gal1",
            zoomType: "inner",
            lensSize: 280,
            cursor: "pointer"
        });
        $("#img_01").bind("click", function (e) {
            var ez = $('#img_01').data('elevateZoom');
            $.fancybox(ez.getGalleryList());
            return false;
        });
    }
    // slide chi tiet
    $("#owl-slide-chitiet").owlCarousel({
        items: 1,
        navigation: true,
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true,
        autoPlay: 4000,
    });
});
(function ($) {
    $.fn.menumaker = function (options) {
        var cssmenu = $(this),
            settings = $.extend({
                title: "Menu",
                format: "dropdown",
                sticky: false
            }, options);
        return this.each(function () {
            cssmenu.prepend('<div id="menu-button">' + settings.title + '</div>');
            $(this).find("#menu-button").on('click', function () {
                $(this).toggleClass('menu-opened');
                var mainmenu = $(this).next('ul');
                if (mainmenu.hasClass('open')) {
                    mainmenu.hide().removeClass('open');
                } else {
                    mainmenu.show().addClass('open');
                    if (settings.format === "dropdown") {
                        mainmenu.find('ul').show();
                    }
                }
            });
            cssmenu.find('li ul').parent().addClass('has-sub');
            multiTg = function () {
                cssmenu.find(".has-sub").prepend('<span class="submenu-button"></span>');
                cssmenu.find('.submenu-button').on('click', function () {
                    $(this).toggleClass('submenu-opened');
                    if ($(this).siblings('ul').hasClass('open')) {
                        $(this).siblings('ul').removeClass('open').hide();
                    } else {
                        $(this).siblings('ul').addClass('open').show();
                    }
                });
            };
            if (settings.format === 'multitoggle') multiTg();
            else cssmenu.addClass('dropdown');
            if (settings.sticky === true) cssmenu.css('position', 'fixed');
            resizeFix = function () {
                if ($(window).width() > 768) {
                    cssmenu.find('ul').show();
                }
                if ($(window).width() <= 768) {
                    cssmenu.find('ul').hide().removeClass('open');
                }
            };
            resizeFix();
            //return $(window).on('resize', resizeFix);
        });
    };
})(jQuery);
(function ($) {
    $(document).ready(function () {
        $("#cssmenu").menumaker({
            title: "Menu",
            format: "multitoggle"
        });
    });
})(jQuery);
// JavaScript Document
function changecard(url, div) {
    //document.getElementById(div).innerHTML="Loadding...";
    url += "&thdiem=".concat((new Date()).getTime());
    try {
        var h;
        if (window.ActiveXObject) h = new ActiveXObject("Microsoft.XMLHTTP");
        else h = new XMLHttpRequest();
        h.onreadystatechange = function () {
            if (h.readyState == 4)
                if (h.status == 200) {
                    document.getElementById(div).innerHTML = h.responseText;
                }
            else alert("Không lấy dữ liệu được. " + h.statusText + "-" + h.responseText);
        }
        h.open("GET", url);
        h.send(null);
    } catch (e) {
        alert("Lỗi " + e.description + "-" + h.responseText);
    }
}
function Get_money(url, div) {
    //document.getElementById('ctl00_udpTop').style.display = 'block';
    url += "&thdiem=".concat((new Date()).getTime());
    try {
        var h;
        if (window.ActiveXObject) h = new ActiveXObject("Microsoft.XMLHTTP");
        else h = new XMLHttpRequest();
        h.onreadystatechange = function () {
            if (h.readyState == 4)
                if (h.status == 200) {
                    document.getElementById(div).innerHTML = h.responseText;
                    //document.getElementById('ctl00_udpTop').style.display = 'none';
                }
            else alert("Không lấy dữ liệu được. " + h.statusText + "-" + h.responseText);
        }
        h.open("GET", url);
        h.send(null);
    } catch (e) {
        alert("Lỗi " + e.description + "-" + h.responseText);
    }
}
function Get_Data(url, div) {
    //document.getElementById(div).innerHTML="Loadding...";
    url += "&thdiem=".concat((new Date()).getTime());
    try {
        var h;
        if (window.ActiveXObject) h = new ActiveXObject("Microsoft.XMLHTTP");
        else h = new XMLHttpRequest();
        h.onreadystatechange = function () {
            if (h.readyState == 4)
                if (h.status == 200) {
                    document.getElementById(div).innerHTML = h.responseText;
                    //document.getElementById('ctl00_udpTop').style.display = 'none';
                }
            //else alert("Không lấy dữ liệu được. " + h.statusText+ "-"+ h.responseText);
        }
        h.open("GET", url);
        h.send(null);
    } catch (e) {
        //alert("Lỗi "+ e.description + "-"+ h.responseText);
    }
}
function Ajax_noreturn(url) {
    //document.getElementById(div).innerHTML="Loadding...";
    url += "&thdiem=".concat((new Date()).getTime());
    try {
        var h;
        if (window.ActiveXObject) h = new ActiveXObject("Microsoft.XMLHTTP");
        else h = new XMLHttpRequest();
        h.onreadystatechange = function () {
            if (h.readyState == 4)
                if (h.status == 200) {
                    // document.getElementById(div).innerHTML = h.responseText;
                    //document.getElementById('ctl00_udpTop').style.display = 'none';
                }
            //else alert("Không lấy dữ liệu được. " + h.statusText+ "-"+ h.responseText);
        }
        h.open("GET", url);
        h.send(null);
    } catch (e) {
        //alert("Lỗi "+ e.description + "-"+ h.responseText);
    }
}
function Ajax_noreturn_swal(url,title) {
    //document.getElementById(div).innerHTML="Loadding...";
    url += "&thdiem=".concat((new Date()).getTime());
    try {
        var h;
        if (window.ActiveXObject) h = new ActiveXObject("Microsoft.XMLHTTP");
        else h = new XMLHttpRequest();
        h.onreadystatechange = function () {
            if (h.readyState == 4)
                if (h.status == 200) {
                    // document.getElementById(div).innerHTML = h.responseText;
                    //document.getElementById('ctl00_udpTop').style.display = 'none';
                }
            //else alert("Không lấy dữ liệu được. " + h.statusText+ "-"+ h.responseText);
        }
        h.open("GET", url);
        h.send(null);
    } catch (e) {
        //alert("Lỗi "+ e.description + "-"+ h.responseText);
    }
    swal(title);
}
function MuaHang(url, div) {
    //document.getElementById(div).innerHTML="Loadding...";
    url += "&thdiem=".concat((new Date()).getTime());
    try {
        var h;
        if (window.ActiveXObject) h = new ActiveXObject("Microsoft.XMLHTTP");
        else h = new XMLHttpRequest();
        h.onreadystatechange = function () {
            if (h.readyState == 4)
                if (h.status == 200) {
                    document.getElementById(div).innerHTML = h.responseText;
                    //document.getElementById('ctl00_udpTop').style.display = 'none';
                }
            //else alert("Không lấy dữ liệu được. " + h.statusText+ "-"+ h.responseText);
        }
        h.open("GET", url);
        h.send(null);
    } catch (e) {
        //alert("Lỗi "+ e.description + "-"+ h.responseText);
    }
}

function ChangeSessionImages2(url, div) {
    $.getJSON(url, {
        divReturn: div
    }, function(data) {
        if (data.error!="success") {
            if (data.error=="quantity_unknown") {
                swal("Số lượng không hợp lệ!", "", "error");
                self.location.href='';
            }
        } else {
            $('#'+div).html(data.result);
        }
    });
}

function ChangeSessionImages(url, div) {
    //document.getElementById(div).innerHTML="Loadding...";
    url += "&thdiem=".concat((new Date()).getTime());
    try {
        var h;
        if (window.ActiveXObject) h = new ActiveXObject("Microsoft.XMLHTTP");
        else h = new XMLHttpRequest();
        h.onreadystatechange = function() {
            if (h.readyState == 4)
                if (h.status == 200) {
                    if(div!=null){
                        document.getElementById(div).innerHTML = h.responseText;
                    }
                    //document.getElementById('ctl00_udpTop').style.display = 'none';
                }
                //else alert("Không lấy dữ liệu được. " + h.statusText+ "-"+ h.responseText);
        }
        h.open("GET", url);
        h.send(null);
    } catch (e) {
        //alert("Lỗi "+ e.description + "-"+ h.responseText);
    }
}




function hidegiohang() {
    document.getElementById("id_sanpham_alt").style.display = 'none';
    document.getElementById("content_wapper_choice").style.display = 'none';
}
function GetPage(pagename, div) {
    document.getElementById(div).innerHTML = "<span style='color:#000000;'><img src='images/icon/loading19.gif' /> Đang tải dữ liệu </span>";
    url = pagename;
    try {
        var h;
        if (window.ActiveXObject) h = new ActiveXObject("Microsoft.XMLHTTP");
        else h = new XMLHttpRequest();
        h.onreadystatechange = function () {
            if (h.readyState == 4)
                if (h.status == 200) {
                    document.getElementById(div).innerHTML = h.responseText;
                    //document.getElementById(div).style.display = 'none';
                }
            else alert("Không lấy dữ liệu được. " + h.statusText + "-" + h.responseText);
        }
        h.open("GET", url);
        h.send(null);
    } catch (e) {
        alert("Lỗi " + e.description + "-" + h.responseText);
    }
}
function check_order(url, div, thongbao) {
    document.getElementById('ctl00_udpTop').style.display = 'block';
    try {
        var h;
        if (window.ActiveXObject) h = new ActiveXObject("Microsoft.XMLHTTP");
        else h = new XMLHttpRequest();
        h.onreadystatechange = function () {
            if (h.readyState == 4)
                if (h.status == 200) {
                    response = h.responseText;
                    if (response == 0) {
                        alert(thongbao);
                    } else {
                        document.getElementById(div).innerHTML = h.responseText;
                    }
                    document.getElementById('ctl00_udpTop').style.display = 'none';
                }
            else alert("Không lấy dữ liệu được. " + h.statusText + "-" + h.responseText);
        }
        h.open("GET", url);
        h.send(null);
    } catch (e) {
        alert("Lỗi " + e.description + "-" + h.responseText);
    }
}
function CheckNumber(number) {
    var pattern = "0123456789.";
    var len = number.value.length;
    if (len != 0) {
        var index = 0;
        while ((index < len) && (len != 0))
            if (pattern.indexOf(number.value.charAt(index)) == -1) {
                if (index == len - 1)
                    number.value = number.value.substring(0, len - 1);
                else if (index == 0)
                    number.value = number.value.substring(1, len);
                else number.value = number.value.substring(0, index) + number.value.substring(index + 1, len);
                index = 0;
                len = number.value.length;
            }
        else index++;
    }
}
function checkorder() {
    if (document.form_order.diadiem.value == '') {
        alert("Bạn chưa nhập địa điểm giao hàng.");
        document.form_order.diadiem.focus();
        return false;
    } else {
        return true;
    }
}
function _substr(srt, dem) {
    var chuoi = explode(' ', str);
}
function xulyloi(obj) {
    obj.className = "an";
}
function showhideclass(div) {
    $("." + div).toggle();
}
function goilai(url, div, id, link, sodienthoaikhongdung, thongbao) {
    if (document.getElementById('txtgoilai').value == "" || isNaN(document.getElementById('txtgoilai').value) || document.getElementById('txtgoilai').value.length <= 6)
        document.getElementById('ycgoilai').innerHTML = '<span style="color:#F00;padding:3px">' + sodienthoaikhongdung + '</span>';
    else {
        var phone = document.getElementById('txtgoilai').value;
        document.getElementById(div).innerHTML = '<span style="color:#000; width: 100%; display: block; text-align: center; padding: 0px; font-size: 120%;"><img src="./application/templates/images/loading.gif"/><br> Đang gửi ...</span>';
        var request = $.ajax({
            type: "POST",
            url: url,
            data: {
                method: 'goilai',
                phone: phone,
                id: id,
                lienket: link
            }
        });
        request.done(function (msg) {
            document.getElementById(div).innerHTML = '<span style="color:#F00; width: 100%; display: block; text-align: center; padding: 5px; font-size: 120%;">' + thongbao + '</span>';
        });
        request.fail(function (jqXHR, textStatus) {});
    }
}
function goilai2(url, div, id, link, sodienthoaikhongdung, thongbao) {
    if (document.getElementById('txtgoilai2').value == "" || isNaN(document.getElementById('txtgoilai2').value) || document.getElementById('txtgoilai2').value.length <= 6)
        document.getElementById('ycgoilai2').innerHTML = '<span style="color:#F00;padding:3px">' + sodienthoaikhongdung + '</span>';
    else {
        var phone = document.getElementById('txtgoilai2').value;
        document.getElementById(div).innerHTML = '<span style="color:#000; width: 100%; display: block; text-align: center; padding: 0px; font-size: 120%;"><img src="./application/templates/images/loading.gif"/><br> Đang gửi ...</span>';
        var request = $.ajax({
            type: "POST",
            url: url,
            data: {
                method: 'goilai',
                phone: phone,
                id: id,
                lienket: link
            }
        });
        request.done(function (msg) {
            document.getElementById(div).innerHTML = '<span style="color:#F00; width: 100%; display: block; text-align: center; padding: 5px; font-size: 120%;">' + thongbao + '</span>';
        });
        request.fail(function (jqXHR, textStatus) {});
    }
}
// LIVE SEARCH
$(function(){
    $('body').click(function(evt){
        if(evt.target.id == "search_cont")
            return;
        if($(evt.target).closest('#search_cont').length)
            return;
        //
        $("#search_result").html('');
        $("#search_cont .txt").val('');
    });
    $("#search_frm").children(".txt").on('change keyup', function() {
        var val = $(this).val();
        if( val.length ) {
            $("#loadin").removeClass("hide");
            $.ajax({
                type: "POST",
                url: $("#rootPath").data('url')+"ajax/",
                data: {
                    value: val,
                    action: "search"
                },
                dataType: "json",
                success: function (response) {
                    $("#search_result").html(response.result);
                    $("#loadin").addClass("hide");
                }
            });
        }
    });
});

$(function(){
    $(".nhom_content > ul > li").live( "click",function(){
        $(this).parent("ul").find("li").removeClass('active');
        $(this).addClass('active');
    });
});


$(document).ready(function() {
   var _link = $("#rootPath").data("url");
    // SETUP AJAX
    $('#registerid').live('click',function(){
        var $thisparent = $(this).parent(".contentlogin");
        var hovaten     = $thisparent.find("input[name='hovaten']");
        var email       = $thisparent.find("input[name='email']");
        var sodienthoai = $thisparent.find("input[name='sodienthoai']");
        var dknhathuoc  = $thisparent.find("input[name='dknhathuoc']");
        var tennhathuoc = $thisparent.find("textarea[name='tennhathuoc']");
        var mst         = $thisparent.find("input[name='mst']");
        var gioitinh    = $thisparent.find("input[name='gioitinh']");
        var password    = $thisparent.find("input[name='password']");
        var repassword  = $thisparent.find("input[name='repassword']");
        var diachi      = $thisparent.find("input[name='diachi']");
        if(hovaten.val()==''){
            hovaten.focus();
            swal(hovaten.data('require'),'','error');
            return false;
        }else if(email.val()==''){
            email.focus();
            swal(email.data('require'),'','error');
            return false;
        }else if(sodienthoai.val()==''){
            sodienthoai.focus();
            swal(sodienthoai.data('require'),'','error');
            // swal($('input.dknhathuoc').attr(':checked'),'','error');
            return false;
        }else if($('#dknhathuoc').attr('checked') && tennhathuoc.val()==''){
            tennhathuoc.focus();
            swal(tennhathuoc.data('require'),'','error');
            return false;
        }else if($('#dknhathuoc').attr('checked') && mst.val()==''){
            mst.focus();
            swal(mst.data('require'),'','error');
            return false;
        }else if(diachi.val()==''){
            diachi.focus();
            swal(diachi.data('require'),'','error');
            return false;
        }else if(password.val()==''){
            password.focus();
            swal(password.data('require'),'','error');
            return false;
        }else if(repassword.val()==''){
            repassword.focus();
            swal(repassword.data('require'),'','error');
            return false;
        }else if(password.val() != repassword.val()){
            repassword.focus();
            swal(repassword.data('matkhausai'),'','error');
            return false;
        }else{
            $.getJSON(_link + 'ajax/', {
                action: 'registerquery',
                hovaten: hovaten.val(),
                email: email.val(),
                sodienthoai: sodienthoai.val(),
                dknhathuoc: dknhathuoc.val(),
                tennhathuoc: tennhathuoc.val(),
                mst: mst.val(),
                diachi: diachi.val(),
                gioitinh: gioitinh.val(),
                password:password.val()
            }, function(json, textStatus) {
                switch(json.status){
                    case'emailexits':
                        swal(json.message,'','error');
                        break;
                    case'sodienthoaiexits':
                        swal(json.message,'','error');
                        break;
                    case'success':
                       $('#myModal').modal('hide');
                       swal(json.message,'','success');
                        $.getJSON(_link + 'ajax/', {
                            action: 'loginquery',
                            email: sodienthoai.val(),
                            password:password.val()
                        });
                        Get_Data(_link+'ajax/?action=getloginuserinfo','login-box');
                        break;
                }
            });
        }
    });
    $('#updateUserId').live('click',function(){
        var $thisparent = $(this).parent(".contentlogin");
        var hovaten     = $thisparent.find("input[name='hovaten']");
        var email       = $thisparent.find("input[name='email']");
        var sodienthoai = $thisparent.find("input[name='sodienthoai']");
        var dknhathuoc  = $thisparent.find("input[name='dknhathuoc']");
        var tennhathuoc = $thisparent.find("textarea[name='tennhathuoc']");
        var mst         = $thisparent.find("input[name='mst']");
        var gioitinh    = $thisparent.find("input[name='gioitinh'].checked");
        var diachi      = $thisparent.find("input[name='diachi']");
        if(hovaten.val()==''){
            hovaten.focus();
            swal(hovaten.data('require'),'','error');
            return false;
        }else if(email.val()==''){
            email.focus();
            swal(email.data('require'),'','error');
            return false;
        }else if(sodienthoai.val()==''){
            sodienthoai.focus();
            swal(sodienthoai.data('require'),'','error');
            // swal($('input.dknhathuoc').attr(':checked'),'','error');
            return false;
        }else if($('#dknhathuoc').attr('checked') && tennhathuoc.val()==''){
            tennhathuoc.focus();
            swal(tennhathuoc.data('require'),'','error');
            return false;
        }else if($('#dknhathuoc').attr('checked') && mst.val()==''){
            mst.focus();
            swal(mst.data('require'),'','error');
            return false;
        }else if(diachi.val()==''){
            diachi.focus();
            swal(diachi.data('require'),'','error');
            return false;
        }else{
            $.getJSON(_link + 'ajax/', {
                action: 'updateuserquery',
                hovaten: hovaten.val(),
                email: email.val(),
                sodienthoai: sodienthoai.val(),
                dknhathuoc: dknhathuoc.val(),
                tennhathuoc: tennhathuoc.val(),
                mst: mst.val(),
                diachi: diachi.val(),
                gioitinh: gioitinh.val()
            }, function(json, textStatus) {
                switch(json.status){
                    case'emailexits':
                        swal(json.message,'','error');
                        break;
                    case'sodienthoaiexits':
                        swal(json.message,'','error');
                        break;
                    case'success':
                       $('#myModal').modal('hide');
                        Get_Data(_link+'ajax/?action=getloginuserinfo','login-box');
                        swal(json.message,'','success');
                        break;
                }
            });
        }
    });
    $('#loginid').live('click',function(){
        var $thisparent = $(this).parent(".contentlogin");
        var email       = $thisparent.find("input[name='email']");
        var password    = $thisparent.find("input[name='password']");
        var remember    = $thisparent.find("input[name='remember']");
        var idsp        = $thisparent.find("input[name='idsp']");
        var refer       = $thisparent.find("input[name='refer']");
        var reval =  0;
        if(remember.is(':checked')){
            reval = 1;
        }
        if(email.val()==''){
            email.focus();
            swal(email.data('require'),'','error');
            return false;
        }else if(password.val()==''){
            password.focus();
            swal(password.data('require'),'','error');
            return false;
        }else{
            $.getJSON(_link + 'ajax/', {
                action: 'loginquery',
                email: email.val(),
                password:password.val(),
                remember: reval
            }, function(json, textStatus) {
                switch(json.status){
                    case'error':
                        swal(json.message,'','error');
                        break;
                    case'success':
                        // if ( $(window).width() > 768 ) {
                        //     $(".liUser").html(json.result);
                        // } else {
                        //     $(".user-mobile").html(json.result);
                        // }
                        $('#myModal').modal('hide');
                        Get_Data(_link+'ajax/?action=getloginuserinfo','login-box');
                        swal(json.message,'','success');
                        // swal(json.message,'','success');
                        // if(refer.val()=='datlich'){
                        //     Get_Data(_link+'ajax/?action='+refer.val()+'&id='+idsp.val(),'loaddatacart');
                        //     // alert("xx");
                        //     // location.reload();
                        // }else{
                        //     var linkre = _link;
                        //     window.location.href=linkre;
                        //     $('#myModal').modal('hide');
                        // }
                        break;
                }
            });
        }
    });
    $("#logout").live("click", function() {
        if (confirm("Bạn có chắc muốn thoát?") === true) {
            $.getJSON(_link + "ajax/?action=logout", {}, function(json, textStatus) {
                if (typeof(json) !== 'undefined') {
                    if (json.error == "success") {
                        location.reload();
                    }
                } else {
                    location.reload();
                }
            });
        }
    });
    $('#activecodeid').live('click',function(){
        var $thisparent = $(this).parent(".contentlogin");
        var sodienthoai = $thisparent.find("input[name='sodienthoai']");
        var activecode  = $thisparent.find("input[name='activecode']");
        var password    = $thisparent.find("input[name='password']");
        if(sodienthoai.val()==''){
            sodienthoai.focus();
            swal(sodienthoai.data('require'),'','error');
            return false;
        }else if(activecode.val()==''){
            activecode.focus();
            swal(activecode.data('require'),'','error');
            return false;
        }else{
            $.getJSON(_link + 'ajax/', {
                action: 'activecodequery',
                sodienthoai: sodienthoai.val(),
                activecode:activecode.val(),
                password:password.val()
            }, function(json, textStatus) {
                switch(json.status){
                    case'error':
                        swal(json.message,'','error');
                        break;
                    case'success':
                        if ( $(window).width() > 768 ) {
                            $(".liUser").html(json.result);
                        } else {
                            $(".user-mobile").html(json.result);
                        }
                        swal(json.message,'','success');
                        $('#myModal').modal('hide');
                        // if(refer.val()=='datlich'){
                        //     Get_Data(_link+'ajax/?method='+refer.val()+'&id='+idsp.val(),'loaddatacart');
                        //     // alert("xx");
                        //     // location.reload();
                        // }else{
                        //     var linkre = _link;
                        //     window.location.href=linkre;
                        //     $('#myModal').modal('hide');
                        // }
                        break;
                }
            });
        }
    });
    // Su dung nut enter
    $("input[name='password'],input[name='email']").live('keyup', function(e) {
        if ( e.keyCode == 13 ){
            $('#loginid').trigger('click');
        }
    });
    $('#forgotpasswordid').live('click',function(){
        var $thisparent = $(this).parent(".contentlogin");
        var email       = $thisparent.find("input[name='email']");
        if(email.val()==''){
            email.focus();
            swal(email.data('require'),'','error');
            return false;
        }else{
            $.getJSON(_link + 'ajax/', {
                action: 'forgotpasswordquery',
                email: email.val()
            }, function(json, textStatus) {
                switch(json.status){
                    case'error':
                        swal(json.message,'','error');
                        break;
                    case'success':
                        swal(json.message,'','success');
                        $('#myModal').modal('hide');
                }
            });
        }
    });
    $('#changepassid').live('click',function(){
        var $thisparent = $(this).parent(".contentlogin");
        var passcurrent    = $thisparent.find("input[name='passcurrent']");
        var passnew    = $thisparent.find("input[name='passnew']");
        var passrenew  = $thisparent.find("input[name='passrenew']");
        var maactive    = $(this).data('maactive');
        if(passcurrent.val()==''){
            passcurrent.focus();
            swal(passcurrent.data('require'),'','error');
            return false;
        }else if(passnew.val()==''){
            passnew.focus();
            swal(passnew.data('require'),'','error');
            return false;
        }else if(passrenew.val()==''){
            passrenew.focus();
            swal(passrenew.data('require'),'','error');
            return false;
        }else if(passrenew.val() != passnew.val()){
            repassword.focus();
            swal(repassword.data('matkhausai'),'','error');
            return false;
        }else{
            $.getJSON(_link + 'ajax/', {
                action: 'changepassquery',
                maactive: maactive,
                passnew:passnew.val()
            }, function(json, textStatus) {
                switch(json.status){
                    case'success':
                        $('#myModal').modal('hide');
                        swal(json.status,'','success');
                        location.href = $("#rootPath").data('url');
                }
            });
        }
    });
});