jQuery(document).ready((function() {

    var list_story = [];
    var current_story = 0;
    var list_chapter = [];
    var current_chapter = 0;
    var arrayOfLines = [];
    var current_url = 0;
    var crawl_active = true;
   $('#btn-crawl-story').click(function(){
     var page = $('#page_story').val();
     $('#list_story').html('');
     arrayOfLines = $('#page_story').val().split('\n');
     if(arrayOfLines.length > 0 ) {
        
        listStory(arrayOfLines[0]);
     } 
     $('#btn-crawl-story').prop('disabled', true);
     $('#btn-crawl-story').addClass('disabled');
     $('#btn-stop-crawl').prop('disabled', false);
     $('#btn-stop-crawl').removeClass('disabled');
     $('#btn-clear-console').prop('disabled', false);
     $('#btn-clear-console').removeClass('disabled');
   });
   $('#btn-clear-console').click(function(){
        $('#list_story').html('');
   });

   $('#btn-stop-crawl').click(function(){
        crawl_active = false;
        $('#btn-restart').prop('disabled', false);
        $('#btn-restart').removeClass('disabled');
        $('.result-message').html('<h4 class="kt-font-primary kt-font-bold"> ======= > Đang tạm dừng lấy truyện ... <======= </h4>');
        setTimeout(function () {
            $('#list_story').append('<h3 class="kt-font-primary kt-font-bold"> ======= > Đang tạm dừng lấy truyện ... <======= </h3>');
        }, 2000);
        
        
    });
    $('#btn-restart').click(function(){
        crawl_active = true;
        $('.result-message').html('<h4 class="kt-font-primary kt-font-bold">Đang thêm truyện vào hệ thống ...</h4>');
        setTimeout(function () {
            listChapter(list_story[current_story]);
        }, 2000);
        $('#btn-restart').prop('disabled', true);
        $('#btn-restart').addClass('disabled');
        
    });
   function listStory(page) {

    list_story = [];
    current_story = 0;
    
    current_url = current_url + 1;
    if(current_url <= arrayOfLines.length) {
        
        $('.current-page').html(page);
        $('.result-message').html('<h4 class="kt-font-primary kt-font-bold">Đang lấy danh sách sản phẩm từ dòng '+ current_url  +'</h4><br>');
        $.ajax({
            url: "admincp/crawler/ajax/list-medicine",
            method: 'get',
            type: 'json',
            data: {
                _token: $("input[name='_token']").val(),
                url: page
            },
            success: function(data){
                var data = JSON.parse(data);
                let percent = Math.round(current_url*100/arrayOfLines.length);
                $('.progress-bar').attr('aria-valuenow', percent);
                $('.progress-bar').css('width', percent+'%');
                $('.progress-bar').html(current_url+' / '+arrayOfLines.length);
                $('#list_story').append(data.message+'<br>');
                listStory(arrayOfLines[current_url]);
                
                
                
            },
            error: function(errors) {
                listStory(arrayOfLines[current_url]);
            }
        });
    } else {
        setTimeout(function () {
            $('.current_page').text('');
            $('.result-message').html('<h4 class="kt-font-primary kt-font-bold"> ======= > Đã hoàn thành quá trình lấy dữ liệu <======= </h4>');
            $('#list_story').append('<h3 class="kt-font-primary kt-font-bold"> ======= > Đã hoàn thành quá trình lấy dữ liệu <======= </h3>');
            $('#btn-crawl-story').prop('disabled', false);
            $('#btn-crawl-story').removeClass('disabled');
        }, 8000);
        
    }
   }

   function listChapter(url) {
    
    if(crawl_active) {
        current_story = current_story + 1;
        $('.result-message').html('<h5 class="kt-font-primary kt-font-bold">Đang kiểm tra '+url+'</h5>');
        $.ajax({
            url: "crawl/ajax/list-chapter",
            method: 'get',
            type: 'json',
            timeout: 30000,
            data: {
                _token: $("input[name='_token']").val(),
                url: url
            },
            success: function(data){
                var data = JSON.parse(data);
                
                if(data.status == 'success') {
                    list_chapter = data.list_chater;
                    
                }
                console.log(current_story+' : '+ list_story.length);
                console.log(list_story[current_story]);
                if(current_story < list_story.length) {
                    listChapter(list_story[current_story]);
                }
                if(current_story == list_story.length && list_story.length != 0) {
                    listStory(arrayOfLines[current_url]);
                }
                $('#list_story').append('<div class="kt-section__desc"><span class="kt-font-danger kt-font-italic">'+url+'</span> : '+data.message+' </div>').slideDown("slow");
                

            },
            error: function(errors) {

                console.log(current_story+' : '+ list_story.length);
                console.log(list_story[current_story]);
                if(current_story < list_story.length) {
                    listChapter(list_story[current_story]);
                }
                if(current_story == list_story.length && list_story.length != 0) {
                    listStory(arrayOfLines[current_url]);
                }
                $('#list_story').append('<div class="kt-section__desc"><span class="kt-font-danger kt-font-italic">'+url+'</span> : '+errors.statusText+' </div>').slideDown("slow");
                
    
            }
            
        });
    }
}
    function addChapter(url) {
        $.ajax({
            url: "crawl/ajax/add-chapter",
            method: 'get',
            type: 'json',
            data: {
                _token: $("input[name='_token']").val(),
                page: page
            },
            success: function(data){
                var data = JSON.parse(data);
                if(data.status == 'success') {
                    list_chapter = data.list_chater;
                    
                }
                $('.result-message').html(data.message);
    
            },
            error: function(errors) {
                console.log(errors);
            }
        });
   }
}))
