! function(e) {
    var o = {};

    function n(t) {
        if (o[t]) return o[t].exports;
        var r = o[t] = {
            i: t,
            l: !1,
            exports: {}
        };
        return e[t].call(r.exports, r, r.exports, n), r.l = !0, r.exports
    }
    n.m = e, n.c = o, n.d = function(e, o, t) {
        n.o(e, o) || Object.defineProperty(e, o, {
            enumerable: !0,
            get: t
        })
    }, n.r = function(e) {
        "undefined" != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {
            value: "Module"
        }), Object.defineProperty(e, "__esModule", {
            value: !0
        })
    }, n.t = function(e, o) {
        if (1 & o && (e = n(e)), 8 & o) return e;
        if (4 & o && "object" == typeof e && e && e.__esModule) return e;
        var t = Object.create(null);
        if (n.r(t), Object.defineProperty(t, "default", {
                enumerable: !0,
                value: e
            }), 2 & o && "string" != typeof e)
            for (var r in e) n.d(t, r, function(o) {
                return e[o]
            }.bind(null, r));
        return t
    }, n.n = function(e) {
        var o = e && e.__esModule ? function() {
            return e.default
        } : function() {
            return e
        };
        return n.d(o, "a", o), o
    }, n.o = function(e, o) {
        return Object.prototype.hasOwnProperty.call(e, o)
    }, n.p = "", n(n.s = 666)
}({
    666: function(e, o, n) {
        "use strict";
        var t = {
            init: function() {
                    var template = '<div class="dz-preview dz-processing dz-image-preview dz-success dz-compare">';
                    template += '<div class="dz-image"><img data-dz-thumbnail=""></div>';
                    template += '<div class="dz-details">';
                    template += 	'<div class="dz-size"><span data-dz-size=""></span></div>';
                    template += 	'<div class="dz-filename"><span data-dz-name=""></span></div>';
                    template += '</div>'
                    template += '<div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress=""></span></div>';
                    template += '<div class="dz-error-message"><span data-dz-errormessage=""></span></div>';
                    template += '<div class="dz-success-mark"></div>';
                    template += '<div class="dz-error-mark"></div>';
                    template += '<a class="dz-copy" href="javascript:;" data-dz-copy="">Copy hình</a>';
                    template += '</div>';
                    $("#kt_dropzone_news_thumbnail").dropzone({
                        url: "/upload-image",
                        paramName: "file",
                        maxFiles: 1,
                        maxFilesize: 5,
                        addRemoveLinks: !0,
                        dataType: 'json',
                        clickable: true,
                        thumbnailWidth: 130,
                        thumbnailHeight: 130,
                        previewsContainer: '#previewsContainer',
                        dictRemoveFile: 'Xóa ảnh',
                        dictMaxFilesExceeded: 'Chỉ Upload 1 ảnh đại diện',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        maxfilesexceeded: function(file) {
                           
                        },
                        success: function(o, data) {
                            console.log(data);
                            if(data) {
                                //$('.dz-image').css({"width":"100%", "height":"auto"});
                                
                                $('#post_thumbnail').val(data.url);
                                
                            }
                           
                        }
                        // accept: function(e, o) {
                        //     console.log(e);
                        //     console.log(o);
                        // },
                
                    }), $("#kt_dropzone_news_images").dropzone({
                        url: "/upload-image",
                        paramName: "file",
                        
                        maxFiles: 10,
                        maxFilesize: 10,
                        addRemoveLinks: !0,
                        clickable: true,
                        thumbnailWidth: 130,
                        thumbnailHeight: 130,
                        dictRemoveFile: 'Xóa ảnh',
                        dictMaxFilesExceeded: 'Chỉ Upload 1 ảnh đại diện',
                        previewTemplate: template,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function(o, data) {
                            console.log(o);
                            //o.find('dz-copy').attr('data-dz-copy', data);
                        },
                        accept: function(e, o) {

                            
                            "justinbieber.jpg" == e.name ? o("Naha, you don't.") : o()
                        }
                    })
            }
        };
        KTUtil.ready((function() {
            t.init()
        }))
    }
});


// TinyMCE

