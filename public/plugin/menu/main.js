function getValidationFields() {
    var e = {
        street_address: {
            minLength: 5,
            errMessage: "Street address must be more than 5 characters."
        },
        firstname: {
            minLength: 2,
            errMessage: "Please enter more than 2 characters"
        },
        lastname: {
            minLength: 2,
            errMessage: "Please enter more than 2 characters"
        },
        password: {
            minLength: 5,
            errMessage: "Must be more than 5 characters"
        },
        password_new: {
            minLength: 5,
            errMessage: "Must be more than 5 characters"
        },
        telephone: {
            minLength: 3,
            errMessage: "Must be more than 3 numbers"
        },
        city: {
            minLength: 3,
            errMessage: "Must be more than 3 characters"
        }
    };
    return e
}
!function() {
    for (var e, t = function() {}
    , n = ["assert", "clear", "count", "debug", "dir", "dirxml", "error", "exception", "group", "groupCollapsed", "groupEnd", "info", "log", "markTimeline", "profile", "profileEnd", "table", "time", "timeEnd", "timeStamp", "trace", "warn"], a = n.length, o = window.console = window.console || {}; a--; )
        e = n[a],
        o[e] || (o[e] = t)
}(),
jQuery(document).ready(function(e) {
    function t(e) {
    }
    function n(n) {
        e("#mast-head").css({
            backgroundImage: "url( " + t(n) + ")"
        })
    }
    function a(t) {
        var a = e(t);
        a.addClass("active");
        var o = a.data("header").replace(" ", "").toLowerCase();
        window.sessionStorage && (sessionStorage.setItem("header", o),
        n(o))
    }
    function o(t) {
        var n = e(t)
          , a = n.data("submenuId");
        e("#" + a),
        n.removeClass("active")
    }
    e(".pphelp").click(function() {
        e("#pphelptext").fadeToggle({
            duration: 300
        })
    });/* ,
    e("form").submit(function() {
        return validateForm(e(this))
    }).attr("novalidate", !0); */
    var r = getValidationFields();
    /* e("input").focus(function() {
        e(".helptip").remove(),
        r[this.name] && showHelpTip(e(this), r[this.name].errMessage)
    }).blur(function() {
        validateField(e(this)) ? e(this).removeClass("invalid") : e(this).addClass("invalid")
    }), */
    e(".btn-order-confirm").on("click", function(t) {
        return e(this).hasClass("wait") ? (t.preventDefault(),
        !1) : (e(this).text("Thank you, please wait.").addClass("wait"),
        void 0)
    }),
    e('input[name="password_confirmation"]').on("keyup", function() {
        passwordMatch(e('input[name="password_new"]'), e(this))
    }),
    e('input[name="confirmation"]').on("keyup", function() {
        passwordMatch(e('input[name="password"]'), e(this))
    });
    var i = e("#mast-head").height()
      , s = e("html").hasClass("ie7") || e("html").hasClass("ie8");
    e("#topScroller").on("click", function(t) {
        return t.preventDefault(),
        e("html,body").animate({
            scrollTop: 0
        }, 100),
        !1
    }),
    window.sessionStorage && n(window.sessionStorage.getItem("header"));
    var l = e('input[name="cart_quantity"]');
    if (l) {
        var c = e("<span>+</span>").addClass("btn-qty-plus")
          , u = e("<span>-</span>").addClass("btn-qty-minus");
        c.on("click", function() {
            l.val(1 * l.val() + 1)
        }),
        u.on("click", function() {
            return l.val() < 2 ? !1 : (l.val(1 * l.val() - 1),
            void 0)
        }),
        l.after(c).before(u)
    }
    var d = e("span.star-rating-widget");
    if (d) {
        var p = d.data("rating")
          , h = e("<span></span>").addClass("rating-reveal").css("left", 100 * (p / 5) + "%");
        d.append(h),
        d.show()
    }
    var m = e(".dropdown-menu-main");
    e("body").hasClass("homepage") && Modernizr.touch && e("#menu-dropper").click(function() {
        o(e(".dropdown-menu-main li.active"))
    }),
    m.menuAim({
        activate: a,
        deactivate: o
    }),
    e(".dropdown-menu-main li").click(function(e) {
        e.stopPropagation()
    });
    var f = null ;
    e(".dropdown-menu-main, #menu-dropper").on("mouseleave", function(t) {
        t.stopPropagation(),
        f = window.setTimeout(function() {
            m.removeClass("active"),
            m.find("li.active").removeClass("active"),
            e("#home-page-banners, .home-page-define").removeClass("lower")
        }, 100)
    }),
    m.on("mouseover", function(t) {
        t.stopPropagation(),
        clearTimeout(f),
        e("#home-page-banners, .home-page-define").addClass("lower")
    }),
    e("#menu-dropper").click(function(e) {
        e.preventDefault(),
        clearTimeout(f),
        m.toggleClass("active")
    }).mouseenter(function() {
        clearTimeout(f),
        f = window.setTimeout(function() {
            m.addClass("active")
        }, 1)
    }),
    e(".petmed-nav a").click(function(t) {
        t.preventDefault();
        var n = e(this).data("id");
        e(this).siblings(".active").removeClass("active"),
        e(this).addClass("active"),
        e(".petmed-content.active").removeClass("active"),
        e(".petmed-content#" + n).addClass("active")
    })
}),
function(e) {
    function t(t) {
        var n = e(this)
          , a = null
          , o = []
          , r = null
          , i = null
          , s = e.extend({
            rowSelector: "> li",
            submenuSelector: "*",
            submenuDirection: "right",
            tolerance: 75,
            enter: e.noop,
            exit: e.noop,
            activate: e.noop,
            deactivate: e.noop,
            exitMenu: e.noop
        }, t)
          , l = 3
          , c = 300
          , u = function(e) {
            o.push({
                x: e.pageX,
                y: e.pageY
            }),
            o.length > l && o.shift()
        }
          , d = function() {
            i && clearTimeout(i),
            s.exitMenu(this) && (a && s.deactivate(a),
            a = null )
        }
          , p = function() {
            i && clearTimeout(i),
            s.enter(this),
            v(this)
        }
          , h = function() {
            s.exit(this)
        }
          , m = function() {
            f(this)
        }
          , f = function(e) {
            e != a && (a && s.deactivate(a),
            s.activate(e),
            a = e)
        }
          , v = function(e) {
            var t = g();
            t ? i = setTimeout(function() {
                v(e)
            }, t) : f(e)
        }
          , g = function() {
            function t(e, t) {
                return (t.y - e.y) / (t.x - e.x)
            }
            if (!a || !e(a).is(s.submenuSelector))
                return 0;
            var i = n.offset()
              , l = {
                x: i.left,
                y: i.top - s.tolerance
            }
              , u = {
                x: i.left + n.outerWidth(),
                y: l.y
            }
              , d = {
                x: i.left,
                y: i.top + n.outerHeight() + s.tolerance
            }
              , p = {
                x: i.left + n.outerWidth(),
                y: d.y
            }
              , h = o[o.length - 1]
              , m = o[0];
            if (!h)
                return 0;
            if (m || (m = h),
            m.x < i.left || m.x > p.x || m.y < i.top || m.y > p.y)
                return 0;
            if (r && h.x == r.x && h.y == r.y)
                return 0;
            var f = u
              , v = p;
            "left" == s.submenuDirection ? (f = d,
            v = l) : "below" == s.submenuDirection ? (f = p,
            v = d) : "above" == s.submenuDirection && (f = l,
            v = u);
            var g = t(h, f)
              , w = t(h, v)
              , b = t(m, f)
              , C = t(m, v);
            return b > g && w > C ? (r = h,
            c) : (r = null ,
            0)
        }
        ;
        n.mouseleave(d).find(s.rowSelector).mouseenter(p).mouseleave(h).click(m),
        e(document).mousemove(u)
    }
    e.fn.menuAim = function(e) {
        return this.each(function() {
            t.call(this, e)
        }),
        this
    }
}(jQuery);